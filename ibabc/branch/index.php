<?php 
$breadcrumbs = [];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>
        
        <div class="tile-area no-padding">
            <div class="tile-container bg-lightTeal">
                <a href="addbc.php">
                    <div class="tile bg-orange fg-white tile-wide">
                        <div class="tile-content iconic">
                            <span class="icon mif-cloud-upload"></span>
                            <span class="tile-label align-center">Add Bank Correspondents</span>
                        </div>
                    </div>
                </a>
                <a href="bclistIndex.php">
                    <div class="tile bg-blue fg-white">
                        <div class="tile-content iconic">
                            <span class="icon mif-list2"></span>
                            <span class="tile-label align-center">BC List</span>
                        </div>
                    </div>
                </a>
                <a href="reports.php">
                    <div class="tile bg-red fg-white">
                        <div class="tile-content iconic">
                            <span class="icon mif-chart-bars2"></span>
                            <span class="tile-label align-center">Reports</span>
                        </div>
                    </div>
                </a>
                <a href="notifications.php">
                    <div class="tile bg-indigo fg-white">
                        <div class="tile-content iconic">
                            <span class="icon mif-notification"></span>
                            <span class="tile-label align-center">Notification</span>
                        </div>
                    </div>
                </a>
            </div>
            
        </div>

        <br>

    </div>
</body>
<script src="../assets/js/jquery-1.12.2.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>

</html>
