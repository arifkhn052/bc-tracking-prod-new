<?php 
$breadcrumbs = [
	"Reports" => "reports.php",
    "Summary Reports" => "reportSummary.php",
	"Active/Inactive BC" => "activeBcGraph.php"
];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
    <link href="../assets/css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>

			<form class="form-inline">
			  <div class="form-group">
			    <label for="exampleInputName2">Start Date</label>
			    <input type="date" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail2">End Date</label>
			    <input type="date" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com">
			  </div>
			  <button type="submit" class="btn btn-default">Generate Data</button>
			</form>
			<hr>

			<form class="form-inline">
            <h4>Select Bank Subdivision Filter</h4>
            <div class="form-group">

            <select class="form-control">
                <option>Select Bank</option>
                <option>State Bank of India</option>
                <option>Bank of Baroda</option>
                <option>Axis Bank</option>
                <option>Union Bank of India</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" id="bankCircle">
                <option value="0">Select Circle</option>
                <option class="dissolv" value="1">Navi Mumbai</option>
                <option class="dissolv" value="6">Thane</option>
                <option class="dissolv" value="9">Mumbai</option>
                <option class="dissolv" value="13">Pune</option>
                <option class="dissolv" value="16">Nanded</option>
                <option class="dissolv" value="18">Nashik</option>
                <option class="dissolv" value="21">Sindhudurg</option>
                <option class="dissolv" value="23">UDUPI</option>
                <option class="dissolv" value="26">Raigad</option>
                <option class="dissolv" value="28">Ahmednagar</option>
                <option class="dissolv" value="30">Aurangabad</option>
                <option class="dissolv" value="33">Ahmedabad</option>
                <option class="dissolv" value="3242">Varanasi</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control"  id="bankState">
                <option value="0">Select State</option>
                <option class="dissolv" value="2">Maharashtra</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" id="bankZone">
                <option value="0">Select Zone</option>
                <option class="dissolv" value="3">NMZ</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" id="bankRegion">
                <option value="0">Select Region</option>
                <option class="dissolv" value="4">Navi Mumbai</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" id="bankCategory">
                <option value="0">Select Category</option>
                <option class="dissolv" value="5">Urban</option>
                <option class="dissolv" value="32">Semi Urban</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" id="bankBranch">
                <option value="0">Select Branch</option>
                <option class="dissolv" value="5">Branch1</option>
                <option class="dissolv" value="32">branch2</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" id="bankBranch">
                <option value="0">Select SSA</option>
                <option class="dissolv" value="5">SSA1</option>
                <option class="dissolv" value="32">SSA3</option>
            </select>
          </div>
          
        </form>
        <br>
        <form>
          <div class="form-group">
            <button type="submit" class="btn btn-default">Show List</button>
          </div>
        </form>
        <hr>

			<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

        
        


        <br>

    </div>
</body>
<script src="../assets/js/jquery-1.12.2.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>
	$(function () {
	    $('#container').highcharts({
	        chart: {
	            type: 'line'
	        },
	        title: {
	            text: 'Active/Inactive BC in the Bank'
	        },
	        xAxis: {
	            type: 'datetime',
	            dateTimeLabelFormats: { // don't display the dummy year
	                month: '%e. %b',
	                year: '%b'
	            },
	            title: {
	                text: 'Date'
	            }
	        },
	        yAxis: {
	            title: {
	                text: 'BC Count'
	            },
	            min: 0
	        },
	        tooltip: {
	            headerFormat: '<b>{series.name}</b><br>',
	            pointFormat: '{point.x:%e. %b}: {point.y:.0f} BC'
	        },

	        plotOptions: {
	            spline: {
	                marker: {
	                    enabled: true
	                }
	            }
	        },

	        series: [{
	            name: 'Active BC',
	            // Define the data points. All series have a dummy year
	            // of 1970/71 in order to be compared on the same x axis. Note
	            // that in JavaScript, months start at 0 for January, 1 for February etc.
	            data: [
	                [Date.UTC(1970, 9, 21), 0],
	                [Date.UTC(1970, 10, 4), 28],
	                [Date.UTC(1970, 10, 9), 25],
	                [Date.UTC(1970, 10, 27), 2],
	                [Date.UTC(1970, 11, 2), 28],
	                [Date.UTC(1970, 11, 26), 28],
	                [Date.UTC(1970, 11, 29), 47],
	                [Date.UTC(1971, 0, 11), 79],
	                [Date.UTC(1971, 0, 26), 72],
	                [Date.UTC(1971, 1, 3), 02],
	                [Date.UTC(1971, 1, 11), 12],
	                [Date.UTC(1971, 1, 25), 2],
	                [Date.UTC(1971, 2, 11), 18],
	                [Date.UTC(1971, 3, 11), 19],
	                [Date.UTC(1971, 4, 1), 85],
	                [Date.UTC(1971, 4, 5), 22],
	                [Date.UTC(1971, 4, 19), 15],
	                [Date.UTC(1971, 5, 3), 0]
	            ]
	        },
	        {
	            name: 'Inactive BC',
	            // Define the data points. All series have a dummy year
	            // of 1970/71 in order to be compared on the same x axis. Note
	            // that in JavaScript, months start at 0 for January, 1 for February etc.
	            data: [
	                [Date.UTC(1970, 9, 21),1 ],
	                [Date.UTC(1970, 10, 4), 12],
	                [Date.UTC(1970, 10, 9), 12],
	                [Date.UTC(1970, 10, 27),1 ],
	                [Date.UTC(1970, 11, 2), 12],
	                [Date.UTC(1970, 11, 26), 12],
	                [Date.UTC(1970, 11, 29), 14],
	                [Date.UTC(1971, 0, 11), 17],
	                [Date.UTC(1971, 0, 26), 17],
	                [Date.UTC(1971, 1, 3), 10],
	                [Date.UTC(1971, 1, 11), 11],
	                [Date.UTC(1971, 1, 25),1 ],
	                [Date.UTC(1971, 2, 11), 11],
	                [Date.UTC(1971, 3, 11), 11],
	                [Date.UTC(1971, 4, 1), 18],
	                [Date.UTC(1971, 4, 5), 12],
	                [Date.UTC(1971, 4, 19), 11],
	                [Date.UTC(1971, 5, 3),1 ]
	            ]
	        }]
	    });
	});
</script>

</html>

