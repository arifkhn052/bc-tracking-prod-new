<?php 
$breadcrumbs = [
	"Bank Correspondents Lists" => "bcListIndex.php",
	"Edit" => "editbc.php"
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
    <link href="../assets/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="../assets/js/jquery-1.12.2.min.js"></script>

</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>

        <div class="example">

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Personal Details</a></li>
				<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Associate Bank Detail</a></li>
				<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Device Allocation</a></li>
				<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Certification Details</a></li>
				<li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Services</a></li>
				<li role="presentation"><a href="#others" aria-controls="settings" role="tab" data-toggle="tab">Other Action</a></li>
			</ul>

			<form class="form-horizontal">
			<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="home">
						<br>
						<?php include('../includes/filledFormEdit/personal.php'); ?>
					</div>
					<div role="tabpanel" class="tab-pane" id="profile">
						<br>
						<?php include('../includes/filledFormEdit/allocation.php'); ?>
						
					</div>
					<div role="tabpanel" class="tab-pane" id="messages">
						<br>
						<?php include('../includes/filledFormEdit/device.php'); ?>
						
					</div>
					<div role="tabpanel" class="tab-pane" id="settings">
						<br>
						<?php include('../includes/filledFormEdit/certification.php'); ?>
						
					</div>
					<div role="tabpanel" class="tab-pane" id="services">
						<br>
						<?php include('../includes/filledFormEdit/services.php'); ?>
						
					</div>
					<div role="tabpanel" class="tab-pane" id="others">
						<br>
						<?php include('../includes/filledFormEdit/other.php'); ?>
						
					</div>
				</div>
			</form>

		</div>
        
        


        <br>

    </div>
</body>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
	   
 	} );
</script>

</html>
