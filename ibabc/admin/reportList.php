<?php 
$breadcrumbs = [
	"Reports" => "reports.php",
    "List Reports" => "reportList.php"
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>
        
        <div class="tile-area no-padding">

            <div class="tile-container bg-lightTeal">
                <a href="listason.php">
                    <div class="tile bg-orange fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-filter"></span>
                            <span class="tile-label align-center">List as on date</span>
                        </div>
                    </div>
                </a>

                <a href="#.php">
                    <div class="tile bg-green fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-filter"></span>
                            <span class="tile-label align-center">Corporate BC's</span>
                        </div>
                    </div>
                </a>
                <a href="#.php">
                    <div class="tile bg-blue fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-filter"></span>
                            <span class="tile-label align-center">Branches Last Updated</span>
                        </div>
                    </div>
                </a>
                <a href="#.php">
                    <div class="tile bg-gray fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-filter"></span>
                            <span class="tile-label align-center">Allocation Based List</span>
                        </div>
                    </div>
                </a>
                <a href="areaWiseAllocation.php">
                    <div class="tile bg-red fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-eye-open"></span>
                            <span class="tile-label align-center">Area wise BC allocation</span>
                        </div>
                    </div>
                </a>
                <a href="#.php">
                    <div class="tile bg-brown fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-filter"></span>
                            <span class="tile-label align-center">BC per products</span>
                        </div>
                    </div>
                </a>
                <a href="#.php">
                    <div class="tile bg-red fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon mif-not"></span>
                            <span class="tile-label align-center">BlackListed BC</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <br>

    </div>
</body>
<script src="../assets/js/jquery-1.12.2.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>

</html>
