<?php 
$breadcrumbs = [
	"Bank Correspondents Lists" => "bcListIndex.php",
	"View" => "viewbc.php"
];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
    <link href="../assets/css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>

        <div class="example">

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Personal Details</a></li>
				<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Associate Bank Detail</a></li>
				<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Allotted Area details</a></li>
				<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Certification Details</a></li>
				<li role="presentation"><a href="#others" aria-controls="settings" role="tab" data-toggle="tab">Record Feed</a></li>
			</ul>

			<form class="form-horizontal">
			<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="home">
						<br>
						<div class="form-group">
		                    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
		                    <div class="col-sm-10">
		                        <input type="text" class="form-control" id="inputEmail3" placeholder="Name" disabled value="Bc 1">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputPassword3" class="col-sm-2 control-label">Phone Number</label>
		                    <div class="col-sm-10">
		                        <input type="text" class="form-control" id="inputPassword3" placeholder="Phone Number" disabled value="1233123112">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail4" class="col-sm-2 control-label">Email</label>
		                    <div class="col-sm-10">
		                        <input type="email" class="form-control" id="inputEmail4" placeholder="Email" disabled value="a@b.co">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail7" class="col-sm-2 control-label">Date Of Birth</label>
		                    <div class="col-sm-10">
		                        <input type="date" class="form-control" id="inputEmail7" placeholder="Date of Birth" disabled value="1992-03-26">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail5" class="col-sm-2 control-label">Aadhar Card</label>
		                    <div class="col-sm-10">
		                        <input type="text" class="form-control" id="inputEmail5" placeholder="Aadhar Card Number" disabled value="AA1231A21">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Pan Card</label>
		                    <div class="col-sm-10">
		                        <input type="text" class="form-control" id="inputEmail6" placeholder="Pan Card Number" disabled value="SASDWQ123A">
		                    </div>
		                </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="profile">
						<br>
						<div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">IFSC Code</label>
		                    <div class="col-sm-10">
		                        <input type="text" class="form-control" id="inputEmail6" placeholder="IFSC Code" value="SBIN4421232" disabled>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Bank Name</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option>State Bank of India</option>
		                            <option>Bank of Baroda</option>
		                            <option>Axis Bank</option>
		                            <option>Union Bank of India</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Circle</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="1">Navi Mumbai</option>
		                            <option class="dissolv" value="6">Thane</option>
		                            <option class="dissolv" value="9">Mumbai</option>
		                            <option class="dissolv" value="13">Pune</option>
		                            <option class="dissolv" value="16">Nanded</option>
		                            <option class="dissolv" value="18">Nashik</option>
		                            <option class="dissolv" value="21">Sindhudurg</option>
		                            <option class="dissolv" value="23">UDUPI</option>
		                            <option class="dissolv" value="26">Raigad</option>
		                            <option class="dissolv" value="28">Ahmednagar</option>
		                            <option class="dissolv" value="30">Aurangabad</option>
		                            <option class="dissolv" value="33">Ahmedabad</option>
		                            <option class="dissolv" value="3242">Varanasi</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">State</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="2">Maharashtra</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Zone</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="3">NMZ</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Region</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="4">Navi Mumbai</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Category</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="5">Urban</option>
		                            <option class="dissolv" value="32">Semi Urban</option>
		                        </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="inputEmail6" class="col-sm-2 control-label">Branch</label>
		                    <div class="col-sm-10">
		                        <select class="form-control" disabled>
		                            <option class="dissolv" value="5">Branch1</option>
		                            <option class="dissolv" value="32">branch2</option>
		                        </select>
		                    </div>
		                </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="messages">
						<br>
						

		                <div class="form-group">
		                    <table id="example" class="display" cellspacing="0" width="100%">
		                        <thead>
		                            <tr>
		                                <th>Village Code</th>
		                                <th>Village Detail</th>
		                                <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <tr>
		                                <td>SASDAW123</td>
		                                <td>Village1, Area 1, UP</td>
		                                <td>delete</td>
		                            </tr>
		                        </tbody>
		                    </table>
		                </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="settings">
						<br>
		                <div class="form-group">
		                    <table id="example2" class="display" cellspacing="0" width="100%">
		                        <thead>
		                            <tr>
		                                <th>Certification Date</th>
		                                <th>Certification Authority</th>
		                                <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <tr>
		                                <td>2012-02-21</td>
		                                <td>Authority 1</td>
		                                <td>delete</td>
		                            </tr>
		                        </tbody>
		                    </table>
		                </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="others">
						<br>
		                <div class="form-group">
		                    <table id="example3" class="display" cellspacing="0" width="100%">
		                        <thead>
		                            <tr>
		                                <th>Date</th>
		                                <th>Action</th>
		                                <th>Detail</th>
		                                <th>Updated By</th>
		                                <th>Updated On</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <tr>
		                                <td>2012-02-21</td>
		                                <td>Certification Received</td>
		                                <td>Authority 1 awarded Certificate</td>
		                                <td>Mr. Banker (B-123121)</td>
		                                <td>2012-03-20</td>
		                            </tr>
		                            <tr>
		                                <td>2012-02-20</td>
		                                <td>Joined SBI</td>
		                                <td>Joined SBI, Branch X</td>
		                                <td>Mr. Banker2 (B-123122)</td>
		                                <td>2012-03-18</td>
		                            </tr>
		                            <tr>
		                                <td>2012-02-27</td>
		                                <td>Village allotted</td>
		                                <td>Village1, Village2 allotted</td>
		                                <td>Mr. Banker1 (B-123122)</td>
		                                <td>2012-03-28</td>
		                            </tr>
		                        </tbody>

		                    </table>
		                </div>
					</div>
				</div>
			</form>

		</div>
        
        


        <br>

    </div>
</body>
<script src="../assets/js/jquery-1.12.2.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
	    var t = $('#example').DataTable({
	    	"paging":   false,
	        "ordering": false,
	        "info":     false,
	        "searching": false
	    });

	    var t2 = $('#example2').DataTable({
	    	"paging":   false,
	        "ordering": false,
	        "info":     false,
	        "searching": false
	    });

	    var t3 = $('#example3').DataTable({});
	 	
	 	$("#villageCode").on('blur', function() {
 			$("#villageDetail").text('village 1, district 1, state');
	 	}).on('focus', function() {
 			$("#villageDetail").text('');

	 	});

		$('#addMoreButton2').on( 'click', function () {
	        t2.row.add( [
	            $("#certificateDate").val(),
	            $("#certificateAuth").val(),
	            'delete'
	        ] ).draw( false );
	        $("#certificateDate").val("");
	        
	    } );
	    $('#addMoreButton').on( 'click', function () {
	        t.row.add( [
	            $("#villageCode").val(),
	            $("#villageDetail").text(),
	            'delete'
	        ] ).draw( false );
	        $("#villageCode").val("");
	        $("#villageDetail").text("");
	    } );
 	} );
</script>

</html>

