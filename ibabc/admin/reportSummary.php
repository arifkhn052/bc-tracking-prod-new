<?php 
$breadcrumbs = [
	"Reports" => "reports.php",
    "Summary Reports" => "reportSummary.php"
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>
        
        <div class="tile-area no-padding">
            <div class="tile-container bg-lightTeal">
                <a href="reportbetween.php">
                    <div class="tile bg-blue fg-white tile-wide">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-stats"></span>
                            <span class="tile-label align-center">Allotment between dates</span>
                        </div>
                    </div>
                </a>
                <a href="unallocated.php">
                    <div class="tile bg-green fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-question-sign"></span>
                            <span class="tile-label align-center">Unallocated BC</span>
                        </div>
                    </div>
                </a>
                <a href="blacklistedBcGraph.php">
                    <div class="tile bg-green fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon mif-not"></span>
                            <span class="tile-label align-center">Black listed BC</span>
                        </div>
                    </div>
                </a>
                <a href="activeBcGraph.php">
                    <div class="tile bg-red fg-white tile">
                        <div class="tile-content iconic">
                            <span class="icon mif-not"></span>
                            <span class="tile-label align-center">Active/Inactive BC</span>
                        </div>
                    </div>
                </a>


                <a href="areaWiseAllocationBetweenDate.php">
                    <div class="tile bg-teal fg-white tile-wide">
                        <div class="tile-content iconic">
                            <span class="icon glyphicon glyphicon-eye-open"></span>
                            <span class="tile-label align-center">Area wise BC Between Dates</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <br>

    </div>
</body>
<script src="../assets/js/jquery-1.12.2.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>

</html>
