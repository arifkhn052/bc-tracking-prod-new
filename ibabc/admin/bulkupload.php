<?php 
$breadcrumbs = [
	"Add Bank Correspondents" => "addbc.php",
	"Bulk Upload" => "bulkupload.php"
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>
        
        <div class="example">
            <form class="form-horizontal">
			  <div class="form-group">
			    <label class="col-sm-4 control-label">Select File</label>
			    <div class="col-sm-8">
				    <input type="file" id="exampleInputFile">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPassword" class="col-sm-4 control-label">Replace Existing</label>
			    <div class="radio col-sm-8">
				  <label>
				    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
				    Replace the Existing Data
				  </label>
				  <label>
				    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
				    Do not replace the existing Data
				  </label>
				</div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-4 col-sm-10">
			      <button type="submit" class="btn btn-default">Submit</button>
			    </div>
			  </div>
			</form>
        </div>

        <br>

    </div>
</body>
<script src="../assets/js/jquery-1.12.2.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>

</html>
