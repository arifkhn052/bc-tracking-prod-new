<?php 
$breadcrumbs = [
    "Bank Correspondents Lists" => "bcListIndex.php",
    "BC Address Wise" => "bcListLocationWise.php"
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>BC Track</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/metro.min.css">
    <link href="../assets/css/metro-icons.css" rel="stylesheet">
    <link href="../assets/css/metro-responsive.min.css" rel="stylesheet">
    <link href="../assets/css/metro-schemes.css" rel="stylesheet">
    <link href="../assets/css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body>
    <?php include('../includes/navbar.php'); ?>

    <div class="container page-content">
        
        
        <?php include('../includes/breadcrumbs.php'); ?>

        <br>
        <form class="form-inline">
          <div class="form-group">
            <select class="form-control" id="bankCircle">
                <option value="0">Select State</option>
                <option class="dissolv" value="1">Navi Mumbai</option>
                <option class="dissolv" value="6">Thane</option>
                <option class="dissolv" value="9">Mumbai</option>
                <option class="dissolv" value="13">Pune</option>
                <option class="dissolv" value="16">Nanded</option>
                <option class="dissolv" value="18">Nashik</option>
                <option class="dissolv" value="21">Sindhudurg</option>
                <option class="dissolv" value="23">UDUPI</option>
                <option class="dissolv" value="26">Raigad</option>
                <option class="dissolv" value="28">Ahmednagar</option>
                <option class="dissolv" value="30">Aurangabad</option>
                <option class="dissolv" value="33">Ahmedabad</option>
                <option class="dissolv" value="3242">Varanasi</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control"  id="bankState">
                <option value="0">Select region</option>
                <option class="dissolv" value="2">Maharashtra</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" id="bankZone">
                <option value="0">Select Division</option>
                <option class="dissolv" value="3">NMZ</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" id="bankRegion">
                <option value="0">Select Area</option>
                <option class="dissolv" value="4">Navi Mumbai</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" id="bankCategory">
                <option value="0">Select PinCode</option>
                <option class="dissolv" value="5">Urban</option>
                <option class="dissolv" value="32">Semi Urban</option>
            </select>
          </div>
          <button type="submit" class="btn btn-default">Show List</button>
        </form>
        <hr>
        
        <table id="example" class="display" cellspacing="0" width="100%">
	        <thead>
	            <tr>
                    <th>ID</th>
	                <th>Name</th>
	                <th>Phone</th>
                    <th>Aadhar Card</th>
                    <th>Certificates</th>
                    <th>Villages</th>
                    <th>Edit</th>
                    <th>View</th>
	            </tr>
	        </thead>
            <tbody>
                <tr>
                    <td>U-21231</td>
                    <td>BC 1</td>
                    <td>999123123</td>
                    <td>A21S1234</td>
                    <td>Auth1, Auth2</td>
                    <td>Vill1, Vill2</td>
                    <td><a href="editbc.php" class="btn btn-danger">Edit</a></td>
                    <td><a href="viewbc.php" class="btn btn-primary">View</a></td>
                </tr>
                <tr>
                    <td>U-21232</td>
                    <td>BC 2</td>
                    <td>9991231234</td>
                    <td>A21S1221</td>
                    <td>Auth1</td>
                    <td>Vill4, Vill3, Vill5</td>
                    <td><a href="editbc.php" class="btn btn-danger">Edit</a></td>
                    <td><a href="viewbc.php" class="btn btn-primary">View</a></td>
                </tr>
            </tbody>
	    </table>
		        


        <br>

    </div>
</body>
<script src="../assets/js/jquery-1.12.2.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/metro.min.js"></script>
<script src="../assets/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
	    var t = $('#example').DataTable({});


 	} );
</script>

</html>
