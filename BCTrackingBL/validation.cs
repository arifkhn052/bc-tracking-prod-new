﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Data;
using System.Globalization;

namespace BCTrackingBL
{
    public static class validation
    {
        public static string IsValidEmailId(string InputEmail)
        {
            if (InputEmail == null)
            {
                return "false";
            }
            else
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(InputEmail);
                if (match.Success)
                    return InputEmail;
                else
                    return "false";
            }
           
        }

        public static string IsCorrectMobileNumber(String strNumber)
        {
            if(strNumber==null)
            {
               
                    return "0";
            }
            else
            {
                Regex mobilePattern = new Regex(@"^[0-9]{10}$");
                Match match = mobilePattern.Match(strNumber);
                if (match.Success)
                    return strNumber;
                else
                    return "0";
            }
        
        }
        public static string IsCorrectPanCard(String strNumber)
        {
            if (strNumber == null)
            {

                return null;
            }
            else
            {
                Regex mobilePattern = new Regex(@"[A-Z]{5}\d{4}[A-Z]{1}");
                Match match = mobilePattern.Match(strNumber);
                if (match.Success)
                    return strNumber;            
                else
                    return "0";
            }

        }
        public static string IsCorrectAdhaar(String strNumber)
        {
            if(strNumber==null)
            {
                return "0";
            }
            else
            {
                Regex mobilePattern = new Regex(@"^[0-9]{12}$");
                Match match = mobilePattern.Match(strNumber);
                if (match.Success)
                    return strNumber;
                else
                    return "0";
            }
           
        }
         public static int IsCorrectStringIsNumber(String strNumber)
        {
             if(strNumber==null)
             {
                 return -1;
             }
             else
             {
                 Regex mobilePattern = new Regex(@"^\d+$");
                 Match match = mobilePattern.Match(strNumber);
                 if (match.Success)
                     return Convert.ToInt32(strNumber);

                 else
                     return -1;
             }

          
        }

     

        public static bool IsInt(string sVal)
        {
            foreach (char c in sVal)
            {
                int iN = (int)c;
                if ((iN > 57) || (iN < 48))
                    return false;
            }
            return true;
        }

        public static string required(string len)
        {
            if (len.Trim().Length > 0)
                return len;
            else
                return len;
        }
        public static string getGendereValidation(string len)
        {
            if (len == "Male")
            {
                return "true";
            }
            else if (len == "Female")
            {
                return "true";
            }
            else if (len == "Other")
            {
                return "true";
            }
            else
            {
                return "false";
            }

        }


        public static string getMonthName(string len)
        {
            if (len == "1")
            {
                return "January";
            }
            else if (len == "2")
            {
                return "February";
            }
            else if (len == "3")
            {
                return "March";
            }
            else if (len == "4")
            {
                return "April";
            }
            else if (len == "5")
            {
                return "May";
            }
            else if (len == "6")
            {
                return "June";
            }
            else if (len == "7")
            {
                return "July";
            }
            else if (len == "8")
            {
                return "August";
            }
            else if (len == "9")
            {
                return "September";
            }
            else if (len == "10")
            {
                return "October";
            }
            else if (len == "11")
            {
                return "November";
            }
            else if (len == "12")
            {
                return "December";
            }
            else
            {
                return "false";
            }

        }
        public static string getAlternateOccupationType(string len)
        {
            if (len == "Government")
            {
                return "true";
            }
            else if (len == "Public Sector")
            {
                return "true";
            }
            else if (len == "Private")
            {
                return "true";
            }
            else if (len == "Self Employed")
            {
                return "true";
            }
           
            else
            {
                return "false";
            }




        }

        public static string getDeviceName(string len)
        {
            if (len == "POS")
            {
                return "true";
            }
            else if (len == "Micro ATM")
            {
                return "true";
            }
            else if (len == "USSD Phones")
            {
                return "true";
            }
            else if (len == "KIOSK")
            {
                return "true";
            }
            else if (len == "Evolute")
            {
                return "true";
            }

            else
            {
                return "false";
            }




        }
        public static string getCategory(string len)
        {
            if (len == "GENERAL")
            {
                return "true";
            }
            else if (len == "OBC")
            {
                return "true";
            }
            else if (len == "SC")
            {
                return "true";
            }
            else if (len == "ST")
            {
                return "true";
            }
            else
            {
                return "false";
            }

        }
        public static string getQualificationValidation(string len)
        {
            if (len == "SSC ")
            {
                return "true";
            }
            else if (len == "HSC")
            {
                return "true";
            }
            else if (len == "Graduate")
            {
                return "true";
            }            

            else
            {
                return "false";
            }

        }
        public static string getBCTypeValidation(string len)
        {
            if (len == "Individual ")
            {
                return "true";
            }
            else if (len == "Corporate")
            {
                return "true";
            }
            else if (len == "Fixed")
            {
                return "true";
            }
            else if (len == "Mobile")
            {
                return "true";
            }
            else if (len == "Both")
            {
                return "true";
            }

            else
            {
                return "false";
            }

        }

        public static DataSet errorFinderdate(string error, int rowNum,string rowsName)
              {
             switch (error)
            {
                case "date":
                    return errData("307", "in row " + rowNum + " of " + rowsName + "  Date format should be dd/mm/yyyy");
                case "bankId":
                    return errData("312", "in row " + rowNum + " of " + rowsName + "  bankId should be number.");
                case "Branchid":
                    return errData("312", "in row " + rowNum + " of " + rowsName + "  Branchid should be number.");
                case "ContactNumber":
                    return errData("312", "in row " + rowNum + " of " + rowsName + " Invalid ContactNumber");


                     
            }
             DataSet ds = new DataSet();
             return ds;
              }
        public static DataSet errorFinder(string error)
        {
            switch (error)
            {
                case "phone1":
                    return errData("300", "Invalid PhoneNumber1");
                case "PhoneNumber2":
                    return errData("300", "Invalid PhoneNumber2");
                case "PhoneNumber3":
                    return errData("300", "Invalid PhoneNumber3");

                case "email":
                    return errData("301", "Invalid Email Id.");

                case "amount":
                    return errData("302", "Invalid Amount.");

                case "userName":
                    return errData("303", "User Name Required.");
                case "ImagePath":
                    return errData("303", "ImagePath Required.");
                case "FatherName":
                    return errData("303", "FatherName Required.");
                case "AadharCard":
                    return errData("303", "AadharCard Required.");
                case "PinCode":
                    return errData("303", "PinCode Required.");
                    
                case "Gender":
                    return errData("304", "Gender Required value should be Male,Female,Transgender");
                case "DOB":
                    return errData("305", "DOB Required format should be dd/mm/yyyy");
                case "DOT":
                    return errData("305", "Date of Terminate Required format should be dd/mm/yyyy");
                case "AppointmentDate":
                    return errData("305", "AppointmentDate Required format should be dd/mm/yyyy");

                case "Handicap":
                    return errData("306", "Handicap Required enter 0 or 1");
                case "Category":
                    return errData("307", "Category Required.");
                case "AlternateOccupationType":
                    return errData("308", "Occupation Required Government,Public Sector,Private,Self Employed");
                case "Qualification":
                    return errData("309", "Qualification Required SSC,HSC,Graduate");
                case "isAllocated":
                    return errData("310", "isAllocated Required Enter true or false");
                case "BCType":
                    return errData("311", "BCType Required Individual,Corporate,Fixed,Mobile,Both");
                case "WorkingDays":
                    return errData("312", "Working day should be number.");
                case "WorkingHours":
                    return errData("312", "Working hours should be number.");
                case "PLStateId":
                    return errData("312", "StateId  Should be number.");
                case "MinimumCashHandlingLimit":
                    return errData("312", "Minimum CashHandling Limit  should be number.");
                case "MonthlyFixedRenumeration":
                    return errData("312", "Monthly Fixed Renumeration  should be number.");
                case "MonthlyVariableRenumeration":
                    return errData("312", "Monthly Variable Renumeration  should be number.");
                case "BankCorrespondId":
                    return errData("312", "BankCorrespondId should be number.");
                case "AllocationBankId":
                    return errData("312", "Allocation BankId required should be number.");
                case "AllocationBranchId":
                    return errData("312", "Allocation BranchId  required should be number.");
                    
                case "certs":
                    return errData("313", "Enter at least 1 certification");
                case "AadharCard1":
                    return errData("314", "Aadhaar card number already exist.");


                    
            }
            DataSet ds = new DataSet();
            return ds;
        }
        public static string DateFormat(string StrDate)
        {
            DateTime Result;
            DateTimeFormatInfo info = new DateTimeFormatInfo();
            CultureInfo culture;
            culture = CultureInfo.CreateSpecificCulture("en-US");
            info.ShortDatePattern = "dd/MM/yyyy";
            if (DateTime.TryParse(StrDate, info, DateTimeStyles.None, out Result))
            {
                return Result.ToShortDateString();
            }
            else
            {
                return "false";
            }
        }

        public static DataSet errData(string errCode, string message)
        {
            DataSet result = new DataSet();
            DataTable dt = new DataTable("errTable");
            dt.Columns.Add(new DataColumn("errCode", typeof(int)));
            dt.Columns.Add(new DataColumn("message", typeof(string)));
            DataRow dr = dt.NewRow();
            dr["errCode"] = errCode;
            dr["message"] = message;
            dt.Rows.Add(dr);
            result.Tables.Add(dt);
            return result;
        }

        public static int getUniqueId()
        {
            Random rnd = new Random();
            return rnd.Next(10000000, 99999999);
        }
    }
}
