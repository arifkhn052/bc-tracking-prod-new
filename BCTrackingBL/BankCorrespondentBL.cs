﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCEntities;
using BCTrackingDAL;
using BCTrackingServices;
using System.IO;
using System.Configuration;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using Microsoft.Azure; //Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob;
using System.Security.Cryptography;
using System.Globalization; // Namespace for Blob storage types
//using ClosedXML.Excel;

namespace BCTrackingBL
{
    public class BankCorrespondentBL
    {

        public int InsertUpdateBC(BankCorrespondence bc, int userId, string mode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            int rtnVal = Constants.DEFAULTVALUE;
            try
            {
                if (mode == Constants.BLACKLISTMODE || mode == Constants.WHITELISTMODE)
                    rtnVal = prov.BlackListBankCorrespond(bc, userId, mode);
                else if (mode == Constants.CHANGESTATUSMODE)
                    rtnVal = prov.ChangeBCStatus(bc, userId, mode);
                else
                    rtnVal = prov.InsertUpdateBankCorresponds(bc, userId, mode);
           
                Utils.SetSessionValue(Constants.BCSESSIONVAR, null);
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BCTrackingBL.InsertUpdateBC: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return rtnVal;


        }
        public int InsertUpdateProspectiveRegistry(BankCorrespondence bc, int userId, string mode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            int rtnVal = Constants.DEFAULTVALUE;
            try
            {
                if (mode == Constants.BLACKLISTMODE || mode == Constants.WHITELISTMODE)
                    rtnVal = prov.BlackListBankCorrespond(bc, userId, mode);
                else if (mode == Constants.CHANGESTATUSMODE)
                    rtnVal = prov.ChangeBCStatus(bc, userId, mode);
                else
                    rtnVal = prov.InsertUpdateProspectiveRegistry(bc, userId, mode);

                Utils.SetSessionValue(Constants.BCSESSIONVAR, null);
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BCTrackingBL.InsertUpdateBC: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return rtnVal;


        }
        public int SaveZipPath(string FolderName, string fileName, string filePath, string aadharnumber, string fileType,string UserId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            int rtnVal = Constants.DEFAULTVALUE;
            try
            {

                rtnVal = prov.SaveZipPath(FolderName, fileName, filePath, aadharnumber, fileType, UserId);


            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in Saving Path: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return rtnVal;


        }
        public int InsertUpdateBCforExcel(BankCorrespondence bc, int userId, string mode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            int rtnVal = Constants.DEFAULTVALUE;
            try
            {
                if (mode == Constants.BLACKLISTMODE || mode == Constants.WHITELISTMODE)
                    rtnVal = prov.BlackListBankCorrespond(bc, userId, mode);
                else if (mode == Constants.CHANGESTATUSMODE)
                    rtnVal = prov.ChangeBCStatus(bc, userId, mode);
                else
                    rtnVal = prov.InsertUpdateBankCorrespondsexcel(bc, userId, mode);

                Utils.SetSessionValue(Constants.BCSESSIONVAR, null);
            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BCTrackingBL.InsertUpdateBC: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return rtnVal;


        }
        public int InsertUpdateBCforExcelCertification(BCCertifications bc, int userId, string mode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            int rtnVal = Constants.DEFAULTVALUE;
            try
            {
                    rtnVal = prov.InsertUpdateBCforExcelCertification(bc, userId, mode);

            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BCTrackingBL.InsertUpdateBC: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return rtnVal;


        }


        public int InsertUpdateBCforExcelRemuneration(CommssionList bc, int userId, string mode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            int rtnVal = Constants.DEFAULTVALUE;
            try
            {
                rtnVal = prov.InsertUpdateBCforExcelRemuneration(bc, userId, mode);

            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BCTrackingBL.InsertUpdateBC: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return rtnVal;


        }

        public int InsertUpdateBCforExcelPreviousExperiene(PreviousExperiene bc, int userId, string mode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            int rtnVal = Constants.DEFAULTVALUE;
            try
            {
                rtnVal = prov.InsertUpdateBCforExcelPreviousExperiene(bc, userId, mode);

            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BCTrackingBL.InsertUpdateBC: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return rtnVal;


        }
        public int InsertUpdateBCforExcelOperationalAreas(OperationalAreas bc, int userId, string mode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            int rtnVal = Constants.DEFAULTVALUE;
            try
            {
                rtnVal = prov.InsertUpdateBCforExcelOperationalAreas(bc, userId, mode);

            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BCTrackingBL.InsertUpdateBC: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return rtnVal;


        }

        public int InsertUpdateBCforExcelBCDevices(BCDevices bc, int userId, string mode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            int rtnVal = Constants.DEFAULTVALUE;
            try
            {
                rtnVal = prov.InsertUpdateBCforExcelBCDevices(bc, userId, mode);

            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BCTrackingBL.InsertUpdateBC: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return rtnVal;


        }
        public int InsertUpdateBCforExcelConnectivityDetails(ConnectivityDetails bc, int userId, string mode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            int rtnVal = Constants.DEFAULTVALUE;
            try
            {
                rtnVal = prov.InsertUpdateBCforExcelConnectivityDetails(bc, userId, mode);

            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BCTrackingBL.InsertUpdateBC: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return rtnVal;


        }
        public int InsertUpdateBCforExcelSSADetails(SsaDetails bc, int userId, string mode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            int rtnVal = Constants.DEFAULTVALUE;
            try
            {
                rtnVal = prov.InsertUpdateBCforExcelSSADetails(bc, userId, mode);

            }
            catch (Exception ex)
            {
                BCTrackingLogger.Error("Error in BCTrackingBL.InsertUpdateBC: " + ex.Message + "<br/>" + ex.StackTrace);

            }
            return rtnVal;


        }

        public List<BankCorrespondence> GetBankCorrespondentsbystate(string stateId, string district, string subdistrict,string village)
        {
            SQLDataProvider prov = new SQLDataProvider();
            List<BankCorrespondence> allBC = null;
             allBC =  prov.GetBankCorrespondentsbystate(stateId, district, subdistrict, village);
            return allBC;
        }
        public List<BankCorrespondence> GeBCpulicDetails(string bcId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            List<BankCorrespondence> allBC = null;
            allBC = prov.GeBCpulicDetails(bcId);
            return allBC;
        }
        
        public List<BankCorrespondence> GetBankCorrespondents(string mode, Dictionary<string, string> filters)
        {
            SQLDataProvider prov = new SQLDataProvider();


            List<BankCorrespondence> allBC = null;
            allBC = Utils.GetSessionValue(Constants.BCSESSIONVAR) as List<BankCorrespondence>;
            if (allBC == null)
            {
                allBC = prov.GetBankCorrespondence(null);
                Utils.SetSessionValue(Constants.BCSESSIONVAR, allBC);
            }

            if (mode == Constants.UNALLOCATED)
            {
                allBC = allBC.Where(b => !b.isAllocated).ToList();

            }
            else if (mode == Constants.ALLOCATED)
            {
                allBC = allBC.Where(b => b.isAllocated).ToList();

            }
            else if (mode == Constants.BLACKLISTMODE)
            {
                allBC = allBC.Where(b => b.IsBlackListed).ToList();
            }


            foreach (KeyValuePair<string, string> kvp in filters)
            {
                switch (kvp.Key)
                {
                    case Constants.BANKMODE:
                        allBC = allBC.Where(b => b.AllocationBankId == Utils.GetIntValue(kvp.Value)).ToList();
                        break;
                    case Constants.BRANCHMODE:
                        allBC = allBC.Where(b => b.AllocationBranchId == Utils.GetIntValue(kvp.Value)).ToList();
                        break;
                    case "bcId":
                        allBC = allBC = prov.GetBankCorrespondence(Convert.ToInt32( kvp.Value));
                        break;
                    case "adhaarNo":
                        allBC = allBC = prov.GetBankCorrespondencebyAdhaar(kvp.Value.ToString());
                        break;
                    case Constants.APPTDATEFROMFILTER:
                        allBC = allBC.Where(b => b.AppointmentDate != null && b.AppointmentDate.Value >= Utils.GetDateTimeValue(kvp.Value)).OrderBy(bs => bs.AppointmentDate).ToList();
                        break;
                    case Constants.APPTDATETOFILTER:
                        allBC = allBC.Where(b => b.AppointmentDate != null && b.AppointmentDate.Value <= Utils.GetDateTimeValue(kvp.Value)).OrderBy(bs => bs.AppointmentDate).ToList();
                        break;
                    case Constants.CREATEDATEFROMFILTER:
                        allBC = allBC.Where(b => b.CreatedOn >= Utils.GetDateTimeValue(kvp.Value)).OrderBy(bs => bs.CreatedOn).ToList();
                        break;
                    case Constants.CREATEDATETOFILTER:
                        allBC = allBC.Where(b => b.CreatedOn <= Utils.GetDateTimeValue(kvp.Value)).OrderBy(bs => bs.CreatedOn).ToList();
                        break;
                 
                    case Constants.BANKCIRCLEID:
                        break;
                    case Constants.STATEID:
                        allBC = allBC.Where(b => b.PLStateId == Utils.GetIntValue(kvp.Value)).OrderBy(bs => bs.CreatedOn).ToList();
                        break;
                    case Constants.BANKREGIONID:
                        break;
                    case Constants.BANKSSA:
                        break;
                    case Constants.BRANCHCATEGORY:
                        break;
                    case Constants.BANKZONEID:
                        break;
                    

                   
                    case Constants.ISBLACKLISTED:
                        allBC = allBC.Where(b => b.IsBlackListed == Utils.GetBoolValue(kvp.Value)).ToList();
                        break;

                      
                }
            }



            //Handle mode
            return allBC;

        }

        public List<BankCorrespondence> GetprospectiveRegistry(string mode, Dictionary<string, string> filters)
        {
            SQLDataProvider prov = new SQLDataProvider();


            List<BankCorrespondence> allBC = null;
            allBC = Utils.GetSessionValue(Constants.BCSESSIONVAR) as List<BankCorrespondence>;
            if (allBC == null)
            {
                allBC = prov.GetBankCorrespondence(null);
                Utils.SetSessionValue(Constants.BCSESSIONVAR, allBC);
            }

            if (mode == Constants.UNALLOCATED)
            {
                allBC = allBC.Where(b => !b.isAllocated).ToList();

            }
            else if (mode == Constants.ALLOCATED)
            {
                allBC = allBC.Where(b => b.isAllocated).ToList();

            }
            else if (mode == Constants.BLACKLISTMODE)
            {
                allBC = allBC.Where(b => b.IsBlackListed).ToList();
            }


            foreach (KeyValuePair<string, string> kvp in filters)
            {
                switch (kvp.Key)
                {
                    case Constants.BANKMODE:
                        allBC = allBC.Where(b => b.AllocationBankId == Utils.GetIntValue(kvp.Value)).ToList();
                        break;
                    case Constants.BRANCHMODE:
                        allBC = allBC.Where(b => b.AllocationBranchId == Utils.GetIntValue(kvp.Value)).ToList();
                        break;
                    case "bcId":
                        allBC = allBC = prov.GetprospectiveRegistry(Convert.ToInt32(kvp.Value));
                        break;
                    case "adhaarNo":
                        allBC = allBC = prov.GetBankCorrespondencebyAdhaar(kvp.Value.ToString());
                        break;
                    case Constants.APPTDATEFROMFILTER:
                        allBC = allBC.Where(b => b.AppointmentDate != null && b.AppointmentDate.Value >= Utils.GetDateTimeValue(kvp.Value)).OrderBy(bs => bs.AppointmentDate).ToList();
                        break;
                    case Constants.APPTDATETOFILTER:
                        allBC = allBC.Where(b => b.AppointmentDate != null && b.AppointmentDate.Value <= Utils.GetDateTimeValue(kvp.Value)).OrderBy(bs => bs.AppointmentDate).ToList();
                        break;
                    case Constants.CREATEDATEFROMFILTER:
                        allBC = allBC.Where(b => b.CreatedOn >= Utils.GetDateTimeValue(kvp.Value)).OrderBy(bs => bs.CreatedOn).ToList();
                        break;
                    case Constants.CREATEDATETOFILTER:
                        allBC = allBC.Where(b => b.CreatedOn <= Utils.GetDateTimeValue(kvp.Value)).OrderBy(bs => bs.CreatedOn).ToList();
                        break;

                    case Constants.BANKCIRCLEID:
                        break;
                    case Constants.STATEID:
                        allBC = allBC.Where(b => b.PLStateId == Utils.GetIntValue(kvp.Value)).OrderBy(bs => bs.CreatedOn).ToList();
                        break;
                    case Constants.BANKREGIONID:
                        break;
                    case Constants.BANKSSA:
                        break;
                    case Constants.BRANCHCATEGORY:
                        break;
                    case Constants.BANKZONEID:
                        break;



                    case Constants.ISBLACKLISTED:
                        allBC = allBC.Where(b => b.IsBlackListed == Utils.GetBoolValue(kvp.Value)).ToList();
                        break;


                }
            }



            //Handle mode
            return allBC;

        }

        public List<Notification> GetNotifications(int? notificationId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetNotifications(notificationId);
        }

        public List<Institutes> GetInstitutes(int? institudeId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetInstitutes(institudeId);

        }
        public List<Corporates> GetPanCard(string cid)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetPanCard(cid);
        }
        public List<Corporates> GetCorporates(int? corporateId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetCorporates(corporateId);
        }
        public List<Bank> getBanks()
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getBanks();
        }
        public List<Bank> getBanks(string userid)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getBanks(userid);
        }
        public List<Products> GetProduct()
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetProduct();
        }
        public List<Circle> getBankState(string BankId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getBankState(BankId);
        }
        public List<Branch> getBankBranch(string BankId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getBankBranch(BankId);
        }
        public List<Branch> getBankBranches(string BankId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getBankBranches(BankId);
        }
        public List<Bank> getbankRefNoDetail(string bankRefNo)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getbankRefNoDetail(bankRefNo);
        }
        public List<Bank> getIfscDetail(string ifsccode)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getIfscDetail(ifsccode);
        }
        public List<BankReport> getBankReport(string userid)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getBankReport(userid);
        }
        public List<FileStatus> getFileError(string userId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getFileError(userId);
        }
        public List<FileStatus> getFileStatus(string userId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getFileStatus(userId);
        }
        public List<FileStatus> getZipStatuss(string userId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.getZipStatuss(userId);
        }
        
        
        public List<Zone> GetCity(string StateId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetCity(StateId);
        }
        public List<Region> GetDistrict(string districtId)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetDistrict(districtId);
        }
        
        public List<Products> getBrnach()
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetProduct();
        }
        public List<Corporates> GetUser(int? userid)
        {
            SQLDataProvider prov = new SQLDataProvider();
            return prov.GetCorporates(userid);
        }
        public List<BCProducts> GetProducts(int? bcId, int? productId)
        {
            SQLDataProvider provider = new SQLDataProvider();
            return provider.GetProducts(bcId, productId);
        }

        private string GetValue(SpreadsheetDocument doc, Cell cell)
        {
            string value;
            if(cell==null)
            {
                value = null;
            }
            else if(cell.CellValue==null)
            {
                value = null;
            }

            
            else
            {
                string valuereturn = cell.CellValue.InnerText;
                value = valuereturn;
                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
                }
            }
            
            return value;
        }
        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }



        public void ExcelRead(string fileName, int userID, string mode, string BankId,string excelName,string fileExtension)
        {
            try
            {
                UserBL businessLogic = new UserBL();
                if (fileExtension == ".xls" || fileExtension == ".xlsx")
                {
                    string CategoryValue = "";
                    bool flag = true;
                    int rowNum = 0;
                  
                    businessLogic.DeleteError(userID.ToString());
                    businessLogic.fileStatus(userID.ToString(), 0, excelName);
                    BankCorrespondence backc = new BankCorrespondence();
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    // Retrieve a reference to a container.
                    CloudBlobContainer container = blobClient.GetContainerReference("dev-raw");
                    // Create the container if it doesn't already exist.

                    // Retrieve reference to a blob named "myblob".
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
                    MemoryStream blobms = new MemoryStream();
                    blockBlob.DownloadToStream(blobms);

                    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(blobms, true))
                    {

                        WorkbookPart workbookpart = doc.WorkbookPart;
                        IEnumerable<Sheet> first = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "BCs");
                        IEnumerable<Sheet> second = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "Certificates");
                        IEnumerable<Sheet> third = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "Experiences");
                        IEnumerable<Sheet> fourth = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "OpreationalAreas");
                        IEnumerable<Sheet> fifth = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "Devices");
                        IEnumerable<Sheet> sixth = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "ConnectivityDetails");
                        IEnumerable<Sheet> sevan = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "PrimaryLocation");
                        IEnumerable<Sheet> eight = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "Remuneration");


                        //Get the Worksheet instance.
                        Worksheet ws1 = (doc.WorkbookPart.GetPartById(first.First().Id.Value) as WorksheetPart).Worksheet;
                        Worksheet ws2 = (doc.WorkbookPart.GetPartById(second.First().Id.Value) as WorksheetPart).Worksheet;
                        Worksheet ws3 = (doc.WorkbookPart.GetPartById(third.First().Id.Value) as WorksheetPart).Worksheet;
                        Worksheet ws4 = (doc.WorkbookPart.GetPartById(fourth.First().Id.Value) as WorksheetPart).Worksheet;
                        Worksheet ws5 = (doc.WorkbookPart.GetPartById(fifth.First().Id.Value) as WorksheetPart).Worksheet;
                        Worksheet ws6 = (doc.WorkbookPart.GetPartById(sixth.First().Id.Value) as WorksheetPart).Worksheet;
                        Worksheet ws7 = (doc.WorkbookPart.GetPartById(sevan.First().Id.Value) as WorksheetPart).Worksheet;
                        Worksheet ws8 = (doc.WorkbookPart.GetPartById(eight.First().Id.Value) as WorksheetPart).Worksheet;

                        ////Worksheet ws7 = (doc.WorkbookPart.GetPartById(seven.First().Id.Value) as WorksheetPart).Worksheet;


                        //Fetch all the rows present in the Worksheet.
                        IEnumerable<Row> r1 = ws1.GetFirstChild<SheetData>().Descendants<Row>();

                        List<BankCorrespondence> allBCs = null;
                        int l = 0;
                        foreach (Row row in r1)
                        {
                            l++;
                            if (row.RowIndex.Value == 1)
                                continue;
                            List<Cell> values = row.Descendants<Cell>().ToList();
                            List<string> errorMsg = new List<string>();
                            // var c = row.ElementAt(1);

                            Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_D = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "D" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_E = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "E" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_F = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "F" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_G = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "G" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_H = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "H" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_I = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "I" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_J = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "J" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_K = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "K" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_L = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "L" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_M = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "M" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_N = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "N" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_O = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "O" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_P = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "P" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_Q = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "Q" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_R = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "R" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_S = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "S" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_T = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "T" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_U = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "U" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_V = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "V" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_W = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "W" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_X = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "X" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_Y = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "Y" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_Z = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "Z" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AA = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AA" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AB = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AB" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AC = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AC" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AD = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AD" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AE = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AE" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AF = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AF" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AG = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AG" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AH = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AH" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AI = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AI" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AJ = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AJ" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AK = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AK" + l.ToString(), false) == 0).FirstOrDefault();
                            Cell CELL_AL = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AL" + l.ToString(), false) == 0).FirstOrDefault();
                          //  Cell CELL_AM = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AM" + l.ToString(), false) == 0).FirstOrDefault();
                          //  Cell CELL_AN = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AN" + l.ToString(), false) == 0).FirstOrDefault();
                            //Cell CELL_AO = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AO" + l.ToString(), false) == 0).FirstOrDefault();
                            //Cell CELL_AP = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AP" + l.ToString(), false) == 0).FirstOrDefault();


                           string bcName= GetValue(doc, CELL_B);
                            string bcDOB= GetValue(doc, CELL_D);
                            string bcFatherName= GetValue(doc, CELL_E);
                            string bcphn_NO= GetValue(doc, CELL_I);
                            string bcAllocationIFSCCode = GetValue(doc, CELL_AC);
                            string bcMinimumCashHandlingLimit = GetValue(doc, CELL_AI);  
                         
                            BankCorrespondence bc = new BankCorrespondence();
                            if (bcName != null && bcDOB != null && bcFatherName != null && bcphn_NO != null && bcAllocationIFSCCode != null && bcMinimumCashHandlingLimit != null)
                            {

                                bc.BankCorrespondId = 0;


                                string name = GetValue(doc, CELL_B);
                                if (name == null)
                                {
                                    errorMsg.Add("303, in row  " + (rowNum + 1) + " User Name Required.");
                                    flag = false;
                                }
                                else
                                {
                                    bc.Name = GetValue(doc, CELL_B);
                                }

                                bc.ImagePath = "PATH";

                                string Gender = validation.getGendereValidation(GetValue(doc, CELL_C));
                                if (Gender == "false")
                                {
                                    errorMsg.Add("304, in row  " + (rowNum + 1) + " Gender Required value should Male,Female,Other");
                                    flag = false;
                                }
                                else
                                {
                                    bc.Gender = GetValue(doc, CELL_C);
                                }


                                string DOB = GetValue(doc, CELL_D);
                                if (DOB == "false")
                                {
                                    errorMsg.Add("305, in row  " + (rowNum + 1) + " DOB Required format should be dd/mm/yyyy");
                                    flag = false;
                                }
                                else
                                {
                                    //DateTime dt = DateTime.ParseExact(DOB, "dd/MM/yyyy",CultureInfo.InvariantCulture);
                                    //string datetime = dt.ToString("yyyy-MM-dd");
                                    //bc.DOB = Convert.ToDateTime(datetime);
                                    bc.DOB = DateTime.FromOADate(Utils.GetDoubleValue(DOB));

                                    bc.dateOfBirth = GetValue(doc, CELL_D);
                                    //    DateTime dateOfBirth = DateTime.ParseExact(string.IsNullOrEmpty(DOB) ? "01/01/1970" : DOB.Replace(".", "/"), "dd/MM/yyyy", null);
                                    //   bc.DOB = dateOfBirth;
                                }
                                string FatherName = GetValue(doc, CELL_E);
                                if (FatherName == null)
                                {
                                    errorMsg.Add("303, in row  " + (rowNum + 1) + " FatherName Required.");
                                    flag = false;
                                }
                                else
                                {
                                    bc.FatherName = GetValue(doc, CELL_E);
                                }
                                string Handicap = GetValue(doc, CELL_H);
                                if (Handicap == null)
                                {
                                    bc.Handicap = "0";
                                    //errorMsg.Add("306, in row  " + (rowNum + 1) + " Handicap Required enter 0 or 1");
                                    //flag = false;
                                }
                                else
                                {
                                    bc.Handicap = GetValue(doc, CELL_H);
                                }
                                string SpouseName = GetValue(doc, CELL_F);
                                if (SpouseName == null)
                                {
                                    bc.SpouseName = "N/A";
                                }
                                else
                                {
                                    bc.SpouseName = GetValue(doc, CELL_F);
                                }


                                string Category = validation.getCategory(GetValue(doc, CELL_G));

                                if (Category == "false")
                                {
                                    bc.Category = "GENERAL";
                                    //errorMsg.Add("303, in row  " + (rowNum + 1) + " Category Required Value Should be GENERAL,OBC,SC,ST");
                                    //flag = false;
                                }
                                else
                                {
                                    CategoryValue = GetValue(doc, CELL_G);
                                    bc.Category = GetValue(doc, CELL_G);
                                }

                                string phn_NO = GetValue(doc, CELL_I);
                                if (phn_NO == null)
                                {
                                    errorMsg.Add("303, in row  " + (rowNum + 1) + " Invalid PhoneNumber1");
                                    flag = false;
                                }
                                else
                                {
                                    var PhoneNumber1 = validation.IsCorrectMobileNumber(GetValue(doc, CELL_I));
                                    if (PhoneNumber1 == "0")
                                    {
                                        errorMsg.Add("303, in row  " + (rowNum + 1) + " Invalid PhoneNumber1");
                                        flag = false;
                                    }
                                    else
                                    {
                                        bc.PhoneNumber1 = GetValue(doc, CELL_I);
                                    }

                                }


                                string PhoneNumber2 = GetValue(doc, CELL_J);
                                if (PhoneNumber2 == null)
                                {
                                    bc.PhoneNumber2 = "N/A";
                                }
                                else
                                {
                                    bc.PhoneNumber2 = GetValue(doc, CELL_J);
                                }

                                string PhoneNumber3 = GetValue(doc, CELL_K);
                                if (PhoneNumber3 == null)
                                {
                                    bc.PhoneNumber3 = "N/A";
                                }
                                else
                                {
                                    bc.PhoneNumber3 = GetValue(doc, CELL_K);
                                }
                                bc.ContactPerson = null;
                                bc.ContactDesignation = null;


                                string NoofComplaint = GetValue(doc, CELL_AL);
                                if (NoofComplaint == null)
                                {

                                    bc.NoofComplaint = null;
                                }
                                else
                                {
                                    int NoofComplaints = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AL));
                                    if (NoofComplaints == -1)
                                    {
                                        errorMsg.Add("301, in row  " + (rowNum + 1) + "  No of complaint should be number.");
                                        flag = false;

                                    }
                                    else
                                    {
                                        bc.NoofComplaint = NoofComplaint.ToString();
                                    }

                                }
                               

                                //string ContactPerson = GetValue(doc, CELL_AN);
                                //if (ContactPerson == null)
                                //{
                                //    bc.ContactPerson = null;
                                //}
                                //else
                                //{
                                //    bc.ContactPerson = GetValue(doc, CELL_AN);
                                //}

                                //string ContactDesignation = GetValue(doc, CELL_AO);
                                //if (ContactDesignation == null)
                                //{
                                //    bc.ContactDesignation = null;
                                //}
                                //else
                                //{
                                //    bc.ContactDesignation = GetValue(doc, CELL_AO);
                                //}
                                //string NoofComplaint = GetValue(doc, CELL_AP);
                                //if (NoofComplaint == null)
                                //{
                                //    bc.NoofComplaint = null;
                                //}
                                //else
                                //{
                                //    bc.NoofComplaint = GetValue(doc, CELL_AP);
                                //}


                                string Email = GetValue(doc, CELL_L);
                                if (Email == null)
                                {

                                    //errorMsg.Add("301, in row  " + (rowNum + 1) + " Invalid Email Id.");
                                    //flag = false;
                                }
                                else
                                {
                                    var email = validation.IsValidEmailId(GetValue(doc, CELL_L));
                                    if (email == "false")
                                    {
                                        errorMsg.Add("301, in row  " + (rowNum + 1) + " Invalid Email Id.");
                                        flag = false;

                                    }
                                    else
                                    {
                                        bc.Email = email;
                                    }

                                }

                                string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                                if (AadharCard == "0")
                                {
                                    bc.AadharCard = "";
                                    errorMsg.Add("303, in row  " + (rowNum + 1) + " AadharCard should be valid.");
                                    flag = false;
                                }
                                else
                                {
                                   // bc.AadharCard = AadharCard;
                                    int valid = businessLogic.getAadhaar(MD5Hash(AadharCard));
                                    if (valid == 1)
                                    {
                                        errorMsg.Add("303, in row  " + (rowNum + 1) + " Aadhaar card number already exist.");
                                        flag = false;
                                    }
                                    else
                                    {
                                        bc.AadharCard = (MD5Hash(GetValue(doc, CELL_A)));
                                    }




                                }


                                string PanCard = validation.IsCorrectPanCard(GetValue(doc, CELL_M));
                                if (PanCard == null)
                                {
                                    bc.PanCard = "N/A";
                                }
                                else if (PanCard == "0")
                                {
                                    errorMsg.Add("311, in row  " + (rowNum + 1) + " Invalid Pan Card. (Pan card should be in Capital words)");
                                    flag = false;
                                }
                                else
                                {
                                    bc.PanCard = GetValue(doc, CELL_M);
                                }




                                bc.VoterCard = GetValue(doc, CELL_N);
                                bc.DriverLicense = GetValue(doc, CELL_O);
                                bc.NregaCard = GetValue(doc, CELL_P);
                                bc.RationCard = GetValue(doc, CELL_Q);



                                string Village = GetValue(doc, CELL_R);
                                if (Village == null)
                                {
                                    errorMsg.Add("311, in row  " + (rowNum + 1) + " village code required");
                                    flag = false;
                                }
                                else
                                {

                                    string valid = businessLogic.getStateDistrictIds(Village.ToString());
                                    if (valid == "")
                                    {
                                        errorMsg.Add("303, in row  " + (rowNum + 1) + " village code does not exist.");
                                        flag = false;
                                    }
                                    else
                                    {
                                        string[] ldval = valid.Split(',');

                                        bc.State = ldval[2].ToString();
                                        bc.District = ldval[1].ToString();
                                        bc.Subdistrict = ldval[0].ToString();
                                        bc.Village = Village.ToString();

                                    }


                                }

                                bc.City = "0";
                                bc.Area = GetValue(doc, CELL_S);

                                string PinCode = GetValue(doc, CELL_T);
                                if (PinCode == null)
                                {
                                    errorMsg.Add("303, in row  " + (rowNum + 1) + " PinCode  Required.");
                                    flag = false;
                                }
                                else
                                {
                                    bc.PinCode = GetValue(doc, CELL_T);
                                }

                                string AlternateOccupationType = validation.getAlternateOccupationType(GetValue(doc, CELL_U));
                                if (AlternateOccupationType == "false")
                                {
                                    errorMsg.Add("303, in row  " + (rowNum + 1) + " Alternate Occupation Type Required Government,Public Sector,Private,Self Employed");
                                    flag = false;
                                }
                                else
                                {
                                    bc.AlternateOccupationType = GetValue(doc, CELL_U);
                                }


                                bc.AlternateOccupationDetail = GetValue(doc, CELL_V);

                                string BankReferenceNumber = GetValue(doc, CELL_W);
                                if (BankReferenceNumber == null)
                                {

                                    errorMsg.Add("303, in row  " + (rowNum + 1) + " Bank Reference Number Required.");
                                    flag = false;
                                }
                                else
                                {

                                    int valid = businessLogic.getBankReferenceNumber(BankReferenceNumber);
                                    if (valid == 1)
                                    {
                                        errorMsg.Add("303, in row  " + (rowNum + 1) + " Bank Reference Number already exist.");
                                        flag = false;
                                    }
                                    else
                                    {
                                        bc.BankReferenceNumber = BankReferenceNumber;
                                    }




                                }


                                string Qualification = GetValue(doc, CELL_X);
                                if (Qualification == null)
                                {
                                    errorMsg.Add("309, in row  " + (rowNum + 1) + " Qualification Required.");
                                    flag = false;
                                }
                                else
                                {
                                    bc.Qualification = GetValue(doc, CELL_X);
                                }

                                bc.OtherQualification = "OtherQualification";

                                int isAllocated = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_Y));
                                if (isAllocated == -1)
                                {
                                    errorMsg.Add("303, in row  " + (rowNum + 1) + " Is allocated Required Enter 0 or 1");
                                    flag = false;
                                }
                                else
                                {
                                    bc.isAllocated = Convert.ToBoolean(isAllocated);
                                    // bc.isAllocated = Convert.ToBoolean(Convert.ToInt16(GetValue(doc, CELL_Y)));


                                }

                                if (bc.isAllocated)
                                {
                                    bc.isAllocated = Convert.ToBoolean(1);
                                    string CorporateId = GetValue(doc, CELL_AA);
                                    if (CorporateId == null)
                                    {

                                        errorMsg.Add("303, in row  " + (rowNum + 1) + " The BC is Corporate Allocated, but No Corporate ID is defined.");
                                        flag = false;

                                    }
                                    else
                                    {
                                        int valid = businessLogic.getCorporate(CorporateId);

                                        if (valid == 0)
                                        {
                                            errorMsg.Add("303, in row  " + (rowNum + 1) + " CorporateId does not exist. Please add the Corporate Id.");
                                            flag = false;
                                        }
                                        else
                                        {
                                            bc.CorporateId = Utils.GetIntValue(GetValue(doc, CELL_AA));
                                        }

                                    }
                                }
                                else
                                {
                                    bc.isAllocated = Convert.ToBoolean(1);
                                    bc.CorporateId = 1;
                                }

                                bc.Status = "active";
                                string AppointmentDate = validation.DateFormat(GetValue(doc, CELL_AB));
                                if (AppointmentDate == "false")
                                {
                                    errorMsg.Add("305, in row  " + (rowNum + 1) + " BCActivityFrom Required format should be dd/mm/yyyy");
                                    flag = false;
                                }
                                else
                                {
                                    //DateTime Appointment_Date = DateTime.ParseExact(string.IsNullOrEmpty(AppointmentDate) ? "01/01/1970" : AppointmentDate.Replace(".", "/"), "dd/MM/yyyy", null);
                                    //bc.AppointmentDate = Appointment_Date;
                                    bc.AppointmentDate = DateTime.FromOADate(Utils.GetDoubleValue(GetValue(doc, CELL_AB)));
                                    bc.Appointment_Date = GetValue(doc, CELL_AB);
                                }


                                string AllocationIFSCCode = GetValue(doc, CELL_AC);
                                if (AllocationIFSCCode == null)
                                {
                                    errorMsg.Add("303, in row  " + (rowNum + 1) + " Allocation IFSC Code Required.");
                                    flag = false;
                                }
                                else
                                {
                                    ////////////////////// Find bankid by IFSC Code

                                    string valid = businessLogic.getBankId(AllocationIFSCCode.ToString(), BankId);
                                    string[] ldval = valid.Split(',');
                                    if (ldval[1] == "not belongs to your bank")
                                    {
                                        errorMsg.Add("303, in row  " + (rowNum + 1) + " IFSC Code does not belongs to your bank");
                                        flag = false;
                                    }
                                    else if (ldval[1] == "does not exists in database")
                                    {
                                        errorMsg.Add("303, in row  " + (rowNum + 1) + " IFSC Code does not exists in database");
                                        flag = false;
                                    }
                                    else
                                    {

                                        bc.AllocationBranchId = Convert.ToInt32(ldval[1]);
                                        bc.AllocationBankId = Convert.ToInt32(ldval[0]);
                                        bc.AllocationIFSCCode = GetValue(doc, CELL_AC);

                                    }



                                }

                                string BCType = GetValue(doc, CELL_AD);
                                if (BCType == null)
                                {
                                    bc.BCType = "Corporate";
                                }
                                else
                                {
                                    bc.BCType = GetValue(doc, CELL_AD);
                                }



                                //string BCType = validation.getBCTypeValidation(GetValue(doc, CELL_AJ));
                                //if (BCType == "false")
                                //{
                                //    errorMsg.Add("311, in row  " + (rowNum + 1) + " Enter Individual,Corporate,Fixed,Mobile,Both");
                                //    flag = false;
                                //}
                                //else
                                //{
                                //    bc.BCType = GetValue(doc, CELL_AJ);
                                //}

                                int WorkingDays = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AE));
                                if (WorkingDays == -1)
                                {
                                    errorMsg.Add("311, in row  " + (rowNum + 1) + " Working day should be number.");
                                    flag = false;
                                }
                                else
                                {
                                    bc.WorkingDays = Utils.GetIntValue(GetValue(doc, CELL_AE));
                                }

                                int WorkingHours = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AF));
                                if (WorkingHours == -1)
                                {
                                    errorMsg.Add("311, in row  " + (rowNum + 1) + " Working hours should be number.");
                                    flag = false;
                                }
                                else
                                {
                                    bc.WorkingHours = Utils.GetIntValue(GetValue(doc, CELL_AF));
                                }


                                bc.PLPostalAddress = "0";
                                bc.PLVillageCode = "0";
                                bc.PLVillageDetail = "0";
                                bc.PLTaluk = "0";
                                bc.PLDistrictId = 0;
                                bc.PLStateId = 0;


                                bc.PLPinCode = "0";

                                string Productid = GetValue(doc, CELL_AG);
                                if (Productid == null)
                                {
                                    //errorMsg.Add("311, in row  " + (rowNum + 1) + " Product Id should be number.");
                                    //flag = false;
                                    bc.productId = "0";
                                }
                                else
                                {
                                    int valid = businessLogic.getProductId(Productid);
                                    if (valid == 0)
                                    {
                                        errorMsg.Add("303, in row  " + (rowNum + 1) + " Group Product Id not exist in database.");
                                        flag = false;
                                    }
                                    else
                                    {
                                        bc.productId = Utils.GetStringValue(GetValue(doc, CELL_AG));
                                    }

                                }





                                bc.UniqueIdentificationNumber = Utils.GetStringValue(GetValue(doc, CELL_AH));
                                int MinimumCashHandlingLimit = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AI));
                                if (MinimumCashHandlingLimit == -1)
                                {

                                    errorMsg.Add("312 in row  " + (rowNum + 1) + " Minimum CashHandling Limit  should be number.");
                                    flag = false;
                                }
                                else
                                {
                                    bc.MinimumCashHandlingLimit = Utils.GetDoubleValue(GetValue(doc, CELL_AI));
                                }


                                //int MonthlyFixedRenumeration = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AJ));
                                //if (MonthlyFixedRenumeration == -1)
                                //{
                                //    bc.MonthlyFixedRenumeration = 0;
                                //    //errorMsg.Add("312 in row  " + (rowNum + 1) + " Commission 1  should be number.");
                                //    //flag = false;
                                //}
                                //else
                                //{
                                //    bc.MonthlyFixedRenumeration = Utils.GetDoubleValue(GetValue(doc, CELL_AJ));
                                //}
                                //int MonthlyVariableRenumeration = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AK));
                                //if (MonthlyVariableRenumeration == -1)
                                //{
                                //    //errorMsg.Add("312 in row  " + (rowNum + 1) + " Commission 2  should be number.");
                                //    //flag = false;
                                //    bc.MonthlyVariableRenumeration = 0;
                                //}
                                //else
                                //{
                                //    bc.MonthlyVariableRenumeration = Utils.GetDoubleValue(GetValue(doc, CELL_AK));
                                //}
                                bc.Latitude = Utils.GetStringValue(GetValue(doc, CELL_AJ));
                                bc.Longitude = Utils.GetStringValue(GetValue(doc, CELL_AK));
                                int access = businessLogic.getBcValidate(name, DOB, FatherName, CategoryValue, phn_NO);
                                if (access == 1)
                                {
                                }
                                else
                                {
                                    string erros = "303, in row  " + (rowNum + 1) + " BC already exist.";
                                    flag = false;
                                    businessLogic.SaveError(userID.ToString(), erros);
                                }





                                if (allBCs == null)
                                    allBCs = new List<BankCorrespondence>();

                                //  InsertUpdateBCforExcel(bc, userID, mode);
                                allBCs.Add(bc);
                                rowNum++;
                                for (int i = 0; i < errorMsg.Count; i++)
                                {
                                    var errors = errorMsg[i];
                                    businessLogic.SaveError(userID.ToString(), errors);

                                }
                            }
                            else
                            {
                                //string erros = "row  " + (rowNum + 1) + " remove to upload excel.";
                                //flag = false;
                                //businessLogic.SaveError(userID.ToString(), erros);
                            }

                        }
                        if (allBCs == null)
                        {

                            flag = false;
                            List<string> errorMsgs = new List<string>();
                            errorMsgs.Add("0 Entries found  in the  Uploaded file.");
                            for (int i = 0; i < errorMsgs.Count; i++)
                            {
                                var errors = errorMsgs[i];
                                businessLogic.SaveError(userID.ToString(), errors);

                            }

                        }


                        //Certifications
                        IEnumerable<Row> r2 = ws2.GetFirstChild<SheetData>().Descendants<Row>();
                        int k = 0;
                        List<BCCertifications> Certification = null;
                        int rowNum7 = 1;

                        foreach (Row row1 in r2)
                        {
                            k++;
                            if (row1.RowIndex.Value == 1)
                                continue;

                            List<Cell> values = row1.Descendants<Cell>().ToList();
                            List<string> certifierrorMsg = new List<string>();

                            Cell CELL_A = row1.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + k.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_B = row1.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + k.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_C = row1.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + k.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_D = row1.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "D" + k.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_E = row1.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "E" + k.ToString(), true) == 0).FirstOrDefault();
                            BCCertifications cr = new BCCertifications();
                            string bankRefNo = GetValue(doc, CELL_A);
                            string DateOfPassing = GetValue(doc, CELL_B);
                            string InstituteName = GetValue(doc, CELL_C);
                            string CourseName = GetValue(doc, CELL_D);

                            if (bankRefNo != null && DateOfPassing != null && InstituteName != null && CourseName != null)
                            {

                                if (bankRefNo == null)
                                {
                                    certifierrorMsg.Add("303, in row  " + (rowNum7 + 1) + " of Certificates sheet Bank Reference Required.");

                                    flag = false;
                                }
                                else
                                {
                                    cr.AadharCard = GetValue(doc, CELL_A);
                                }

                                //if (AadharCard == "0")
                                //{
                                //    certifierrorMsg.Add("303, in row  " + (rowNum7 + 1) + " of Certificates sheet AadharCard should valid.");
                                //    flag = false;
                                //}
                                //else
                                //{
                                //    cr.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                                //}


                                if (DateOfPassing == "false")
                                {
                                    certifierrorMsg.Add("305, in row  " + (rowNum7 + 1) + " of Certificates sheet Date of Passing Required format should be dd/mm/yyyy");
                                    flag = false;
                                }
                                else
                                {

                                    //DateTime Date_Of_Passing = DateTime.ParseExact(string.IsNullOrEmpty(DateOfPassing) ? "01/01/1970" : DateOfPassing.Replace(".", "/"), "dd/MM/yyyy", null);
                                    //cr.DateOfPassing = Date_Of_Passing;
                                    cr.DateOfPassing = DateTime.FromOADate(Utils.GetDoubleValue(DateOfPassing));
                                    cr.DateOf_Passing = GetValue(doc, CELL_B);
                                }



                                if (InstituteName == null)
                                {
                                    certifierrorMsg.Add("303, in row  " + (rowNum7 + 1) + "  of Certificates sheet Institute Name Required.");
                                    flag = false;
                                }
                                else
                                {
                                    cr.InstituteName = Utils.GetStringValue(GetValue(doc, CELL_C));

                                }


                                if (CourseName == null)
                                {
                                    certifierrorMsg.Add("303, in row  " + (rowNum7 + 1) + " of Certificates sheet Course Name Required.");
                                    flag = false;
                                }
                                else
                                {
                                    cr.CourseName = Utils.GetStringValue(GetValue(doc, CELL_D));

                                }



                                string Grade = GetValue(doc, CELL_E);
                                if (Grade == null)
                                {
                                    cr.Grade = "N/A";

                                }
                                else
                                {
                                    cr.Grade = Utils.GetStringValue(GetValue(doc, CELL_E));
                                }



                                if (Certification == null)
                                {
                                    Certification = new List<BCCertifications>();
                                }
                                rowNum7++;
                                Certification.Add(cr);
                                for (int i = 0; i < certifierrorMsg.Count; i++)
                                {
                                    var errors = certifierrorMsg[i];
                                    businessLogic.SaveError(userID.ToString(), errors);

                                }



                                if (Certification != null)
                                {
                                    //  bc.Certifications = Certification;
                                }
                            }
                        }


                        //Experience
                        IEnumerable<Row> r3 = ws3.GetFirstChild<SheetData>().Descendants<Row>();

                        List<PreviousExperiene> experience = null;
                        int rowNum1 = 1;
                        int n = 0;
                        foreach (Row row in r3)
                        {
                            n++;
                            if (row.RowIndex.Value == 1)
                                continue;

                            List<Cell> values = row.Descendants<Cell>().ToList();
                            List<string> experrorMsg = new List<string>();
                            Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + n.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + n.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + n.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_D = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "D" + n.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_E = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "E" + n.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_F = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "F" + n.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_G = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "G" + n.ToString(), true) == 0).FirstOrDefault();

                            PreviousExperiene pr = new PreviousExperiene();

                            string AllocationIFSCCode = GetValue(doc, CELL_B);
                            string FromDate = GetValue(doc, CELL_E);
                            string AadharCard = GetValue(doc, CELL_A);
                            string ToDate = GetValue(doc, CELL_F);

                            if (AllocationIFSCCode != null && FromDate != null && AadharCard != null && ToDate != null)
                            {

                                if (AllocationIFSCCode == null)
                                {
                                    experrorMsg.Add("303, in row  " + (rowNum + 1) + "  of Experiences sheet  Branch IFSC Code Required.");
                                    flag = false;
                                }
                                else
                                {
                                    ////////////////////// Find bankid by IFSC Code
                                    if (AllocationIFSCCode == "0")
                                    {
                                        pr.BankName = GetValue(doc, CELL_C);
                                        pr.BranchName = GetValue(doc, CELL_D);
                                        pr.BankId = 0;
                                    }
                                    else
                                    {
                                        string valid = businessLogic.getBankIdForPreviousexp(AllocationIFSCCode.ToString(), BankId);
                                        if (valid == "")
                                        {
                                            experrorMsg.Add("303, in row  " + (rowNum + 1) + " of Experiences sheet Branch IFSC Code does not exist.");
                                            flag = false;
                                        }
                                        else
                                        {
                                            string[] ldval = valid.Split(',');
                                            pr.Branchid = Convert.ToInt32(ldval[1]);
                                            pr.BankId = Convert.ToInt32(ldval[0]);
                                            pr.BankName = "N/A";
                                            pr.BranchName = "N/A";

                                        }
                                    }




                                }







                                if (FromDate == "false")
                                {
                                    experrorMsg.Add("305, in row  " + (rowNum1 + 1) + " of Experiences sheet From Date Required format should be dd/mm/yyyy");
                                    flag = false;
                                }
                                else
                                {

                                    //DateTime From_Date = DateTime.ParseExact(string.IsNullOrEmpty(FromDate) ? "01/01/1970" : FromDate.Replace(".", "/"), "dd/MM/yyyy", null);
                                    //pr.FromDate = From_Date;
                                    pr.FromDate = DateTime.FromOADate(Utils.GetDoubleValue(FromDate));
                                    pr.From_Date = GetValue(doc, CELL_E);
                                }


                                if (ToDate == "false")
                                {
                                    experrorMsg.Add("305, in row  " + (rowNum1 + 1) + " of Experiences sheet From Date Required format should be dd/mm/yyyy");
                                    flag = false;
                                }
                                else
                                {

                                    //DateTime To_Date = DateTime.ParseExact(string.IsNullOrEmpty(FromDate) ? "01/01/1970" : FromDate.Replace(".", "/"), "dd/MM/yyyy", null);
                                    //pr.ToDate = To_Date;
                                    pr.ToDate = DateTime.FromOADate(Utils.GetDoubleValue(ToDate));
                                    pr.To_Date = GetValue(doc, CELL_F);
                                }
                                string bankRefNo = GetValue(doc, CELL_A);
                                if (bankRefNo == null)
                                {
                                    experrorMsg.Add("303, in row  " + (rowNum7 + 1) + " of Experiences sheet Bank Reference Required.");

                                    flag = false;
                                }
                                else
                                {
                                    pr.AadharCard = GetValue(doc, CELL_A);
                                }
                                //if (AadharCard == "0")
                                //{
                                //    //experrorMsg.Add("303, in row  " + (rowNum7 + 1) + " of Experiences sheet AadharCard should valid.");
                                //    //flag = false;
                                //}
                                //else
                                //{
                                //    pr.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                                //}


                                string Reason = GetValue(doc, CELL_G);
                                if (Reason == null)
                                {
                                    pr.Reason = "N/A";

                                }
                                else
                                {
                                    pr.Reason = Utils.GetStringValue(GetValue(doc, CELL_G));
                                }


                                if (experience == null)
                                {
                                    experience = new List<PreviousExperiene>();
                                }
                                experience.Add(pr);
                                rowNum1++;
                                for (int i = 0; i < experrorMsg.Count; i++)
                                {
                                    var errors = experrorMsg[i];
                                    businessLogic.SaveError(userID.ToString(), errors);

                                }

                                //}

                                if (experience != null)
                                {
                                    // bc.PreviousExperience = experience;
                                }
                            }
                        }


                        //Opreational Areas
                        IEnumerable<Row> r4 = ws4.GetFirstChild<SheetData>().Descendants<Row>();

                        List<OperationalAreas> Areas = null;
                        int arowNum8 = 1;
                        int m = 0;
                        foreach (Row row in r4)
                        {
                            m++;
                            if (row.RowIndex.Value == 1)
                                continue;
                            arowNum8++;
                            List<Cell> values = row.Descendants<Cell>().ToList();
                            List<string> AreaserrorMsg = new List<string>();
                            Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + m.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + m.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + m.ToString(), true) == 0).FirstOrDefault();

                            OperationalAreas oa = new OperationalAreas();

                            string bcVillage = GetValue(doc, CELL_A);
                            string bcVillageDetail = GetValue(doc, CELL_B);
                            string bcbankRefNo = GetValue(doc, CELL_C);


                            if (bcVillage != null && bcVillageDetail != null && bcbankRefNo != null)
                            {
                                string Village = GetValue(doc, CELL_B);
                                if (Village == null)
                                {
                                    AreaserrorMsg.Add("311, in row  " + (arowNum8 + 1) + "  of OpreationalAreas sheet village code required should be number.");
                                    flag = false;
                                }
                                else
                                {

                                    string valid = businessLogic.getStateDistrictIds(Village.ToString());
                                    if (valid == "")
                                    {
                                        AreaserrorMsg.Add("303, in row  " + (arowNum8 + 1) + "  of OpreationalAreas sheet village code does not exist.");
                                        flag = false;
                                    }
                                    else
                                    {
                                        string[] ldval = valid.Split(',');
                                        oa.VillageCode = Utils.GetStringValue(GetValue(doc, CELL_B));
                                    }
                                }

                                string VillageDetail = GetValue(doc, CELL_C);
                                if (VillageDetail == null)
                                {
                                    oa.VillageDetail = "N/A";

                                }
                                else
                                {
                                    oa.VillageDetail = Utils.GetStringValue(GetValue(doc, CELL_C));
                                }


                                //string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                                //if (AadharCard == "0")
                                //{
                                //    //AreaserrorMsg.Add("303, in row  " + (arowNum8 + 1) + " of OpreationalAreas sheet AadharCard should valid.");
                                //    //flag = false;
                                //}
                                //else
                                //{
                                //    oa.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                                //}

                                string bankRefNo = GetValue(doc, CELL_A);
                                if (bankRefNo == null)
                                {
                                    AreaserrorMsg.Add("303, in row  " + (arowNum8 + 1) + " of OpreationalAreas sheet Bank Reference Required.");
                                    flag = false;
                                }
                                else
                                {
                                    oa.AadharCard = GetValue(doc, CELL_A);
                                }

                                if (Areas == null)
                                {
                                    Areas = new List<OperationalAreas>();
                                }
                                Areas.Add(oa);
                                for (int i = 0; i < AreaserrorMsg.Count; i++)
                                {
                                    var errors = AreaserrorMsg[i];
                                    businessLogic.SaveError(userID.ToString(), errors);

                                }



                                if (Areas != null)
                                {
                                    //  bc.OperationalAreas = Areas;
                                }
                            }
                        }


                        //Remuneration
                        IEnumerable<Row> r8 = ws8.GetFirstChild<SheetData>().Descendants<Row>();

                        List<CommssionList> Commssion = null;
                        int arowNum9 = 1;
                        int p = 0;
                        foreach (Row row in r8)
                        {
                            p++;
                            if (row.RowIndex.Value == 1)
                                continue;

                            List<Cell> values = row.Descendants<Cell>().ToList();
                            List<string> CommssionerrorMsg = new List<string>();
                            Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + p.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + p.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + p.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_D = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "D" + p.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_E = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "E" + p.ToString(), true) == 0).FirstOrDefault();

                            CommssionList cl = new CommssionList();

                            string bcMonth = GetValue(doc, CELL_B);
                            string Year = GetValue(doc, CELL_C);
                            string bcbankRefNo = GetValue(doc, CELL_A);


                            if (bcMonth != null && Year != null && bcbankRefNo != null)
                            {


                                string bankRefNo = GetValue(doc, CELL_A);
                                if (bankRefNo == null)
                                {
                                    CommssionerrorMsg.Add("303, in row  " + (arowNum9 + 1) + " of Remuneration sheet Bank Reference Required.");
                                    flag = false;
                                }
                                else
                                {
                                    cl.BankRefNo = GetValue(doc, CELL_A);
                                }

                                string month = GetValue(doc, CELL_B);
                                if (month == null)
                                {
                                    CommssionerrorMsg.Add("301, in row  " + (arowNum9 + 1) + "   of Remuneration sheet Month is required");
                                    flag = false;
                                    
                                }
                                else
                                {
                                    int months = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_B));
                                    if (months == -1)
                                    {
                                        CommssionerrorMsg.Add("301, in row  " + (arowNum9 + 1) + "  of Remuneration sheet Month should be number.");
                                        flag = false;

                                    }
                                    else
                                    {
                                        cl.Month = month.ToString();
                                        string MonthName = validation.getMonthName(month.ToString());
                                        cl.MonthName = MonthName;
                                    }

                                }

                                string year = GetValue(doc, CELL_C);
                                if (year == null)
                                {
                                    CommssionerrorMsg.Add("301, in row  " + (arowNum9 + 1) + "   of Remuneration sheet Year is required");
                                    flag = false;

                                }
                                else
                                {
                                    int years = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_C));
                                    if (years == -1)
                                    {
                                        CommssionerrorMsg.Add("301, in row  " + (arowNum9 + 1) + "  of Remuneration sheet Year should be number.");
                                        flag = false;

                                    }
                                    else
                                    {
                                        cl.Year = year.ToString();
                                       
                                    }

                                }



                                string Commssion1 = GetValue(doc, CELL_D);
                                if (Commssion1 == null)
                                {
                                    cl.Commission1 = "0";
                                }
                                else
                                {
                                    int Commissions1 = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_D));
                                    if (Commissions1 == -1)
                                    {
                                        CommssionerrorMsg.Add("301, in row  " + (rowNum + 1) + "  of Remuneration sheet Commission1 should be number.");
                                        flag = false;

                                    }
                                    else
                                    {
                                        cl.Commission1 = Commssion1.ToString();
                                    }

                                }


                                string Commssion2 = GetValue(doc, CELL_E);
                                if (Commssion2 == null)
                                {
                                    cl.Commission2 = "0";
                                }
                                else
                                {
                                    int Commissions2 = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_E));
                                    if (Commissions2 == -1)
                                    {
                                        CommssionerrorMsg.Add("301, in row  " + (rowNum + 1) + "  of Remuneration sheet Commission2 should be number.");
                                        flag = false;

                                    }
                                    else
                                    {
                                        cl.Commission2 = Commssion2.ToString();
                                    }

                                }


                                if (Commssion == null)
                                {
                                    Commssion = new List<CommssionList>();
                                }
                                Commssion.Add(cl);
                                for (int i = 0; i < CommssionerrorMsg.Count; i++)
                                {
                                    var errors = CommssionerrorMsg[i];
                                    businessLogic.SaveError(userID.ToString(), errors);

                                }



                                if (Commssion != null)
                                {
                                    //  bc.OperationalAreas = Areas;
                                }
                            }
                        }

                        //Devices
                        IEnumerable<Row> r5 = ws5.GetFirstChild<SheetData>().Descendants<Row>();

                        List<BCDevices> Devices = null;
                        int rowNum2 = 1;
                        int s1 = 0;
                        foreach (Row row in r5)
                        {
                            s1++;
                            if (row.RowIndex.Value == 1)
                                continue;

                            List<Cell> values = row.Descendants<Cell>().ToList();
                            List<string> DeviceserrorMsg = new List<string>();
                            Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + s1.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + s1.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + s1.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_D = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "D" + s1.ToString(), true) == 0).FirstOrDefault();


                            BCDevices dc = new BCDevices();
                            string bcGivenOn = GetValue(doc, CELL_A);
                            string bcDevice = GetValue(doc, CELL_B);
                            string bcDeviceCode = GetValue(doc, CELL_C);
                            string bcbankRefNo = GetValue(doc, CELL_D);


                            if (bcGivenOn != null && bcDevice != null && bcDeviceCode != null && bcbankRefNo != null)
                            {
                                //  dc.DeviceId = Utils.GetIntValue(GetValue(doc, values[0]));                  

                                string GivenOn = validation.DateFormat(GetValue(doc, CELL_B));
                                if (GivenOn == "false")
                                {
                                    DeviceserrorMsg.Add("305, in row  " + (rowNum2 + 1) + " of Devices sheet Give On Date Required format should be dd/mm/yyyy");
                                    flag = false;
                                }
                                else
                                {

                                    //DateTime Given_On = DateTime.ParseExact(string.IsNullOrEmpty(GivenOn) ? "01/01/1970" : GivenOn.Replace(".", "/"), "dd/MM/yyyy", null);
                                    //dc.GivenOn = Given_On;
                                    dc.GivenOn = DateTime.FromOADate(Utils.GetDoubleValue(GivenOn));
                                    dc.Given_On = GetValue(doc, CELL_B);
                                }
                                string Device = validation.getDeviceName(GetValue(doc, CELL_C));
                                if (Device == "false")
                                {
                                    DeviceserrorMsg.Add("303, in row  " + (rowNum2 + 1) + " of Devices sheet Device Name Should be POS,Micro ATM,USSD Phones,KIOSK.");
                                    flag = false;
                                }
                                else
                                {
                                    dc.Device = Utils.GetStringValue(GetValue(doc, CELL_C));
                                }

                                string DeviceCode = GetValue(doc, CELL_D);
                                if (DeviceCode == null)
                                {
                                    string dName = Utils.GetStringValue(GetValue(doc, CELL_C));
                                    if (dName == "POS" || dName == "USSD Phones")
                                    {
                                        DeviceserrorMsg.Add("303, in row  " + (rowNum2 + 1) + " of Devices sheet Device Code Required.");
                                        flag = false;
                                    }
                                    else
                                    {
                                        dc.DeviceCode = Utils.GetStringValue(GetValue(doc, CELL_D));
                                    }


                                }
                                else
                                {


                                    dc.DeviceCode = Utils.GetStringValue(GetValue(doc, CELL_D));
                                }



                                string bankRefNo = GetValue(doc, CELL_A);
                                if (bankRefNo == null)
                                {
                                    DeviceserrorMsg.Add("303, in row  " + (rowNum2 + 1) + " of Devices sheet Bank Reference Required.");
                                    flag = false;
                                }
                                else
                                {
                                    dc.AadharCard = GetValue(doc, CELL_A);
                                }

                                //string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                                //if (AadharCard == "0")
                                //{
                                //    DeviceserrorMsg.Add("303, in row  " + (rowNum2 + 1) + " of Devices sheet AadharCard should valid.");
                                //    flag = false;
                                //}
                                //else
                                //{
                                //    dc.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                                //}

                                if (Devices == null)
                                {
                                    Devices = new List<BCDevices>();
                                }
                                Devices.Add(dc);
                                rowNum2++;
                                for (int i = 0; i < DeviceserrorMsg.Count; i++)
                                {
                                    var errors = DeviceserrorMsg[i];
                                    businessLogic.SaveError(userID.ToString(), errors);

                                }




                                if (Devices != null)
                                {
                                    // bc.Devices = Devices;
                                }
                            }
                        }


                        //SSA Details
                        IEnumerable<Row> r7 = ws7.GetFirstChild<SheetData>().Descendants<Row>();

                        List<SsaDetails> SsaDetail = null;
                        int rowNum4 = 1;
                        int ss1 = 0;

                        foreach (Row row in r7)
                        {
                            ss1++;
                            if (row.RowIndex.Value == 1)
                                continue;

                            List<Cell> values = row.Descendants<Cell>().ToList();
                            List<string> SSAeserrorMsg = new List<string>();
                            Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + ss1.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + ss1.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + ss1.ToString(), true) == 0).FirstOrDefault();


                            SsaDetails cd = new SsaDetails();
                            string bcbankRefNo = GetValue(doc, CELL_A);
                            string bcSsa = GetValue(doc, CELL_B);
                            string bcVillage = GetValue(doc, CELL_C);


                            if (bcbankRefNo != null && bcSsa != null && bcVillage != null && bcbankRefNo != null)
                            {

                                string bankRefNo = GetValue(doc, CELL_A);
                                if (bankRefNo == null)
                                {

                                    SSAeserrorMsg.Add("303, in row  " + (rowNum4 + 1) + " of PrimaryLocation Bank Reference Required.");
                                    flag = false;
                                }
                                else
                                {
                                    cd.AadharCard = GetValue(doc, CELL_A);
                                }

                                //string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                                //if (AadharCard == "0")
                                //{
                                //    SSAeserrorMsg.Add("303, in row  " + (rowNum4 + 1) + " of PrimaryLocation sheet AadharCard should valid.");
                                //    flag = false;
                                //}
                                //else
                                //{

                                //    cd.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                                //}

                                string Ssa = GetValue(doc, CELL_B);
                                if (Ssa == null)
                                {
                                    SSAeserrorMsg.Add("303, in row  " + (rowNum4 + 1) + " of PrimaryLocation sheet SSA Required.");
                                    flag = false;
                                }
                                else
                                {

                                    cd.Ssa = Utils.GetStringValue(GetValue(doc, CELL_B));
                                }


                                string Village = GetValue(doc, CELL_C);
                                if (Village == null)
                                {
                                    SSAeserrorMsg.Add("303, in row  " + (rowNum4 + 1) + " of PrimaryLocation sheet Villages Required.");
                                    flag = false;
                                }
                                else
                                {

                                    string valid = businessLogic.getStateDistrictIds(Village.ToString());
                                    if (valid == "")
                                    {
                                        SSAeserrorMsg.Add("303, in row  " + (rowNum + 1) + " PrimaryLocation not exists.");
                                        flag = false;
                                    }
                                    else
                                    {
                                        string[] ldval = valid.Split(',');

                                        cd.State = ldval[2].ToString();
                                        cd.District = ldval[1].ToString();
                                        cd.SubDistrict = ldval[0].ToString();
                                        cd.Village = Village.ToString();
                                    }
                                }

                                if (SsaDetail == null)
                                {
                                    SsaDetail = new List<SsaDetails>();
                                }
                                SsaDetail.Add(cd);
                                for (int i = 0; i < SSAeserrorMsg.Count; i++)
                                {
                                    var errors = SSAeserrorMsg[i];
                                    businessLogic.SaveError(userID.ToString(), errors);

                                }



                                if (SsaDetail != null)
                                {
                                    // bc.ConnectivityDetails = Connectivity;
                                }
                            }
                        }
                        IEnumerable<Row> r6 = ws6.GetFirstChild<SheetData>().Descendants<Row>();

                        List<ConnectivityDetails> Connectivity = null;
                        int rowNum3 = 1;
                        int sss1 = 0;
                        foreach (Row row in r6)
                        {
                            sss1++;
                            if (row.RowIndex.Value == 1)
                                continue;

                            List<Cell> values = row.Descendants<Cell>().ToList();
                            List<string> ConnectivityDetailseserrorMsg = new List<string>();
                            Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + sss1.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + sss1.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + sss1.ToString(), true) == 0).FirstOrDefault();
                            Cell CELL_D = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "D" + sss1.ToString(), true) == 0).FirstOrDefault();

                            ConnectivityDetails cd = new ConnectivityDetails();
                            string bcConnectivityMode = GetValue(doc, CELL_A);
                            string bcConnectivityProvider = GetValue(doc, CELL_B);
                            string bcContactNumber = GetValue(doc, CELL_C);
                            string bcbankRefNo = GetValue(doc, CELL_D);


                            if (bcConnectivityMode != null && bcConnectivityProvider != null && bcContactNumber != null && bcbankRefNo != null)
                            {

                                string ConnectivityMode = GetValue(doc, CELL_B);
                                if (ConnectivityMode == null)
                                {
                                    ConnectivityDetailseserrorMsg.Add("303, in row  " + (rowNum3 + 1) + " of ConnectivityDetails sheet  Connectivity Type Required.");
                                    flag = false;
                                }
                                else
                                {
                                    cd.ConnectivityMode = Utils.GetStringValue(GetValue(doc, CELL_B));
                                }

                                string ConnectivityProvider = GetValue(doc, CELL_C);
                                if (ConnectivityProvider == null)
                                {
                                    ConnectivityDetailseserrorMsg.Add("303, in row  " + (rowNum3 + 1) + " of ConnectivityDetails sheet  Provider Required.");
                                    flag = false;
                                }
                                else
                                {
                                    cd.ConnectivityProvider = Utils.GetStringValue(GetValue(doc, CELL_C));
                                }

                                var ContactNumber = validation.IsCorrectMobileNumber(GetValue(doc, CELL_D));
                                if (ContactNumber == "0")
                                {
                                    ConnectivityDetailseserrorMsg.Add("303, in row  " + (rowNum3 + 1) + " of ConnectivityDetails sheet Invalid Number should be 10 digit. ");
                                    flag = false;
                                }
                                else
                                {
                                    cd.ContactNumber = Utils.GetStringValue(GetValue(doc, CELL_D));
                                }
                                string bankRefNo = GetValue(doc, CELL_A);
                                if (bankRefNo == null)
                                {
                                    ConnectivityDetailseserrorMsg.Add("303, in row  " + (rowNum4 + 1) + " of ConnectivityDetails sheet Bank Reference Required.");

                                    flag = false;
                                }
                                else
                                {
                                    cd.AadharCard = GetValue(doc, CELL_A);
                                }
                                //string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                                //if (AadharCard == "0")
                                //{
                                //    ConnectivityDetailseserrorMsg.Add("303, in row  " + (rowNum4 + 1) + " of ConnectivityDetails sheet AadharCard should valid.");
                                //    flag = false;
                                //}
                                //else
                                //{
                                //    cd.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                                //}


                                if (Connectivity == null)
                                {
                                    Connectivity = new List<ConnectivityDetails>();
                                }
                                Connectivity.Add(cd);
                                for (int i = 0; i < ConnectivityDetailseserrorMsg.Count; i++)
                                {
                                    var errors = ConnectivityDetailseserrorMsg[i];
                                    businessLogic.SaveError(userID.ToString(), errors);

                                }


                                if (Connectivity != null)
                                {
                                    // bc.ConnectivityDetails = Connectivity;
                                }
                            }
                        }


                        if (flag == true)
                        {
                            businessLogic.fileStatusChange(userID.ToString(), 1);
                            for (int i = 0; i < allBCs.Count; i++)
                            {

                                backc.BankCorrespondId = allBCs[i].BankCorrespondId;
                                backc.Name = allBCs[i].Name;
                                backc.ImagePath = allBCs[i].ImagePath;
                                backc.Gender = allBCs[i].Gender;
                                backc.DOB = allBCs[i].DOB;
                                backc.FatherName = allBCs[i].FatherName;
                                backc.Handicap = allBCs[i].Handicap;
                                backc.SpouseName = allBCs[i].SpouseName;
                                backc.Category = allBCs[i].Category;
                                backc.PhoneNumber1 = allBCs[i].PhoneNumber1;
                                backc.PhoneNumber2 = allBCs[i].PhoneNumber2;
                                backc.PhoneNumber3 = allBCs[i].PhoneNumber3;
                                backc.Email = allBCs[i].Email;
                                backc.AadharCard = allBCs[i].AadharCard;
                                backc.PanCard = allBCs[i].PanCard;
                                backc.VoterCard = allBCs[i].VoterCard;
                                backc.DriverLicense = allBCs[i].DriverLicense;
                                backc.NregaCard = allBCs[i].NregaCard;
                                backc.RationCard = allBCs[i].RationCard;
                                backc.State = allBCs[i].State;
                                backc.City = allBCs[i].City;
                                backc.District = allBCs[i].District;
                                backc.Subdistrict = allBCs[i].Subdistrict;
                                backc.Area = allBCs[i].Area;
                                backc.PinCode = allBCs[i].PinCode;
                                backc.AlternateOccupationType = allBCs[i].AlternateOccupationType;
                                backc.AlternateOccupationDetail = allBCs[i].AlternateOccupationDetail;
                                backc.UniqueIdentificationNumber = allBCs[i].UniqueIdentificationNumber;
                                backc.BankReferenceNumber = allBCs[i].BankReferenceNumber;
                                backc.Qualification = allBCs[i].Qualification;
                                backc.OtherQualification = allBCs[i].OtherQualification;
                                backc.isAllocated = allBCs[i].isAllocated;
                                backc.CreatedOn = allBCs[i].CreatedOn;
                                backc.CreatedBy = allBCs[i].CreatedBy;
                                backc.UpdatedOn = allBCs[i].UpdatedOn;
                                backc.UpdatedBy = allBCs[i].UpdatedBy;
                                backc.IsActive = allBCs[i].IsActive;
                                backc.CurrentStatusSince = allBCs[i].CurrentStatusSince;
                                backc.Status = allBCs[i].Status;
                                backc.CorporateId = allBCs[i].CorporateId;
                                backc.AppointmentDate = allBCs[i].AppointmentDate;
                                backc.AllocationIFSCCode = allBCs[i].AllocationIFSCCode;
                                backc.AllocationBankId = allBCs[i].AllocationBankId;
                                backc.AllocationBranchId = allBCs[i].AllocationBranchId;
                                backc.BCType = allBCs[i].BCType;
                                backc.WorkingDays = allBCs[i].WorkingDays;
                                backc.WorkingHours = allBCs[i].WorkingHours;
                                backc.PLPostalAddress = allBCs[i].PLPostalAddress;
                                backc.PLVillageCode = allBCs[i].PLVillageCode;
                                backc.PLVillageDetail = allBCs[i].PLVillageDetail;
                                backc.PLTaluk = allBCs[i].PLTaluk;
                                backc.PLDistrictId = allBCs[i].PLDistrictId;
                                backc.PLStateId = allBCs[i].PLStateId;
                                backc.PLPinCode = allBCs[i].PLPinCode;
                                backc.MinimumCashHandlingLimit = allBCs[i].MinimumCashHandlingLimit;
                                backc.MonthlyFixedRenumeration = allBCs[i].MonthlyFixedRenumeration;
                                backc.MonthlyVariableRenumeration = allBCs[i].MonthlyVariableRenumeration;
                                backc.IsBlackListed = allBCs[i].IsBlackListed;
                                backc.BlacklistDate = allBCs[i].BlacklistDate;
                                backc.BlackListReason = allBCs[i].BlackListReason;
                                backc.BlackListApprovalNumber = allBCs[i].BlackListApprovalNumber;
                                backc.StatusChangeReason = allBCs[i].StatusChangeReason;
                                backc.productId = allBCs[i].productId;
                                backc.Village = allBCs[i].Village;
                                backc.Latitude = allBCs[i].Latitude;
                                backc.Longitude = allBCs[i].Longitude;
                                backc.dateOfBirth = allBCs[i].dateOfBirth;
                                backc.Appointment_Date = allBCs[i].Appointment_Date;
                                backc.ContactPerson = allBCs[i].ContactPerson;
                                backc.ContactDesignation = allBCs[i].ContactDesignation;
                                backc.NoofComplaint = allBCs[i].NoofComplaint;    
                                InsertUpdateBCforExcel(backc, userID, mode);

                            }

                            BCCertifications cr = new BCCertifications();
                            if (Certification != null)
                            {
                                for (int i = 0; i < Certification.Count; i++)
                                {

                                    cr.DateOfPassing = Certification[i].DateOfPassing;
                                    cr.DateOf_Passing = Certification[i].DateOf_Passing;
                                    cr.InstituteName = Certification[i].InstituteName;
                                    cr.CourseName = Certification[i].CourseName;
                                    cr.Grade = Certification[i].Grade;
                                    cr.AadharCard = Certification[i].AadharCard;
                                    InsertUpdateBCforExcelCertification(cr, userID, mode);

                                }
                            }

                            CommssionList cl = new CommssionList();
                            if (Commssion != null)
                            {
                                for (int i = 0; i < Commssion.Count; i++)
                                {
                                    cl.BankRefNo = Commssion[i].BankRefNo;
                                    cl.Month = Commssion[i].Month;
                                    cl.Year = Commssion[i].Year;
                                    cl.Commission1 = Commssion[i].Commission1;
                                    cl.Commission2 = Commssion[i].Commission2;
                                    cl.MonthName = Commssion[i].MonthName;

                                    InsertUpdateBCforExcelRemuneration(cl, userID, mode);

                                }
                            }



                            PreviousExperiene exp = new PreviousExperiene();
                            if (experience != null)
                            {
                                for (int i = 0; i < experience.Count; i++)
                                {
                                    exp.BankId = experience[i].BankId;
                                    exp.Branchid = experience[i].Branchid;
                                    exp.FromDate = experience[i].FromDate;
                                    exp.ToDate = experience[i].ToDate;
                                    exp.From_Date = experience[i].From_Date;
                                    exp.To_Date = experience[i].To_Date;
                                    exp.Reason = experience[i].Reason;
                                    exp.BankName = experience[i].BankName;
                                    exp.BranchName = experience[i].BranchName;
                                    exp.AadharCard = experience[i].AadharCard;
                                    InsertUpdateBCforExcelPreviousExperiene(exp, userID, mode);
                                }
                            }

                            OperationalAreas ar = new OperationalAreas();
                            if (Areas != null)
                            {
                                for (int i = 0; i < Areas.Count; i++)
                                {
                                    ar.VillageCode = Areas[i].VillageCode;
                                    ar.VillageDetail = Areas[i].VillageDetail;
                                    ar.AadharCard = Areas[i].AadharCard;
                                    InsertUpdateBCforExcelOperationalAreas(ar, userID, mode);
                                }
                            }
                            BCDevices dcs = new BCDevices();
                            if (Devices != null)
                            {
                                for (int i = 0; i < Devices.Count; i++)
                                {

                                    dcs.GivenOn = Devices[i].GivenOn;
                                    dcs.Given_On = Devices[i].Given_On;
                                    dcs.Device = Devices[i].Device;
                                    dcs.AadharCard = Devices[i].AadharCard;
                                    dcs.DeviceCode = Devices[i].DeviceCode;

                                    InsertUpdateBCforExcelBCDevices(dcs, userID, mode);
                                }
                            }

                            ConnectivityDetails cds = new ConnectivityDetails();
                            if (Connectivity != null)
                            {
                                for (int i = 0; i < Connectivity.Count; i++)
                                {

                                    cds.ConnectivityMode = Connectivity[i].ConnectivityMode;
                                    cds.ConnectivityProvider = Connectivity[i].ConnectivityProvider;
                                    cds.ContactNumber = Connectivity[i].ContactNumber;
                                    cds.AadharCard = Connectivity[i].AadharCard;
                                    InsertUpdateBCforExcelConnectivityDetails(cds, userID, mode);
                                }
                            }

                            if (SsaDetail != null)
                            {
                                SsaDetails ssds = new SsaDetails();
                                for (int i = 0; i < SsaDetail.Count; i++)
                                {

                                    ssds.Ssa = SsaDetail[i].Ssa;
                                    ssds.State = SsaDetail[i].State;
                                    ssds.District = SsaDetail[i].District;
                                    ssds.SubDistrict = SsaDetail[i].SubDistrict;
                                    ssds.AadharCard = SsaDetail[i].AadharCard;
                                    ssds.Village = SsaDetail[i].Village;
                                    InsertUpdateBCforExcelSSADetails(ssds, userID, mode);
                                }
                            }

                            businessLogic.fileStatusChange(userID.ToString(), 2);

                            // return errorMsg;
                        }
                        else
                        {
                            businessLogic.fileStatusChange(userID.ToString(), 3);
                        }
                    }
                }

                else
                {

                    businessLogic.DeleteError(userID.ToString());
                    businessLogic.fileStatus(userID.ToString(), 0, excelName);
                    businessLogic.fileStatusChange(userID.ToString(), 3);
                    businessLogic.SaveError(userID.ToString(), "File should be .xls, .xlsx format.");
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message.ToString();
                UserBL businessLogic = new UserBL();
                businessLogic.DeleteError(userID.ToString());
                businessLogic.fileStatus(userID.ToString(), 0, excelName);
                businessLogic.fileStatusChange(userID.ToString(), 3);
                businessLogic.SaveError(userID.ToString(), str);
            }
        

        }

        public List<string> ReadExcel(string fileName, int userID, string mode, string BankId)
        {



            bool flag = true;
            int rowNum = 0;
            UserBL businessLogic = new UserBL();
            List<string> errorMsgtemp = new List<string>();
            businessLogic.DeleteError(userID.ToString());
            businessLogic.fileStatus(userID.ToString(), 0, "");
            BankCorrespondence backc = new BankCorrespondence();
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve a reference to a container.
            CloudBlobContainer container = blobClient.GetContainerReference("dev-raw");
            // Create the container if it doesn't already exist.

            // Retrieve reference to a blob named "myblob".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
            MemoryStream blobms = new MemoryStream();
            blockBlob.DownloadToStream(blobms);

            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(blobms, true))
            {

                WorkbookPart workbookpart = doc.WorkbookPart;
                IEnumerable<Sheet> first = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "BCs");
                IEnumerable<Sheet> second = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "Certificates");
                IEnumerable<Sheet> third = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "Experiences");
                IEnumerable<Sheet> fourth = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "OpreationalAreas");
                IEnumerable<Sheet> fifth = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "Devices");
                IEnumerable<Sheet> sixth = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "ConnectivityDetails");
                IEnumerable<Sheet> sevan = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "PrimaryLocation");
                ////IEnumerable<Sheet> seven = doc.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == "Products");


                //Get the Worksheet instance.
                Worksheet ws1 = (doc.WorkbookPart.GetPartById(first.First().Id.Value) as WorksheetPart).Worksheet;
                Worksheet ws2 = (doc.WorkbookPart.GetPartById(second.First().Id.Value) as WorksheetPart).Worksheet;
                Worksheet ws3 = (doc.WorkbookPart.GetPartById(third.First().Id.Value) as WorksheetPart).Worksheet;
                Worksheet ws4 = (doc.WorkbookPart.GetPartById(fourth.First().Id.Value) as WorksheetPart).Worksheet;
                Worksheet ws5 = (doc.WorkbookPart.GetPartById(fifth.First().Id.Value) as WorksheetPart).Worksheet;
                Worksheet ws6 = (doc.WorkbookPart.GetPartById(sixth.First().Id.Value) as WorksheetPart).Worksheet;
                Worksheet ws7 = (doc.WorkbookPart.GetPartById(sevan.First().Id.Value) as WorksheetPart).Worksheet;

                ////Worksheet ws7 = (doc.WorkbookPart.GetPartById(seven.First().Id.Value) as WorksheetPart).Worksheet;


                //Fetch all the rows present in the Worksheet.
                IEnumerable<Row> r1 = ws1.GetFirstChild<SheetData>().Descendants<Row>();

                List<BankCorrespondence> allBCs = null;
                int l = 0;
                foreach (Row row in r1)
                {
                    l++;
                    if (row.RowIndex.Value == 1)
                        continue;
                    List<Cell> values = row.Descendants<Cell>().ToList();
                    List<string> errorMsg = new List<string>();
                    // var c = row.ElementAt(1);

                    Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_D = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "D" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_E = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "E" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_F = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "F" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_G = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "G" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_H = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "H" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_I = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "I" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_J = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "J" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_K = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "K" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_L = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "L" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_M = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "M" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_N = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "N" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_O = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "O" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_P = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "P" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_Q = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "Q" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_R = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "R" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_S = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "S" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_T = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "T" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_U = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "U" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_V = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "V" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_W = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "W" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_X = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "X" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_Y = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "Y" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_Z = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "Z" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AA = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AA" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AB = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AB" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AC = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AC" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AD = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AD" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AE = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AE" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AF = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AF" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AG = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AG" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AH = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AH" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AI = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AI" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AJ = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AJ" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AK = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AK" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AL = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AL" + l.ToString(), false) == 0).FirstOrDefault();
                    Cell CELL_AM = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "AM" + l.ToString(), false) == 0).FirstOrDefault();





                    BankCorrespondence bc = new BankCorrespondence();

                    bc.BankCorrespondId = 0;


                    string name = GetValue(doc, CELL_B);
                    if (name == null)
                    {
                        errorMsg.Add("303, in row  " + (rowNum + 1) + " User Name Required.");
                        flag = false;
                    }
                    else
                    {
                        bc.Name = GetValue(doc, CELL_B);
                    }

                    bc.ImagePath = "PATH";

                    string Gender = validation.getGendereValidation(GetValue(doc, CELL_C));
                    if (Gender == "false")
                    {
                        errorMsg.Add("304, in row  " + (rowNum + 1) + " Gender Required value should Male,Female,Other");
                        flag = false;
                    }
                    else
                    {
                        bc.Gender = GetValue(doc, CELL_C);
                    }


                    string DOB = validation.DateFormat(GetValue(doc, CELL_D));
                    if (DOB == "false")
                    {
                        errorMsg.Add("305, in row  " + (rowNum + 1) + " DOB Required format should be dd/mm/yyyy");
                        flag = false;
                    }
                    else
                    {
                        //  bc.DOB = DateTime.FromOADate(Utils.GetDoubleValue(DOB)); 
                        DateTime dateOfBirth = DateTime.ParseExact(string.IsNullOrEmpty(DOB) ? "01/01/1970" : DOB.Replace(".", "/"), "dd/MM/yyyy", null);
                        bc.DOB = dateOfBirth;
                    }
                    string FatherName = GetValue(doc, CELL_E);
                    if (FatherName == null)
                    {
                        errorMsg.Add("303, in row  " + (rowNum + 1) + " FatherName Required.");
                        flag = false;
                    }
                    else
                    {
                        bc.FatherName = GetValue(doc, CELL_E);
                    }
                    string Handicap = GetValue(doc, CELL_H);
                    if (Handicap == null)
                    {
                        errorMsg.Add("306, in row  " + (rowNum + 1) + " Handicap Required enter 0 or 1");
                        flag = false;
                    }
                    else
                    {
                        bc.Handicap = GetValue(doc, CELL_H);
                    }
                    string SpouseName = GetValue(doc, CELL_F);
                    if (SpouseName == null)
                    {
                        bc.SpouseName = "N/A";
                    }
                    else
                    {
                        bc.SpouseName = GetValue(doc, CELL_F);
                    }


                    string Category = validation.getCategory(GetValue(doc, CELL_G));
                    string CategoryValue;
                    if (Category == "false")
                    {
                        errorMsg.Add("303, in row  " + (rowNum + 1) + " Category Required Value Should be GENERAL,OBC,SC,ST");
                        flag = false;
                    }
                    else
                    {
                        CategoryValue = GetValue(doc, CELL_G);
                        bc.Category = GetValue(doc, CELL_G);
                    }

                    string phn_NO = GetValue(doc, CELL_I);
                    if (phn_NO == null)
                    {
                        errorMsg.Add("303, in row  " + (rowNum + 1) + " Invalid PhoneNumber1");
                        flag = false;
                    }
                    else
                    {
                        var PhoneNumber1 = validation.IsCorrectMobileNumber(GetValue(doc, CELL_I));
                        if (PhoneNumber1 == "0")
                        {
                            errorMsg.Add("303, in row  " + (rowNum + 1) + " Invalid PhoneNumber1");
                            flag = false;
                        }
                        else
                        {
                            bc.PhoneNumber1 = GetValue(doc, CELL_I);
                        }

                    }


                    string PhoneNumber2 = GetValue(doc, CELL_J);
                    if (PhoneNumber2 == null)
                    {
                        bc.PhoneNumber2 = "N/A";
                    }
                    else
                    {
                        bc.PhoneNumber2 = GetValue(doc, CELL_J);
                    }

                    string PhoneNumber3 = GetValue(doc, CELL_K);
                    if (PhoneNumber3 == null)
                    {
                        bc.PhoneNumber3 = "N/A";
                    }
                    else
                    {
                        bc.PhoneNumber3 = GetValue(doc, CELL_K);
                    }


                    string Email = GetValue(doc, CELL_L);
                    if (Email == null)
                    {

                        errorMsg.Add("301, in row  " + (rowNum + 1) + " Invalid Email Id.");
                        flag = false;
                    }
                    else
                    {
                        var email = validation.IsValidEmailId(GetValue(doc, CELL_L));
                        if (email == "false")
                        {
                            errorMsg.Add("301, in row  " + (rowNum + 1) + " Invalid Email Id.");
                            flag = false;

                        }
                        else
                        {
                            bc.Email = email;
                        }

                    }

                    string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                    if (AadharCard == "0")
                    {
                        errorMsg.Add("303, in row  " + (rowNum + 1) + " AadharCard should be valid.");
                        flag = false;
                    }
                    else
                    {

                        int valid = businessLogic.getAadhaar(MD5Hash(AadharCard));
                        if (valid == 1)
                        {
                            errorMsg.Add("303, in row  " + (rowNum + 1) + " Aadhaar card number already exist.");
                            flag = false;
                        }
                        else
                        {
                            bc.AadharCard = (MD5Hash(GetValue(doc, CELL_A)));
                        }




                    }


                    string PanCard = validation.IsCorrectPanCard(GetValue(doc, CELL_M));
                    if (PanCard == "0")
                    {
                        bc.PanCard = "N/A";
                    }
                    else
                    {
                        bc.PanCard = GetValue(doc, CELL_M);
                    }

                    bc.VoterCard = GetValue(doc, CELL_N);
                    bc.DriverLicense = GetValue(doc, CELL_O);
                    bc.NregaCard = GetValue(doc, CELL_P);
                    bc.RationCard = GetValue(doc, CELL_Q);



                    string Village = GetValue(doc, CELL_R);
                    if (Village == null)
                    {
                        errorMsg.Add("311, in row  " + (rowNum + 1) + " village code required");
                        flag = false;
                    }
                    else
                    {

                        string valid = businessLogic.getStateDistrictIds(Village.ToString());
                        if (valid == "")
                        {
                            errorMsg.Add("303, in row  " + (rowNum + 1) + " village code does not exist.");
                            flag = false;
                        }
                        else
                        {
                            string[] ldval = valid.Split(',');

                            bc.State = ldval[2].ToString();
                            bc.District = ldval[1].ToString();
                            bc.Subdistrict = ldval[0].ToString();
                            bc.Village = ldval[3].ToString();

                        }


                    }

                    bc.City = "0";
                    bc.Area = GetValue(doc, CELL_S);

                    string PinCode = GetValue(doc, CELL_T);
                    if (PinCode == null)
                    {
                        errorMsg.Add("303, in row  " + (rowNum + 1) + " PinCode  Required.");
                        flag = false;
                    }
                    else
                    {
                        bc.PinCode = GetValue(doc, CELL_T);
                    }

                    string AlternateOccupationType = validation.getAlternateOccupationType(GetValue(doc, CELL_U));
                    if (AlternateOccupationType == "false")
                    {
                        errorMsg.Add("303, in row  " + (rowNum + 1) + " Alternate Occupation Type Required Government,Public Sector,Private,Self Employed");
                        flag = false;
                    }
                    else
                    {
                        bc.AlternateOccupationType = GetValue(doc, CELL_U);
                    }


                    bc.AlternateOccupationDetail = GetValue(doc, CELL_V);

                    bc.BankReferenceNumber = GetValue(doc, CELL_W);

                    string Qualification = GetValue(doc, CELL_X);
                    if (Qualification == null)
                    {
                        errorMsg.Add("309, in row  " + (rowNum + 1) + " Qualification Required.");
                        flag = false;
                    }
                    else
                    {
                        bc.Qualification = GetValue(doc, CELL_X);
                    }

                    bc.OtherQualification = "OtherQualification";

                    int isAllocated = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_Y));
                    if (isAllocated == -1)
                    {
                        errorMsg.Add("303, in row  " + (rowNum + 1) + " Is allocated Required Enter 0 or 1");
                        flag = false;
                    }
                    else
                    {
                        bc.isAllocated = Convert.ToBoolean(Convert.ToInt16(GetValue(doc, CELL_Y)));


                    }

                    if (bc.isAllocated)
                    {


                        string CorporateId = GetValue(doc, CELL_AA);
                        if (PanCard == null)
                        {
                            bc.CorporateId = 1;
                        }
                        else
                        {
                            bc.CorporateId = Utils.GetIntValue(GetValue(doc, CELL_AA));
                        }



                    }
                    bc.Status = "active";
                    string AppointmentDate = validation.DateFormat(GetValue(doc, CELL_AB));
                    if (AppointmentDate == "false")
                    {
                        errorMsg.Add("305, in row  " + (rowNum + 1) + " BCActivityFrom Required format should be dd/mm/yyyy");
                        flag = false;
                    }
                    else
                    {

                        DateTime Appointment_Date = DateTime.ParseExact(string.IsNullOrEmpty(AppointmentDate) ? "01/01/1970" : AppointmentDate.Replace(".", "/"), "dd/MM/yyyy", null);
                        bc.AppointmentDate = Appointment_Date;
                        // bc.AppointmentDate = DateTime.FromOADate(Utils.GetDoubleValue(AppointmentDate));
                        bc.Appointment_Date = GetValue(doc, CELL_AB);
                    }


                    string AllocationIFSCCode = GetValue(doc, CELL_AC);
                    if (AllocationIFSCCode == null)
                    {
                        errorMsg.Add("303, in row  " + (rowNum + 1) + " Allocation IFSC Code Required.");
                        flag = false;
                    }
                    else
                    {
                        ////////////////////// Find bankid by IFSC Code

                        string valid = businessLogic.getBankId(AllocationIFSCCode.ToString(), BankId);
                        if (valid == "")
                        {
                            errorMsg.Add("303, in row  " + (rowNum + 1) + " IFSC Code does not exist.");
                            flag = false;
                        }
                        else
                        {
                            string[] ldval = valid.Split(',');
                            bc.AllocationBranchId = Convert.ToInt32(ldval[1]);
                            bc.AllocationBankId = Convert.ToInt32(ldval[0]);
                            bc.AllocationIFSCCode = GetValue(doc, CELL_AC);

                        }



                    }

                    string BCType = GetValue(doc, CELL_AD);
                    if (BCType == null)
                    {
                        bc.BCType = "Corporate";
                    }
                    else
                    {
                        bc.BCType = GetValue(doc, CELL_AD);
                    }



                    //string BCType = validation.getBCTypeValidation(GetValue(doc, CELL_AJ));
                    //if (BCType == "false")
                    //{
                    //    errorMsg.Add("311, in row  " + (rowNum + 1) + " Enter Individual,Corporate,Fixed,Mobile,Both");
                    //    flag = false;
                    //}
                    //else
                    //{
                    //    bc.BCType = GetValue(doc, CELL_AJ);
                    //}

                    int WorkingDays = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AE));
                    if (WorkingDays == -1)
                    {
                        errorMsg.Add("311, in row  " + (rowNum + 1) + " Working day should be number.");
                        flag = false;
                    }
                    else
                    {
                        bc.WorkingDays = Utils.GetIntValue(GetValue(doc, CELL_AE));
                    }

                    int WorkingHours = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AF));
                    if (WorkingHours == -1)
                    {
                        errorMsg.Add("311, in row  " + (rowNum + 1) + " Working hours should be number.");
                        flag = false;
                    }
                    else
                    {
                        bc.WorkingHours = Utils.GetIntValue(GetValue(doc, CELL_AF));
                    }


                    bc.PLPostalAddress = "0";
                    bc.PLVillageCode = "0";
                    bc.PLVillageDetail = "0";
                    bc.PLTaluk = "0";
                    bc.PLDistrictId = 0;
                    bc.PLStateId = 0;


                    bc.PLPinCode = "0";

                    int Productid = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AG));
                    if (Productid == -1)
                    {
                        //errorMsg.Add("311, in row  " + (rowNum + 1) + " Product Id should be number.");
                        //flag = false;
                        bc.productId = "0";
                    }
                    else
                    {
                        bc.productId = Utils.GetStringValue(GetValue(doc, CELL_AG));
                    }
                    bc.UniqueIdentificationNumber = Utils.GetStringValue(GetValue(doc, CELL_AH));
                    int MinimumCashHandlingLimit = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AI));
                    if (MinimumCashHandlingLimit == -1)
                    {

                        errorMsg.Add("312 in row  " + (rowNum + 1) + " Minimum CashHandling Limit  should be number.");
                        flag = false;
                    }
                    else
                    {
                        bc.MinimumCashHandlingLimit = Utils.GetDoubleValue(GetValue(doc, CELL_AI));
                    }
                    int MonthlyFixedRenumeration = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AJ));
                    if (MonthlyFixedRenumeration == -1)
                    {
                        bc.MonthlyFixedRenumeration = 0;
                        //errorMsg.Add("312 in row  " + (rowNum + 1) + " Commission 1  should be number.");
                        //flag = false;
                    }
                    else
                    {
                        bc.MonthlyFixedRenumeration = Utils.GetDoubleValue(GetValue(doc, CELL_AJ));
                    }
                    int MonthlyVariableRenumeration = validation.IsCorrectStringIsNumber(GetValue(doc, CELL_AK));
                    if (MonthlyVariableRenumeration == -1)
                    {
                        //errorMsg.Add("312 in row  " + (rowNum + 1) + " Commission 2  should be number.");
                        //flag = false;
                        bc.MonthlyVariableRenumeration = 0;
                    }
                    else
                    {
                        bc.MonthlyVariableRenumeration = Utils.GetDoubleValue(GetValue(doc, CELL_AK));
                    }
                    bc.Latitude = Utils.GetStringValue(GetValue(doc, CELL_AL));
                    bc.Longitude = Utils.GetStringValue(GetValue(doc, CELL_AM));





                    if (allBCs == null)
                        allBCs = new List<BankCorrespondence>();

                    //  InsertUpdateBCforExcel(bc, userID, mode);
                    allBCs.Add(bc);
                    rowNum++;
                    for (int i = 0; i < errorMsg.Count; i++)
                    {
                        var errors = errorMsg[i];
                        businessLogic.SaveError(userID.ToString(), errors);

                    }

                }
                if (allBCs == null)
                {

                    flag = false;
                    List<string> errorMsgs = new List<string>();
                    errorMsgs.Add("0 Entries found  in the  Uploaded file.");
                    for (int i = 0; i < errorMsgs.Count; i++)
                    {
                        var errors = errorMsgs[i];
                        businessLogic.SaveError(userID.ToString(), errors);

                    }

                }


                //Certifications
                IEnumerable<Row> r2 = ws2.GetFirstChild<SheetData>().Descendants<Row>();
                int k = 0;
                List<BCCertifications> Certification = null;
                int rowNum7 = 1;

                foreach (Row row1 in r2)
                {
                    k++;
                    if (row1.RowIndex.Value == 1)
                        continue;

                    List<Cell> values = row1.Descendants<Cell>().ToList();
                    List<string> certifierrorMsg = new List<string>();

                    Cell CELL_A = row1.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + k.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_B = row1.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + k.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_C = row1.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + k.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_D = row1.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "D" + k.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_E = row1.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "E" + k.ToString(), true) == 0).FirstOrDefault();
                    BCCertifications cr = new BCCertifications();


                    string bankRefNo = GetValue(doc, CELL_A);
                    if (bankRefNo == null)
                    {
                        certifierrorMsg.Add("303, in row  " + (rowNum7 + 1) + " of Certificates sheet Bank Reference Required.");
                     
                        flag = false;
                    }
                    else
                    {
                        cr.AadharCard = GetValue(doc, CELL_A);
                    }


                    //string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                    //if (AadharCard == "0")
                    //{
                    //    certifierrorMsg.Add("303, in row  " + (rowNum7 + 1) + " of Certificates sheet AadharCard should valid.");
                    //    flag = false;
                    //}
                    //else
                    //{
                    //    cr.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                    //}

                    string DateOfPassing = validation.DateFormat(GetValue(doc, CELL_B));
                    if (DateOfPassing == "false")
                    {
                        certifierrorMsg.Add("305, in row  " + (rowNum7 + 1) + " of Certificates sheet Date of Passing Required format should be dd/mm/yyyy");
                        flag = false;
                    }
                    else
                    {

                        DateTime Date_Of_Passing = DateTime.ParseExact(string.IsNullOrEmpty(DateOfPassing) ? "01/01/1970" : DateOfPassing.Replace(".", "/"), "dd/MM/yyyy", null);
                        cr.DateOfPassing = Date_Of_Passing;
                        // cr.DateOfPassing = DateTime.FromOADate(Utils.GetDoubleValue(DateOfPassing)); 
                    }


                    string InstituteName = GetValue(doc, CELL_C);
                    if (InstituteName == null)
                    {
                        certifierrorMsg.Add("303, in row  " + (rowNum7 + 1) + "  of Certificates sheet Institute Name Required.");
                        flag = false;
                    }
                    else
                    {
                        cr.InstituteName = Utils.GetStringValue(GetValue(doc, CELL_C));

                    }

                    string CourseName = GetValue(doc, CELL_D);
                    if (CourseName == null)
                    {
                        certifierrorMsg.Add("303, in row  " + (rowNum7 + 1) + " of Certificates sheet Course Name Required.");
                        flag = false;
                    }
                    else
                    {
                        cr.CourseName = Utils.GetStringValue(GetValue(doc, CELL_D));

                    }



                    string Grade = GetValue(doc, CELL_E);
                    if (Grade == null)
                    {
                        cr.Grade = "N/A";

                    }
                    else
                    {
                        cr.Grade = Utils.GetStringValue(GetValue(doc, CELL_E));
                    }



                    if (Certification == null)
                    {
                        Certification = new List<BCCertifications>();
                    }
                    rowNum7++;
                    Certification.Add(cr);
                    for (int i = 0; i < certifierrorMsg.Count; i++)
                    {
                        var errors = certifierrorMsg[i];
                        businessLogic.SaveError(userID.ToString(), errors);

                    }
                }

                if (Certification != null)
                {
                    //  bc.Certifications = Certification;
                }


                //Experience
                IEnumerable<Row> r3 = ws3.GetFirstChild<SheetData>().Descendants<Row>();

                List<PreviousExperiene> experience = null;
                int rowNum1 = 1;
                int n = 0;
                foreach (Row row in r3)
                {
                    n++;
                    if (row.RowIndex.Value == 1)
                        continue;

                    List<Cell> values = row.Descendants<Cell>().ToList();
                    List<string> experrorMsg = new List<string>();
                    Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + n.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + n.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + n.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_D = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "D" + n.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_E = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "E" + n.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_F = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "F" + n.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_G = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "G" + n.ToString(), true) == 0).FirstOrDefault();

                    PreviousExperiene pr = new PreviousExperiene();

                    //   pr.ExperienceId = Utils.GetIntValue(GetValue(doc, values[0]));
                    string AllocationIFSCCode = GetValue(doc, CELL_B);
                    if (AllocationIFSCCode == null)
                    {
                        experrorMsg.Add("303, in row  " + (rowNum + 1) + "  of Experiences sheet  Branch IFSC Code Required.");
                        flag = false;
                    }
                    else
                    {
                        ////////////////////// Find bankid by IFSC Code
                        if (AllocationIFSCCode == "0")
                        {
                            pr.BankName = GetValue(doc, CELL_C);
                            pr.BranchName = GetValue(doc, CELL_D);
                            pr.BankId = 0;
                        }
                        else
                        {
                            string valid = businessLogic.getBankId(AllocationIFSCCode.ToString(), BankId);
                            if (valid == "")
                            {
                                experrorMsg.Add("303, in row  " + (rowNum + 1) + " of Experiences sheet Branch IFSC Code does not belong to any bank.");
                                flag = false;
                            }
                            else
                            {
                                string[] ldval = valid.Split(',');
                                pr.Branchid = Convert.ToInt32(ldval[1]);
                                pr.BankId = Convert.ToInt32(ldval[0]);
                                pr.BankName = "N/A";
                                pr.BranchName = "N/A";

                            }
                        }




                    }





                    string FromDate = validation.DateFormat(GetValue(doc, CELL_E));
                    if (FromDate == "false")
                    {
                        experrorMsg.Add("305, in row  " + (rowNum1 + 1) + " of Experiences sheet From Date Required format should be dd/mm/yyyy");
                        flag = false;
                    }
                    else
                    {

                        DateTime From_Date = DateTime.ParseExact(string.IsNullOrEmpty(FromDate) ? "01/01/1970" : FromDate.Replace(".", "/"), "dd/MM/yyyy", null);
                        pr.FromDate = From_Date;
                        //  pr.FromDate = DateTime.FromOADate(Utils.GetDoubleValue(FromDate)); 
                    }

                    string ToDate = validation.DateFormat(GetValue(doc, CELL_F));
                    if (ToDate == "false")
                    {
                        experrorMsg.Add("305, in row  " + (rowNum1 + 1) + " of Experiences sheet From Date Required format should be dd/mm/yyyy");
                        flag = false;
                    }
                    else
                    {

                        DateTime To_Date = DateTime.ParseExact(string.IsNullOrEmpty(FromDate) ? "01/01/1970" : FromDate.Replace(".", "/"), "dd/MM/yyyy", null);
                        pr.ToDate = To_Date;
                        //  pr.ToDate = DateTime.FromOADate(Utils.GetDoubleValue(ToDate)); 
                    }

                    string bankRefNo = GetValue(doc, CELL_A);
                    if (bankRefNo == null)
                    {
                        experrorMsg.Add("303, in row  " + (rowNum7 + 1) + " of Experiences sheet Bank Reference Required.");                     

                        flag = false;
                    }
                    else
                    {
                        pr.AadharCard = GetValue(doc, CELL_A);
                    }

                    //string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                    //if (AadharCard == "0")
                    //{
                    //    experrorMsg.Add("303, in row  " + (rowNum7 + 1) + " of Experiences sheet AadharCard should valid.");
                    //    flag = false;
                    //}
                    //else
                    //{
                    //    pr.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                    //}


                    string Reason = GetValue(doc, CELL_G);
                    if (Reason == null)
                    {
                        pr.Reason = "N/A";

                    }
                    else
                    {
                        pr.Reason = Utils.GetStringValue(GetValue(doc, CELL_G));
                    }


                    if (experience == null)
                    {
                        experience = new List<PreviousExperiene>();
                    }
                    experience.Add(pr);
                    rowNum1++;
                    for (int i = 0; i < experrorMsg.Count; i++)
                    {
                        var errors = experrorMsg[i];
                        businessLogic.SaveError(userID.ToString(), errors);

                    }

                }
                if (experience != null)
                {
                    // bc.PreviousExperience = experience;
                }


                //Opreational Areas
                IEnumerable<Row> r4 = ws4.GetFirstChild<SheetData>().Descendants<Row>();

                List<OperationalAreas> Areas = null;
                int arowNum8 = 1;
                int m = 0;
                foreach (Row row in r4)
                {
                    m++;
                    if (row.RowIndex.Value == 1)
                        continue;

                    List<Cell> values = row.Descendants<Cell>().ToList();
                    List<string> AreaserrorMsg = new List<string>();
                    Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + m.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + m.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + m.ToString(), true) == 0).FirstOrDefault();

                    OperationalAreas oa = new OperationalAreas();


                    string Village = GetValue(doc, CELL_B);
                    if (Village == null)
                    {
                        AreaserrorMsg.Add("311, in row  " + (arowNum8 + 1) + "  of OpreationalAreas sheet village code required should be number.");
                        flag = false;
                    }
                    else
                    {

                        string valid = businessLogic.getStateDistrictIds(Village.ToString());
                        if (valid == "")
                        {
                            AreaserrorMsg.Add("303, in row  " + (arowNum8 + 1) + "  of OpreationalAreas sheet village code does not exist.");
                            flag = false;
                        }
                        else
                        {
                            string[] ldval = valid.Split(',');
                            oa.VillageCode = Utils.GetStringValue(GetValue(doc, CELL_B));
                        }
                    }

                    string VillageDetail = GetValue(doc, CELL_C);
                    if (VillageDetail == null)
                    {
                        oa.VillageDetail = "N/A";

                    }
                    else
                    {
                        oa.VillageDetail = Utils.GetStringValue(GetValue(doc, CELL_C));
                    }

                    string bankRefNo = GetValue(doc, CELL_A);
                    if (bankRefNo == null)
                    {
                        AreaserrorMsg.Add("303, in row  " + (arowNum8 + 1) + " of OpreationalAreas sheet Bank Reference Required.");
                        flag = false;
                    }
                    else
                    {
                        oa.AadharCard = GetValue(doc, CELL_A);
                    }

                    //string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                    //if (AadharCard == "0")
                    //{
                    //    AreaserrorMsg.Add("303, in row  " + (arowNum8 + 1) + " of OpreationalAreas sheet AadharCard should valid.");
                    //    flag = false;
                    //}
                    //else
                    //{
                    //    oa.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                    //}
                    if (Areas == null)
                    {
                        Areas = new List<OperationalAreas>();
                    }
                    Areas.Add(oa);
                    for (int i = 0; i < AreaserrorMsg.Count; i++)
                    {
                        var errors = AreaserrorMsg[i];
                        businessLogic.SaveError(userID.ToString(), errors);

                    }


                }
                if (Areas != null)
                {
                    //  bc.OperationalAreas = Areas;
                }

                //Devices
                IEnumerable<Row> r5 = ws5.GetFirstChild<SheetData>().Descendants<Row>();

                List<BCDevices> Devices = null;
                int rowNum2 = 1;
                int s1 = 0;
                foreach (Row row in r5)
                {
                    s1++;
                    if (row.RowIndex.Value == 1)
                        continue;

                    List<Cell> values = row.Descendants<Cell>().ToList();
                    List<string> DeviceserrorMsg = new List<string>();
                    Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + s1.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + s1.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + s1.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_D = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "D" + s1.ToString(), true) == 0).FirstOrDefault();


                    BCDevices dc = new BCDevices();

                    //  dc.DeviceId = Utils.GetIntValue(GetValue(doc, values[0]));                  

                    string GivenOn = validation.DateFormat(GetValue(doc, CELL_B));
                    if (GivenOn == "false")
                    {
                        DeviceserrorMsg.Add("305, in row  " + (rowNum2 + 1) + " of Devices sheet Give On Date Required format should be dd/mm/yyyy");
                        flag = false;
                    }
                    else
                    {

                        DateTime Given_On = DateTime.ParseExact(string.IsNullOrEmpty(GivenOn) ? "01/01/1970" : GivenOn.Replace(".", "/"), "dd/MM/yyyy", null);
                        dc.GivenOn = Given_On;
                        //  dc.GivenOn = DateTime.FromOADate(Utils.GetDoubleValue(GivenOn)); 
                    }
                    string Device = GetValue(doc, CELL_C);
                    if (Device == null)
                    {
                        DeviceserrorMsg.Add("303, in row  " + (rowNum2 + 1) + " of Devices sheet Device Name Required.");
                        flag = false;
                    }
                    else
                    {
                        dc.Device = Utils.GetStringValue(GetValue(doc, CELL_C));
                    }

                    string DeviceCode = GetValue(doc, CELL_D);
                    if (DeviceCode == null)
                    {
                        DeviceserrorMsg.Add("303, in row  " + (rowNum2 + 1) + " of Devices sheet Device Code Name Required.");
                        flag = false;
                    }
                    else
                    {
                        dc.DeviceCode = Utils.GetStringValue(GetValue(doc, CELL_D));
                    }


                    string bankRefNo = GetValue(doc, CELL_A);
                    if (bankRefNo == null)
                    {
                        DeviceserrorMsg.Add("303, in row  " + (rowNum2 + 1) + " of Devices sheet Bank Reference Required.");                      
                        flag = false;
                    }
                    else
                    {
                        dc.AadharCard = GetValue(doc, CELL_A);
                    }

                    //string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                    //if (AadharCard == "0")
                    //{
                    //    DeviceserrorMsg.Add("303, in row  " + (rowNum2 + 1) + " of Devices sheet AadharCard should valid.");
                    //    flag = false;
                    //}
                    //else
                    //{
                    //    dc.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                    //}

                    if (Devices == null)
                    {
                        Devices = new List<BCDevices>();
                    }
                    Devices.Add(dc);
                    rowNum2++;
                    for (int i = 0; i < DeviceserrorMsg.Count; i++)
                    {
                        var errors = DeviceserrorMsg[i];
                        businessLogic.SaveError(userID.ToString(), errors);

                    }


                }

                if (Devices != null)
                {
                    // bc.Devices = Devices;
                }


                //SSA Details
                IEnumerable<Row> r7 = ws7.GetFirstChild<SheetData>().Descendants<Row>();

                List<SsaDetails> SsaDetail = null;
                int rowNum4 = 1;
                int ss1 = 0;

                foreach (Row row in r7)
                {
                    ss1++;
                    if (row.RowIndex.Value == 1)
                        continue;

                    List<Cell> values = row.Descendants<Cell>().ToList();
                    List<string> SSAeserrorMsg = new List<string>();
                    Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + ss1.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + ss1.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + ss1.ToString(), true) == 0).FirstOrDefault();


                    SsaDetails cd = new SsaDetails();

                    string bankRefNo = GetValue(doc, CELL_A);
                    if (bankRefNo == null)
                    {
                        SSAeserrorMsg.Add("303, in row  " + (rowNum4 + 1) + " of PrimaryLocation Bank Reference Required.");                       
                        flag = false;
                    }
                    else
                    {
                        cd.AadharCard = GetValue(doc, CELL_A);
                    }

                    //string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                    //if (AadharCard == "0")
                    //{
                    //    SSAeserrorMsg.Add("303, in row  " + (rowNum4 + 1) + " of PrimaryLocation sheet AadharCard should valid.");
                    //    flag = false;
                    //}
                    //else
                    //{

                    //    cd.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                    //}

                    string Ssa = GetValue(doc, CELL_B);
                    if (Ssa == null)
                    {
                        SSAeserrorMsg.Add("303, in row  " + (rowNum4 + 1) + " of PrimaryLocation sheet SSA Required.");
                        flag = false;
                    }
                    else
                    {

                        cd.Ssa = Utils.GetStringValue(GetValue(doc, CELL_B));
                    }


                    string Village = GetValue(doc, CELL_C);
                    if (Village == null)
                    {
                        SSAeserrorMsg.Add("303, in row  " + (rowNum4 + 1) + " of PrimaryLocation sheet Villages Required.");
                        flag = false;
                    }
                    else
                    {

                        string valid = businessLogic.getStateDistrictIds(Village.ToString());
                        if (valid == "")
                        {
                            SSAeserrorMsg.Add("303, in row  " + (rowNum + 1) + " PrimaryLocation not exists.");
                            flag = false;
                        }
                        else
                        {
                            string[] ldval = valid.Split(',');

                            cd.State = ldval[2].ToString();
                            cd.District = ldval[1].ToString();
                            cd.SubDistrict = ldval[0].ToString();
                            cd.Village = Village.ToString();
                        }
                    }

                    if (SsaDetail == null)
                    {
                        SsaDetail = new List<SsaDetails>();
                    }
                    SsaDetail.Add(cd);
                    for (int i = 0; i < SSAeserrorMsg.Count; i++)
                    {
                        var errors = SSAeserrorMsg[i];
                        businessLogic.SaveError(userID.ToString(), errors);

                    }


                }
                if (SsaDetail != null)
                {
                    // bc.ConnectivityDetails = Connectivity;
                }
                IEnumerable<Row> r6 = ws6.GetFirstChild<SheetData>().Descendants<Row>();

                List<ConnectivityDetails> Connectivity = null;
                int rowNum3 = 1;
                int sss1 = 0;
                foreach (Row row in r6)
                {
                    sss1++;
                    if (row.RowIndex.Value == 1)
                        continue;

                    List<Cell> values = row.Descendants<Cell>().ToList();
                    List<string> ConnectivityDetailseserrorMsg = new List<string>();
                    Cell CELL_A = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "A" + sss1.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_B = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "B" + sss1.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_C = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "C" + sss1.ToString(), true) == 0).FirstOrDefault();
                    Cell CELL_D = row.Descendants<Cell>().Where(c => string.Compare(c.CellReference.Value, "D" + sss1.ToString(), true) == 0).FirstOrDefault();

                    ConnectivityDetails cd = new ConnectivityDetails();


                    string ConnectivityMode = GetValue(doc, CELL_B);
                    if (ConnectivityMode == null)
                    {
                        ConnectivityDetailseserrorMsg.Add("303, in row  " + (rowNum3 + 1) + " of ConnectivityDetails sheet  Connectivity Type Required.");
                        flag = false;
                    }
                    else
                    {
                        cd.ConnectivityMode = Utils.GetStringValue(GetValue(doc, CELL_B));
                    }

                    string ConnectivityProvider = GetValue(doc, CELL_C);
                    if (ConnectivityProvider == null)
                    {
                        ConnectivityDetailseserrorMsg.Add("303, in row  " + (rowNum3 + 1) + " of ConnectivityDetails sheet  Provider Required.");
                        flag = false;
                    }
                    else
                    {
                        cd.ConnectivityProvider = Utils.GetStringValue(GetValue(doc, CELL_C));
                    }

                    var ContactNumber = validation.IsCorrectMobileNumber(GetValue(doc, CELL_D));
                    if (ContactNumber == "0")
                    {
                        ConnectivityDetailseserrorMsg.Add("303, in row  " + (rowNum3 + 1) + " of ConnectivityDetails sheet Invalid Number should be 10 digit. ");
                        flag = false;
                    }
                    else
                    {
                        cd.ContactNumber = Utils.GetStringValue(GetValue(doc, CELL_D));
                    }
                    string AadharCard = validation.IsCorrectAdhaar(GetValue(doc, CELL_A));
                    if (AadharCard == "0")
                    {
                        ConnectivityDetailseserrorMsg.Add("303, in row  " + (rowNum4 + 1) + " of ConnectivityDetails sheet AadharCard should valid.");
                        flag = false;
                    }
                    else
                    {
                        cd.AadharCard = Utils.GetStringValue(MD5Hash(GetValue(doc, CELL_A)));
                    }
                    if (Connectivity == null)
                    {
                        Connectivity = new List<ConnectivityDetails>();
                    }
                    Connectivity.Add(cd);
                    for (int i = 0; i < ConnectivityDetailseserrorMsg.Count; i++)
                    {
                        var errors = ConnectivityDetailseserrorMsg[i];
                        businessLogic.SaveError(userID.ToString(), errors);

                    }

                }
                if (Connectivity != null)
                {
                    // bc.ConnectivityDetails = Connectivity;
                }


                if (flag == true)
                {
                    businessLogic.fileStatusChange(userID.ToString(), 1);
                    for (int i = 0; i < allBCs.Count; i++)
                    {

                        backc.BankCorrespondId = allBCs[i].BankCorrespondId;
                        backc.Name = allBCs[i].Name;
                        backc.ImagePath = allBCs[i].ImagePath;
                        backc.Gender = allBCs[i].Gender;
                        backc.DOB = allBCs[i].DOB;
                        backc.FatherName = allBCs[i].FatherName;
                        backc.Handicap = allBCs[i].Handicap;
                        backc.SpouseName = allBCs[i].SpouseName;
                        backc.Category = allBCs[i].Category;
                        backc.PhoneNumber1 = allBCs[i].PhoneNumber1;
                        backc.PhoneNumber2 = allBCs[i].PhoneNumber2;
                        backc.PhoneNumber3 = allBCs[i].PhoneNumber3;
                        backc.Email = allBCs[i].Email;
                        backc.AadharCard = allBCs[i].AadharCard;
                        backc.PanCard = allBCs[i].PanCard;
                        backc.VoterCard = allBCs[i].VoterCard;
                        backc.DriverLicense = allBCs[i].DriverLicense;
                        backc.NregaCard = allBCs[i].NregaCard;
                        backc.RationCard = allBCs[i].RationCard;
                        backc.State = allBCs[i].State;
                        backc.City = allBCs[i].City;
                        backc.District = allBCs[i].District;
                        backc.Subdistrict = allBCs[i].Subdistrict;
                        backc.Area = allBCs[i].Area;
                        backc.PinCode = allBCs[i].PinCode;
                        backc.AlternateOccupationType = allBCs[i].AlternateOccupationType;
                        backc.AlternateOccupationDetail = allBCs[i].AlternateOccupationDetail;
                        backc.UniqueIdentificationNumber = allBCs[i].UniqueIdentificationNumber;
                        backc.BankReferenceNumber = allBCs[i].BankReferenceNumber;
                        backc.Qualification = allBCs[i].Qualification;
                        backc.OtherQualification = allBCs[i].OtherQualification;
                        backc.isAllocated = allBCs[i].isAllocated;
                        backc.CreatedOn = allBCs[i].CreatedOn;
                        backc.CreatedBy = allBCs[i].CreatedBy;
                        backc.UpdatedOn = allBCs[i].UpdatedOn;
                        backc.UpdatedBy = allBCs[i].UpdatedBy;
                        backc.IsActive = allBCs[i].IsActive;
                        backc.CurrentStatusSince = allBCs[i].CurrentStatusSince;
                        backc.Status = allBCs[i].Status;
                        backc.CorporateId = allBCs[i].CorporateId;
                        backc.AppointmentDate = allBCs[i].AppointmentDate;
                        backc.AllocationIFSCCode = allBCs[i].AllocationIFSCCode;
                        backc.AllocationBankId = allBCs[i].AllocationBankId;
                        backc.AllocationBranchId = allBCs[i].AllocationBranchId;
                        backc.BCType = allBCs[i].BCType;
                        backc.WorkingDays = allBCs[i].WorkingDays;
                        backc.WorkingHours = allBCs[i].WorkingHours;
                        backc.PLPostalAddress = allBCs[i].PLPostalAddress;
                        backc.PLVillageCode = allBCs[i].PLVillageCode;
                        backc.PLVillageDetail = allBCs[i].PLVillageDetail;
                        backc.PLTaluk = allBCs[i].PLTaluk;
                        backc.PLDistrictId = allBCs[i].PLDistrictId;
                        backc.PLStateId = allBCs[i].PLStateId;
                        backc.PLPinCode = allBCs[i].PLPinCode;
                        backc.MinimumCashHandlingLimit = allBCs[i].MinimumCashHandlingLimit;
                        backc.MonthlyFixedRenumeration = allBCs[i].MonthlyFixedRenumeration;
                        backc.MonthlyVariableRenumeration = allBCs[i].MonthlyVariableRenumeration;
                        backc.IsBlackListed = allBCs[i].IsBlackListed;
                        backc.BlacklistDate = allBCs[i].BlacklistDate;
                        backc.BlackListReason = allBCs[i].BlackListReason;
                        backc.BlackListApprovalNumber = allBCs[i].BlackListApprovalNumber;
                        backc.StatusChangeReason = allBCs[i].StatusChangeReason;
                        backc.productId = allBCs[i].productId;
                        backc.Village = allBCs[i].Village;
                        backc.Latitude = allBCs[i].Latitude;
                        backc.Longitude = allBCs[i].Longitude;
                        InsertUpdateBCforExcel(backc, userID, mode);

                    }

                    BCCertifications cr = new BCCertifications();
                    if (Certification != null)
                    {
                        for (int i = 0; i < Certification.Count; i++)
                        {

                            cr.DateOfPassing = Certification[i].DateOfPassing;
                            cr.InstituteName = Certification[i].InstituteName;
                            cr.CourseName = Certification[i].CourseName;
                            cr.Grade = Certification[i].Grade;
                            cr.AadharCard = Certification[i].AadharCard;
                            InsertUpdateBCforExcelCertification(cr, userID, mode);

                        }
                    }


                    PreviousExperiene exp = new PreviousExperiene();
                    if (experience != null)
                    {
                        for (int i = 0; i < experience.Count; i++)
                        {
                            exp.BankId = experience[i].BankId;
                            exp.Branchid = experience[i].Branchid;
                            exp.FromDate = experience[i].FromDate;
                            exp.ToDate = experience[i].ToDate;
                            exp.Reason = experience[i].Reason;
                            exp.BankName = experience[i].BankName;
                            exp.BranchName = experience[i].BranchName;
                            exp.AadharCard = experience[i].AadharCard;
                            InsertUpdateBCforExcelPreviousExperiene(exp, userID, mode);
                        }
                    }

                    OperationalAreas ar = new OperationalAreas();
                    if (Areas != null)
                    {
                        for (int i = 0; i < Areas.Count; i++)
                        {
                            ar.VillageCode = Areas[i].VillageCode;
                            ar.VillageDetail = Areas[i].VillageDetail;
                            ar.AadharCard = Areas[i].AadharCard;
                            InsertUpdateBCforExcelOperationalAreas(ar, userID, mode);
                        }
                    }
                    BCDevices dcs = new BCDevices();
                    if (Devices != null)
                    {
                        for (int i = 0; i < Devices.Count; i++)
                        {

                            dcs.GivenOn = Devices[i].GivenOn;
                            dcs.Device = Devices[i].Device;
                            dcs.AadharCard = Devices[i].AadharCard;
                            dcs.DeviceCode = Devices[i].DeviceCode;

                            InsertUpdateBCforExcelBCDevices(dcs, userID, mode);
                        }
                    }

                    ConnectivityDetails cds = new ConnectivityDetails();
                    if (Connectivity != null)
                    {
                        for (int i = 0; i < Connectivity.Count; i++)
                        {

                            cds.ConnectivityMode = Connectivity[i].ConnectivityMode;
                            cds.ConnectivityProvider = Connectivity[i].ConnectivityProvider;
                            cds.ContactNumber = Connectivity[i].ContactNumber;
                            cds.AadharCard = Connectivity[i].AadharCard;
                            InsertUpdateBCforExcelConnectivityDetails(cds, userID, mode);
                        }
                    }

                    if (SsaDetail != null)
                    {
                        SsaDetails ssds = new SsaDetails();
                        for (int i = 0; i < SsaDetail.Count; i++)
                        {

                            ssds.Ssa = SsaDetail[i].Ssa;
                            ssds.State = SsaDetail[i].State;
                            ssds.District = SsaDetail[i].District;
                            ssds.SubDistrict = SsaDetail[i].SubDistrict;
                            ssds.AadharCard = SsaDetail[i].AadharCard;
                            ssds.Village = SsaDetail[i].Village;
                            InsertUpdateBCforExcelSSADetails(ssds, userID, mode);
                        }
                    }

                    businessLogic.fileStatusChange(userID.ToString(), 2);

                    return errorMsgtemp;
                }
                else
                {
                    businessLogic.fileStatusChange(userID.ToString(), 3);
                    return errorMsgtemp;
                }
            }





        }

        
     

        public void ExportExcel(string filepath)
        {
            BankCorrespondence bc = new BankCorrespondence();

            // Create a spreadsheet document by supplying the filepath.
            SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(filepath, SpreadsheetDocumentType.Workbook);

            // Add a WorkbookPart to the document.
            WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
            workbookpart.Workbook = new Workbook();

            // Add a WorksheetPart to the WorkbookPart.
            //ws = WorksheetPart
            WorksheetPart ws1 = workbookpart.AddNewPart<WorksheetPart>();
            ws1.Worksheet = new Worksheet(new SheetData());

            Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());
            // Append a new worksheet and associate it with the workbook.
            Sheet BCs = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(ws1), SheetId = 1, Name = "BCs" };

            WorksheetPart ws2 = workbookpart.AddNewPart<WorksheetPart>();
            ws2.Worksheet = new Worksheet(new SheetData());

            Sheet Certificates = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(ws2), SheetId = 2, Name = "Certificates" };

            WorksheetPart ws3 = workbookpart.AddNewPart<WorksheetPart>();
            ws3.Worksheet = new Worksheet(new SheetData());

            Sheet Experiences = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(ws3), SheetId = 3, Name = "Experiences" };

            WorksheetPart ws4 = workbookpart.AddNewPart<WorksheetPart>();
            ws4.Worksheet = new Worksheet(new SheetData());

            Sheet OpreationalAreas = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(ws4), SheetId = 4, Name = "OpreationalAreas" };

            WorksheetPart ws5 = workbookpart.AddNewPart<WorksheetPart>();
            ws5.Worksheet = new Worksheet(new SheetData());

            Sheet Devices = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(ws5), SheetId = 5, Name = "Devices" };

            WorksheetPart ws6 = workbookpart.AddNewPart<WorksheetPart>();
            ws6.Worksheet = new Worksheet(new SheetData());

            Sheet ConnectivityDetails = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(ws6), SheetId = 6, Name = "ConnectivityDetails" };

            WorksheetPart ws7 = workbookpart.AddNewPart<WorksheetPart>();
            ws7.Worksheet = new Worksheet(new SheetData());

            Sheet Products = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(ws7), SheetId = 7, Name = "Products" };

            //Appending Sheets

            foreach (Sheet i in sheets)
            {
                sheets.Append(i);
            }

            // Get the sheetData cell table.
            // SheetData = sd
            SheetData sd1 = ws1.Worksheet.GetFirstChild<SheetData>();
            SheetData sd2 = ws2.Worksheet.GetFirstChild<SheetData>();
            SheetData sd3 = ws3.Worksheet.GetFirstChild<SheetData>();
            SheetData sd4 = ws4.Worksheet.GetFirstChild<SheetData>();
            SheetData sd5 = ws5.Worksheet.GetFirstChild<SheetData>();
            SheetData sd6 = ws6.Worksheet.GetFirstChild<SheetData>();
            SheetData sd7 = ws7.Worksheet.GetFirstChild<SheetData>();


            Row row;
            row = new Row() { RowIndex = 1 };
            sd1.Append(row);

            spreadsheetDocument.Close();
        }
    }
}
