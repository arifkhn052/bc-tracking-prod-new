﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class OperationalAreas
    {
        public int AreaId { get; set; }
        public string VillageCode { get; set; }
        public string VillageDetail { get; set; }
        public string AadharCard { get; set; }
        public int BCId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
    }
}
