﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class Bank
    {



        public int BranchId { get; set; }

        public int BankId { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string IfscCode { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string BankReferenceNumber { get; set; }
        
        public string Email { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public List<Branch> Branches { get; set; }
        public List<Circle> BankCircles { get; set; }
    }

    public class Circle
    {
        public int CircleId { get; set; }
        public string CircleName { get; set; }
        public int BankId { get; set; }
        public List<Zone> CircleZones { get; set; }

    }
    public class BankReport
    {

        public string bcCount { get; set; }
        public string bankCount { get; set; }
        public string branchCount { get; set; }


    }
    public class FileStatus
    {
        public int userId { get; set; }
        public string fileStatus { get; set; }
        public string Error { get; set; }
        public string fileName { get; set; }
        

    }


    public class Zone
    {
        public int ZoneId { get; set; }
        public string ZoneName { get; set; }
        public int CircleId { get; set; }

        public string CircleName { get; set; }
        
        public List<Region> ZoneRegions { get; set; }

    }

    public class Region
    {
        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public string ZoneName { get; set; }
        public int ZoneId { get; set; }
        public List<Branch> RegionBranches { get; set; }

    }



}
