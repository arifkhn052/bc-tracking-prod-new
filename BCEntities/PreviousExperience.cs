﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BCEntities
{
    public class PreviousExperiene
    {
        public int ExperienceId { get; set; }
        public int BankId { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
         public string oBankName { get; set; }
         public string oBranchName { get; set; }
 
        public int Branchid { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Reason { get; set; }
        public string To_Date { get; set; }
        public string From_Date { get; set; }
        public string AadharCard { get; set; }
        public int BankCorrespondenceId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
    }
}
