﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class State
    {
        public int? StateId { get; set; }
        public string StateName { get; set; }
        public string StateCode { get; set; }        
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public List<District> Districts { get; set; }

     
        
    }

    public class BankStates
    {
        public int StateID { get; set; }
        public string StateName { get; set; }
        public int bankid { get; set; }


    }
    public class BankCity
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }


    }
    public class DistrictList
    {
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public int CityId { get; set; }
        

    }
    public class District
    {
        public List<DistrictList> DistrictList { get; set; }
        public int? DistrictId { get; set; }
        public int StateId { get; set; }
        public string DistrictName { get; set; }
        public string DistrictCode { get; set; }
        
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public List<City> Cities { get; set; }
        public List<Taluka> Talukas { get; set;}
        public List<Village> Villages { get; set; }
        public List<SubDistrict> SubDistrict { get; set; }

    }
    public class SubDistrict
    {
        public int? SubDistrictId { get; set; }
        public string SubDistrictName { get; set; }
        public string SubDistrictCode { get; set; }
        public int? DistrictId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }

        public List<Village> Villages { get; set; }
    }

    public class City
    {
        public int? CityId { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string CityName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
    }

    public class Taluka
    {
        public int? TalukaId { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string TalukaName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public List<Village> Villages { get; set; }
    }

    public class Village
    {
        public int? VillageId { get; set; }
        public int? SubDistrictId { get; set; }
        public string VillageName { get; set; }
        public string SubDistrictName { get; set; }
        public string VillageCode { get; set; }
        public int CreatedBy { get; set; }
      
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
    }
}
