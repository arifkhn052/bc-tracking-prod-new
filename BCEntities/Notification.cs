﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class Notification
    {
        public int NotificationId { get; set; }
        public string NotificationType { get; set; }
        public string NotificationText { get; set; }
        public string NotificationPriority { get; set; }
        public string NotificationDate { get; set; }


    }
}
