﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCEntities
{
    public class Branch
    {
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string IFSCCode { get; set; }

        public string RegionName { get; set; }
        public string MICRCode { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public byte IsActive { get; set; }
        public int BankId { get; set; }
        public Bank OwnerBank { get; set; }

        public int RegionId { get; set; }
       // public Banks OBank { get; set; }

        public int StateId { get; set; }
       // public State OwnerBank { get; set; }

        public int DistrictId { get; set; }
       // public Banks OwnerBank { get; set; }

        public int TalukaId { get; set; }
       // public Banks OwnerBank { get; set; }

        public int CityId { get; set; }
     //   public Banks OwnerBank { get; set; }

        public int VillageId { get; set; }
    //    public Banks OwnerBank { get; set; }

        public string PinCode { get; set; }
    

    }
}
