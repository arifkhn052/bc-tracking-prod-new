﻿$(document).ready(function () {
    
    GetCircle();
    GetNetwork();
    GetZone();
    GetRegion();
    GetBranch();
       

});


$("#DDLCircle").change(function () {
    $("#DDLNetwork").html('');
    $("#DDLZone").html('');
    $("#DDLRegion").html('');
    $("#DDLBranch").html('');
    GetNetwork();
    
});

$("#DDLNetwork").change(function () {
    $("#DDLZone").html('');
    $("#DDLRegion").html('');
    $("#DDLBranch").html('');
    GetZone();
    GetRegion();
    GetBranch();
});

$("#DDLZone").change(function () {
    $("#DDLRegion").html('');
    $("#DDLBranch").html('');
    GetRegion();
    GetBranch();
});

$("#DDLRegion").change(function () {
    $("#DDLBranch").html('');
    GetBranch();
});

$("#btnTransferUser").click(function () {
    
    var lgid = $("#txtEmpId").val();
    var branchid = $("#DDLBranch").val();
    if (validate() == true) {
        
        window.SBICommon.sendAjaxCall({
            url: 'UserTransfer.aspx/CheckLead',
            data: "{'lgId': '" + $("#txtEmpId").val() + "'}",
            requestType: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {
                if (data.d == "0")
                {
                    userTransferwithoutLead(lgid, branchid);
                   
                }
                    
                else if (data.d == "1") {
                    window.location.href = '/TransferUser.aspx?EmpId=' + lgid + '&BranchId=' + branchid ;
                }

            },

            showLoader: true
            , complete: function (xhr, data) {
                console.log(xhr.status);
                if (xhr.status == 401)
                    window.location.href = '/rmmain.aspx';
            }
        });
    }
    else {
        ShowDialog("Please enter all the values correctly.", 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }

});



function validate() {
    var circle = $("#DDLCircle").val();
    var network = $("#DDLNetwork").val();
    var zone = $("#DDLZone").val();
    var region = $("#DDLRegion").val();
    var branch = $("#DDLBranch").val();
    var empId = $('#txtEmpId').val();
    
    var result = true;

    if (circle == "-1") {
        $("#divCircle").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divCircle").removeClass("form-group has-error");
    }
    if (network == "-1") {
        $("#divNetwork").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divNetwork").removeClass("form-group has-error");
    }
    if (zone == "-1") {
        $("#divZone").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divZone").removeClass("form-group has-error");
    }
    if (region == "-1") {
        $("#divRegion").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divRegion").removeClass("form-group has-error");
    }
    if (branch == "-1") {
        $("#divBranch").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divBranch").removeClass("form-group has-error");
    }
    if (empId == "") {
        $('#divEmpId').addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divEmpId").removeClass("form-group has-error");
    }

    return result;
}



function GetCircle() {
    $.ajax({
        url: 'AddUser.aspx/GetCircleName',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLCircle').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetNetwork() {
    $.ajax({
        url: 'AddUser.aspx/GetNetwork',
        type: "POST",
        dataType: "json",
        data: "{'circle': '" + $("#DDLCircle").val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLNetwork').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetZone() {
    $.ajax({
        url: 'AddUser.aspx/GetZone',
        type: "POST",
        dataType: "json",
        data: "{'circle': '" + $("#DDLCircle").val() + "','network' : '" + $('#DDLNetwork').val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLZone').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetRegion() {
    $.ajax({
        url: 'AddUser.aspx/GetRegion',
        type: "POST",
        dataType: "json",
        data: "{'circle': '" + $("#DDLCircle").val() + "','network' : '" + $('#DDLNetwork').val() + "','zone' : '" + $('#DDLZone').val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLRegion').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetBranch() {
    $.ajax({
        url: 'AddUser.aspx/GetBranch',
        type: "POST",
        dataType: "json",
        data: "{'circle': '" + $("#DDLCircle").val() + "','network' : '" + $('#DDLNetwork').val() + "','zone' : '" + $('#DDLZone').val() + "','region' : '" + $('#DDLRegion').val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {
                    $('#DDLBranch').append('<option value="' + k + '">' + v + '</option>');
                });
            });
        }
    });
}

$("#btnFind").click(function () {
    var rsult = true;
    var lgid = $("#txtEmpId").val();
    if (lgid == "") {
        $('#divEmpId').addClass("form-group has-error");
        rsult = false;
    }
    else {
        $("#divEmpId").removeClass("form-group has-error");
    }
    if (rsult == true) {
        GetUserDetail(lgid);
    }

});

function GetUserDetail(EmpId) {
   
    $.ajax({
        url: 'UserTransfer.aspx/TransferUser',
        type: "POST",
        dataType: "json",
        data: "{'lgId': '" + EmpId + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            var user = data.d;
            if (user["msg"] == "0") {
                $("#showtable").hide();
                ShowDialog("User doesn't exists, please check LG Id of the user.", 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
            }
           else if (user["msg"] == "1") {
                $("#showtable").show();
                $("#DDLCircle").val(user["circle"]);
                $("#DDLNetwork").html('');
                $("#DDLZone").html('');
                $("#DDLRegion").html('');
                $("#DDLBranch").html('');
                GetNetwork();
                $("#DDLNetwork").val(user["network"]);
                GetZone();
                $("#DDLZone").val(user["zone"]);
                GetRegion();
                $("#DDLRegion").val(user["region"]);
                GetBranch();
                $("#DDLBranch").val(user["branch_id"].trim());
            }
        }
    });
}

function userTransferwithoutLead(EmpId,BranchId) {
    $.ajax({
        url: 'UserTransfer.aspx/UserTransferWithoutLead',
        type: "POST",
        dataType: "json",
        data: "{'lgId': '" + EmpId + "','branchId': '" + BranchId + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            var user = data.d;
            if (user == "1") {
                $("#showtable").hide();
                ShowDialog("User has been transfered succesfully", 100, 300, 'Message', 10, '<span class="icon-info"></span>', true);
            }
        }
    });
}
