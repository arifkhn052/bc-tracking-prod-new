﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TrackCustomerLead.aspx.cs" Inherits="SBI.Web.TrackCustomerLead" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery-ui.css" rel="stylesheet" />
   <style>
      .abc {
      margin: 0px;
      padding: 2px;
      background: rgb(251,251,251); /* Old browsers g*/
      /* IE9 SVG, needs conditional override of 'filter' to 'none' */
      background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZiZmJmYiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmMmYyZjIiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
      background: -moz-linear-gradient(top, rgba(251,251,251,1) 0%, rgba(242,242,242,1) 100%); /* FF3.6+ */
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(251,251,251,1)), color-stop(100%,rgba(242,242,242,1))); /* Chrome,Safari4+ */
      background: -webkit-linear-gradient(top, rgba(251,251,251,1) 0%,rgba(242,242,242,1) 100%); /* Chrome10+,Safari5.1+ */
      background: -o-linear-gradient(top, rgba(251,251,251,1) 0%,rgba(242,242,242,1) 100%); /* Opera 11.10+ */
      background: -ms-linear-gradient(top, rgba(251,251,251,1) 0%,rgba(242,242,242,1) 100%); /* IE10+ */
      background: linear-gradient(to bottom, rgba(251,251,251,1) 0%,rgba(242,242,242,1) 100%); /* W3C */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fbfbfb', endColorstr='#f2f2f2',GradientType=0 ); /* IE6-8 */
      font-size: 18px;
      padding: 5px;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      border: 1px solid #ddd;
      margin: 7px auto;
      filter: none;
      font-weight:bold;
      font-family:inherit;
      color:#999;
      }
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <script src="Angularjs/angular15.js"></script>
   <script src="Angularjs/MainModule.js"></script>
   <script src="Angularjs/TrackCustomerLead.js"></script>
       <script>
           function isNumberKey(evt) {
               var charCode = (evt.which) ? evt.which : evt.keyCode;
               if (charCode != 46 && charCode > 31
                 && (charCode < 48 || charCode > 57))
                   return false;

               return true;
           }
   </script>  
    
 <div style="margin-top: 20px" class="container" ng-app="MTSBI" ng-controller="ctrltracklead">
       <div >
         <div >
            <h1 class="tab-head">Lead Details</h1>
            <table class="table">
               <tbody>
                  
                  <tr>
                     <td > <b>Customer Id :-</b> </td>
                     <td> <asp:Label ID="lblcusomerid" runat="server" Text="Label"></asp:Label></td>
                     <td>Customer Name</td>
                     <td> <asp:Label ID="lblFullName" runat="server" Text="Label"></asp:Label></td>
                  </tr>
                   <tr>
                     <td > <b>Lead  Id :-</b> </td>
                     <td>	 <asp:Label ID="lblLeadId" runat="server" Text="Label"></asp:Label> </td>
                     <td>	Product Interest</td>
                     <td> <asp:Label ID="lblProduct" runat="server" Text="Label"></asp:Label></td>
                  </tr>
                  <tr>
                     <td>Customer Address</td>
                     <td> <asp:Label ID="lblAddress" runat="server" Text="Label"></asp:Label></td>
                     <td>Customer Phone</td>
                     <td> <asp:Label ID="lblPhone" runat="server" Text="Label"></asp:Label></td>
                  </tr>
                   <tr>
                     <td>Current Status</td>
                     <td>	 <asp:Label ID="lblStatus" runat="server" Text="Label"></asp:Label></td>
                     <td>Lead Assignee Id</td>
                     <td>	 <asp:Label ID="lblLeadAssigneeId" runat="server" Text="Label"></asp:Label></td>
                  </tr>
                   <asp:HiddenField ID="hdnStatus" runat="server" />
                  <tr>
                     <td style="text-align:right;" colspan="4">
                       
                        <ul class="tabs">
                         
                           
                        </ul>
                     </td>
                  </tr>
               </tbody>
            </table>
            <ul class="tabs" runat="server" id="UL3">
                 <li><a href="#_page_process" class="btn btn-primary btn-xs" id="tabProcess">Process</a></li>
               <li><a href="#_page_meeting" id="tabeMeeting">Meeting</a></li>
               <li><a href="#_page_feed" id="tabFeed"> Feeds</a></li>
               <li><a href="#_page_products" id="tabProduct"> Product</a></li>
               <li><a href="#_page_flag_list" id="tabflaglist">Flag</a></li>
                 <li><a href="#_page_schedulemeetinglist" id="tabscedule">Schedule Meeting</a></li>
            </ul>
            
         </div>
      </div>
      
      <div class="tab-control" data-role="tab-control" data-effect="fade[slide]" id="tab-with-event">
         <asp:HiddenField ID="hdnEmpId" ClientIDMode="Static" runat="server" />
         <asp:HiddenField ID="hdnStaffType" ClientIDMode="Static" runat="server" />
         <div class="frames">
            
            <div class="frame leadClousre" id="_page_process">
               <h1 class="tab-head">Status Of Lead</h1>
               <div class="row">
                  <div class="col-sm-3">
                     <label>Status of Lead</label>
                  </div>
                  <div class="col-sm-9">
                     <div class="input-control select" id="divStatus">
                        <select id="DDLStatus" class="form-control">
                           <option value="0">Lead Added</option>
                           <option value="1">Open For Action</option>
                           <option value="2">Lost lead</option>
                           <option value="3">Junk lead</option>
                           <option value="4">Lead Completed</option>
                           <option value="5">Flagged Lead</option>
                        </select>
                        <input type="hidden" value="" id="currentStatus" />
                     </div>
                  </div>
                     </div>
               
                <div id="divflagstatus03">
               <div id="divaddflagproc1">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Flag For</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="input-control select" id="divFlagFor">
                           <select id="DDLFLAGFOR" class="form-control">
                              <option value="">Select</option>
                              <option value="Branch">Branch</option>
                              <option value="TMO">TMO</option>
                              <option value="Other">Other</option>
                           </select>
                           <input type="hidden" value="" id="currentStatus1" />
                        </div>
                     </div>
                  </div>
                  <div class="row" id="divFlagBranch">
                     <div class="col-sm-3">
                        <label>Select Branch</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="input-control select" >
                           <select id="DDLBRANCH" class="form-control">
                              <option value="-1">Select</option>
                           </select>
                           <input type="hidden" value="" id="currentStatus2" />
                        </div>
                     </div>
                  </div>
                  <div class="row" id="divTMU">
                     <div class="col-sm-3">
                        <label>Select TMU</label>
                     </div>
                     <div class="col-sm-9">
                        <select id="DDLTMU" class="form-control">
                           <option value="-1">Select</option>
                        </select>
                        <input type="hidden" value="" id="currentStatus3" />
                     </div>
                  </div>
                  <div class="row" id="divother" >
                     <div class="col-sm-3">
                        <label>Enter Email Id</label>
                     </div>
                     <div class="col-sm-9">
                        <div class='col-sm-9 input-control text' >
                           <input type='text' class="form-control" id="txtotheremail" placeholder="Enter Email" />
                        </div>
                     </div>
                  </div>
                  <div class="row" id="divflagDesction">
                     <div class="col-sm-3">
                        <label>Flag Description</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="col-sm-9 input-control textarea" data-role="input-control" >
                           <textarea id="txtflagdesgrption" placeholder="Comments"></textarea>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div id="divNextAppointmentDate" style="display: none;">
                     <div class="col-sm-3">
                        <label>
                        Next Appointment<br />
                        <span style="color: red; font-size: 12px; font-weight: 200;">(Optional, if lead is still open)
                        </span>
                        </label>
                     </div>
                     <div class='input-group input-append date ' id='divNextAppointment'>
                        <input type='text' class="form-control date" id="txtNextAppointment" placeholder="dd/mm/yyyy" />
                     </div>
                  </div>
               </div>
           
               <div id="divConverted" style="display: none;">
                  <h2>Product 1</h2>
                  <div class="row">
                     <div class="full">
                        <label class="col-sm-3">Product Sold<i>(if sale made)</i></label>
                        <div class=" col-sm-9 input-control select" id="divProductSold0">
                           <select id="DDLProductSold0" class="form-control">
                              <option value="-1">Select</option>
                           </select>
                        </div>
                     </div>
                     <div class="full" id="divplain" >
                        <label class="col-sm-3">Plain vanilla Option 1</label>
                        <div class="col-sm-9 input-control select" >
                           <select id="DDLPlain" class="form-control">
                                  <option value="" >Select</option>
                           <option value="Cost Reduction structure" >Cost Reduction structure</option>
                           <option value="Zero cost structure" >Zero cost structure</option>
                           <option value="Exotic structure" > Exotic structure</option>
                           <option  value="Risk Reversal structure"> Risk Reversal structure</option>
                           <option value="Swaps">Swaps</option>
                           </select>
                        </div>
                     </div>
                     <div class="full" id="divSwaps" style="display: none">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-9 input-control select" >
                         <select id="DDLSwaps" class="form-control">
                           <option value="" >Select</option>
                           <option value="IRS" >IRS</option>
                           <option value="POS">POS</option>
                           <option value="COS">COS</option>
                           <option value="CIRS">CIRS</option>
                        </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Quantity</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="input-control text" data-role="input-control" id="divAccount0">
                           <input type="text"  onkeypress="return isNumberKey(event)"  id="txtAccount0" placeholder="Quantity" required />
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="span9">
                        <label class="col-sm-3">Amount<i>(in INR)</i></label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAmount0">
                           <input type="text" id="txtAmount0"  placeholder="xx-xxxx" required />
                           <label id="lblAmount0" class="toplabel"></label>
                        </div>
                     </div>
                  </div>
                    
                     <div id="divproduct2" >
                  <h2>Product 2</h2>
                
                  <div class="row" >
                     <label class="col-sm-3">Product Sold<i>(if sale made)</i></label>
                     <div class="col-sm-9" id="divProductSold1">
                        <select id="DDLProductSold1" class="form-control">
                           <option value="-1">Select</option>
                        </select>
                     </div>
                 <div class="full" id="divplain1" >
                        <label class="col-sm-3">Plain vanilla Option 2</label>
                        <div class="col-sm-9 input-control select" >
                           <select id="DDLPlain1" class="form-control">
                                  <option value="" >Select</option>
                           <option value="Cost Reduction structure" >Cost Reduction structure</option>
                           <option value="Zero cost structure" >Zero cost structure</option>
                           <option value="Exotic structure" > Exotic structure</option>
                           <option  value="Risk Reversal structure"> Risk Reversal structure</option>
                           <option value="Swaps">Swaps</option>
                           </select>
                        </div>
                     </div>
                     <div class="full" id="divSwaps1" style="display: none">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-9 input-control select" >
                         <select id="DDLSwaps1" class="form-control">
                           <option value="" >Select</option>
                           <option value="IRS" >IRS</option>
                           <option value="POS">POS</option>
                           <option value="COS">COS</option>
                           <option value="CIRS">CIRS</option>
                        </select>
                        </div>
                     </div>
                  </div>
                     
                  <div class="row" >
                     <label class="col-sm-3">Quantity</label>
                     <div class="col-sm-9 input-control text" data-role="input-control" id="divAccount1">
                        <input type="text" onkeypress="return isNumberKey(event)" id="txtAccount1" placeholder="Quantity" />
                     </div>
                  </div>
                  <div class="row">
                     <label class="col-sm-3">Amount<i>(in INR)</i></label>
                     <div class="col-sm-9 input-control text" data-role="input-control" id="divAmount1">
                        <input type="text" id="txtAmount1" placeholder="xx-xxxx" />
                        <label id="lblAmount1" class="toplabel"></label>
                     </div>
                  </div>
                       </div>
                  <%--Start Product 3--%>
                  <div id="divproduct3" style="display:none">
                     <h2>
                        <table id="tbl1" style="width:100%; border-bottom:none">
                           <tr style="border-bottom:none">
                              <td>Product 3 </td>
                              <td style="text-align:right">
                                 <input type="button" class="btn btn-primary  btn-sm" value="X" title="Delete" id="btnDelete3" onclick="reply_click(this.id)">
                              </td>
                           </tr>
                        </table>
                     </h2>
                         
                  <div class="row">
                     <label class="col-sm-3">Product Sold<i>(if sale made)</i></label>
                     <div class="col-sm-9" id="divProductSold2">
                        <select id="DDLProductSold2" class="form-control">
                           <option value="-1">Select</option>
                        </select>
                     </div>
                         <div class="full" id="divplain2" >
                        <label class="col-sm-3">Plain vanilla Option 3</label>
                        <div class="col-sm-9 input-control select" >
                           <select id="DDLPlain2" class="form-control">
                                  <option value="" >Select</option>
                           <option value="Cost Reduction structure" >Cost Reduction structure</option>
                           <option value="Zero cost structure" >Zero cost structure</option>
                           <option value="Exotic structure" > Exotic structure</option>
                           <option  value="Risk Reversal structure"> Risk Reversal structure</option>
                           <option value="Swaps">Swaps</option>
                           </select>
                        </div>
                     </div>
                     <div class="full" id="divSwaps2" style="display: none">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-9 input-control select" >
                         <select id="DDLSwaps2" class="form-control">
                           <option value="" >Select</option>
                           <option value="IRS" >IRS</option>
                           <option value="POS">POS</option>
                           <option value="COS">COS</option>
                           <option value="CIRS">CIRS</option>
                        </select>
                        </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Quantity</label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAccount2">
                           <input type="text" onkeypress="return isNumberKey(event)" id="txtAccount2" placeholder="Quantity" />
                        </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Amount<i>(in INR)</i></label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAmount2">
                           <input type="text" id="txtAmount2" placeholder="xx-xxxx" />
                           <label id="lblAmount2" class="toplabel"></label>
                        </div>
                     </div>
                  </div>
                  
                      <%--End Product 3--%>
                  <%-- Start Product 4 --%>
                  <div id="divproduct4" style="display:none">
                     <h2>
                        <table id="Table1" style="width:100%; border-bottom:none">
                           <tr style="border-bottom:none">
                              <td>Product 4 </td>
                              <td style="text-align:right">
                                 <input type="button" class="btn btn-primary  btn-sm" value="X" title="Delete" id="btnDelete4" onclick="reply_click(this.id)">
                              </td>
                           </tr>
                        </table>
                     </h2>
                     <div class="row">
                        <label class="col-sm-3">Product Sold<i>(if sale made)</i></label>
                        <div class="col-sm-9" id="divProductSold3">
                           <select id="DDLProductSold3" class="form-control">
                              <option value="-1">Select</option>
                           </select>
                        </div>
                          <div class="full" id="divplain3" >
                        <label class="col-sm-3">Plain vanilla Option 4</label>
                        <div class="col-sm-9 input-control select" >
                           <select id="DDLPlain3" class="form-control">
                                  <option value="" >Select</option>
                           <option value="Cost Reduction structure" >Cost Reduction structure</option>
                           <option value="Zero cost structure" >Zero cost structure</option>
                           <option value="Exotic structure" > Exotic structure</option>
                           <option  value="Risk Reversal structure"> Risk Reversal structure</option>
                           <option value="Swaps">Swaps</option>
                           </select>
                        </div>
                     </div>
                     <div class="full" id="divSwaps3" style="display: none">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-9 input-control select" >
                         <select id="DDLSwaps3" class="form-control">
                           <option value="" >Select</option>
                           <option value="IRS" >IRS</option>
                           <option value="POS">POS</option>
                           <option value="COS">COS</option>
                           <option value="CIRS">CIRS</option>
                        </select>
                        </div>
                     </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Quantity</label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAccount3">
                           <input type="text"onkeypress="return isNumberKey(event)" id="txtAccount3" placeholder="Quantity" />
                        </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Amount<i>(in INR)</i></label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAmount3">
                           <input type="text" id="txtAmount3" placeholder="xx-xxxx" />
                           <label id="lblAmount3" class="toplabel"></label>
                        </div>
                     </div>
                  </div>
                  <%--End Product 4--%>
                  <%-- Start Product 5 --%>
                  <div id="divproduct5" style="display:none">
                     <h2>
                        <table id="Table2" style="width:100%; border-bottom:none">
                           <tr style="border-bottom:none">
                              <td>Product 5 </td>
                              <td style="text-align:right">
                                 <input type="button" class="btn btn-primary  btn-sm" value="X" title="Delete" id="btnDelete5" onclick="reply_click(this.id)">
                              </td>
                           </tr>
                        </table>
                     </h2>
                     <div class="row">
                        <label class="col-sm-3">Product Sold<i>(if sale made)</i></label>
                        <div class="col-sm-9" id="divProductSold4">
                           <select id="DDLProductSold4" class="form-control">
                              <option value="-1">Select</option>
                           </select>
                        </div>
                          <div class="full" id="divplain4" >
                        <label class="col-sm-3">Plain vanilla Option 5</label>
                        <div class="col-sm-9 input-control select" >
                           <select id="DDLPlain4" class="form-control">
                                  <option value="" >Select</option>
                           <option value="Cost Reduction structure" >Cost Reduction structure</option>
                           <option value="Zero cost structure" >Zero cost structure</option>
                           <option value="Exotic structure" > Exotic structure</option>
                           <option  value="Risk Reversal structure"> Risk Reversal structure</option>
                           <option value="Swaps">Swaps</option>
                           </select>
                        </div>
                     </div>
                     <div class="full" id="divSwaps4" style="display: none">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-9 input-control select" >
                         <select id="DDLSwaps4" class="form-control">
                           <option value="" >Select</option>
                           <option value="IRS" >IRS</option>
                           <option value="POS">POS</option>
                           <option value="COS">COS</option>
                           <option value="CIRS">CIRS</option>
                        </select>
                        </div>
                     </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Quantity</label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAccount4">
                           <input type="text" onkeypress="return isNumberKey(event)" id="txtAccount4" placeholder="Quantity" />
                        </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Amount<i>(in INR)</i></label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAmount4">
                           <input type="text" id="txtAmount4" placeholder="xx-xxxx" />
                           <label id="lblAmount4" class="toplabel"></label>
                        </div>
                     </div>
                  </div>
                  <%--End Product 5--%>
                  <%-- Start Product 6 --%>
                  <div id="divproduct6" style="display:none">
                     <h2>
                        <table id="Table3" style="width:100%; border-bottom:none">
                           <tr style="border-bottom:none">
                              <td>Product 6 </td>
                              <td style="text-align:right">
                                 <input type="button" class="btn btn-primary  btn-sm" value="X" title="Delete" id="btnDelete6" onclick="reply_click(this.id)">
                              </td>
                           </tr>
                        </table>
                     </h2>
                     <div class="row">
                        <label class="col-sm-3">Product Sold<i>(if sale made)</i></label>
                        <div class="col-sm-9" id="divProductSold5">
                           <select id="DDLProductSold5" class="form-control">
                              <option value="-1">Select</option>
                           </select>
                        </div>
                           <div class="full" id="divplain5" >
                        <label class="col-sm-3">Plain vanilla Option 6</label>
                        <div class="col-sm-9 input-control select" >
                           <select id="DDLPlain5" class="form-control">
                                  <option value="" >Select</option>
                           <option value="Cost Reduction structure" >Cost Reduction structure</option>
                           <option value="Zero cost structure" >Zero cost structure</option>
                           <option value="Exotic structure" > Exotic structure</option>
                           <option  value="Risk Reversal structure"> Risk Reversal structure</option>
                           <option value="Swaps">Swaps</option>
                           </select>
                        </div>
                     </div>
                     <div class="full" id="divSwaps5" style="display: none">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-9 input-control select" >
                         <select id="DDLSwaps5" class="form-control">
                           <option value="" >Select</option>
                           <option value="IRS" >IRS</option>
                           <option value="POS">POS</option>
                           <option value="COS">COS</option>
                           <option value="CIRS">CIRS</option>
                        </select>
                        </div>
                     </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Quantity</label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAccount5">
                           <input type="text"onkeypress="return isNumberKey(event)" id="txtAccount5" placeholder="Quantity" />
                        </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Amount<i>(in INR)</i></label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAmount5">
                           <input type="text" id="txtAmount5" placeholder="xx-xxxx" />
                           <label id="lblAmount5" class="toplabel"></label>
                        </div>
                     </div>
                  </div>
                  <%--End Product 6--%>
                  <%-- Start Product 7 --%>
                  <div id="divproduct7" style="display:none">
                     <h2>
                        <table id="Table4" style="width:100%; border-bottom:none">
                           <tr style="border-bottom:none">
                              <td>Product 7 </td>
                              <td style="text-align:right">
                                 <input type="button" class="btn btn-primary  btn-sm" value="X" title="Delete" id="btnDelete7" onclick="reply_click(this.id)">
                              </td>
                           </tr>
                        </table>
                     </h2>
                     <div class="row">
                        <label class="col-sm-3">Product Sold<i>(if sale made)</i></label>
                        <div class="col-sm-9" id="divProductSold6">
                           <select id="DDLProductSold6" class="form-control">
                              <option value="-1">Select</option>
                           </select>
                        </div>
                          <div class="full" id="divplain6" >
                        <label class="col-sm-3">Plain vanilla Option 7</label>
                        <div class="col-sm-9 input-control select" >
                           <select id="DDLPlain6" class="form-control">
                                  <option value="" >Select</option>
                           <option value="Cost Reduction structure" >Cost Reduction structure</option>
                           <option value="Zero cost structure" >Zero cost structure</option>
                           <option value="Exotic structure" > Exotic structure</option>
                           <option  value="Risk Reversal structure"> Risk Reversal structure</option>
                           <option value="Swaps">Swaps</option>
                           </select>
                        </div>
                     </div>
                     <div class="full" id="divSwaps6" style="display: none">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-9 input-control select" >
                         <select id="DDLSwaps6" class="form-control">
                           <option value="" >Select</option>
                           <option value="IRS" >IRS</option>
                           <option value="POS">POS</option>
                           <option value="COS">COS</option>
                           <option value="CIRS">CIRS</option>
                        </select>
                        </div>
                     </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Quantity</label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAccount6">
                           <input type="text"onkeypress="return isNumberKey(event)" id="txtAccount6" placeholder="Quantity" />
                        </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Amount<i>(in INR)</i></label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAmount6">
                           <input type="text" id="txtAmount6" placeholder="xx-xxxx" />
                           <label id="lblAmount6" class="toplabel"></label>
                        </div>
                     </div>
                  </div>
                  <%--End Product 7--%>
                  <%-- Start Product 8 --%>
                  <div id="divproduct8" style="display:none">
                     <h2>
                        <table id="Table5" style="width:100%; border-bottom:none">
                           <tr style="border-bottom:none">
                              <td>Product 8 </td>
                              <td style="text-align:right">
                                 <input type="button" class="btn btn-primary  btn-sm" value="X" title="Delete" id="btnDelete8" onclick="reply_click(this.id)">
                              </td>
                           </tr>
                        </table>
                     </h2>
                     <div class="row">
                        <label class="col-sm-3">Product Sold<i>(if sale made)</i></label>
                        <div class="col-sm-9" id="divProductSold7">
                           <select id="DDLProductSold7" class="form-control">
                              <option value="-1">Select</option>
                           </select>
                        </div>
                          <div class="full" id="divplain7" >
                        <label class="col-sm-3">Plain vanilla Option 8</label>
                        <div class="col-sm-9 input-control select" >
                           <select id="DDLPlain7" class="form-control">
                                  <option value="" >Select</option>
                           <option value="Cost Reduction structure" >Cost Reduction structure</option>
                           <option value="Zero cost structure" >Zero cost structure</option>
                           <option value="Exotic structure" > Exotic structure</option>
                           <option  value="Risk Reversal structure"> Risk Reversal structure</option>
                           <option value="Swaps">Swaps</option>
                           </select>
                        </div>
                     </div>
                     <div class="full" id="divSwaps7" style="display: none">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-9 input-control select" >
                         <select id="DDLSwaps7" class="form-control">
                           <option value="" >Select</option>
                           <option value="IRS" >IRS</option>
                           <option value="POS">POS</option>
                           <option value="COS">COS</option>
                           <option value="CIRS">CIRS</option>
                        </select>
                        </div>
                     </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Quantity</label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAccount7">
                           <input type="text"onkeypress="return isNumberKey(event)" id="txtAccount7" placeholder="Quantity" />
                        </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Amount<i>(in INR)</i></label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAmount7">
                           <input type="text" id="txtAmount7" placeholder="xx-xxxx" />
                           <label id="lblAmount7" class="toplabel"></label>
                        </div>
                     </div>
                  </div>
                  <%--End Product 8--%>
                  <%-- Start Product 9 --%>
                  <div id="divproduct9" style="display:none">
                     <h2>
                        <table id="Table6" style="width:100%; border-bottom:none">
                           <tr style="border-bottom:none">
                              <td>Product 9 </td>
                              <td style="text-align:right">
                                 <input type="button" class="btn btn-primary  btn-sm" value="X" title="Delete" id="btnDelete9" onclick="reply_click(this.id)">
                              </td>
                           </tr>
                        </table>
                     </h2>
                     <div class="row">
                        <label class="col-sm-3">Product Sold<i>(if sale made)</i></label>
                        <div class="col-sm-9" id="divProductSold8">
                           <select id="DDLProductSold8" class="form-control">
                              <option value="-1">Select</option>
                           </select>
                        </div>
                          <div class="full" id="divplain8" >
                        <label class="col-sm-3">Plain vanilla Option 9</label>
                        <div class="col-sm-9 input-control select" >
                           <select id="DDLPlain8" class="form-control">
                                  <option value="" >Select</option>
                           <option value="Cost Reduction structure" >Cost Reduction structure</option>
                           <option value="Zero cost structure" >Zero cost structure</option>
                           <option value="Exotic structure" > Exotic structure</option>
                           <option  value="Risk Reversal structure"> Risk Reversal structure</option>
                           <option value="Swaps">Swaps</option>
                           </select>
                        </div>
                     </div>
                     <div class="full" id="divSwaps8" style="display: none">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-9 input-control select" >
                         <select id="DDLSwaps8" class="form-control">
                           <option value="" >Select</option>
                           <option value="IRS" >IRS</option>
                           <option value="POS">POS</option>
                           <option value="COS">COS</option>
                           <option value="CIRS">CIRS</option>
                        </select>
                        </div>
                     </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Quantity</label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAccount8">
                           <input type="text" onkeypress="return isNumberKey(event)" id="txtAccount8" placeholder="Quantity" />
                        </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Amount<i>(in INR)</i></label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAmount8">
                           <input type="text" id="txtAmount8" placeholder="xx-xxxx" />
                           <label id="lblAmount8" class="toplabel"></label>
                        </div>
                     </div>
                  </div>
                  <%--End Product 9--%>
                  <%-- Start Product 10 --%>
                  <div id="divproduct10" style="display:none">
                     <h2>
                        <table id="Table7" style="width:100%; border-bottom:none">
                           <tr style="border-bottom:none">
                              <td>Product 10 </td>
                              <td style="text-align:right">
                                 <input type="button" class="btn btn-primary  btn-sm" value="X" title="Delete" id="btnDelete10" onclick="reply_click(this.id)">
                              </td>
                           </tr>
                        </table>
                     </h2>
                     <div class="row">
                        <label class="col-sm-3">Product Sold<i>(if sale made)</i></label>
                        <div class="col-sm-9" id="divProductSold9">
                           <select id="DDLProductSold9" class="form-control">
                              <option value="-1">Select</option>
                           </select>
                        </div>
                          <div class="full" id="divplain9" >
                        <label class="col-sm-3">Plain vanilla Option 1</label>
                        <div class="col-sm-9 input-control select" >
                           <select id="DDLPlain9" class="form-control">
                                  <option value="" >Select</option>
                           <option value="Cost Reduction structure" >Cost Reduction structure</option>
                           <option value="Zero cost structure" >Zero cost structure</option>
                           <option value="Exotic structure" > Exotic structure</option>
                           <option  value="Risk Reversal structure"> Risk Reversal structure</option>
                           <option value="Swaps">Swaps</option>
                           </select>
                        </div>
                     </div>
                     <div class="full" id="divSwaps9" style="display: none">
                        <label class="col-sm-3"></label>
                        <div class="col-sm-9 input-control select" >
                         <select id="DDLSwaps9" class="form-control">
                           <option value="" >Select</option>
                           <option value="IRS" >IRS</option>
                           <option value="POS">POS</option>
                           <option value="COS">COS</option>
                           <option value="CIRS">CIRS</option>
                        </select>
                        </div>
                     </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Quantity</label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAccount9">
                           <input type="text" onkeypress="return isNumberKey(event)" id="txtAccount9" placeholder="Quantity" />
                        </div>
                     </div>
                     <div class="row">
                        <label class="col-sm-3">Amount<i>(in INR)</i></label>
                        <div class="col-sm-9 input-control text" data-role="input-control" id="divAmount9">
                           <input type="text" id="txtAmount9" placeholder="xx-xxxx" />
                           <label id="lblAmount9" class="toplabel"></label>
                        </div>
                     </div>
                  </div>
                  <%--End Product 10--%>
                  <div style="text-align:right;padding-right:15px;">
                     <input type="button" class="btn btn-primary  btn-sm" value="Add More Product" id="Addproduct">
                  </div>
               </div>

                </div>
               <h2 class="abc">Date of Action</h2>
               <div class="row">
                  <div id="divreason" >
                     <label class="col-sm-3">Reason(mandatory)</label>
                     <div class='col-sm-9 input-control text' >
                        <input type='text' class="form-control" id="textreason" placeholder="Reason" />
                     </div>
                  </div>
                  <label class="col-sm-3">Date of action(mandatory)</label>
                  <div class='col-sm-9 input-control text' id='dpDateOfAction'>
                     <input type='text' class="form-control" id="txtDateOfAction" placeholder="dd/mm/yyyy" />
                  </div>
                  <%--<div class="" data-role="datepicker" data-effect="slide" data-other-days="1" id="divActionDate">
                     <input type="text"  />
                     </div>--%>
                  <input type="hidden" id="hdnPrevStage" />
                  <input type="hidden" id="hdnPrevActionDate" />
               </div>
               <div class="row">
                  <label class="col-sm-3">Comments(mandatory)</label>
                  <div class="col-sm-9 input-control textarea" data-role="input-control" id="divComments">
                     <textarea id="txtComments" placeholder="Comments"></textarea>
                  </div>
               </div>
               <div class="row">
                  <input type="button" style="font-weight: bold;" class="info smtBtn" value="Submit" id="btnSubmit" />
               </div>
            </div>
             </div>

            <div class="frame" id="_page_feed">
               <h1 class="tab-head"> Feed</h1>
               <table class="display" id="tblFeed">
                  <thead>
                     <tr>
                        <th class="text-left">Stage</th>
                        <th class="text-left">Completion Date</th>
                        <th class="text-left">Description</th>
                        <th class="text-left">Completed By</th>
                        <th class="text-left">Reason</th>
                     </tr>
                  </thead>
               </table>
            </div>
                 <div class="frame" id="_page_products">
            <div class="container app">
               <div class="col-sm-12">
                  <div data-effect="helix" id="panels" class="panel panel-info effect-helix in" style="transition: all 0.7s ease-in-out 0s;">
                     <div class="panel-heading">Product</div>
                     <div class="panel-body">
                     </div>
                     <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL4">
                        <span class="pull-right">
                           
                        </span>
                     </ul>
                     <div id="Div1" visible="false" runat="server">
                        <asp:Label ID="Label1" runat="server" Style="color: red"></asp:Label>
                        <div style="margin-bottom: 10px"></div>
                     </div>
                          <!--Dynamic table -->
                     <table class="display" id="tblProduct">
                        <thead>
                           <tr>
                             <th class="text-left">	Product Name</th>
                              <th class="text-left">Quantity</th>
                              <th class="text-left">Value</th>
                                <th class="text-left">Date</th>
                            
                           </tr>
                        </thead>
                     </table>

                            <!--END table -->
                
                  </div>
               </div>
            </div>
         </div>
         
            <div class="frame" id="_page_meeting">
            <h1 class="tab-head"> Meeting List</h1>
               <div class="row">
  <div class="col-xs-2"><b>Select Meeting Type :- </b>  </div>
  <div class="col-xs-2"> <select id="ddlmeetingstatus"  class="form-control" >
                        <option value="">Select</option>
                        <option value="Open">Open</option>
                        <option value="Close">Close</option>
                      
                     </select></div>
                     <div class="col-xs-8">   <ul class="tabs" style="margin-bottom:20px" runat="server" >
               <span class="pull-right">
                  <li><a href="#_page_add_meeting" class="btn btn-primary btn-sm" id="tabAddMeeting1">Add New Meeting</a></li>
               </span>
            </ul> </b>  </div>
</div>
              
  
         
            <div style="margin-top:10px">
               <!--dynamic table-->
               <table class="display" id="tblMeeting">
                  <thead>
                     <tr>
                      
                      <th class="text-left">Meeting Type</th>                    
                        <th class="text-left">Meeting Date</th>
                        <th class="text-left">Along with person</th>
                          <th class="text-left">Description</th>
                          <th class="text-left"> View</th>
                      
                     </tr>
                  </thead>
                  
                  </table>
               <!--dynamic end-->
            
            </div>
         </div>

                <div class="frame add-meeting" id="_page_add_meeting">
            <h1 class="tab-head">Add Meeting </h1>

               <div class="row">
               <div class="col-sm-3">
                  <label>Meeting Type</label>
               </div>
               <div class="col-sm-9">
                  <div class="input-control select" >
                     <select id="ddlSMeetingType" ng-model="ddlSMeetingType" class="form-control" >
                        <option value="">Select</option>
                        <option value="Customer">Customer</option>
                        <option value="Branch">Branch</option>
                        <option value="Other">Other</option>
                     </select>
                  </div>
               </div>
            </div>

            <div class="row" id="DivBranch">
               <div class="col-sm-3">
                  <label>Select Branch</label>
               </div>
               <div class="col-sm-9">
                  <div class="input-control select" >
                     <select id="DddlBranchMeeting" ng-model="ddlbranch" class="form-control" >
                        <option value="">Select</option>
                     </select>
                     <input type="hidden" value="" id="Hidden1" />
                  </div>
               </div>
            </div>
            <div class="row" id="DivOther">
               <label class="col-sm-3">Enter Details for Others</label>
               <div class="col-sm-9 input-control textarea" data-role="input-control" >
                  <input type="text" ng-model="txtotherdetails" id="txtotherdetails" placeholder="Enter Details for Others">
               </div>
            </div>
            
              <div id ="divMeet">
            <div class="row" id="DivMeeting">
               <div class="col-sm-3">
                  <label>Meeting For</label>
               </div>
               <div class="col-sm-9">
                  <div class="input-control select" >
                     <select ng-model="DdlMeetingWith" class="form-control" id="DdlMeetingWith">
                        <option value="">Select</option>
                        <option value="Specified Meeting">Specified Meeting</option>                      
                        <option value="Genral Meeting">General Meeting</option>
                     </select>
                  </div>
               </div>
            </div>
      
            <div class="row" id="DivCustomerLead">
               <div class="col-sm-3">
                  <label>Select Lead</label>
               </div>
               <div class="col-sm-9">
                  <div class="input-control select">
                     <select ng-model="Meetingleadid" id="Meetingleadid" class="form-control" >
                        <option value="">Select</option>
                    
                     </select>
                  </div>
               </div>
            </div>
                 </div>
            <div class="row">
               <label class="col-sm-3">Meeting Date</label>                
               <div class='input-group input-append date ' id='divaddMeetingMeetDate'>
                  <input type='text'  ng-model="ddlmeetingdate" class="form-control date" id="txtAddMeetingMeetDate" placeholder="dd/mm/yyyy" />
               </div>
            </div>
                   <div class="row">
                          <label class="col-sm-3">Meeting Time</label> 
                           <div class="row">
                           <div class="col-md-2">      <input type='text'  ng-model="txtFromTime" class="form-control date" id="txtFromTime" placeholder="00:00" /></div>
                           <div class="col-md-1">    <label class="col-sm-1">To </label>    </div>
                           <div class="col-md-2">    <input type='text'  ng-model="txtToTime" class="form-control date" id="txtToTime" placeholder="00:00" /></div>
                        </div>

            
                  </div>
                    <div class="row">
                     <label class="col-sm-3">Person to meet</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control">
                        <input type="text"  ng-model="txtmeetperson" id="txtmeetperson" placeholder="Enter Person to meet ">
                     </div>
                  </div>
          
            <div class="row">
               <label class="col-sm-3">Along With Other users</label>
               <div class="col-sm-9 input-control textarea" data-role="input-control">
                  <input type="text"  ng-model="txtalongotheruser" id="txtalongotheruser" placeholder="Enter Username of the users along with you for meeting">
               </div>
            </div>
            <div class="row">
               <label class="col-sm-3">Meeting Description</label>
               <div class="col-sm-9 input-control textarea" data-role="input-control" id="divdesciprition">
                  <textarea id="txtmeetingdescription" ng-model="txtmeetingdescription" placeholder="Meeting Description"></textarea>
               </div>
            </div>
            <div class="row">
               <table class="table">
                  <thead>
                     <th></th>
                     <th> </th>
                     <th> </th>
                     <th></th
                     <th></th>
                     </tr>
                  </thead>
                  <tbody >
                  </tbody>
                  </table>
            </div>
                    <div style="background-color:#CCDEDE">
            <div class="row">
               <label class="col-sm-3">Issue</label>
               <div class="col-sm-9 input-control textarea" data-role="input-control" id="divissue">
                  <textarea id="txtissue" ng-model="txtissue" placeholder="Issue"></textarea>
               </div>
            </div>
           
            <div class="row">
               <table class="table">
                  <thead>
                     <th></th>
                     <th> </th>
                     <th> </th>
                     <th></th
                     <th></th>
                     </tr>
                  </thead>
                  <tbody >
                  </tbody>
               </table>
            </div>
            <div class="row">
               <label class="col-sm-3">Action Plan</label>
               <div class="col-sm-9 input-control textarea" data-role="input-control" id="divaction">
                  <textarea id="txtactionplan" ng-model="txtactionplan" placeholder="Action Plan"></textarea>
               </div>
            </div>
            <div class="row">
               <label class="col-sm-3">Mail Text</label>
               <div class="col-sm-9 input-control textarea" data-role="input-control" id="divmailtext">
                  <textarea id="txtmailtext" ng-model="txtmailtext" placeholder="Email Body"></textarea>
               </div>
            </div>
            <div class="row" id="divreciption">
               <label class="col-sm-3">Recipient Id</label>
               <div class="col-sm-9 input-control textarea" data-role="input-control" id="divrecipientid">
                  <input type="text" id="txtrecipientid" ng-model="txtrecipientid" placeholder="Enter comma saperated email ID">
               </div>
            </div>
            <div class="row" id="div13">
               <label class="col-sm-3"></label>
               <div class="col-sm-9 input-control textarea" data-role="input-control" id="div14">
            
                  <div class="row">
                     <input style="font-weight: bold; width:auto;" ng-click="addactioncustomer()" class="btn-success pull-right btn-sm" value="Submit  Action Plan"  type="button">
                  </div>
                    <div class="row">
               <input style="font-weight: bold; width:auto;" ng-click="New()" class="btn-warning pull-right btn-sm " value="New Issue  "  type="button">
            </div>
               </div>
            </div>
            <div class="row">
               <table class="table">
                  <thead>
                     <th>Issue</th>
                     <th>Action Plan</th>
                     <th>Mail Text</th>
                     <th>Recipient</th>
                     <th>Remove</th>
                     </tr>
                  </thead>
                  <tbody >
                     <tr ng-repeat="item in actionlist1 ">
                        <td>{{item.issue}}</td>
                        <td>{{item.action_plan}}</td>
                        <td>{{item.mailtext}}</td>
                        <td>{{item.recipient_id}}</td>
                        <td>
                           <div ng-click="removeItem1($index)" class="btn btn-sm btn-danger">Remove</div>
                        </td>
                     </tr>
                  </tbody>
                  </table>
            </div>
                                   
                        </div>
            <div class="row">
            
               <input type="button" style="font-weight: bold;" class="info smtBtn" value="Submit" ng-click="SaveMeetingcustomer()"  />
            </div>
         </div>

            <div class="frame add-meeting" id="_page_add_flags">
               <h1 class="tab-head">Add Flags</h1>
               <div id="divaddflagproc">
                  <div class="row">
                     <div class="col-sm-3">
                        <label>Flag For</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="input-control select" id="divFlagFor1">
                           <select id="DDLFLAGFOR1" class="form-control">
                              <option value="">Select</option>
                              <option value="Branch">Branch</option>
                              <option value="TMO">TMO</option>
                              <option value="Other">Other</option>
                           </select>
                           <input type="hidden" value="" id="currentStatus11" />
                        </div>
                     </div>
                  </div>
                  <div class="row" id="divFlagBranch1">
                     <div class="col-sm-3">
                        <label>Select Branch</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="input-control select" >
                           <select id="DDLBRANCH1" class="form-control">
                              <option value="-1">Select</option>
                           </select>
                           <input type="hidden" value="" id="currentStatus21" />
                        </div>
                     </div>
                  </div>
                  <div class="row" id="divTMU1">
                     <div class="col-sm-3">
                        <label>Select TMU</label>
                     </div>
                     <div class="col-sm-9">
                        <select id="DDLTMU1" class="form-control">
                           <option value="-1">Select</option>
                        </select>
                        <input type="hidden" value="" id="currentStatus31" />
                     </div>
                  </div>
                  <div class="row" id="divother1" >
                     <div class="col-sm-3">
                        <label>Enter Email Id</label>
                     </div>
                     <div class="col-sm-9">
                        <div class='col-sm-9 input-control text' >
                           <input type='text' class="form-control" id="txtotheremail1" placeholder="Enter Email" />
                        </div>
                     </div>
                  </div>
                  <div class="row" id="divflagDesction1">
                     <div class="col-sm-3">
                        <label>Flag Description</label>
                     </div>
                     <div class="col-sm-9">
                        <div class="col-sm-9 input-control textarea" data-role="input-control" >
                           <textarea id="txtflagdesgrption1" placeholder="Comments"></textarea>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <input type="button" style="font-weight: bold;" class="info smtBtn" value="Submit" id="btnSubmitFlag" />
                  </div>
               </div>
            </div>

             <div class="frame" id="_page_flag_list">
            <h1 class="tab-head">Flag List</h1>
            <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL5">
               <span class="pull-right">
                   
                  <li><a href="#_page_add_flags" class="btn btn-primary btn-sm" id="tabAddFlags">Add New Flag</a></li>
               </span>
            </ul>
            <div style="margin-top:10px">
               <!--dynamic table-->
               <table class="display" id="tblflaglist">
                  <thead>
                     <tr>
                        <th class="text-left">Lead</th> 
             
                              <th class="text-left">Assing Flag to</th>
                           <th class="text-left">Meeting Person</th>
                        <th class="text-left">Assign Authority  </th>
                           <th class="text-left">Assign Date  </th>
                        <th class="text-left">Description</th>
                      
                       
                     </tr>
                  </thead>
                  </table>
               <!--dynamic end-->
             
            </div>
         </div>

                <div class="frame" id="_page_schedulemeetinglist">
            <h1 class="tab-head">Schedule Meeting List</h1>
                    
            <ul class="tabs" style="margin-bottom:10px" runat="server" id="UL8">
               <span class="pull-right">
                 <li><a href="#_page_add_schedulemeeting_meeting" class="btn btn-primary btn-sm" id="A10">Add New Schedule Meeting</a></li>
               </span>
            </ul>
            <div style="margin-top:10px">
               <!--dynamic table-->
               <table class="display" id="tblscedulemeeting">
                  <thead>
                     <tr>
                     
                       <th class="text-left">Meeting Type</th>                    
                        <th class="text-left">Meeting Date</th>
                        <th class="text-left">Along with person</th>
                          <th class="text-left">Description</th>
                          <th class="text-left">Single View</th>
                     </tr>
                  </thead>
                  
                  </table>
               <!--dynamic end-->
            
            </div>
         </div>

          <div class="frame add-meeting" id="_page_add_schedulemeeting_meeting">
           <h1 class="tab-head">Scheduled Meeting </h1>
                 <div class="row">
               <div class="col-sm-3">
                  <label>Meeting Type</label>
               </div>
               <div class="col-sm-9">
                  <div class="input-control select" >
                     <select id="ddlSMeetingTypes" ng-model="ddlSMeetingTypes" class="form-control" >
                        <option value="">Select</option>
                        <option value="Customer">Customer</option>
                        <option value="Branch">Branch</option>
                        <option value="Other">Other</option>
                     </select>
                  </div>
               </div>
            </div>

            <div class="row" id="DivBranchs">
               <div class="col-sm-3">
                  <label>Select Branch</label>
               </div>
               <div class="col-sm-9">
                  <div class="input-control select" >
                     <select id="DddlBranchMeetings" ng-model="ddlbranchs" class="form-control" >
                        <option value="">Select</option>
                     </select>
                     <input type="hidden" value="" id="Hidden1s" />
                  </div>
               </div>
            </div>
            <div class="row" id="DivOthers">
               <label class="col-sm-3">Enter Details for Others</label>
               <div class="col-sm-9 input-control textarea" data-role="input-control" >
                  <input type="text" ng-model="txtotherdetailss" id="txtotherdetailss" placeholder="Enter Details for Others">
               </div>
            </div>
            
              <div id ="divMeets">
            <div class="row" id="DivMeetings">
               <div class="col-sm-3">
                  <label>Meeting For</label>
               </div>
               <div class="col-sm-9">
                  <div class="input-control select" >
                     <select ng-model="DdlMeetingWiths" class="form-control" id="DdlMeetingWiths">
                        <option value="">Select</option>
                        <option value="Specified Meeting">Specified Meeting</option>                      
                        <option value="Genral Meeting">General Meeting</option>
                     </select>
                  </div>
               </div>
            </div>
        
            <div class="row" id="DivCustomerLeads">
               <div class="col-sm-3">
                  <label>Select Lead</label>
               </div>
               <div class="col-sm-9">
                  <div class="input-control select">
                     <select ng-model="Meetingleadids" id="Meetingleadids" class="form-control" >
                        <option value="">Select</option>
                      
                     </select>
                  </div>
               </div>
            </div>
                 </div>
            
           
  <div class="row" id="DivSchedules12">
               <label class="col-sm-3">Schedule<i>Meeting</i></label>
               <div class='input-group input-append date ' id='divaddMeetingNextAppointmentDate'>
                  <input type='text' ng-model="ddlmeetingapointdate"  class="form-control date" id="txtAddMeetingNextAppointmentDate" placeholder="dd/mm/yyyy" />
               </div>
            </div>
                           <div class="row">
                          <label class="col-sm-3">Meeting Time</label> 
                           <div class="row">
                           <div class="col-md-2">      <input type='text'  ng-model="txtFromTimes" class="form-control date" id="txtFromTimes" placeholder="00:00" /></div>
                           <div class="col-md-1">    <label class="col-sm-1">To </label>    </div>
                           <div class="col-md-2">    <input type='text'  ng-model="txtToTimes" class="form-control date" id="txtToTimes" placeholder="00:00" /></div>
                        </div>

            
                  </div>
                    <div class="row">
                     <label class="col-sm-3">Person to meet</label>
                     <div class="col-sm-9 input-control textarea" data-role="input-control">
                        <input type="text"  ng-model="txtmeetpersons" id="txtmeetpersons" placeholder="Enter Person to meet ">
                     </div>
                  </div>

          
            <div class="row">
               <label class="col-sm-3">Along With Other users</label>
               <div class="col-sm-9 input-control textarea" data-role="input-control">
                  <input type="text"  ng-model="txtalongotherusers" id="txtalongotherusers" placeholder="Enter Username of the users along with you for meeting">
               </div>
            </div>
            <div class="row">
               <label class="col-sm-3">Meeting Description</label>
               <div class="col-sm-9 input-control textarea" data-role="input-control" id="divdescipritionst">
                  <textarea id="txtmeetingdescriptions" ng-model="txtmeetingdescriptiontss" placeholder="Meeting Description"></textarea>
               </div>
            </div>

        
        
            <div class="row">
               <input type="button" style="font-weight: bold;" class="info smtBtn" value="Submit" ng-click="SaveScheduleMeeting()"  />
            </div>
         </div>

            
           
       
         <ul class="tabs" runat="server" id="UL2">
          
         </ul>
            <ul class="tabs" runat="server" id="UL1">
          
         </ul>
      
      
         <asp:Label ID="lblMsg" runat="server" Text="This lead is under process so it can not be deleted." ForeColor="Red" Visible="False"></asp:Label>
     
   
            </div>
        </div>
       </div>
   <script src="Scripts/Customerleaddetails.js"></script>
   <script src="js/jquery-ui.js"></script>
</asp:Content>