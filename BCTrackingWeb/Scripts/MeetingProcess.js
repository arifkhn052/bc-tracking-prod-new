﻿//$("#A7").click(function () {
//    window.location.href = "Home.aspx";
//});

$(document).ready(function () {
   
    $("[data-toggle=tooltip]").tooltip();
   
    tabing();
  


})

var tabing = function () {
    var nav = $(".tabs"),
     slideCon = $(".frames");
    slideCon.find(".frame").hide();
    var id = nav.find(".selected").attr("href");
    $(id).show();
    $('a[href*="' + "#" + id + '"]').addClass("selected");
    nav.on("click", "li a", function (e) {
        e.preventDefault();
        var $this = $(this);
        var parentItem = $($this.attr("href"));
        var currentTab = $this.attr("href");
        if (parentItem.is(":visible")) {
            return;
        }
        else {
            slideCon.find(".frame").hide();
            parentItem.show();
            $this.addClass("selected").closest("li").siblings().find("a").removeClass("selected");
            var leadId = getParameterByName('lead_id');
            if (currentTab == "#_page_process") {
                $.ajax({
                    url: 'TrackLead.aspx/GetProcessDetails',
                    type: "POST",
                    dataType: "json",
                    data: "{'leadId': '" + leadId + "'}",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (data) {

                        var obj = JSON.parse(data.d);
                        console.log(obj);
                        if ('CurrentStatus' in obj) {
                            $("#DDLStatus option:contains(" + obj['CurrentStatus'] + ")").attr('selected', 'selected');
                            $("#currentStatus").val(obj['CurrentStatus']);
                        }
                        if ('new_enhance' in obj) {
                            $("#DDLNewEnhance option:contains(" + obj['new_enhance'] + ")").attr('selected', 'selected');
                            $("#DDLNewEnhance").val(obj['new_enhance']);
                        }
                        if ($("#DDLStatus").val() > 3) {
                            $("#divNewEnhancement").show();
                        }

                        if ($("#DDLStatus").val() > 3)
                            $("#divConverted").show();

                        if ('ProductSold0' in obj)
                            $("#DDLProductSold0").val(obj['ProductSold0']);
                        if ('ac_no0' in obj)
                            $("#txtAccount0").val(obj['ac_no0']);
                        if ('amount0' in obj)
                            $("#txtAmount0").val(obj['amount0']);

                        if ('ProductSold1' in obj)
                            $("#DDLProductSold1").val(obj['ProductSold1']);
                        if ('ac_no1' in obj)
                            $("#txtAccount1").val(obj['ac_no1']);
                        if ('amount1' in obj)
                            $("#txtAmount1").val(obj['amount1']);

                        if ('ProductSold2' in obj) {
                            $("#divproduct3").show();
                            $("#DDLProductSold2").val(obj['ProductSold2']);
                        }
                        if ('ac_no2' in obj)
                            $("#txtAccount2").val(obj['ac_no2']);
                        if ('amount2' in obj)
                            $("#txtAmount2").val(obj['amount2']);

                        if ('ProductSold3' in obj) {
                            $("#divproduct4").show();
                            $("#DDLProductSold3").val(obj['ProductSold3']);
                        }
                        if ('ac_no3' in obj)
                            $("#txtAccount3").val(obj['ac_no3']);
                        if ('amount3' in obj)
                            $("#txtAmount3").val(obj['amount3']);

                        if ('ProductSold4' in obj) {
                            $("#divproduct5").show();
                            $("#DDLProductSold4").val(obj['ProductSold4']);
                        }
                        if ('ac_no4' in obj)
                            $("#txtAccount4").val(obj['ac_no4']);
                        if ('amount4' in obj)
                            $("#txtAmount4").val(obj['amount4']);

                        if ('ProductSold5' in obj) {
                            $("#divproduct6").show();
                            $("#DDLProductSold5").val(obj['ProductSold5']);
                        }
                        if ('ac_no5' in obj)
                            $("#txtAccount5").val(obj['ac_no5']);
                        if ('amount5' in obj)
                            $("#txtAmount5").val(obj['amount5']);

                        if ('ProductSold6' in obj) {
                            $("#divproduct7").show();
                            $("#DDLProductSold6").val(obj['ProductSold6']);
                        }
                        if ('ac_no6' in obj)
                            $("#txtAccount6").val(obj['ac_no6']);
                        if ('amount6' in obj)
                            $("#txtAmount6").val(obj['amount6']);

                        if ('ProductSold7' in obj) {
                            $("#divproduct8").show();
                            $("#DDLProductSold7").val(obj['ProductSold7']);
                        }
                        if ('ac_no7' in obj)
                            $("#txtAccount7").val(obj['ac_no7']);
                        if ('amount7' in obj)
                            $("#txtAmount7").val(obj['amount7']);

                        if ('ProductSold8' in obj) {
                            $("#divproduct9").show();
                            $("#DDLProductSold8").val(obj['ProductSold8']);
                        }
                        if ('ac_no8' in obj)
                            $("#txtAccount8").val(obj['ac_no8']);
                        if ('amount8' in obj)
                            $("#txtAmount8").val(obj['amount8']);

                        if ('ProductSold9' in obj) {
                            $("#divproduct10").show();
                            $("#DDLProductSold9").val(obj['ProductSold9']);
                        }
                        if ('ac_no9' in obj)
                            $("#txtAccount9").val(obj['ac_no9']);
                        if ('amount9' in obj)
                            $("#txtAmount9").val(obj['amount9']);

                        if (obj['ProductSold0'] == 'E-DFS' || obj['ProductSold0'] == 'E-VFS')
                            $('#outerDivIndustry0').show();
                        if ('industry_major0' in obj)
                            $("#DDLIndustryMajor0").val(obj['industry_major0']);

                        if (obj['ProductSold0'] == 'Fleet Financing')
                            $('#outerDivVendor0').show();
                        if ('vendor_name0' in obj)
                            $("#DDLVendor0").val(obj['vendor_name0']);

                        if (obj['ProductSold1'] == 'E-DFS' || obj['ProductSold1'] == 'E-VFS')
                            $('#outerDivIndustry1').show();
                        if ('industry_major1' in obj)
                            $("#DDLIndustryMajor1").val(obj['industry_major1']);

                        if (obj['ProductSold1'] == 'Fleet Financing')
                            $('#outerDivVendor1').show();
                        if ('vendor_name1' in obj)
                            $("#DDLVendor1").val(obj['vendor_name1']);

                        if (obj['ProductSold2'] == 'E-DFS' || obj['ProductSold2'] == 'E-VFS')
                            $('#outerDivIndustry2').show();
                        if ('industry_major2' in obj)
                            $("#DDLIndustryMajor2").val(obj['industry_major2']);

                        if (obj['ProductSold2'] == 'Fleet Financing')
                            $('#outerDivVendor2').show();
                        if ('vendor_name2' in obj)
                            $("#DDLVendor2").val(obj['vendor_name2']);

                        if (obj['ProductSold3'] == 'E-DFS' || obj['ProductSold3'] == 'E-VFS')
                            $('#outerDivIndustry3').show();
                        if ('industry_major3' in obj)
                            $("#DDLIndustryMajor3").val(obj['industry_major3']);

                        if (obj['ProductSold3'] == 'Fleet Financing')
                            $('#outerDivVendor3').show();
                        if ('vendor_name3' in obj)
                            $("#DDLVendor3").val(obj['vendor_name3']);

                        if (obj['ProductSold4'] == 'E-DFS' || obj['ProductSold4'] == 'E-VFS')
                            $('#outerDivIndustry4').show();
                        if ('industry_major4' in obj)
                            $("#DDLIndustryMajor4").val(obj['industry_major4']);

                        if (obj['ProductSold4'] == 'Fleet Financing')
                            $('#outerDivVendor4').show();
                        if ('vendor_name4' in obj)
                            $("#DDLVendor4").val(obj['vendor_name4']);

                        if (obj['ProductSold5'] == 'E-DFS' || obj['ProductSold5'] == 'E-VFS')
                            $('#outerDivIndustry5').show();
                        if ('industry_major5' in obj)
                            $("#DDLIndustryMajor5").val(obj['industry_major5']);

                        if (obj['ProductSold5'] == 'Fleet Financing')
                            $('#outerDivVendor5').show();
                        if ('vendor_name5' in obj)
                            $("#DDLVendor5").val(obj['vendor_name5']);

                        if (obj['ProductSold6'] == 'E-DFS' || obj['ProductSold6'] == 'E-VFS')
                            $('#outerDivIndustry6').show();
                        if ('industry_major6' in obj)
                            $("#DDLIndustryMajor6").val(obj['industry_major6']);

                        if (obj['ProductSold6'] == 'Fleet Financing')
                            $('#outerDivVendor6').show();
                        if ('vendor_name6' in obj)
                            $("#DDLVendor6").val(obj['vendor_name6']);

                        if (obj['ProductSold7'] == 'E-DFS' || obj['ProductSold7'] == 'E-VFS')
                            $('#outerDivIndustry7').show();
                        if ('industry_major7' in obj)
                            $("#DDLIndustryMajor7").val(obj['industry_major7']);

                        if (obj['ProductSold7'] == 'Fleet Financing')
                            $('#outerDivVendor7').show();
                        if ('vendor_name7' in obj)
                            $("#DDLVendor7").val(obj['vendor_name7']);

                        if (obj['ProductSold8'] == 'E-DFS' || obj['ProductSold8'] == 'E-VFS')
                            $('#outerDivIndustry8').show();
                        if ('industry_major8' in obj)
                            $("#DDLIndustryMajor8").val(obj['industry_major8']);

                        if (obj['ProductSold8'] == 'Fleet Financing')
                            $('#outerDivVendor8').show();
                        if ('vendor_name8' in obj)
                            $("#DDLVendor8").val(obj['vendor_name8']);

                        if (obj['ProductSold9'] == 'E-DFS' || obj['ProductSold9'] == 'E-VFS')
                            $('#outerDivIndustry9').show();
                        if ('industry_major9' in obj)
                            $("#DDLIndustryMajor9").val(obj['industry_major9']);

                        if (obj['ProductSold9'] == 'Fleet Financing')
                            $('#outerDivVendor9').show();
                        if ('vendor_name9' in obj)
                            $("#DDLVendor9").val(obj['vendor_name9']);

                        if ('action_date' in obj)
                            $("#hdnPrevActionDate").val(obj['action_date']);


                        if ($("#DDLStatus").val() > 0) {
                            $("#divCif").show();
                            if ($("#txtCif").val(obj['cif_no']) == "" || $("#txtCif").val(obj['cif_no']) == null) {
                                $("#txtCif").prop('disabled', false);
                            }
                            else {
                                $("#txtCif").val(obj['cif_no']);
                                $("#txtCif").prop('disabled', false);
                            }
                            if ($("#hdnStatus").val() == "Disbursed") {
                                $("#txtCif").prop('disabled', true);
                            }
                            else {
                                $("#txtCif").prop('disabled', false);
                            }
                        }
                        else {
                            $("#divCif").hide();

                        }
                    }
                });
            }

            else if (currentTab == "#_page_feed") {
                var feedTable = $('#tblFeed').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 25,
                    "aLengthMenu": [[25, 30, 45, 60], [25, 30, 45, 60]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,//TODO :- Sorting searching to happen on DB side.
                    "bDestroy": true,
                    "sAjaxSource": "MeetingProcess.aspx/GetFeed",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblFeed").show();
                                }
                        });
                    }
                });
            }
            else if (currentTab == "#_page_products") {
                var productTable = $('#tblProduct').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "EditCustomer.aspx/GetProductSold",
                    "columnDefs": [

                       {
                           "orderable": false,
                           "targets": -2,
                           "render": function (data, type, full, meta) {
                               return '<a href="#" onclick="return deleteUser(' + full[4] + ');">Delete</a>';
                           }
                       },
                    {
                        "orderable": false,
                        "targets": -1,
                        "render": function (data, type, full, meta) {
                            return '<a href="UpdateProduct.aspx?id=' + full[4] + '">Update</a>';
                        }
                    }
                    ],
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblProduct").show();
                                        }
                        });
                    }
                });

                var crossproductTable = $('#tblCrossSellProduct').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "TrackLead.aspx/GetCrossSellProductSold",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblCrossSellProduct").show();
                                        }
                        });
                    }
                });

                //
            }
            else if (currentTab == "#_page_lead_list") {
                var leadTable = $('#tblleadlists').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "sDom": 'T<"clear">lfrtip',
                    "tableTools": {

                        "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "EditCustomer.aspx/GetCustomerLead",
                    "columnDefs": [{
                        "orderable": false,
                        "targets": -1,
                        "render": function (data, type, full, meta) {
                            return '<a  href="TrackCustomerLead.aspx?lead_id=' + full[0] + '">Edit</a>';
                        }
                    }],
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblleadlists").show();
                                        }
                        });
                    }
                });
            }
            else if (currentTab == "#_page_flag_list") {
                
                var productTable = $('#tblflaglist').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search ",

                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "MeetingProcess.aspx/GetIssueActionPlan",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblflaglist").show();
                                        }
                        });
                    }
                });
            }
            else if (currentTab == "#_page_forex") {
                var productTable = $('#tblforex').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search ",

                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "EditCustomer.aspx/GetForexTrunover",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblforex").show();
                                        }
                        });
                    }
                });
            }
            else if (currentTab == "#_page_budget") {
                var productTable = $('#tblbudget').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search ",

                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "EditCustomer.aspx/GetBuget",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblbudget").show();
                                        }
                        });
                    }
                });
            }
            else if (currentTab == "#_page_schedulemeetinglist") {
                var leadTable = $('#tblscedulemeeting').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "sDom": 'T<"clear">lfrtip',
                    "tableTools": {

                        "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "EditCustomer.aspx/GetScheduleMeetingList",
                    "columnDefs": [
                    {
                        "orderable": false,
                        "targets": -1,
                        "render": function (data, type, full, meta) {

                            return '<a  href="ModifyMeeting.aspx?Meetingid=' + full[4] + '">Edit</a>';

                        }
                    },
                      {
                          "orderable": false,
                          "targets": -2,
                          "render": function (data, type, full, meta) {

                              return '<a target="_blank" href="CustomercallReport.aspx?Meetingid=' + full[4] + '">View</a>';

                          }
                      },

                { orderable: true, "targets": -2 }


                    ],
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblscedulemeeting").show();
                                        }
                        });
                    }
                });
            }
            else if (currentTab == "#_page_comment") {
                var leadTable = $('#tblcomment').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "sDom": 'T<"clear">lfrtip',
                    "tableTools": {

                        "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "MeetingProcess.aspx/GetComment",
                    "columnDefs": [
                          
                    ],
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblcomment").show();
                                        }
                        });
                    }
                });
            }
            else if (currentTab == "#_page_meeting") {
                var ddlmeetingstatus = 'Close';
                GetMeetingbystatus(ddlmeetingstatus)

            }

            else if (currentTab == "#_page_add_product") {
                $.ajax({
                    url: 'TrackLead.aspx/GetCrossSellProducts',
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (data) {
                        var content = '<h1 class="tab-head">Add Cross Sell Product</h1><div style="margin-left:15px;margin-bottom:20px;">';
                        $.each(data, function () {
                            $.each(this, function (k, v) {
                                content += '<label class="inline-block"><input type="checkbox" id="' + v + '" /><span class="check" style="margin-left:1em"></span>' + k + '</label>';
                                if (v.indexOf('Type1') > 0) {
                                    content += '<input id="txt' + v + '" style="margin-left:5em;display:none;" placeholder="a/c no" onkeydown ="return (event.keyCode!=13);" />'
                                }
                                content += '<br/><br/>';
                            });
                        });

                        content += '<input type="button" class="btn btn-primary btn-sm" value="Add Product" id="btnAddProduct" /></div>';
                        $("#_page_add_product").html(content);
                    }
                });
            }

        }

    });


}

