﻿$(document).ready(function () {

    Get_Customer_Product();
    Get_Customer_forex();
    Get_Customer_budget();

})

var Get_Customer_Product = function()
{
    var productTable = $('#tblProduct').dataTable({
        "oLanguage": {
            "sZeroRecords": "No records to display",
            "sSearch": "Search "
        },
        "iDisplayLength": 15,
        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
        "bSortClasses": false,
        "bStateSave": false,
        "bPaginate": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "CustomerDetail.aspx/GetProductSold",
        "bDeferRender": true,
        "fnServerData": function (sSource, aoData, fnCallback) {
          
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success":
                            function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblProduct").show();
                            }
            });
        }
    });

    ////
   

}



var Get_Customer_forex = function()
{
    var productTable = $('#tblforex').dataTable({
        "oLanguage": {
            "sZeroRecords": "No records to display",
            "sSearch": "Search ",

        },
        "iDisplayLength": 15,
        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
        "bSortClasses": false,
        "bStateSave": false,
        "bPaginate": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "CustomerDetail.aspx/GetForexTrunover",
        "bDeferRender": true,
        "fnServerData": function (sSource, aoData, fnCallback) {
          //  aoData["leadId"] = leadId;
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success":
                            function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblforex").show();
                            }
            });
        }
    });
    ///

}


var Get_Customer_budget = function () {

    var productTable = $('#tblbudget').dataTable({
        "oLanguage": {
            "sZeroRecords": "No records to display",
            "sSearch": "Search ",

        },
        "iDisplayLength": 15,
        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
        "bSortClasses": false,
        "bStateSave": false,
        "bPaginate": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "CustomerDetail.aspx/GetBuget",
        "bDeferRender": true,
        "fnServerData": function (sSource, aoData, fnCallback) {
          //  aoData["leadId"] = leadId;
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success":
                            function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblbudget").show();
                            }
            });
        }
    });

}