﻿$(document).ready(function () {
    GetBranch();
    

    var Id = getParameterByName('Id');
    if (Id != "")
    {
        GetRmDetials(Id);
        $("#btnAddrm").val("Update RM");

    }
    else
    {
        $("#btnAddrm").val("Add RM");
       
    }

    var brnach = getParameterByName('Branchcode');
    if (brnach != "") {
        GetBrnachDetails(brnach);
        $("#btnAddbranch").val("Update Branch");

    }
    else {
        $("#btnAddbranch").val("Add Branch");

    }
   
});

function GetRmDetials(Id) {

    $.ajax({
        url: 'AddBranchrm.aspx/GetRmDetials',
        type: "POST",
        dataType: "json",
        data: "{'empId': '" + Id + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            var user = data.d;
            if (user["id"] == null) {
                ShowDialog("User does not exist.", 100, 300, 'Message', 10, '<span class="icon-info"></span>', true);
            }
            else {
                
                $("#txtPFNO").val(user["pfid"]);
                $("#ddlBranch").val(user["branch_code"]);
                $("#txtPFNO").prop("disabled", true);

               

                $("#txtPhone").val(user["rm_telephone"]);
                $("#txtMobile").val(user["rm_phone"]);
                $("#txtName").val(user["rm_name"]);
            }
        }
    });
}

function GetBrnachDetails(Id) {

    $.ajax({
        url: 'AddBranchrm.aspx/GetBrnachDetails',
        type: "POST",
        dataType: "json",
        data: "{'empId': '" + Id + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            var user = data.d;
            if (user["id"] == null) {
                ShowDialog("User does not exist.", 100, 300, 'Message', 10, '<span class="icon-info"></span>', true);
            }
            else {

                
                $("#txtBranchCode").val(user["branch_code"]);
                $("#txtBranchCode").prop("disabled", true);
                $("#txtBranchName").val(user["branch_name"]);

               
            }
        }
    });
}
var errorMsg= '';
var errorMessage = '';
function validate1() {
     errorMsg = '';
    var BranchName = $("#txtBranchName").val();
    var BranchCode = $("#txtBranchCode").val();

    var result = true;

   
    if (BranchCode == "") {
        $("#txtBranchCode").addClass("form-group has-error");
        errorMsg = errorMsg + "<br/>Please enter branch code";
        result = false;
    }
    else {
        $("#txtBranchCode").removeClass("form-group has-error");
    }
    if (BranchName == "") {
        $("#txtBranchName").addClass("form-group has-error");
        errorMsg = errorMsg + "<br/>Please enter branch name";
        result = false;
    }
    else {
        $("#txtBranchName").removeClass("form-group has-error");
    }
    return result;
}
function validate2() {
     errorMsg = '';
    var BranchName = $("#ddlBranch").val();
    var PFNO = $("#txtPFNO").val();
    var Name = $("#txtName").val();
    var Phone = $("#txtPhone").val();
    var Mobile = $("#txtMobile").val();

    var result = true;

    if (BranchName == "") {
        $("#ddlBranch").addClass("form-group has-error");
        errorMsg = errorMsg + "<br/>Please enter branch name";
        result = false;
    }
    else {
        $("#ddlBranch").removeClass("form-group has-error");
    }
    if (PFNO == "") {
        $("#txtPFNO").addClass("form-group has-error");
        errorMsg = errorMsg + "<br/>Please enter PF NO ";
        result = false;
    }
    else {
        $("#txtPFNO").removeClass("form-group has-error");
    }
    if (Name == "") {
        $("#txtName").addClass("form-group has-error");
        errorMsg = errorMsg + "<br/>Please enter RM Name";
        result = false;
    }
    else {
        $("#txtName").removeClass("form-group has-error");
    }
   
    return result;
}

$("#btnAddbranch").click(function () {
   

    if (validate1() == true) {
        var action;
        if ($("#btnAddbranch").val() == "Update Branch")
            action = "Modify";
        else
            action = "Insert";

       


        window.SBICommon.sendAjaxCall({
            url: 'AddBranchrm.aspx/CreateBranch',
            data: "{'action': '" + action + "','branchcode': '" + $('#txtBranchCode').val() + "','branchname' : '" + $('#txtBranchName').val() + "'}",
            requestType: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',

            success: function (data) {
                if (data.d == "Branch code Already Exists")
                    ShowDialog(data.d, 400, 600, 'Message', 10, '<span class="icon-info"></span>', false);

                else if (data.d == "Branch updated successfully.")
                {
                    var messageContent = "<center><h1>Thank You</h1><p><hr><p><h3>" + data.d + " </h3></p>" +
            "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                    ShowDialog(messageContent, 400, 600, 'Message', 10, '<span class="icon-info"></span>', true, 'AddBranchrm.aspx');


                }
                
                
                else {
                    var messageContent = "<center><h1>Thank You</h1><p>You have successfully created the Branch<hr><p><h3>" + data.d + " </h3></p>" +
             "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                    ShowDialog(messageContent, 400, 600, 'Message', 10, '<span class="icon-info"></span>', true, 'AddBranchrm.aspx');
                }

            },

            showLoader: true
            , complete: function (xhr, data) {
                console.log(xhr.status);
                if (xhr.status == 401)
                    window.location.href = '';
            }
        });


      
    }
    else {
        
        ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }

});

GetBranch = function () {
    
    $.ajax({
        url: 'AddBranchrm.aspx/GetRBranch',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            
            $.each(data, function () {
                $.each(this, function (k, v) {
                    $('#ddlBranch').append('<option value="' + k + '">' + v + '</option>');

                });
            });
        }
    });
}

$("#btnAddrm").click(function () {
    
    var branchid = $('#ddlBranch').val();
    var list = branchid.split("|");
    var brach = list[0].trim();


    if (validate2() == true) {
        
        var action;
        if ($("#btnAddrm").val() == "Update RM")
            action = "Modify";
        else
            action = "Insert";

        window.SBICommon.sendAjaxCall({
            url: 'AddBranchrm.aspx/CreateRM',
            data: "{'action': '" + action + "','ddlbranchs': '" + brach + "','pfno' : '" + $('#txtPFNO').val() + "','name' : '" + $('#txtName').val() + "','phone' : '" + $('#txtPhone').val() + "','mobile' : '" + $('#txtMobile').val() + "'}",
            requestType: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',

            success: function (data) {
                
                if (data.d == "PF NO Already Exists" || data.d == "Mobile No Already Exists")
                {
                    ShowDialog(data.d, 400, 600, 'Message', 10, '<span class="icon-info"></span>', false);

                }
                else if (data.d == "Update successfully.") {
                    var messageContent = "<center><h1>Thank You</h1><p><hr><p><h3>" + data.d + " </h3></p>" +
          "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                    ShowDialog(messageContent, 400, 600, 'Message', 10, '<span class="icon-info"></span>', true, 'AddBranchrm.aspx');

                }
                else {
                    var messageContent = "<center><h1>Thank You</h1><p>You have successfully created the RM <hr><p><h3>" + data.d + " </h3></p>" +
             "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                    ShowDialog(messageContent, 400, 600, 'Message', 10, '<span class="icon-info"></span>', true, 'AddBranchrm.aspx');

                }

            },

            showLoader: true
            , complete: function (xhr, data) {
                console.log(xhr.status);
                if (xhr.status == 401)1
                    window.location.reload();
            }
        });



    }
    else {
        ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }

});