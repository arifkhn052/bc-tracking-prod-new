﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="BlackListBC.aspx.cs" Inherits="BCTrackingWeb.BlackListBC" %>


    <div ng-init="getBcDetails();datetimeBind();">
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-6">
                    <h2 class="title">Black List BC</h2>

                </div>


                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                        <li><a  ui-sref="all">Business Correspondent List</a></li>
                        <li><a >Change Status</a></li>


                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">


                    <!-- /.col-md-6 -->

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    {{lblName}}
                                </div>
                            </div>
                            <div class="panel-body p-20">

                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label for="fileImage" class=" control-label">Date</label>
                                        </div>
                                        <div class="col-sm-4 form-group">

                                            <input type="text" class="form-control formcontrolheight" id="txtDate"
                                                required placeholder="Date">
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="fileImage" class="control-label">
                                                Reason</label>
                                        </div>
                                        <div class="col-sm-4 form-group">
                                           <input type="text" class="form-control formcontrolheight" id="txtReason"
                                                placeholder="Reason">
                                        </div>
                                    </div>
<div class="row">
                                        <div class="col-sm-2">
                                            <label for="fileImage" class=" control-label">Approval Number</label>
                                        </div>
                                        <div class="col-sm-10 form-group">
                                             <input type="text" class="form-control formcontrolheight"
                                                id="txtApprovalNumber" placeholder="Approval Number">
                                        </div>
                                       
                                    </div>
                                    

                               
                             
                                    <div class="formmargin center-block">
                                        <div class="center-block">
                                            <button type="button" id="btnSubmitBlackList"
                                                class="btn btn-success center-block">
                                                Submit
                                            </button>
                                        </div>
                                    </div>

                                </div>


                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-8 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>


  <%--   <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js"></script>  --%>

