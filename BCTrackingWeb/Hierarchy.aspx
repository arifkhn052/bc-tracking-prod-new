﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="Hierarchy.aspx.cs" Inherits="BCTrackingWeb.Hierarchy" %>
          <div >
        <div class="container-fluid" ng-init="GetBanks()">
            <div class="row page-title-div">
                <div class="col-md-12">
                    <h2 class="title">Add Bank  Branch</h2>
                </div>

                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                       <li><a href="#"  ui-sref="home"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#" ui-sref="bankList"> Bank Branch</a></li>
                      

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">

                                </div>
                            </div>
                            <div class="panel-body">

                                <!-- Nav tabs -->
                            
                                </ul>

                                <!-- Tab panes -->
                             
                                    <div role="tabpanel" class="tab-pane active" ng-hide="divhome" >


                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel">

                                                        <div class="panel-body p-20">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">Bank<a style="color:red">*</a></label>
                                                                    </div>
                                                                    <div class="col-sm-7 form-group">                                                                
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBank"></select>
                                                                    </div>
                                                                            <div class="col-sm-2" ng-hide="divbtnBank">
                                                                           <a ui-sref="addUser" class="btn bg-black" role="button" href="#/addbank"><i class="fa fa-plus"></i>Add Bank</a>
                                                                        </div>
                                                                </div>


                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">State<a style="color:red">*</a></label>
                                                                    </div>
                                                                    <div class="col-sm-7 form-group">                                                                
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selState"></select>
                                                                    </div>
                                                                            <div class="col-sm-2" ng-hide="divbtnState">
                                                                           <a class="btn bg-black" role="button" href="#/addBankState/{{Bankid}}"><i class="fa fa-plus"></i>Add State</a>
                                                                        </div>
                                                                </div>


                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">City<a style="color:red">*</a></label>
                                                                    </div>
                                                                    <div class="col-sm-7 form-group">                                                                
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selCity"></select>
                                                                    </div>
                                                                            <div class="col-sm-2" ng-hide="divbtnCity">
                                                                           <a  class="btn bg-black" role="button" href="#/addBankCity/{{StateId}}"><i class="fa fa-plus"></i>Add City</a>
                                                                        </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">District<a style="color:red">*</a></label>
                                                                    </div>
                                                                    <div class="col-sm-7 form-group">                                                                
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selDistrict"></select>
                                                                    </div>
                                                                            <div class="col-sm-2" ng-hide="divbtnDistrict">
                                                                           <a  class="btn bg-black" role="button" href="#/addBankDistrict/{{CityID}}"><i class="fa fa-plus"></i>Add District</a>
                                                                        </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">IFSC Code<a style="color:red">*</a></label>
                                                                    </div>
                                                                    <div class="col-sm-3 form-group">                                                                
                                                                             <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchIFSCCode"
                                                                                   placeholder="IFSC Code">
                                                                    </div>
                                                                      <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">Branch Name<a style="color:red">*</a></label>
                                                                    </div>
                                                                    <div class="col-sm-3 form-group">                                                                
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchName"
                                                                                   placeholder="Branch Name">
                                                                    </div>
                                                                        
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">Address</label>
                                                                    </div>
                                                                    <div class="col-sm-3 form-group">                                                                
                                                                             <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchAddress"
                                                                                   placeholder="Address">
                                                                    </div>
                                                                      <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">Contact</label>
                                                                    </div>
                                                                    <div class="col-sm-3 form-group">                                                                
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchContact"
                                                                                   placeholder="Contact">
                                                                    </div>
                                                                        
                                                                </div>
                                                                

                                                            </div>


                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                    </div>
                                                   
                                                    <!-- /.panel -->
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="formmargin center-block">
                    <div class="center-block">
                        <button type="button"  ng-click="addBranch()" class="btn btn-success center-block">Submit</button>
                    </div>
                </div>

                              

                             
                    
                
                                <!-- /.src-code -->

                            </div>
                              
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>

