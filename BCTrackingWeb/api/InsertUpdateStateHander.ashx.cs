﻿using System;
using BCEntities;
using BCTrackingServices;
using Newtonsoft.Json;
using System.Web;
using BCTrackingBL;
using System.Text;

namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for InsertUpdateStateHander
    /// </summary>
    public class InsertUpdateStateHander : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context) {
          string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;

            if (access == 1)
            {
                try
                {
                    UserEntity user = businessLogic.AuthenticateUserData(userId);
                    // UserEntity user = context.Session[Constants.USERSESSIONID] as UserEntity;
                    if (user != null)
                    {
                        string state = context.Request["state"];
                        if (state == null)
                            throw new Exception("Empty JSON object");
                        State s = JsonConvert.DeserializeObject<State>(state);
                        StateBL bl = new StateBL();

                        string operationMode = Constants.ADDMODE;
                        if (s.StateId != null)
                            operationMode = Constants.UPDATEMODE;

                        if (bl.InsertUpdateState(s, operationMode, user.UserId) <= 0)
                            throw new Exception(String.Format("Adding/Updating {0} failed", s.StateName));
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}