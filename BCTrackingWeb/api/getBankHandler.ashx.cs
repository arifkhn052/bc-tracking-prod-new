﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBI.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for GetBankHandler
    /// </summary>
    public class GetBankHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            if (access == 1)
            {
                UserEntity userdata = businessLogic.AuthenticateUserData(userId);
                string mode = Utils.GetStringValue(context.Request[Constants.MODE]);
                BankCorrespondentBL bl = new BankCorrespondentBL();
                string BankId = Utils.GetStringValue(context.Request["BankId"]);
                string StateId = Utils.GetStringValue(context.Request["StateId"]);
                string districtId = Utils.GetStringValue(context.Request["districtId"]);
                string IfscCode = Utils.GetStringValue(context.Request["IfscCode"]);
                string bankRefNo = Utils.GetStringValue(context.Request["bankRefNo"]);
                if (mode == "All")
                {
                    List<Bank> allCorporates = bl.getBanks();
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "Bank")
                {
                    List<Bank> allCorporates = bl.getBanks(userdata.UserId.ToString());
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "Branch")
                {
                    List<Branch> allCorporates = bl.getBankBranch(BankId);
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "State")
                {
                    List<Circle> allCorporates = bl.getBankState(BankId);
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "City")
                {
                    List<Zone> allCorporates = bl.GetCity(StateId);
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "District")
                {
                    List<Region> allCorporates = bl.GetDistrict(districtId);
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "IFSC")
                {
                    List<Bank> allCorporates = bl.getIfscDetail(IfscCode);
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "bankRefNo")
                {
                    List<Bank> allCorporates = bl.getbankRefNoDetail(bankRefNo);
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "Home")
                {
                    List<BankReport> ReportList = bl.getBankReport(userdata.UserName.ToString());
                    context.Response.Write(JsonConvert.SerializeObject(ReportList));
                }
                if (mode == "FileStatus")
                {
                    List<FileStatus> allCorporates = bl.getFileStatus(userdata.UserId.ToString());
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "ZipStatus")
                {
                    List<FileStatus> allCorporates = bl.getZipStatuss(userdata.UserId.ToString());
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "FileError")
                {
                    List<FileStatus> allCorporates = bl.getFileError(userdata.UserId.ToString());
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
                if (mode == "Districts")
                {
                    List<Branch> allCorporates = bl.getBankBranches(districtId);
                   
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));
                }
            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }




        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}