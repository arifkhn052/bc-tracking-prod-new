﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using System.Configuration;
using BCTrackingBL;
using System.Web.SessionState;
using BCEntities;
using BCTrackingServices;
using System.Text;
using Newtonsoft.Json;
using System.Collections;
using System.IO.Compression;
using Microsoft.Azure; //Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading.Tasks; // Namespace for Blob storage types

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for ZipUploader
    /// </summary>
    public class ZipUploader : IHttpHandler
    {
        string FolderName;
        public void ProcessRequest(HttpContext context)
        {

            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            if (access == 1)
            {
                UserEntity user = businessLogic.AuthenticateUserData(userId);
                //    UserEntity user = context.Session[Constants.USERSESSIONID] as UserEntity;
                string mode = Utils.GetStringValue(context.Request[Constants.MODE]);
                string extension = string.Empty;
                context.Response.ContentType = "text/plain";
                context.Response.Expires = -1;

                foreach (string keyName in context.Request.Files)
                {
                    extension = Path.GetExtension(context.Request.Files[keyName].FileName);
                }

                if (context.Request.Files != null && context.Request.Files.Count > 0 && extension == ".zip")
                {
                    int fileStatus = businessLogic.getZipStatus(user.UserId.ToString());
                    if (fileStatus == 0)
                    {
                        context.Response.Write(JsonConvert.SerializeObject("zip uploading in progress..."));
                    }
                    else if (fileStatus == 1)
                    {
                        context.Response.Write(JsonConvert.SerializeObject("zip uploading in progress..."));
                    }
                    else
                    {


                      
                        HttpPostedFile postedFile = context.Request.Files[0];

                        //string path = System.AppDomain.CurrentDomain.BaseDirectory;
                        //string zippath = System.Web.HttpContext.Current.Server.MapPath("~/ZipUpload/") + Path.GetFileName(postedFile.FileName);
                        //string tempPath = "";

                        //string savepath = Path.Combine(path, "ZipUpload");

                        //string filename = postedFile.FileName;
                        //if (Directory.Exists(savepath))
                        //{
                        //    Directory.Delete(savepath, true);
                        //    Directory.CreateDirectory(savepath);
                        //}
                        //else
                        //{
                        //    Directory.CreateDirectory(savepath);

                        //}
                     //   postedFile.SaveAs(zippath);
                        //context.Response.Write(tempPath + "/" + filename);
                        //context.Response.StatusCode = 200;

                        //  string zipfolder = zippath;
                        //   string extractpath = System.Web.HttpContext.Current.Server.MapPath("~/ZipUpload/");
                        //  System.IO.Compression.ZipFile.ExtractToDirectory(zippath, extractpath);




                        var contentType = postedFile.ContentType;
                        var streamContents = postedFile.InputStream;
                        var blobName = postedFile.FileName;
                        var StoreBlob = "Zip_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + user.UserId.ToString() + "_" + blobName;

                        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                        CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                        CloudBlobContainer container = blobClient.GetContainerReference("dev-raw");
                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(StoreBlob);
                        blockBlob.Properties.ContentType = contentType;
                        blockBlob.UploadFromStream(streamContents);
                        businessLogic.zipStatus(user.UserId.ToString(), 0, blobName);
                        zipupload obj = new zipupload();
                        Task task1 = Task.Run(() => obj.uploadzip(StoreBlob, user.UserId.ToString(), blobName));


                        ////// Retrieve a reference to a container.
                        ////CloudBlobContainer container1 = blobClient.GetContainerReference("dev-raw");
                        ////// Create the container if it doesn't already exist.

                        ////// Retrieve reference to a blob named "myblob".
                        ////CloudBlockBlob blockBlobs = container.GetBlockBlobReference(StoreBlob);
                        ////MemoryStream blobms = new MemoryStream();
                        ////blockBlobs.DownloadToStream(blobms);
                        ////using (var zip = new ZipArchive(blobms))
                        ////{
                        ////    //Each entry here represents an individual file or a folder
                        ////    Task t = Task.Run(() =>
                        ////   {
                        ////       foreach (var entry in zip.Entries)
                        ////       {
                        ////           businessLogic.ZipStatusChange(user.UserId.ToString(), 1);
                        ////           if (entry.Name == "")
                        ////           {
                        ////               FolderName = entry.FullName;
                        ////           }
                        ////           else
                        ////           {

                        ////               string fileName = entry.Name;
                        ////               string[] strArray = fileName.Split('_');
                        ////               string BCCode = strArray[0].ToString();
                        ////               string fileType = strArray[1].ToString();
                        ////               fileType = fileType.Substring(0, 5);

                        ////               BankCorrespondentBL bc = new BankCorrespondentBL();
                        ////               bc.SaveZipPath(FolderName, fileName, "", BCCode, fileType, user.UserId.ToString());

                        ////               using (var stream = entry.Open())
                        ////               {

                        ////                   CloudBlobContainer container11 = blobClient.GetContainerReference("dev-photos");
                        ////                   CloudBlockBlob blockBlob1 = container11.GetBlockBlobReference(fileName);
                        ////                   blockBlob1.Properties.ContentType = contentType;
                        ////                   blockBlob1.UploadFromStream(stream);


                        ////               }
                        ////           }
                        ////       }
                        ////       businessLogic.ZipStatusChange(user.UserId.ToString(), 2);
                        ////   });

                        ////}



                    }
                }
                     
                   

            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}