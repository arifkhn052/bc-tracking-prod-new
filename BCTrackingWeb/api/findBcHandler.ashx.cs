﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBI.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;


namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for findBcHandler
    /// </summary>
    public class findBcHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {  string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");

            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;

            if (access == 1)
            {

                var jsonString = String.Empty;
                context.Request.InputStream.Position = 0;
                using (var inputStream = new StreamReader(context.Request.InputStream))
                {
                    jsonString = inputStream.ReadToEnd();
                }
                JObject jsonObject = JObject.Parse(jsonString);
                var bccode = jsonObject["bccode"].ToString();
                var adharno = jsonObject["adharno"].ToString();
                UserBL logic = new UserBL();
                string authenticatedEntity;
                if (bccode == "")
                {
                    authenticatedEntity = businessLogic.getCheckAadharno(adharno);
                }
                else
                {
                     authenticatedEntity = businessLogic.getCheckbccode(bccode);
                }
               
                if (authenticatedEntity != null)
                {
                    context.Response.Write(JsonConvert.SerializeObject(authenticatedEntity));
                }
                else
                {
                    context.Response.ContentType = "application/json";
                    context.Response.ContentEncoding = System.Text.Encoding.UTF8;
                    context.Response.Write(JsonConvert.SerializeObject("Userid or password is invalid"));
                }
            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}