﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for GetBCHandler
    /// </summary>
    public class GetBCHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            //if (access == 1)
            //{
                UserEntity userdata = businessLogic.AuthenticateUserData(userId);      

                string mode = Utils.GetStringValue(context.Request[Constants.MODE]);
                string user = Utils.GetStringValue(context.Request[Constants.USERMODE]);
                string bank = Utils.GetStringValue(context.Request[Constants.BANKMODE]);
                string branch = Utils.GetStringValue(context.Request[Constants.BRANCHMODE]);
                string userMenu = Utils.GetStringValue(context.Request[Constants.userMenu]);
                int bcId = Utils.GetIntValue(context.Request["bcId"]);
                string adhaarNo = Utils.GetStringValue(context.Request["adhaarNo"]);
                string panno = Utils.GetStringValue(context.Request["panno"]);
                int bankId = Utils.GetIntValue(context.Request["bankId"]);
                int productId = Utils.GetIntValue(context.Request["productId"]);
                int userid = Utils.GetIntValue(context.Request["userId"]);
                int cid = Utils.GetIntValue(context.Request["cid"]);
                int groupId = Utils.GetIntValue(context.Request["groupId"]);                
                string apptDateFrom = context.Request[Constants.APPTDATEFROMFILTER];
                string apptDateTo = context.Request[Constants.APPTDATETOFILTER];
                string createDateFrom = context.Request[Constants.CREATEDATEFROMFILTER];
                string createDateTo = context.Request[Constants.CREATEDATETOFILTER];
                //<<<<<<< .mine
                string bankCircleId = context.Request[Constants.BANKCIRCLEID];
                string stateId = context.Request[Constants.STATEID];
                string regionId = context.Request[Constants.BANKREGIONID];
                string branchId = context.Request[Constants.BRANCHID];
                string bankSsa = context.Request[Constants.BANKSSA];
                string branchCategory = context.Request[Constants.BRANCHCATEGORY];
                string zoneId = context.Request[Constants.BANKZONEID];
                string pinCode = context.Request[Constants.PINCODE];
                //||||||| .r567
                //=======
                string IsBlackListed = context.Request["IsBlackListed"];

                Dictionary<string, string> filters = new Dictionary<string, string>();
                if (!String.IsNullOrEmpty(user))
                    filters.Add(Constants.USERMODE, userid.ToString());
                if (!String.IsNullOrEmpty(bank))
                    filters.Add(Constants.BANKMODE, bankId.ToString());
                if (!String.IsNullOrEmpty(branch))
                    filters.Add(Constants.BRANCHMODE, branch.ToString());
                if (!String.IsNullOrEmpty(apptDateFrom))
                    filters.Add(Constants.APPTDATEFROMFILTER, apptDateFrom);
                if (!String.IsNullOrEmpty(apptDateTo))
                    filters.Add(Constants.APPTDATETOFILTER, apptDateTo);
                //<<<<<<< .mine
                if (!String.IsNullOrEmpty(createDateFrom))
                    filters.Add(Constants.CREATEDATEFROMFILTER, createDateFrom);
                //||||||| .r567
                if (!String.IsNullOrEmpty(createDateFrom))
                    filters.Add(Constants.APPTDATETOFILTER, createDateFrom);
                //=======
                if (!String.IsNullOrEmpty(createDateFrom))
                    filters.Add(Constants.CREATEDATEFROMFILTER, createDateFrom);
                //>>>>>>> .r584
                if (!String.IsNullOrEmpty(createDateTo))
                    filters.Add(Constants.CREATEDATETOFILTER, createDateTo);

                if (!String.IsNullOrEmpty(bankCircleId))
                    filters.Add(Constants.BANKCIRCLEID, bankCircleId);
                if (!String.IsNullOrEmpty(branchId))
                    filters.Add(Constants.BRANCHID, branchId);
                if (!String.IsNullOrEmpty(stateId))
                    filters.Add(Constants.STATEID, stateId);
                if (!String.IsNullOrEmpty(regionId))
                    filters.Add(Constants.BANKREGIONID, regionId);
                //if (!String.IsNullOrEmpty(branchId))
                //    filters.Add(Constants.BRANCHID, branchId);
                if (!String.IsNullOrEmpty(bankSsa))
                    filters.Add(Constants.BANKSSA, bankSsa);
                if (!String.IsNullOrEmpty(branchCategory))
                    filters.Add(Constants.BRANCHCATEGORY, branchCategory);
                if (!String.IsNullOrEmpty(zoneId))
                    filters.Add(Constants.BANKZONEID, zoneId);
                if (!String.IsNullOrEmpty(IsBlackListed))
                    filters.Add(Constants.ISBLACKLISTED, IsBlackListed);

                if (mode == "One" && bcId != Constants.DEFAULTVALUE)
                    filters.Add("bcId", context.Request["bcId"]);

                if (mode == "prospectiveRegistry" && bcId != Constants.DEFAULTVALUE)
                    filters.Add("bcId", context.Request["bcId"]);

              
                if (mode == "One" && adhaarNo != Constants.DEFAULTVALUEs)
                    filters.Add("adhaarNo", context.Request["adhaarNo"]);


                BankCorrespondentBL bl = new BankCorrespondentBL();
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = Encoding.UTF8;

                if (mode == Constants.BANKMODE)
                {
                    BankBL bankBL = new BankBL();
                    List<Bank> allBanks = null;
                    if (bankId > 0)
                    {
                        allBanks = bankBL.GetBanks(bankId,userdata.UserId.ToString());
                        context.Response.Write(JsonConvert.SerializeObject(allBanks[0]));
                    }
                    else
                    {
                        allBanks = bankBL.GetBanks(null, userdata.UserId.ToString());
                        context.Response.Write(JsonConvert.SerializeObject(allBanks));
                    }

                }
                //if (mode == Constants.USERMODE)
                //{
                //    UserBL userBal = new UserBL();
                //    List<UserEntity> allUsers = null;
                //    if (userid > 0)
                //    {
                //        allUsers = userBal.GetUsers(userid);
                //        context.Response.Write(JsonConvert.SerializeObject(allUsers[0]));
                //    }
                //    else
                //    {
                //        allUsers = userBal.GetUsers(null);
                //        context.Response.Write(JsonConvert.SerializeObject(allUsers));
                //    }

                //}
                else if (mode == Constants.USERMODE)
                {
                    UserBL userBal = new UserBL();
                    List<UserEntity> allUsers = null;
                    allUsers = userBal.GetUsers(userid);
                    context.Response.Write(JsonConvert.SerializeObject(allUsers[0]));

                }
                else if (mode == Constants.userMenu)
                {
                    UserBL userBal = new UserBL();
                    List<UserEntity> allUsers = null;
                    allUsers = userBal.getUserMenu(userdata.UserRoles[0].RoleId, userdata.UserName);
                    context.Response.Write(JsonConvert.SerializeObject(allUsers));

                }

                else if (mode == Constants.CORPORATEMODE)
                {
                    List<Corporates> allCorporates = bl.GetCorporates(null);
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates));

                }
                else if (mode == "corporateid")
                {
                    List<Corporates> allCorporates = bl.GetCorporates(cid);
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates[0]));

                }
                else if (mode == "corporateid")
                {
                    List<Corporates> allCorporates = bl.GetCorporates(cid);
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates[0]));

                }
                else if (mode == "Pan")
                {
                    List<Corporates> allCorporates = bl.GetPanCard(panno);
                    context.Response.Write(JsonConvert.SerializeObject(allCorporates[0]));

                }


                else if (mode == Constants.NOTIFICATIONMODE)
                {
                    List<Notification> allNotifications = bl.GetNotifications(null);
                    context.Response.Write(JsonConvert.SerializeObject(allNotifications));

                }
                //else if (mode == Constants.STATESMODE)
                //{
                //    int stateIdLocal;
                //    if (!int.TryParse(context.Request["stateId"], out stateIdLocal))
                //        stateIdLocal = -1;
                //    StateBL stateBl = new StateBL();
                //    List<State> allStates = null;
                //    if (stateIdLocal == -1)
                //    {
                //        allStates = stateBl.GetStates(null);
                //    }
                //    else
                //    {
                //        allStates = stateBl.GetStates(stateIdLocal);
                //    }
                //    context.Response.Write(JsonConvert.SerializeObject(allStates));
                //}
                else if (mode == Constants.PRODUCTMODE)
                {
                    List<BCProducts> products = bl.GetProducts(bcId, productId);
                    context.Response.Write(JsonConvert.SerializeObject(products));
                }

                else if (mode == "userProduct")
                {
                    UserBL userBal = new UserBL();
                    List<UserEntity> allUsers = null;
                    allUsers = userBal.getProducts(productId);
                    context.Response.Write(JsonConvert.SerializeObject(allUsers));

                }
             
                else if (mode == "publicbcdetails")
                {
                    List<BankCorrespondence> bcs = bl.GeBCpulicDetails(bcId.ToString());

                    context.Response.Write(JsonConvert.SerializeObject(bcs));

                }
                    
                else if (mode == "userProductS")
                {
                    UserBL userBal = new UserBL();
                    List<UserEntity> allUsers = null;
                    allUsers = userBal.getUserProducts();
                    context.Response.Write(JsonConvert.SerializeObject(allUsers));
                }

                else if (mode == "groupProduct")
                {
                    UserBL userBal = new UserBL();
                    List<ProductGroupName> allUsers = null;
                    allUsers = userBal.getGroupUserProducts();
                    context.Response.Write(JsonConvert.SerializeObject(allUsers));
                }
                else if (mode == "userGroupProduct")
                {
                    UserBL userBal = new UserBL();
                    List<ProductGroupName> allUsers = null;
                    allUsers = userBal.getGroupProducts(groupId.ToString());
                    context.Response.Write(JsonConvert.SerializeObject(allUsers));
                }
                    
                else if (mode == "Institue")
                {
                    UserBL userBal = new UserBL();
                    List<Institute> allUsers = null;
                    allUsers = userBal.getInstitue();
                    context.Response.Write(JsonConvert.SerializeObject(allUsers));
                }
                 else if (mode == "course")
                {
                    UserBL userBal = new UserBL();
                    List<Coursess> allUsers = null;
                    allUsers = userBal.getCouse();
                    context.Response.Write(JsonConvert.SerializeObject(allUsers));
                }
                else if (mode == "branch")
                {
                    UserBL userBal = new UserBL();
                    List<Branch> allUsers = null;
                    allUsers = userBal.getBranch(userId);
                    context.Response.Write(JsonConvert.SerializeObject(allUsers));
                }
                else if (mode == "prospectiveRegistry")
                {
                    List<BankCorrespondence> bcs = bl.GetprospectiveRegistry(mode, filters);
                    if (mode == "prospectiveRegistry")
                        context.Response.Write(JsonConvert.SerializeObject(bcs[0]));
                    else
                        context.Response.Write(JsonConvert.SerializeObject(bcs));
              
                }
                    

                else
                {

                    List<BankCorrespondence> bcs = bl.GetBankCorrespondents(mode, filters);
                    if (mode == "One")
                        context.Response.Write(JsonConvert.SerializeObject(bcs[0]));
                    else
                        context.Response.Write(JsonConvert.SerializeObject(bcs));
                }



            //}
            //else
            //{
            //    context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            //}
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}