﻿using BCTrackingBL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for LogoutHandler
    /// </summary>
    public class LogoutHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            string userId = context.Request["userId"];
            string TokenId = context.Request["TokenId"];
            UserBL businessLogic = new UserBL();
            int access = businessLogic.logout(userId, TokenId);
            if (access == 1)
            {
                context.Response.Write(JsonConvert.SerializeObject("successfully logout"));

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}