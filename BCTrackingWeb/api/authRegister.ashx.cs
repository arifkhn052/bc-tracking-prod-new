﻿using BCTrackingBL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for authRegister
    /// </summary>
    public class authRegister : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var jsonString = String.Empty;
            context.Request.InputStream.Position = 0;
            using (var inputStream = new StreamReader(context.Request.InputStream))
            {
                jsonString = inputStream.ReadToEnd();
            }
            JObject jsonObject = JObject.Parse(jsonString);
            var userId = jsonObject["userId"].ToString();
            var password = jsonObject["password"].ToString();
            var TokenId = Guid.NewGuid().ToString();
            UserBL logic = new UserBL();
            var userids = logic.createToken(userId, password, TokenId);
            List<userToken> AllData = null;
            userToken token = null;          
            token = new userToken();
            token.userId = userids;
            token.TokenId = TokenId;       
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            context.Response.Write(JsonConvert.SerializeObject(token));
        }

        public class userToken
        {

            public string userId { get; set; }
            public string TokenId { get; set; }

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}