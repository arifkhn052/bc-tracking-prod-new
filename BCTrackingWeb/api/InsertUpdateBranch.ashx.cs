﻿using System;
using BCEntities;
using BCTrackingBL;
using Newtonsoft.Json;
using BCTrackingServices;
using System.Web;
using System.Text;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for InsertUpdateBranch
    /// </summary>
    public class InsertUpdateBranch : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            if (access == 1)
            {
                try
                {
                    UserEntity userEntity = businessLogic.AuthenticateUserData(userId);
                    if (userEntity != null)
                    {
                        string postedData = context.Request["branch"];
                        Branch Branch = JsonConvert.DeserializeObject<Branch>(postedData);
                        BankBL logic = new BankBL();

                        string mode = Constants.ADDMODE;
                        //if (bank.BankId != 0)
                        //    mode = Constants.UPDATEMODE;

                        if (logic.InsertUpdateBrnach(Branch, mode, userEntity.UserId) != 0)
                            throw new Exception(String.Format("Adding bank {0} failed", Branch.BranchName));
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}