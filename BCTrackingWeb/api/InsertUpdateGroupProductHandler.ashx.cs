﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for InsertUpdateGroupProductHandler
    /// </summary>
    public class InsertUpdateGroupProductHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string userId = context.Request.Headers.Get("userId");
                string TokenId = context.Request.Headers.Get("TokenId");
                UserBL businessLogic = new UserBL();
                int access = businessLogic.getToken(userId, TokenId);
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = Encoding.UTF8;

                if (access == 1)
                {
                    UserEntity userEntity = businessLogic.AuthenticateUserData(userId);
                    if (userEntity != null)
                    {
                        string postdata = context.Request["ProductGroupName"];
                        ProductGroupName ProductGroupName = JsonConvert.DeserializeObject<ProductGroupName>(postdata);

                    //    string postdata1 = context.Request["ProductGroup"];
                        ProductGroup ProductGroup = JsonConvert.DeserializeObject<ProductGroup>(postdata);
                        List<ProductGroup> allBCs = null;
                        string rtnexps = context.Request["ProductGroup"];
                        if (!String.IsNullOrEmpty(rtnexps))
                        {
                            rtnexps = HttpUtility.UrlDecode(rtnexps);//new line
                            List<ProductGroup> ProductGroups = JsonConvert.DeserializeObject<List<ProductGroup>>(rtnexps);
                            ProductGroupName.ProductGrouplist = ProductGroups;
                        }


                        BankBL logic = new BankBL();
                        if (logic.insertUpdateGroupProduct(ProductGroupName, ProductGroup, userEntity.UserId) != 0)
                            throw new Exception(String.Format("Adding group Product {0} failed", ProductGroup));
                    }
                }
                else
                {
                    context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}