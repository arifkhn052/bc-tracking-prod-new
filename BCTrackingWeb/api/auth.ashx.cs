﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBI.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for auth
    /// </summary>
    public class auth : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            var jsonString = String.Empty;
            context.Request.InputStream.Position = 0;
            using (var inputStream = new StreamReader(context.Request.InputStream))
            {
                jsonString = inputStream.ReadToEnd();
            }
            JObject jsonObject = JObject.Parse(jsonString);
            var userId = jsonObject["userId"].ToString();
            var password = jsonObject["password"].ToString();
            UserBL businessLogic = new UserBL();
            UserBL logic = new UserBL();
            UserEntity authenticatedEntity = businessLogic.AuthenticateUser(userId, password);
            AzureStorages obj = new AzureStorages();
          //  Task.Run((Action)obj.ReadExcel("",1,""));
        //    var task = Task.Run(() => obj.ReadExcel("",1,""));
            Task.WaitAll();

            if (authenticatedEntity != null)
            {
                var TokenId = Guid.NewGuid().ToString();            
                LoggedInUserInfo loggedInUser = new LoggedInUserInfo();
                loggedInUser.userrole_id = authenticatedEntity.UserId;
                 context.Session["userid"] = userId;   
                var userids = logic.createToken(userId, password, TokenId);
                List<userToken> AllData = null;
                userToken token = null;
                token = new userToken();
                token.userId = userids;
                token.TokenId = TokenId;
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = System.Text.Encoding.UTF8;
                context.Response.Write(JsonConvert.SerializeObject(token));
            }
            else
            {
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = System.Text.Encoding.UTF8;
                context.Response.Write(JsonConvert.SerializeObject("Userid or password is invalid"));
            }
           
        }
        public class userToken
        {
            public string userId { get; set; }
            public string TokenId { get; set; }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}