﻿using BCEntities;
using BCTrackingBL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for BCList
    /// </summary>
    public class BCList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");

            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            //if (access == 1)
            //{

            var jsonString = String.Empty;
            context.Request.InputStream.Position = 0;
            using (var inputStream = new StreamReader(context.Request.InputStream))
            {
                jsonString = inputStream.ReadToEnd();
            }
            JObject jsonObject = JObject.Parse(jsonString);
            string latitude = jsonObject["latitude"].ToString();
            string longitude = jsonObject["longitude"].ToString(); 
            UserBL logic = new UserBL();
            List<BankCorrespondence> state = businessLogic.GetBCListByLatLong(latitude, longitude);
            if (state != null)
            {
                context.Response.Write(JsonConvert.SerializeObject(state));
            }
            else
            {
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = System.Text.Encoding.UTF8;
                context.Response.Write(JsonConvert.SerializeObject("Userid or password is invalid"));
            }
            //}
            //else
            //{
            //    context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            //}


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}