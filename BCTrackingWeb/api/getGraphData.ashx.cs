﻿using BCEntities;
using BCTrackingBL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBI.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for getGraphData
    /// </summary>
    public class getGraphData : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");

            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            if (access == 1)
            {

                var jsonString = String.Empty;
                context.Request.InputStream.Position = 0;
                using (var inputStream = new StreamReader(context.Request.InputStream))
                {
                    jsonString = inputStream.ReadToEnd();
                }
                JObject jsonObject = JObject.Parse(jsonString);
                string reportType = jsonObject["reportType"].ToString();
                string stateId = jsonObject["stateId"].ToString();
                string districtId = jsonObject["districtId"].ToString();
                string subdistrictId = jsonObject["subdistrictId"].ToString();
                string villageId = jsonObject["villageId"].ToString();
                string startDate = jsonObject["startDate"].ToString();
                string endDate = jsonObject["endDate"].ToString();
                UserBL logic = new UserBL();
                DataTable reports = businessLogic.GetReportData(stateId, districtId, subdistrictId, villageId,reportType,startDate,endDate);
                if (reports != null)
                {
                    context.Response.Write(JsonConvert.SerializeObject(reports));
                }
                else
                {
                    context.Response.ContentType = "application/json";
                    context.Response.ContentEncoding = System.Text.Encoding.UTF8;
                    context.Response.Write(JsonConvert.SerializeObject("Userid or password is invalid"));
                }
            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}