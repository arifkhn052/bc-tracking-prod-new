﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for InsertUpdateUserHandler
    /// </summary>
    public class InsertUpdateUserHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        { string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            if (access == 1)
            {
                try
                {
                    UserEntity userEntity = businessLogic.AuthenticateUserData(userId);
                    if (userEntity != null)
                    {
                        string postedData = context.Request["user"];
                        UserEntity user = JsonConvert.DeserializeObject<UserEntity>(postedData);
                        UserBL logic = new UserBL();
                        string mode = Constants.ADDMODE;
                        if (user.UserId != 0)
                            mode = Constants.UPDATEMODE;


                        int row;
                        if (user.IsActive == 1)
                            row = 0;
                        else
                        {
                           
                            if(mode=="Update")
                                 row = 2;
                            else
                            {
                                 row = businessLogic.getUserAviable(user.UserName);
                            }
                        }

                           
                        if(row==0)
                        {
                          
                            if (logic.InsertUpdateUser(user, mode, userEntity.UserId) != 0)
                                context.Response.Write(JsonConvert.SerializeObject("User"));
                                //throw new Exception(String.Format("Adding bank {0} failed", user.UserName));
                            else
                                context.Response.Write(JsonConvert.SerializeObject("User"));
                        }
                        else  if(row==2)
                        {
                            if (logic.InsertUpdateUser(user, mode, userEntity.UserId) != 0)
                                context.Response.Write(JsonConvert.SerializeObject("User"));
                            //throw new Exception(String.Format("Adding bank {0} failed", user.UserName));
                            else
                                context.Response.Write(JsonConvert.SerializeObject("User"));
                        }
                        else
                        {
                            context.Response.Write(JsonConvert.SerializeObject("UserId already exists."));
                        }
                       

                   
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}