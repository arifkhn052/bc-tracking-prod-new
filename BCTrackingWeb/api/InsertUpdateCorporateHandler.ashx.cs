﻿using System;
using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web;
using System.Web.SessionState;

namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for 
    /// </summary>
    public class InsertUpdateCorporateHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context) {
            try {
                UserEntity userEntity = context.Session[Constants.USERSESSIONID] as UserEntity;
                if (userEntity != null) {
                    string postedData = context.Request["info"];
                    Corporates corporate = JsonConvert.DeserializeObject<Corporates>(postedData);
                    CorporatesBL logic = new CorporatesBL();

                    string mode = Constants.ADDMODE;
                    if (corporate.CorporateId != 0)
                        mode = Constants.UPDATEMODE;

                    if (logic.InsertUpdateCorporates(corporate, mode, userEntity.UserId) != 0)
                        throw new Exception(String.Format("Adding corporate {0} failed",corporate.CorporateName));
                }
            }
            catch (Exception ex) {
                string message = ex.Message;
            }            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}