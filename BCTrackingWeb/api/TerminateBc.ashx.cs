﻿using BCEntities;
using BCTrackingBL;
using BCTrackingServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace BCTrackingWeb.api
{
    /// <summary>
    /// Summary description for TerminateBc
    /// </summary>
    public class TerminateBc : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string userId = context.Request.Headers.Get("userId");
            string TokenId = context.Request.Headers.Get("TokenId");
            UserBL businessLogic = new UserBL();
            int access = businessLogic.getToken(userId, TokenId);
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            DateTime From_Date;
            if (access == 1)
            {
            
                UserEntity user = businessLogic.AuthenticateUserData(userId);
                bool isvalid = true;
                DataSet result = new DataSet();
                var jsonString = String.Empty;
                context.Request.InputStream.Position = 0;
                using (var inputStream = new StreamReader(context.Request.InputStream))
                {
                    jsonString = inputStream.ReadToEnd();
                }
                JObject jsonObject = JObject.Parse(jsonString);

                var adhaarNo = validation.required(jsonObject["adhaarno"].ToString());
                if (adhaarNo == "")
                {
                    isvalid = false;
                    result = validation.errorFinder("AadharCard");
                }
                var FromDate = validation.required(jsonObject["dateofterminate"].ToString());
                if (FromDate == "")
                {
                    isvalid = false;
                    result = validation.errorFinder("DOT");
                } 
                else
                {
                     
                  
                }              
               
                var reason = jsonObject["reason"].ToString(); 
                if(isvalid==false)
                {
                    string JSONString = string.Empty;
                    JSONString = JsonConvert.SerializeObject(result.Tables[0]);
                    context.Response.Write(JSONString);

                }
                else
                {
                    UserBL logic = new UserBL();
                    From_Date = DateTime.ParseExact(string.IsNullOrEmpty(FromDate) ? "01/01/1970" : FromDate.Replace(".", "/"), "dd/MM/yyyy", null);
                    int value = businessLogic.terminateBC(adhaarNo, From_Date, reason, user.UserId);
                    if(value==1)
                    {
                        context.Response.Write(JsonConvert.SerializeObject("BC terminated successfully."));
                    }
                    else
                    {
                        context.Response.Write(JsonConvert.SerializeObject("Adhaar number is not exists."));
                    }
                }         
               

            }
            else
            {
                context.Response.Write(JsonConvert.SerializeObject("You are not authorized for this token"));
            }




        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}