﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using BCTrackingBL;
using BCEntities;

namespace BCTrackingWeb
{
    public partial class BankList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string getBank()
        {
            // Paging parameters:
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);

            // Sorting parameters
            var iSortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var iSortDir = HttpContext.Current.Request.Params["sSortDir_0"];
            // Search parameters
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToLower();
            var userId = HttpContext.Current.Request.Params["userId"].ToLower();
            int totalRecords = 0;
            if (System.Web.HttpContext.Current.Session["UserInfo"] != null) ;
            var leadList = Common.getBank(iDisplayStart, iDisplayLength, iSortCol, iSortDir, sSearch, out totalRecords, userId);

            Func<Bank, object> order = p =>
            {
                if (iSortCol == 0)
                {
                    return p.BankId;
                }

                else if (iSortCol == 1)
                {
                    return p.BankName;
                }
                else if (iSortCol == 2)
                {
                    return p.Address;
                }
                else if (iSortCol == 3)
                {
                    return p.ContactNumber;
                }
                else if (iSortCol == 4)
                {
                    return p.Email;
                }

                return p.BankId;
            };

            //    // Define the order direction based on the iSortDir parameter
            if ("desc" == iSortDir)
            {
                leadList = leadList.OrderByDescending(order).ToList<Bank>();
            }
            else
            {
                leadList = leadList.OrderBy(order).ToList<Bank>();
            }
            var finalResult = leadList
                    .Select(p => new[] { p.BankId.ToString(), p.BankName, p.Address, p.ContactNumber, p.Email, });

            int count = finalResult.Count();

            // prepare an anonymous object for JSON serialization
            var result = new
            {
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = finalResult
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            return json;
        }
      
    }
}