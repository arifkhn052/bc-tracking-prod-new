﻿using System;
using BCEntities;
using BCTrackingBL;
using Newtonsoft.Json;
using BCTrackingServices;
using System.Web;

namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for GetBankHandler
    /// </summary>
    public class GetBankHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context) {
            try {
                UserEntity userEntity = context.Session[Constants.USERSESSIONID] as UserEntity;
                if (userEntity != null) {
                    string postedData = context.Request["info"];
                    Banks bank = JsonConvert.DeserializeObject<Banks>(postedData);
                    BankBL logic = new BankBL();
                    
                    string mode = Constants.ADDMODE;
                    if (bank.BankId != 0)
                        mode = Constants.UPDATEMODE;

                    if (logic.InsertUpdateBank(bank, mode, userEntity.UserId) != 0)    
                        throw new Exception(String.Format("Adding bank {0} failed", bank.BankName));
                }
            }
            catch (Exception ex) {
                string message = ex.Message;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}