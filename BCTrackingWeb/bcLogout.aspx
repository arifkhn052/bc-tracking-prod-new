﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bcLogout.aspx.cs" Inherits="BCTrackingWeb.bcLogout" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div >
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">                                  
                                   
                                </div>
                             
                             
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                  
                                </div>
                                    <h6 class="text-center" class="title">Do you want to logout</h6>
                                <br />
                                <div class="text-center" >
                                    <button type="button" class="btn btn-danger" ng-click="logout()">
                                           Logout
                                        </button>
                                     
                                </div>
                              
                                <!-- /.col-md-6 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        
                        <!-- /.section -->

                    </div>
    </form>
</body>
</html>
