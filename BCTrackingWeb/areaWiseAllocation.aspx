﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="areaWiseAllocation.aspx.cs" Inherits="BCTrackingWeb.areaWiseAllocation" %>

    <%-- <div class="main-page">--%>
    <div class="container-fluid" ng-init="datetimeBind();getAreaWiseData()">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">List of Area wise BC Allocation</h2>

            </div>

            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="areaWiseAllocation.aspx"><span>Area wise BC Allocation</span></a></li>

                </ul>
            </div>

            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid">

            <div class="row">

                <div class="col-md-12">
                    <div class="panel">

                        <div class="panel-body p-20">


                            <div class="panel-body">

                                <div class="formmargin">

                                
                                    <div class="col-md-5">
                                          <strong>Start Date</strong>
                                        <input type='text' class="form-control" id="createFromDate" placeholder="dd/mm/yyyy" autocomplete="off" />

                                    </div>
                                    
                                    <div class="col-md-5">
                                          <strong>End  Date</strong>
                                        <input type='text' class="form-control" id="EndDate" placeholder="dd/mm/yyyy" autocomplete="off" />

                                    </div>
                                    <div class="col-md-2">
                                        <br />
                                        <button type="button" class="btn btn-success" ng-click="getAreaWiseDatafilter()">Show List</button>

                                    </div>





                                </div>



                            </div>


                            <hr />
                            <table id="tblAreaWiseAllocation" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Area ID</th>
                                        <th>BC Count</th>
                                        <th>Last update Date</th>

                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>

            </div>
        </div>

    </section>


    <%--</div>

    
    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/bc.js" type="text/javascript"></script>
    <script src="js/report.js" type="text/javascript"></script>
    <script src="js/getbc.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            var t = $('#example').DataTable({});


        });
    </script>--%>

