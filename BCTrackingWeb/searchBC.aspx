﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="searchBC.aspx.cs" Inherits="BCTrackingWeb.searchBC" %>

    <div>
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-6">
                    <h2 class="title">Track - Add BC</h2>

                </div>


                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                        <li><a>Track - Add BC</a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">


                    <!-- /.col-md-6 -->

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">
                                </div>
                            </div>
                            <div class="panel-body p-20">
                                <div class="panel-body">

                                   <div class="formmargin">

                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                <input type="password" ng-keypress="filterValue($event)" class="form-control" required maxlength="12" id="txtaadhaar" placeholder="Enter Aadharcard No">
                                            </div>
                                         
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                <input type="password" ng-keypress="filterValue($event)" class="form-control" required maxlength="12" id="txtretypeaadhaar" placeholder="Re Enter Aadharcard No">
                                            </div>
                                         
                                        </div>
                                      
                                        <div class="col-sm-1">
                                            <button type="button" class="btn btn-success"  OnClientClick="return EncryptPassword();" id="btnSearch">Search</button>

                                        </div>
                                         <div class="col-sm-9">
                                            <div class="input-group">
                                               <label id="errormsg" style="color:red">Aadhar Card Mismatch!!</label>
                                            </div>
                                         
                                        </div>

                                    </div>



                                </div>
                            </div>

                        </div>

                        <div class="panel" ng-hide="notexists">
                            <div class="panel-heading">
                                <div class="panel-title">
                                  There is no existing BC with this Aadhaar Id. Do you want to create a new BC?
                                </div>
                            </div>
                            <div class="panel-body p-20">
                                <div class="panel-body">
                                    <div class="formmargin">                                        
                                        <div class="col-sm-1">
                                        <a type="button"  href="#/singleEntry/{{adharcharNo}}" + data + "" class="btn btn-danger">Add Single Entry</a>

                                        </div>

                                    </div>



                                </div>
                            </div>

                        </div>

                        <div class="panel" ng-hide="notterminate">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    This BC is currently associated with {{bankname}} Bank.
                                </div>
                            </div>
                            <div class="panel-body p-20">
                                <div class="panel-body">
                                    <div class="">   
                                         
                                                                 
                                        <div>
                                             Details of this BC :-                                          <a type="button" ng-hide="btnviewdetails"  href="#/viewallCorresponds/{{bcid}}" + data + "" class="btn btn-danger">View Details</a>
                                           <br />  Name :- {{BcName}}      <br />  Date :- {{DOB}}      <br /> Village :- {{VillageName}}      <br />



                                        </div>
                                        <div>
                                            If you want add this BC in your Bank, please ask the BC to get a Termination from the Current Bank. 
                                        </div>
                                    </div>
                                    



                                </div>
                            </div>

                        </div>

                        <div class="panel" ng-hide="terminated">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    This BC exists in the system and can be allocated.
                                </div>
                            </div>
                            <div class="panel-body p-20">
                                <div class="panel-body">
                                    <div class="formmargin">                                        
                                        <div class="">
                                          
                                              Details of this BC :-   
                                           <br />  Name :- {{BcName}}      <br />  Date :- {{DOB}}      <br /> Village :- {{VillageName}}      <br />

                                        </div>
                                        <br />
                                         <div class="">
                                              To allocate the BC to your Bank
                                           <a type="button" href="#/editallCorresponds/{{bcid}}" class="btn btn-danger">Allocate</a>

                                             </div>
                                    </div>



                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>

    <%--   <br/><br/><br/><br/><br/>

    <div class="container page-content">

        <div class="loader"></div>
        <div>
            <ul class="breadcrumbs mini">
                <li><a href="Home.aspx"><span class="icon mif-home"></span></a></li>
                <li><a href="AddBCIndex.aspx"><span>Add Business Correspondents</span></a></li>
                <li><a href="bulkupload.aspx"><span>Bulk Upload</span></a></li>

            </ul>
        </div>

        <br>

        <div class="example">
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-4 control-label">Select File</label>
                    <div class="col-sm-8">
                        <input type="file" id="uploadcsv">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Select Zip File</label>
                    <div class="col-sm-8">
                        <input type="file" id="imgupload"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="col-sm-4 control-label">Replace Existing</label>
                    <div class="radio col-sm-8">
                        <label>
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                            Replace the Existing Data
                        </label>
                        <label>
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                            Do not replace the existing Data
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-10">
                        <button type="submit" class="btn btn-default" id="btnsubmit">Submit</button>
                    </div>
                </div>
            </form>
        </div>

        <br>

    </div>--%>
      <script>
          function EncryptPassword() {
               
              $('#txtaadhaar').val($.md5($('#txtaadhaar').val()));
          }
    </script>

