﻿bctrackApp.controller("addCorporateController", function ($scope, $state, $cookies, $timeout) {
   
    var b = $state.params.corporateId;
    var cid = '';
    if (b != undefined)
        cid = parseInt(b);
    else
        bankId = 0;
    if (cid != 0) {
        
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            contentType: 'application/json',
            url: "api/GetBCHandler.ashx?mode=corporateid&cid=" + cid,
            async: false,
            success: function (c) {
                 
                $('#txtName').val(c.CorporateName);
                $('#txtEmail').val(c.Email);
                $('#Select1').val(c.CorporateType);
                $('#txtAddress').val(c.Address);
                $('#txtContactNo').val(c.ContactNumber);
                $('#txtPanNO').val(c.PanNo);
                $('#txtWebsite').val(c.WebSite);
                $('#txtContactPerson').val(c.ContactPerson);
                $('#txtContactDesignation').val(c.ContactDesignation);
              //  $('#txtNoOfComplaint').val(c.NoOfComplaint);

            }
        });
    }
    var errorMessage = "";
    function validate() {

        errorMsg = '';

        var email = $('#txtEmail').val();
        var phone = $('#txtContactNo').val();
        var panNo = $("#txtPanNO").val().toUpperCase();
        var corporateName = $('#txtName').val();
        var Type = $('#Select1').val();


        var result = true;
        if (corporateName == "") {
            $('#divPhone').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Corporate Name.";
            result = false;
        }
        if (Type == -1) {
            $('#divPhone').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select Type.";
            result = false;
        }


        if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
            $('#divPhone').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Valid Mobile Number.";
            result = false;
        }
        else {
            $("#divPhone").removeClass("form-group has-error");
        }
        if (!IsEmail(email)) {
            $('#txtEmail').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter valid email Id.";
            result = false;
        }
        else {

        }
        if (!IsPanNumber(panNo) || panNo == "") {
            $('#txtPanNO').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter valid Pan Number.";
            result = false;
        }
        else {

        }



        return result;
    }
    function IsPanNumber(number) {
        var regex = /^([A-Z]{5}\d{4}[A-Z]{1})+$/;
        return regex.test(number);
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    $scope.btnName = "Update";
    //////////////////////////////////////////////////////////// Add Corporate \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    $('#btnAddCorporate').on('click', function () {
        
        var corporate = new Object();
        if (validate() == true) {
            $("#btnAddCorporate").prop("disabled", true);
            $timeout(function () { $scope.btnName = "Loading..."; }, 0);
            var corporate = new Object();
            corporate.CorporateId= $state.params.corporateId;
            corporate.CorporateName = $('#txtName').val();
            corporate.CorporateType = $('#Select1').val();

            corporate.Email = $('#txtEmail').val();
            corporate.Address = $('#txtAddress').val();
            corporate.PanNo = $("#txtPanNO").val().toUpperCase();
            corporate.WebSite = $('#txtWebsite').val();

            corporate.ContactNumber = $('#txtContactNo').val();
            corporate.ContactPerson = $('#txtContactPerson').val();
            corporate.ContactDesignation = $('#txtContactDesignation').val();
            corporate.NoOfComplaint = 0;

            $.ajax({
                url: "api/GetCorporateHandler.ashx",
                method: 'POST',
                datatype: 'json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                data: { 'info': JSON.stringify(corporate) },
                success: function () {
                    $("#btnAddCorporate").prop("disabled", false);
                    alert('Corporate details update successfully.');
                    window.location.href = "#/corporatelist";
              
                },
                error: function (xhr, errorString, errorMessage) {
                    $("#btnAddCorporate").prop("disabled", false);
                    alert('Corporate details update successfully.');
                    window.location.href = "#/corporatelist";
                }
            });
        }
        else {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
        });

        $('#btnDelete').on('click', function () {

            if (confirm("Do you want to delete?")) {

                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    headers: {
                        "userId": $cookies.get("userId"),
                        "TokenId": $cookies.get("TokenId")
                    },
                    url: "api/DeleteBcHandler.ashx?mode=Corporate&corporateId=" + cid,
                    async: false,
                    success: function () {
                        alert('Corporate details deleted successfully.');
                        window.location.href = "#/corporatelist";
                    },
                    error: function (xhr, errorString, errorMessage) {
                        alert('Corporate details deleted successfully.');
                        window.location.href = "#/corporatelist";
                    }
                });
            }

        });
        $('#txtPanNO').on('change', function () {

            var pan = $('#txtPanNO').val();

            $.ajax({
                url: "api/GetBCHandler.ashx?mode=Pan&panno=" + pan,
                method: 'POST',
                datatype: 'json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },

                success: function (data) {

                    if (data.PanNo == 1) {
                        $('#txtPanNO').val('');
                        alert('Pan no is already exists.')
                    }
                    else {

                    }

                },
                error: function (xmlHttpRequest, errorString, errorMessage) {

                }
            });
        });
});