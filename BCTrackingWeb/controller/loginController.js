﻿'use strict';

bctrackApp.controller('loginController', [
    '$scope',
    '$location',
    '$window',
    '$log',
    '$rootScope',
    '$state',
    'authenticationService',
    function ($scope, $location, $window, $log, $rootScope, $state, authenticationService) {
        $scope.myAlert = true;
        $scope.user = {
            "userId": "",
            "password": ""
        };
        $scope.error = "";

        $scope.login = function (isValid) {
            
            if (isValid) {
                
                var pass = $.md5($scope.user.password)
                $scope.user.password = pass;
                delete $scope.user.password;
                $scope.user.password = pass;
                $rootScope.$broadcast('showLoader');
                authenticationService.authenticateUser($scope.user)
                
                    .then(function (successData) {
                        
                        console.log(successData);
                        if (successData.hasOwnProperty("TokenId")) {
                           $rootScope.$broadcast('hideLoader');
                           $state.go("home");
                               
                        }
                        else {
                            $scope.myAlert = false;
                          //  alert(successData);
                            $rootScope.$broadcast('hideLoader');
                            
                        }


                    }, function (err) {
                        $rootScope.$broadcast('hideLoader');

                    });
            }

        }


        //$('#btnLogin').on('click', function () {
        //    
        //    $.ajax({
        //        type: "POST",
        //        contentType: "application/json; charset=utf-8",
        //        url: "api/auth.ashx",
        //        data: '{ "userId": "' + $('#txtUsername').val() + '","password": "' + $('#txtPassword').val() + '"}',
        //        dataType: "json",
        //        success: function (data) {

        //            if (data == "Userid or password is invalid") {
        //                alert(data);
        //            }
        //            else {
        //                sessionStorage.setItem("userId", data.userId);
        //                sessionStorage.setItem("TokenId", data.TokenId);
        //                window.location.href = "#/home";
        //            }


        //        },
        //        error: function (xmlHttpRequest, errorString, errorMessage) {
        //            alert(errorMessage);
        //        }
        //        //url: "api/auth.ashx",
        //        //method: "POST",
        //        //Header:'Content-Type: application/json; charset=UTF8',

        //        //data: { userId: $('#txtUsername').val(), password: $('#txtPassword').val() },
        //        //success: function (data) {

        //        //    alert('User Saved!');
        //        //},
        //        //error: function (xmlHttpRequest, errorString, errorMessage) {
        //        //    alert(errorMessage);
        //        //}
        //    });
        //});


    }
]);
