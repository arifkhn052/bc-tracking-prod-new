﻿bctrackApp.controller("summaryChartController", function ($scope, $cookies) {
    $scope.datetimeBind = function () {
        //tabing();
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        var firstDay = '01/' + mm + '/' + yyyy;
        $("#txtStartDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: firstDay
        });
        $("#txtStartDate").val(firstDay);

        $("#txtEndDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtEndDate").val(today);
      



    }


    var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    var origYearFrom = 2017;
    var origMonthFrom = 0;
    var range = [];
    var secondrange = [];
    var monthRange = 1;
    var apptMode = 'AppointmentMode';
    var createMode = 'CreateMode';
    blackListMode = 'BlackList';
    var yearTo = 2017;
    let allBanks = new Array();

    $('#genDataAppointDates').on('click', function () {
        
        var from = $("#txtStartDate").val().split("/");
        strtDates = from[2] + "-" + from[1] + "-" + from[0];
        var strtDate = strtDates;

        var eNdfrom = $("#txtEndDate").val().split("/");
        EndDates = eNdfrom[2] + "-" + eNdfrom[1] + "-" + eNdfrom[0];
        var endDate = EndDates;

        //var endDate = $('#txtEndDate').val();
        var state = $('#selPLState').val();
        var district = $('#selPLDistrict').val();
        var subDistrict = $('#selPLSubDistrict').val();
        var village = $('#selPLdVillages').val();
        var type = 1;

        mode = 'Allocated';
        var bcList = GetBC(state, district, subDistrict, village, type, strtDate, endDate);

        var uprwaliArray = [];
        var neecheWaliArray = [];
        uprwaliArray = bcList.map(function (value, index, array) {
            return value.bcCount;

        })
        neecheWaliArray = bcList.map(function (value, index, array) {
            return value.timespan;

        })
        Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'BC Allotment in the Bank'
            },


            xAxis: {
                categories: neecheWaliArray,
                title: {
                    text: 'Date'
                }
            },

            series: [{
                name: "Allocacted BC: " + new Date(strtDate) .getYear()+ "-"+ new Date(endDate).getYear(),
                data: uprwaliArray
            }]
        });
        

        //var dtTo = new Date(bcList[bcList.length - 1].AppointmentDate);
        //var dtFrom = new Date(bcList[0].AppointmentDate);

        //CreateChartData(bcList, dtFrom, dtTo, apptMode);
        //PlotChart('BC allocations ' + origYearFrom + ' - ' + yearTo);

    });

    function GetBC(state, district, subDistrict, village, type, strtDate, endDate) {
        var requrl = "api/getGraphData.ashx";
        if (state == null || state == '')
        {
            state = '-1';
        }
        if (district == null || district == '') {
            district = '-1';
        }
        if (subDistrict == null || subDistrict == '') {
            subDistrict = '-1';
        }
        if (village == null || village == '') {
            village = '-1';
        }

        var bcl = [];

        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: '{ "stateId": "' + state + '","districtId": "' + district + '","subdistrictId": "' + subDistrict + '","villageId": "' + village + '","startDate": "' + strtDate + '","endDate": "' + endDate + '","reportType": "' + type + '"}',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            contentType: 'application/json',
            url: requrl,
            async: false,
            success: function (BankCorrs) {
                bcl = BankCorrs;
            }
        });

        return bcl;
    }
    function CreateChartData(bcList, dtFrom, dtTo, dateMode) {
         
        var monthFrom = dtFrom.getMonth();
        var monthTo = dtTo.getMonth();

        var yearFrom = dtFrom.getFullYear();
        var yearTo = dtTo.getFullYear();
        origYearFrom = yearFrom;
        origMonthFrom = monthFrom;

        var diffMonths = monthTo - monthFrom;
        diffMonths += (yearTo - yearFrom) * 12;

        monthRange = parseInt(diffMonths / diffMonths);
        range.push(GetBCWithinDateRange(bcList, yearFrom, monthFrom, dateMode));


        for (var i = 0; i < diffMonths ; i++) {

            monthFrom += monthRange;
            if (monthFrom > 12) {
                yearFrom++;
                monthFrom -= 12;
            }
            range.push(GetBCWithinDateRange(bcList, yearFrom, monthFrom, dateMode));

        }

        yearTo = yearFrom;


    }
    function PlotChart(seriesName, secondSeriesName) {

        var seriesOption = [{ name: seriesName, data: range }];
        if (secondSeriesName != null)
            seriesOption.push({ name: secondSeriesName, data: secondrange });

        $('#container').highcharts({
            chart: {
                type: 'line'
            },
            title: {
                text: 'BC Allotment in the Bank'
            },
            xAxis: {
                type: 'datetime',
                //    dateTimeLabelFormats: { // don't display the dummy year
                //        month: '%e. %b',
                //        year: '%b'
                //    },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                title: {
                    text: 'BC Count'
                },
                min: 0
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.0f} BC'
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                },
                series: {
                    pointStart: Date.UTC(origYearFrom, origMonthFrom, 1),
                    pointIntervalUnit: 'month'
                }
            },

            series: seriesOption
        });

    }
    function GetBCWithinDateRange(bcList, yearFrom, monthFrom, dateMode, getActive) {
        var bcCnt = 0;
        for (var b = 0; b < bcList.length; b++) {

            var bc = bcList[b];
            var apptDate = new Date(bc.AppointmentDate);
            var createDate = new Date(bc.CreatedOn);
            var blackListDate = new Date(bc.BlacklistDate);

            if (getActive != null && getActive != undefined) {
                if (getActive)
                    if (bc.Status != 'Active' || bc.IsBlackListed)
                        continue;
                    else if (!getActive) {
                        if (bc.Status == 'Active')
                            continue;
                    }
            }


            if (dateMode == apptMode) {
                if (apptDate <= Date.UTC(yearFrom, monthFrom, days[monthFrom]) && apptDate >= Date.UTC(yearFrom, monthFrom, 1))
                    bcCnt++;
            }
            else if (dateMode == createMode) {
                if (createDate <= Date.UTC(yearFrom, monthFrom, days[monthFrom]) && (!bc.isAllocated || apptDate > Date.UTC(yearFrom, monthFrom, days[monthFrom])))
                    bcCnt++;
            }
            else if (dateMode == blackListMode) {
                if (blackListDate <= Date.UTC(yearFrom, monthFrom, days[monthFrom]))
                    bcCnt++;
            }



        }
        return bcCnt;
    }
    $scope.GetBanks = function () {      
      
        var requestUrl = 'api/getBankHandler.ashx?mode=Bank';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            contentType: 'application/json',
            url: requestUrl,
            async: false,
            success: function (bankList) {
                if (bankList.length != 0) {
                    for (let i = 0; i < bankList.length; i++) {
                        let bank = new Object();
                        bank.BankId = bankList[i].BankId;
                        bank.BankName = bankList[i].BankName;
                        bank.BankCircles = bankList[i].BankCircles;
                        bank.Branches = bankList[i].Branches;
                        allBanks.push(bank);
                    }
                }
             
                if (allBanks.length != 0) {
                    for (let i = 0; i < allBanks.length; i++) {
                        $('#allBanks').append($('<option>', {
                            value: allBanks[i].BankId,
                            text: allBanks[i].BankName
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allBanks;
    }
    $('#btnGenDataUnallocated').on('click', function () {
        var from = $("#txtStartDate").val().split("/");
        strtDates = from[2] + "-" + from[1] + "-" + from[0];
        var strtDate = strtDates;

        var eNdfrom = $("#txtEndDate").val().split("/");
        EndDates = eNdfrom[2] + "-" + eNdfrom[1] + "-" + eNdfrom[0];
        var endDate = EndDates;
        var state = $('#selPLState').val();
        var district = $('#selPLDistrict').val();
        var subDistrict = $('#selPLSubDistrict').val();
        var village = $('#selPLdVillages').val();
        var type = 2;

        mode = 'Allocated';
        var bcList = GetBC(state, district, subDistrict, village, type, strtDate, endDate);

        var uprwaliArray = [];
        var neecheWaliArray = [];
        uprwaliArray = bcList.map(function (value, index, array) {
            return value.bcCount;

        })
        neecheWaliArray = bcList.map(function (value, index, array) {
            return value.timespan;

        })
        Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'Former BC in the Bank'
            },


            xAxis: {
                categories: neecheWaliArray,
                title: {
                    text: 'Date'
                }
            },

            series: [{
                name: "Former BC: " + new Date(strtDate).getYear() + "-" + new Date(endDate).getYear(),
                data: uprwaliArray
            }]
        });



    });
    $('#btnGenDataBlackList').on('click', function () {
        var from = $("#txtStartDate").val().split("/");
        strtDates = from[2] + "-" + from[1] + "-" + from[0];
        var strtDate = strtDates;

        var eNdfrom = $("#txtEndDate").val().split("/");
        EndDates = eNdfrom[2] + "-" + eNdfrom[1] + "-" + eNdfrom[0];
        var endDate = EndDates;
        var state = $('#selPLState').val();
        var district = $('#selPLDistrict').val();
        var subDistrict = $('#selPLSubDistrict').val();
        var village = $('#selPLdVillages').val();
        var type = 3;

        mode = 'Allocated';
        var bcList = GetBC(state, district, subDistrict, village, type, strtDate, endDate);

        var uprwaliArray = [];
        var neecheWaliArray = [];
        uprwaliArray = bcList.map(function (value, index, array) {
            return value.bcCount;

        })
        neecheWaliArray = bcList.map(function (value, index, array) {
            return value.timespan;

        })
        Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'Blacklisted BC in the Bank'
            },


            xAxis: {
                categories: neecheWaliArray,
                title: {
                    text: 'Date'
                }
            },

            series: [{
                name: "Blacklisted BC: " + new Date(strtDate).getYear() + "-" + new Date(endDate).getYear(),
                data: uprwaliArray
            }]
        });

       

    });
    $('#btnGenDateStatus').on('click', function () {
        
        var from = $("#txtStartDate").val().split("/");
        strtDates = from[2] + "-" + from[1] + "-" + from[0];
        var strtDate = strtDates;

        var eNdfrom = $("#txtEndDate").val().split("/");
        EndDates = eNdfrom[2] + "-" + eNdfrom[1] + "-" + eNdfrom[0];
        var endDate = EndDates;
        var state = $('#selPLState').val();
        var district = $('#selPLDistrict').val();
        var subDistrict = $('#selPLSubDistrict').val();
        var village = $('#selPLdVillages').val();
        var type = 4;

        mode = 'Allocated';
        var bcList = GetBC(state, district, subDistrict, village, type, strtDate, endDate);

        var uprwaliArray = [];
        var neecheWaliArray = [];
        uprwaliArray = bcList.map(function (value, index, array) {
            return value.bcCount;

        })
        neecheWaliArray = bcList.map(function (value, index, array) {
            return value.timespan;

        })
        Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'Active BC / Inactive Bc in the Bank'
            },


            xAxis: {
                categories: neecheWaliArray,
                title: {
                    text: 'Date'
                }
            },

            series: [{
                name: "Active BC / Inactive Bc: " + new Date(strtDate).getYear() + "-" + new Date(endDate).getYear(),
                data: uprwaliArray
            }]
        });

        

    });
    $scope.GetStates = function () {

        //  $('#selAddressState').empty();
        $('#selPLState').empty();
        $('#selPLDistrict').empty();

        $('#selPLSubDistrict').empty();

        $('#selPLdVillages').empty();


        var allStates = new Array();
        var requestUrl = 'api/state.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        let state = new Object();
                        state.StateId = stateList[i].StateId;
                        state.StateName = stateList[i].StateName;
                        state.StateCode = stateList[i].StateCode;
                        state.StateCircles = stateList[i].StateCircles;
                        state.Branches = stateList[i].Branches;
                        allStates.push(state);
                    }
                }
                states = stateList;
                if (allStates.length != 0) {
                    $('#selAddressState').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLState').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLSubDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLdVillages').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selAddressState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }
                if (allStates.length != 0) {
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selPLState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }

            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allStates;
    }
    $('#selPLState').on('change', function () {

        $('#selPLDistrict').empty();
        var allDistrict = new Array();
        var requestUrl = 'api/Districts.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"stateId": "' + $('#selPLState').val() + '"}'
           ,
            async: false,
            success: function (districtList) {
                if (districtList.length != 0) {
                    for (let i = 0; i < districtList.length; i++) {
                        let district = new Object();
                        district.DistrictCode = districtList[i].DistrictCode;
                        district.DistrictName = districtList[i].DistrictName;
                        district.DistrictId = districtList[i].DistrictId;

                        allDistrict.push(district);
                    }
                }
                // states = districtList;
                if (allDistrict.length != 0) {


                    $('#selPLDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allDistrict.length; i++) {
                        $('#selPLDistrict').append($('<option>', {
                            value: allDistrict[i].DistrictId,
                            text: allDistrict[i].DistrictName + '(' + allDistrict[i].DistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;

    });
    $('#selPLDistrict').on('change', function () {

        $('#selPLSubDistrict').empty();

        var allSubDistrict = new Array();
        var requestUrl = 'api/GetSubDistrict.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")

            },
            data: '{"distirctId": "' + $('#selPLDistrict').val() + '"}'
           ,
            async: false,
            success: function (subdistrictList) {
                if (subdistrictList.length != 0) {
                    for (let i = 0; i < subdistrictList.length; i++) {
                        let subdistrict = new Object();
                        subdistrict.SubDistrictCode = subdistrictList[i].SubDistrictCode;
                        subdistrict.SubDistrictName = subdistrictList[i].SubDistrictName;
                        subdistrict.SubDistrictId = subdistrictList[i].SubDistrictId;

                        allSubDistrict.push(subdistrict);
                    }
                }
                $('#selPLSubDistrict').append($('<option>', { value: 0, text: 'Select' }));
                if (allSubDistrict.length != 0) {

                    for (let i = 0; i < allSubDistrict.length; i++) {
                        $('#selPLSubDistrict').append($('<option>', {
                            value: allSubDistrict[i].SubDistrictId,
                            text: allSubDistrict[i].SubDistrictName + '(' + allSubDistrict[i].SubDistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;

    });
    $('#selPLSubDistrict').on('change', function () {
        $('#selPLdVillages').empty();

        var allVillage = new Array();
        var requestUrl = 'api/GetVillage.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"subDistrictId": "' + $('#selPLSubDistrict').val() + '"}'
           ,
            async: false,
            success: function (villageList) {
                if (villageList.length != 0) {
                    for (let i = 0; i < villageList.length; i++) {
                        let village = new Object();
                        village.VillageCode = villageList[i].VillageCode;
                        village.VillageName = villageList[i].VillageName;
                        village.VillageId = villageList[i].VillageId;

                        allVillage.push(village);
                    }
                }
                $scope.allVillageList = allVillage;
                $('#selPLdVillages').append($('<option>', { value: 0, text: '--Select--' }));
                if (allVillage.length != 0) {
                    for (let i = 0; i < allVillage.length; i++) {
                        $('#selPLdVillages').append($('<option>', {
                            value: allVillage[i].VillageId,
                            text: allVillage[i].VillageName + '(' + allVillage[i].VillageCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the village list could not be populated!\n' + errorMessage);
            }
        });
        return allVillage;
        // 
        //var StateId = parseInt($('#selPLState').val());
        //var DistrictId = parseInt($('#selPLDistrict').val());
        //var SubDistrictId = parseInt($('#selPLSubDistrict').val());

        //var distrcts = states.filter(function (x) {
        //    return x.StateId === StateId;
        //});

        //var cities = distrcts[0].Districts.filter(function (x) {
        //    return x.DistrictId === DistrictId;
        //});


        //var village = cities[0].Villages


        //$('#selPLdVillages').empty();
        //$('#selPLdVillages').append($('<option>', { value: 0, text: 'Select Village' }));
        //if (village.length >= 1) {
        //    for (i = 0; i < village.length; i++) {
        //        $('#selPLdVillages').append($('<option>', { value: village[i].VillageCode, text: village[i].VillageName + '(' + village[i].VillageCode + ')' }));
        //    }
        //}
    });
    $('#allBanks').on('change', function () {
        $('#bankCircle').empty();
        $('#bankBranch').empty();
        $('#bankCircle').append($('<option>', { value: 0, text: 'Select Circle' }));
        $('#bankBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        var selectedBankId = $('#allBanks :selected').val();
        if (selectedBankId != 0) {
            let bank = getBank(allBanks, selectedBankId);
            let bankCircles = null, bankBranches = null;
            if (bank.BankCircles != null)
                bankCircles = bank.BankCircles;
            if (bank.Branches != null)
                bankBranches = bank.Branches;
            if (bankCircles != null) {
                for (let i = 0; i < bankCircles.length; i++) {
                    $('#bankCircle').append($('<option>', { value: bankCircles[i].CircleId, text: bankCircles[i].CircleName }));
                }
            }
        }
    });
    function getBank(bankList, bankId) {
        
        let bank = new Object();
        for (let i = 0; i < bankList.length; i++) {
            if (bankList[i].BankId == bankId) {
                bank = bankList[i];
                break;
            }
        }
        return bank;
    }
    //Given a bank's circle, list all of it's zones
    $('#bankCircle').on('change', function () {
        let selectedBankId = $('#allBanks :selected').val();
        let selectedBank = getBank(allBanks, selectedBankId);
        let selectedCircleId = $('#bankCircle :selected').val();
        $('#bankZone').empty();
        $('#bankZone').append($('<option>', { value: 0, text: 'Select Zone' }));
        if (selectedBank != null) {
            if (selectedBank.BankCircles != null) {
                let zones = getZones(selectedBank.BankCircles, selectedCircleId);
                if (zones != null) {
                    for (let i = 0; i < zones.length; i++) {
                        $('#bankZone').append($('<option>', { value: zones[i].ZoneId, text: zones[i].ZoneName }));
                    }
                }
            }
        }
    });
    function getZones(circleList, circleId) {
        let zones = new Array();
        for (let i = 0; i < circleList.length; i++) {
            if (circleList[i].CircleId == circleId) {
                if (circleList[i].CircleZones != null) {
                    zones = circleList[i].CircleZones;
                    break;
                }
            }
        }
        return zones;
    }
    //Given a bank's region, list all of it's branches under that region
    $('#bankRegion').on('change', function () {
        $('#bankBranch').empty();
        $('#bankBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        let selectedBankId = $('#allBanks :selected').val();
        let selectedCircleId = $('#bankCircle :selected').val();
        let selectedZoneId = $('#bankZone :selected').val();
        let selectedRegionId = $('#bankRegion :selected').val();
        let bank = getBank(allBanks, selectedBankId);
        if (bank != null) {
            if (bank.BankCircles != null) {
                let zones = getZones(bank.BankCircles, selectedCircleId);
                if (zones != null) {
                    let regions = getRegions(zones, selectedZoneId);
                    if (regions != null) {
                        let branches = getBranches(regions, selectedRegionId);
                        if (branches != null) {
                            for (let i = 0; i < branches.length; i++) {
                                $('#bankBranch').append($('<option>', { value: branches[i].BranchId, text: branches[i].BranchName }));
                            }
                        }
                    }
                }
            }
        }
    });
    function getRegions(zoneList, zoneId) {
        let regions = new Array();
        for (let i = 0; i < zoneList.length; i++) {
            if (zoneList[i].ZoneId == zoneId) {
                if (zoneList[i].ZoneRegions != null) {
                    regions = zoneList[i].ZoneRegions;
                    break;
                }
            }
        }
        return regions;
    }
    //Given a bank's zone, list all of it's regions
    $('#bankZone').on('change', function () {
        let selectedZoneId = $('#bankZone :selected').val();
        let selectedBankId = $('#allBanks :selected').val();
        let selectedCircleId = $('#bankCircle :selected').val();
        let bank = getBank(allBanks, selectedBankId);
        $('#bankRegion').empty();
        $('#bankRegion').append($('<option>', { value: 0, text: 'Select Region' }));
        if (bank != null) {
            if (bank.BankCircles != null) {
                let zones = getZones(bank.BankCircles, selectedCircleId);
                if (zones != null) {
                    let regions = getRegions(zones, selectedZoneId);
                    if (regions != null) {
                        for (let i = 0; i < regions.length; i++) {
                            $('#bankRegion').append($('<option>', { value: regions[i].RegionId, text: regions[i].RegionName }));
                        }
                    }
                }
            }
        }
    });
    function getBranches(regionList, regionId) {
        let branches = new Array();
        for (let i = 0; i < regionList.length; i++) {
            if (regionId == regionList[i].RegionId) {
                if (regionList[i].RegionBranches != null) {
                    branches = regionList[i].RegionBranches;
                    break;
                }
            }
        }
        return branches;
    }
});