﻿bctrackApp.controller("changePasswordController", function ($scope, $cookies, PasswordService, $timeout, $rootScope) {
  
 
    $scope.password = {
        "oldPassword": "",
        "newpassword": "",
        "userId": "",
        "txtrePassword": ""
    };

    $scope.changePassword = function () {
        
        var oldpass = $.md5($scope.password.oldPassword)
        $scope.password.oldPassword = oldpass;
        var newpass = $.md5($scope.password.newpassword)
        $scope.password.newpassword = newpass;
        $scope.password.userId = $cookies.get("userId"),
        $rootScope.$broadcast('showLoader');
         PasswordService.changePassword($scope.password)
                .then(function (response) {
                    alert(response);
                    $scope.password = {
                        "oldPassword": "",
                        "newpassword": "",
                        "userId": "",
                        "txtrePassword": ""
                    };
                }, function (err) {
                    $rootScope.$broadcast('hideLoader');
                    alert("something went wrong");
                    $scope.password = {
                        "oldPassword": "",
                        "newpassword": "",
                        "userId": "",
                        "txtrePassword":""
                    };
                });
      
    }


   
    $("#btnChangePassword").hide();
    $("#errormsg").hide();
    $("#txtPassword").on("focusout", function (e) {
        if ($(this).val() != $("#txtrePassword").val()) {
            $("#btnChangePassword").hide();
            $("#errormsg").hide();
            $timeout(function () { $scope.notexists = true; }, 100);
            $timeout(function () { $scope.notterminate = true; }, 100);
            $timeout(function () { $scope.terminated = true; }, 100);

        } else {

            $("#btnChangePassword").show();
            $("#errormsg").hide();
        }
    });

    $("#txtrePassword").on("keyup", function (e) {
        if ($("#txtPassword").val() != $(this).val()) {
            $("#btnChangePassword").hide();
            $("#errormsg").show();
            $timeout(function () { $scope.notexists = true; }, 100);
            $timeout(function () { $scope.notterminate = true; }, 100);
            $timeout(function () { $scope.terminated = true; }, 100);

        } else {
            $("#btnChangePassword").show();
            $("#errormsg").hide();

        }
    });
});