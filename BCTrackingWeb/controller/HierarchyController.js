﻿

bctrackApp.controller("HierarchyController", function ($scope, $state, $timeout, $cookies) {
   
    $scope.addBrnach=function()
    {

    }
    $scope.divbtnBank = true;
    $scope.divbtnState = true;
    $scope.divbtnCity = true;
    $scope.divbtnDistrict = true;

   
    $scope.GetBanks = function () {
        //
        //if ($cookies.get("BankId") == -1) {
         
        //    $timeout(function () { $scope.divbtnBank = false; }, 100);
        //}
        //else {

        //}
        let banks = new Array();
        var requrl = 'api/getBankHandler.ashx?mode=Bank';
    
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
               

                //console('Got it!');
                for (var cnt = 0; cnt < Banks.length; cnt++) {
                    let bank = new Object();
                    bank.BankId = Banks[cnt].BankId;
                    bank.BankName = Banks[cnt].BankName;
                    bank.Branches = Banks[cnt].Branches;
                    bank.BankCircles = Banks[cnt].BankCircles;
                    banks.push(bank);
                }
                if ($cookies.get("userId") == "admin") {
                    allBanks = banks;
                }
                else {
                  
                    var bankid = parseInt($cookies.get("BankId"));
                    var updateBankList = banks.filter(function (x) {
                        return x.BankId === bankid
                    });
                    allBanks = updateBankList;
                    $('#allBanks').val($cookies.get("BankId"));


                }
                $('#selBank').append($('<option>', { value: 0, text: '--Select Bank--' }));
                console.log(allBanks);
                for (let i = 0; i < allBanks.length; i++) {
                    $('#selBank').append($('<option>', {
                        value: allBanks[i].BankId,
                        text: allBanks[i].BankName
                    }));
                }
            

                $('.loader').hide();
            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }
        });
        return banks;
    }

    $('#selBank').on('change', function () {
     
      
         
        $('#selState').empty();
        Bankid = $('#selBank').val();
        $('#selState').append($('<option>', { value: 0, text: '--Select State--' }));
        if (Bankid == 0) {
            $timeout(function () { $scope.divbtnState = true; }, 100);
        }
        else {
            $timeout(function () { $scope.divbtnState = false; }, 100);
        }
        $scope.Bankid = Bankid;
    
        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=State&BankId=" + Bankid;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
                 
                if (Banks.length > 0) {

                }
                else {
                  
                    $timeout(function () { $scope.divbtnCity = true; }, 100);
                    $timeout(function () { $scope.divbtnDistrict = true; }, 100);
                    $('#selCity').empty();
                    $('#selCity').append($('<option>', { value: 0, text: '--Select City--' }));
                    $('#selDistrict').empty();
                    $('#selDistrict').append($('<option>', { value: 0, text: '--Select District--' }));

                    

                }
             
                for (let i = 0; i < Banks.length; i++) {
                    $('#selState').append($('<option>', {
                        value: Banks[i].CircleId,
                        text: Banks[i].CircleName
                    }));
                }
            },
            error: function (err) {

            }
        });
        return banks;
    });
    $('#selState').on('change', function () {
       
        
        $('#selCity').empty();
        StateId = $('#selState').val();
        $('#selCity').append($('<option>', { value: 0, text: '--Select City--' }));
        if (StateId == 0) {
            $timeout(function () { $scope.divbtnCity = true; }, 100);
        }
        else {
            $timeout(function () { $scope.divbtnCity = false; }, 100);
        }
        $scope.StateId = StateId;
        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=City&StateId=" + StateId;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
                 
                for (let i = 0; i < Banks.length; i++) {
                    $('#selCity').append($('<option>', {
                        value: Banks[i].ZoneId,
                        text: Banks[i].ZoneName
                    }));
                }
            },
            error: function (err) {

            }
        });
        return banks;
    });
    $('#selCity').on('change', function () {
         
       
     
        $('#selDistrict').empty();
        var  districtId = $('#selCity').val();
        $('#selDistrict').append($('<option>', { value: 0, text: '--Select District--' }));
        if (districtId == 0) {
            $timeout(function () { $scope.divbtnDistrict = true; }, 100);
        }
        else {
            $timeout(function () { $scope.divbtnDistrict = false; }, 100);
        }
    
        $scope.CityID = districtId;
        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=District&districtId=" + districtId;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
                 
                for (let i = 0; i < Banks.length; i++) {
                    $('#selDistrict').append($('<option>', {
                        value: Banks[i].RegionId,
                        text: Banks[i].RegionName
                    }));
                }
            },
            error: function (err) {

            }
        });
        return banks;
    });
   
    var errorMessage = '';
    function validate() {
         
        errorMsg = '';
     

        var BranchName = $('#txtBranchName').val();
        var BranchIFSCCode = $('#txtBranchIFSCCode').val();

        var selBank = $('#selBank').val();
        var selState = $('#selState').val();
        var selCity = $('#selCity').val();
        var selDistrict = $('#selDistrict').val();

        var result = true;




        if (BranchName == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Branch Name.";
            result = false;
        }
        if (BranchIFSCCode == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Branch IFSCCode";
            result = false;
        }
        if (selBank ==0) {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select Bank";
            result = false;
        }
        if (selState == null) {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select State";
            result = false;
        }
        if (selCity == null) {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select City";
            result = false;
        }
        if (selDistrict == null) {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select District";
            result = false;
        }
        
        return result;
    }
    $scope.addBranch = function () {
         
        if (validate() == true) {
            var branch = new Object();

            branch.BranchName = $('#txtBranchName').val();
            branch.IFSCCode = $('#txtBranchIFSCCode').val();
            branch.Address = $('#txtBranchAddress').val();
            branch.ContactNumber = $('#txtBranchContact').val();

            branch.BankId = $('#selBank').val();
            branch.StateId = $('#selState').val();
            branch.CityId = $('#selCity').val();
            branch.DistrictId = $('#selDistrict').val();


            $.ajax({
                url: "api/InsertUpdateBranch.ashx",
                method: "POST",
                datatype: "json",
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                data: { "branch": JSON.stringify(branch) },
                success: function () {
                    alert('Branch saved successfully.');
                    //  window.location.href = "#/banklist";
                },
                error: function (xmlHttpRequest, errorString, errorMessage) {
                    alert('Branch saved successfully.');
                    // window.location.href = "#/banklist";
                }
            });
        }
        else
        {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
      
  



    };
})