﻿bctrackApp.controller("uploadController", function ($scope, $cookies) {
    
    $('#btnsubmit').on('click', function () {
        
        $.ajax({
            url: "FileUploader.ashx",
            method: "POST",
            contentType: 'text/plain',
            async: false,
            success: function (data) {

            var fileName = $(this).val().replace(/^.*[\\\/]/, '');
            var currFile = $(this)[0].files[0];
            var fieldId = $(this).attr('id');
            var xhr = new XMLHttpRequest();
            var uploadData = new FormData();
            var url = 'FileUploader.ashx';
            uploadData.append('file', currFile);
            xhr.open("POST", url, true);
            xhr.send(uploadData);
            alert(data);
            },
            error: function () {

            var fileName = $(this).val().replace(/^.*[\\\/]/, '');
            var currFile = $(this)[0].files[0];
            var fieldId = $(this).attr('id');
            var xhr = new XMLHttpRequest();
            var uploadData = new FormData();
            var url = 'FileUploader.ashx';
            uploadData.append('file', currFile);
            xhr.open("POST", url, true);
            xhr.send(uploadData);

          }
        });
    });

    $(document).on('change', '#uploadcsv', function () {
        var fileName = $(this).val().replace(/^.*[\\\/]/, '');
        var currFile = $(this)[0].files[0];
        var fieldId = $(this).attr('id');
        var xhr = new XMLHttpRequest();
        var uploadData = new FormData();
        var url = 'FileUploader.ashx';
        uploadData.append('file', currFile);
        xhr.open("POST", url, true);
        xhr.send(uploadData);
        alert('File uploaded successfully');
    });

    $(document).on('change', '#imgupload', function () {
        var fileName = $(this).val().replace(/^.*[\\\/]/, '');
        var currFile = $(this)[0].files[0];
        var fieldId = $(this).attr('id');
        var xhr = new XMLHttpRequest();
        var uploadData = new FormData();
        var url = 'FileUploader.ashx';
        uploadData.append('file', currFile);
        xhr.open("POST", url, true);
        xhr.send(uploadData);
        alert('File uploaded successfully');
    });

    var fullurl = $(location).attr('href');

    var c = fullurl.match(/cId=([^&]+)/);
    var cId = '';
    if (c != null)
        cId = c[1];

    var t2 = '';

    if ($('#tblBCList').length > 0) {
        t2 = $('#tblBCList').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": true,
            dom: 'Bfrtip',
            buttons: ['excel']
        });
         
        var mode = '';
        var url = $(location).attr('href');
        if (url.indexOf('BCListAll') > 0 || url.indexOf('bclistStatus') > 0)
            mode = 'All';
        else if (url.indexOf('bclistBankWise') > 0 || url.indexOf('bclistLocationWise') > 0)
            mode = 'Allocated';
        else if (url.indexOf('bclistUnallocated') > 0)
            mode = 'Unallocated';

        var bcList = [];

        bcList = GetBC(mode, '');
        FillTable(t2);
        GetBanks();

    }

    function GetBC(mode, filter) {
        
        var requrl = "GetBCHandler.ashx?mode=" + mode;
        if (filter != '')
            requrl += filter;

        var bcl = [];

        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            async: false,
            success: function (BankCorrs) {
                bcl = BankCorrs;
            }
        });

        return bcl;
    }

    function FillTable(table) {
        

        table.clear().draw();

        for (var cnt = 0; cnt < bcList.length; cnt++) {
            var bankc = bcList[cnt];


            var certs = '';
            if (bankc.Certifications != null) {
                for (var j = 0; j < bankc.Certifications.length; j++)
                    certs += bankc.Certifications[j].CourseName;
            }

            var villages = '';
            if (bankc.OperationalAreas != null) {
                for (var k = 0; k < bankc.OperationalAreas.length; k++)
                    villages += bankc.OperationalAreas[k].VillageCode;
            }
            var black = "BlackList";
            var glp = 'ban-circle';
            if (bankc.IsBlackListed) {
                black = "WhiteList";
                glp = 'ok-circle';
            }

            var actionRow = '<a href="editEntry.aspx?bc=' + bankc.BankCorrespondId + '&mode=Edit" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="editEntry.aspx?bc=' + bankc.BankCorrespondId + '&mode=View"  title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="BlackListBC.aspx?bc=' + bankc.BankCorrespondId + '&mode=' + black + '" title="' + black + '"><span class="glyphicon glyphicon-' + glp + '" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="ChangeStatus.aspx?bc=' + bankc.BankCorrespondId + '" title="ChangeStatus"><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span></a>';

            table.row.add([bankc.BankCorrespondId, bankc.Name, bankc.PhoneNumber1, bankc.AadharCard, certs, villages, actionRow
            ]).draw(false);

        }

    }

    function GetBanks() {

        let banks = new Array();
        var requrl = "GetBCHandler.ashx?mode=Bank";
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            async: false,
            success: function (Banks) {
                
                //console('Got it!');
                for (var cnt = 0; cnt < Banks.length; cnt++) {
                    let bank = new Object();
                    bank.BankId = Banks[cnt].BankId;
                    bank.BankName = Banks[cnt].BankName;
                    bank.Branches = Banks[cnt].Branches;
                    bank.BankCircles = Banks[cnt].BankCircles;
                    banks.push(bank);
                }
                $('.loader').hide();
            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }
        });
        return banks;
    }

    let allBanks = GetBanks();
    for (let i = 0; i < allBanks.length; i++) {
        $('#selBank').append($('<option>', {
            value: allBanks[i].BankId,
            text: allBanks[i].BankName
        }));
    }

    if ($('#tblCorporateList').length > 0) {

        var mode = "Corporate";
        if (cId != '')
            mode += "One";



        var t4 = $('#tblCorporateList').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": true
        });

        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: "GetBCHandler.ashx?mode=" + mode + "&cId=" + cId,
            async: false,
            success: function (Corporates) {
                //console('Got it!');
                if (mode == "Corporate") {
                    for (var cnt = 0; cnt < Corporates.length; cnt++) {
                        var Corporate = Corporates[cnt];
                        t4.row.add([Corporate.CorporateId, Corporate.CorporateName, Corporate.CorporateType, Corporate.Address, Corporate.ContactNumber + '<br />' + Corporate.Email,
                         '<td><a href="AddCorporate.aspx?CId=' + Corporate.CorporateId + '" class="btn btn-success btn-rounded icon-only" id="btnEditCorporate"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>',
                          '<td><input type="button" class="btn btn-danger btn-sm btn-labeled btn-rounded" value="Delete">'
                        ]).draw(false);
                    }
                }
                else if (mode == "CorporateOne") {
                    {
                        var corporate = Corporates[0];
                        //Fill the controls using the above object                  
                    }

                }
                $('.loader').hide();
            },
            error: function (err) {
                //  alert(err);
                $('.loader').hide();
            }
        });
    }
    $('#tblCorporateList').on('click', 'tr', function () {
        
        var corporateId = $(this).find('td:first').text();
        //$.ajax({
        //    type: 'POST',
        //    dataType: 'json',
        //    contentType: 'application/json',
        //    url: "DeleteBcHandler.ashx?mode=&corporateId=" + corporateId,
        //    async: false,
        //    success: function () {
        //        window.location.reload();

        //    }
        //});


    });

    if ($('#tblNotificationList').length > 0) {

        var t4 = $('#tblNotificationList').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": true
        });


        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: "GetBCHandler.ashx?mode=Notification",
            async: false,
            success: function (nots) {
                //console('Got it!');

                for (var cnt = 0; cnt < nots.length; cnt++) {
                    var n = nots[cnt];

                    t4.row.add([n.NotificationId, n.NotificationType, n.NotificationText, n.NotificationDate, n.NotificationPriority
                    ]).draw(false);

                }

                $('.loader').hide();

            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }

        });
    }

    function getBankObject(bankList, bankId) {
        let bankObject = new Object();
        for (let i = 0; i < bankList.length; i++) {
            if (bankList[i].BankId == bankId) {
                bankObject = bankList[i];
                break;
            }
        }
        return bankObject;
    }

    function getZones(bankList, bankId, circleId) {
        let zones = new Array();
        let bankObject = getBankObject(bankList, bankId);
        if (bankObject != null) {
            let correspondingCircle = function (circles, circleId) {
                let c = new Object();
                for (let i = 0; i < circles.length; i++) {
                    if (circleId == circles[i].CircleId) {
                        c = circles[i];
                        break;
                    }
                }
                return c;
            }(bankObject.BankCircles, circleId);
            if (correspondingCircle.CircleZones != null) {
                for (let i = 0; i < correspondingCircle.CircleZones.length; i++) {
                    zones.push(correspondingCircle.CircleZones[i]);
                }
            }
        }
        return zones;
    }

    function getZones(bankList, bankId) {
        let zones = new Array();
        let bank = getBankObject(bankList, bankId);
        if (bank != null) {
            if (bank.BankCircles != null) {
                for (let i = 0; i < bank.BankCircles.length; i++) {
                    if (bank.BankCircles[i].CircleZones != null) {
                        for (let j = 0; j < bank.BankCircles[i].CircleZones.length; j++) {
                            zones.push(bank.BankCircles[i].CircleZones[j]);
                        }
                    }
                }
            }
        }
        return zones;
    }

    function getZoneRegions(zones, zoneId) {
        let regions = new Array();
        for (let i = 0; i < zones.length; i++) {
            if (zoneId == zones[i].ZoneId) {
                if (zones[i].ZoneRegions != null) {
                    for (let j = 0; j < zones[i].ZoneRegions.length; j++) {
                        regions.push(zones[i].ZoneRegions[j]);
                    }
                }
            }
        }
        return regions;
    }

    function getCommonFilter(filter) {
        let bankId = $('#allBanks :selected').val();
        let bankCircle = $('#bankCircle :selected').val();
        let bankZone = $('#bankZone :selected').val();
        let bankRegion = $('#bankRegion :selected').val();
        let bankBranch = $('#bankBranch :selected').val();
        let category = $('#bankCategory :selected').val();
        let ssa = $('#bankSsa :selected').val();
        let state = $('#bankState :selected').val();

        if (bankId != 0 && bankId !== 'Select Bank')
            filter += "&bankId=" + bankId;
        if (bankCircle != 0)
            filter += "&bankCircleId=" + bankCircle;
        if (bankZone != 0)
            filter += "&bankZoneId=" + bankZone;
        if (bankRegion != 0)
            filter += "&bankRegionId=" + bankRegion;
        if (category != 0 && category !== 'Select Category')
            filter += "&bankCategory=" + category;
        if (ssa !== 'Select SSA' && ssa != 0)
            filter += "&bankSsa=" + ssa;
        if (state != 0)
            filter += "&stateId=" + state;

        return filter;
    }

    if ($('#tblUserList').length > 0) {

        var t5 = $('#tblUserList').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": true
        });


        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: "GetUserHandler.ashx?mode=All",
            async: false,
            success: function (users) {
                //console('Got it!');

                for (var cnt = 0; cnt < users.length; cnt++) {
                    var user = users[cnt];

                    var roles = '';
                    for (var j = 0; j < user.UserRoles.length; j++) {
                        roles += user.UserRoles[j].Role;
                    }

                    t5.row.add([user.UserId, user.FirstName + ' ' + user.LastName, roles,
                     '<td><a href="AddUser.aspx?id=' + user.UserId + '" class="btn btn-success btn-rounded icon-only"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>',
                     '<td><input type="button" class="btn btn-danger btn-sm btn-labeled btn-rounded" value="Delete">'
                    ]).draw(false);

                }

                $('.loader').hide();

            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }

        });
    }
    $('#tblUserList').on('click', 'tr', function () {
        
        var userId = $(this).find('td:first').text();
        //$.ajax({
        //    type: 'POST',
        //    dataType: 'json',
        //    contentType: 'application/json',
        //    url: "DeleteBcHandler.ashx?mode=User&userId=" + userId,
        //    async: false,
        //    success: function (user) {
        //        window.location.reload();

        //    }
        //});


    });
    $('#selBank').on('change', function () {
        let selectedBankId = $('#selBank :selected').val();
        if (selectedBankId != 0) {
            let selectedBankObject = getBankObject(allBanks, selectedBankId);
            let circles = selectedBankObject.BankCircles;
            $('#selCircle').empty();
            $('#selCircle').append($('<option>', { value: 0, text: 'Select Circle' }));
            if (circles != null && circles.length != 0) {
                for (let i = 0; i < circles.length; i++) {
                    $('#selCircle').append($('<option>', { value: circles[i].CircleId, text: circles[i].CircleName }));
                }
            }
        }
    });

    $('#selBank').on('change', function () {
        let selectedBankId = $('#selBank :selected').val();
        if (selectedBankId != 0) {
            let selectedBankObject = getBankObject(allBanks, selectedBankId);
            let branches = selectedBankObject.Branches;
            if (branches.length != 0) {
                for (let i = 0; i < branches.length; i++) {
                    $('#selBranch').append($('<option>', { value: branches[i].BranchId, text: branches[i].BranchName }));
                }
            }
        }
    });

    $('#selCircle').on('change', function () {
        let selectedBankId = $('#selBank :selected').val();
        let selectedCircleId = $('#selCircle :selected').val();
        if (selectedBankId != 0 && selectedCircleId != 0) {
            let zones = getZones(allBanks, selectedBankId, selectedCircleId);
            $('#selZone').empty();
            $('#selZone').append($('<option>', { value: 0, text: 'Select Zone' }));
            if (zones.length != 0) {
                for (let i = 0; i < zones.length; i++) {
                    $('#selZone').append($('<option>', { value: zones[i].ZoneId, text: zones[i].ZoneName }));
                }
            }
        }
    });

    $('#selBank').on('change', function () {
        let selectedBankId = $('#selBank :selected').val();
        $('#selBankZone').empty();
        $('#selBankZone').append($('<option>', { value: 0, text: 'Select Zone' }));
        let zones = getZones(allBanks, selectedBankId);
        if (zones != null) {
            for (let i = 0; i < zones.length; i++) {
                $('#selBankZone').append($('<option>', { value: zones[i].ZoneId, text: zones[i].ZoneName }));
            }
        }
    });

    $('#selBankZone').on('change', function () {
        let selectedBankZoneId = $('#selBankZone :selected').val();
        let selectedBankId = $('#selBank :selected').val();
        $('#selRegion').empty();
        $('#selRegion').append($('<option>', { value: 0, text: 'Select Region' }));
        let zones = getZones(allBanks, selectedBankId);
        if (zones != null) {
            let zoneRegions = getZoneRegions(zones, selectedBankZoneId);
            if (zoneRegions != null) {
                for (let i = 0; i < zoneRegions.length; i++) {
                    $('#selRegion').append($('<option>', { value: zoneRegions[i].RegionId, text: zoneRegions[i].RegionName }));
                }
            }
        }
        else {
            alert('No regions available!');
        }
    });

    $('#selZone').on('change', function () {
        let selectedCircleId = $('#selCircle :selected').val();
        let selectedZoneId = $('#selZone :selected').val();
        let selectedBankId = $('#selBank :selected').val();
        let zones = getZones(allBanks, selectedBankId, selectedCircleId);
        if (zones != null) {
            let selectedZoneObject = function (zones, zoneId) {
                let z = new Object();
                for (let i = 0; i < zones.length; i++) {
                    if (zoneId == zones[i].ZoneId) {
                        z = zones[i];
                        break;
                    }
                }
                return z;
            }(zones, selectedZoneId);
            $('#selRegion').empty();
            $('#selRegion').append($('<option>', { value: 0, text: 'Select Region' }));
            if (selectedZoneObject.ZoneRegions != null) {
                console.log(JSON.stringify(selectedZoneId.ZoneRegions));
                for (let i = 0; i < selectedZoneObject.ZoneRegions.length; i++) {
                    $('#selRegion').append($('<option>', { value: selectedZoneObject.ZoneRegions[i].ZoneId, text: selectedZoneObject.ZoneRegions[i].ZoneName }));
                }
            }
            else {
                alert('Regions not found!');
            }
        }
    });

    function getAllStates() {
        let allStates = new Array();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: 'GETBCHandler.ashx?mode=state',
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        allStates.push(stateList[i]);
                    }
                }
            },
            error: function (xhr, errorString, errorMsg) {
                alert(errorMsg);
            }
        });
        return allStates;
    }
    let allStates = getAllStates();
    if (allStates.length != 0) {
        for (let i = 0; i < allStates.length; i++) {
            $('#selState').append($('<option>', { value: allStates[i].StateId, text: allStates[i].StateName }));
        }
    }

    $('#btnFilterBC').on('click', function () {
        var bank = $('#selBank').val()
        let bankCircle = $('#selCircle :selected').val();
        let state = $('#selState :selected').val();
        let zone = $('#selZone :selected').val();
        let region = $('#selRegion :selected').val();
        let category = $('#selCategory :selected').val();
        let branch = $('#selBranch :selected').val();
        var filter;

        if (bank != 0)
            filter = "&bankId=" + bank;
        if (bankCircle != 0)
            filter += "&bankCircleId=" + bankCircle;
        if (state != 0)
            filter += "&stateId=" + state;
        if (zone != 0)
            filter += "&bankZoneId=" + zone;
        if (region != 0)
            filter += "&bankRegionId=" + region;
        if (category != 0)
            filter += "&branchCategory=" + category;
        if (branch != 0)
            filter += "&branchid=" + branch;

        bcList = GetBC(mode, filter);
        FillTable(t2);


    });

    $('#btnListAsOnDate').on('click', function () {
        let date = $('#createFromDate').val();
        let filter = "&createFromDate=" + date;
        filter = getCommonFilter(filter);
        let mode = 'Allocated';
        let bcList = GetBC(mode, filter);
        if (bcList != null) {
            console.log(JSON.stringify(bcList));
            for (let i = 0; i < bcList.length; i++) {
                t2.row.add([bcList[i].BankCorrespondId, bcList[i].Name, bcList[i].PhoneNumber1, bcList[i].UniqueIdentificationNumber, bcList[i].Certificates, bcList[i].Villages]).draw(false);
            }
        }
        else {
            alert('No data!');
        }
    });

    $('#btnListCorporateBCs').on('click', function () {
        let date = $('#txtCreateDate').val();
        let corporateId = $('#selCorporate :selected').val();
        let filter = "&createFromDate=" + date + "&corporateId=" + corporateId;
        filter = getCommonFilter(filter);
    });
});