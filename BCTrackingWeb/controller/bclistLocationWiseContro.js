﻿bctrackApp.controller("bclistLocationWiseContro", function ($scope, $cookies, $timeout, $window) {
    $scope.getBcBranchWise = function () {
        var leadTable = $('#tblBranchList').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "BCListAlls.aspx/getBcAllCorresponds",
            "columnDefs": [

              {
                  "orderable": false,
                  "targets": -1,
                  "render": function (data, type, full, meta) {
                      return '';//'<a class="btn btn-danger  btn-sm"    href="#/chnageStatusallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Terminate</a>';
                  }
              },
              {
                  "orderable": false,
                  "targets": -2,
                  "render": function (data, type, full, meta) {

                      if (full[6] == "True")
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i> Blacklisted</a>';
                      else
                          return '';//'<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Blacklist</a>';
                  }
              },
                {
                    "orderable": false,
                    "targets": -3,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-primary  btn-sm"   href="#/viewallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>';
                    }
                },
                  {
                      "orderable": false,
                      "targets": -4,
                      "render": function (data, type, full, meta) {
                          return '<a class="btn btn-primary  btn-sm"   href="#/editallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Edit</a>';
                      }
                  }

            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["adminuserid"] = $cookies.get("userId"),
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblBranchList").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);

    }
    //$scope.GetBanks = function () {

    //    let banks = new Array();
    //    var requrl = "api/getBankHandler.ashx?mode=Bank";
    //    $('.loader').show();
    //    $.ajax({
    //        type: 'POST',
    //        dataType: 'json',
    //        contentType: 'application/json',
    //        url: requrl,
    //        headers: {
    //            "userId": $cookies.get("userId"),
    //            "TokenId": $cookies.get("TokenId")

    //        },
    //        async: false,
    //        success: function (Banks) {

    //            //console('Got it!');
    //            for (var cnt = 0; cnt < Banks.length; cnt++) {
    //                let bank = new Object();
    //                bank.BankId = Banks[cnt].BankId;
    //                bank.BankName = Banks[cnt].BankName;
    //                bank.Branches = Banks[cnt].Branches;
    //                bank.BankCircles = Banks[cnt].BankCircles;
    //                banks.push(bank);
    //            }
    //            allBanks = banks;

    //            console.log(allBanks);
    //            for (let i = 0; i < allBanks.length; i++) {
    //                $('#selBank').append($('<option>', {
    //                    value: allBanks[i].BankId,
    //                    text: allBanks[i].BankName
    //                }));
    //            }
    //            for (let i = 0; i < allBanks.length; i++) {
    //                $('#allBanks').append($('<option>', {
    //                    value: allBanks[i].BankId,
    //                    text: allBanks[i].BankName
    //                }));
    //            }

    //            $('.loader').hide();
    //        },
    //        error: function (err) {
    //            alert(err);
    //            $('.loader').hide();
    //        }
    //    });
    //    return banks;
    //}
    //$('#allBanks').on('change', function () {



    //    $('#bankBranch').empty();
    //    Bankid = $('#allBanks').val();
    //    if (Bankid == "Select Bank") {
    //        Bankid = 0;
    //    }
    //    else {
    //        Bankid = Bankid;
    //    }
    //    $('#bankBranch').append($('<option>', { value: 0, text: '--Select Branch--' }));


    //    let banks = new Array();
    //    var requrl = "api/getBankHandler.ashx?mode=Branch&BankId=" + Bankid;
    //    $('.loader').show();
    //    $.ajax({
    //        type: 'POST',
    //        dataType: 'json',
    //        contentType: 'application/json',
    //        url: requrl,
    //        headers: {
    //            "userId": $cookies.get("userId"),
    //            "TokenId": $cookies.get("TokenId")

    //        },
    //        async: false,
    //        success: function (Banks) {


    //            for (let i = 0; i < Banks.length; i++) {
    //                $('#bankBranch').append($('<option>', {
    //                    value: Banks[i].BranchId,
    //                    text: Banks[i].BranchName
    //                }));
    //            }
    //        },
    //        error: function (err) {

    //        }
    //    });
    //    return banks;
    //});
    $scope.getBcBranchWiseFilter = function () {
        debugger;
        var url;
        var bankId = $('#selBank').val();
        var branchId = $('#bankBranch').val();
        var state = $('#selState').val();
        var city = $('#selCity').val();
        var district = $('#selDistrict').val();
      
        

        if (bankId == "0" && branchId == "0") {

            url = "BCListAlls.aspx/getBcAllCorresponds";
        }
        else {
            url = "bcbclistBankWise.aspx/getBcByBrnachFilter";
        }


        var leadTable = $('#tblBranchList').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": url,
            "columnDefs": [

               {
                   "orderable": false,
                   "targets": -1,
                   "render": function (data, type, full, meta) {
                       return '';//'<a class="btn btn-danger  btn-sm"    href="#/chnageStatusallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Terminate</a>';
                   }
               },
              {
                  "orderable": false,
                  "targets": -2,
                  "render": function (data, type, full, meta) {

                      if (full[6] == "True")
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i> Blacklisted</a>';
                      else
                          return '';//'<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Blacklist</a>';
                  }
              },
                {
                    "orderable": false,
                    "targets": -3,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-primary  btn-sm"   href="#/viewallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>';
                    }
                },
                  {
                      "orderable": false,
                      "targets": -4,
                      "render": function (data, type, full, meta) {
                          return '<a class="btn btn-primary  btn-sm"   href="#/editallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Edit</a>';
                      }
                  }

            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["adminbankId"] = bankId;
                aoData["adminbranchId"] = branchId;
                aoData["adminstateId"] = state;
                aoData["admincityId"] = city;
                aoData["admindistrictId"] = district;
                aoData["adminuserid"] = $cookies.get("userId"),
           
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblBranchList").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);

    }

      //Given a bank, list all of it's circles
   
    $('#selDistrict').on('change', function () {


         
        $('#bankBranch').empty();
        Districtid = $('#selDistrict').val();
        if (Districtid == "Select District")
        {
            Districtid = 0;
        }
        else
        {
            Districtid = Districtid;
        }
        $('#bankBranch').append($('<option>', { value: 0, text: '--Select Branch--' }));
       

        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=Districts&districtId=" + Districtid;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")

            },
            async: false,
            success: function (Banks) {
               

                for (let i = 0; i < Banks.length; i++) {
                    $('#bankBranch').append($('<option>', {
                        value: Banks[i].BranchId,
                        text: Banks[i].BranchName
                    }));
                }
            },
            error: function (err) {

            }
        });
        return banks;
    });
    $scope.GetBanks = function () {
      
        if ($cookies.get("BankId") == -1) {

            $timeout(function () { $scope.divbtnBank = false; }, 100);
        }
        else {

        }
        let banks = new Array();
        var requrl = 'api/getBankHandler.ashx?mode=All';

        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {


                //console('Got it!');
                for (var cnt = 0; cnt < Banks.length; cnt++) {
                    let bank = new Object();
                    bank.BankId = Banks[cnt].BankId;
                    bank.BankName = Banks[cnt].BankName;
                    bank.Branches = Banks[cnt].Branches;
                    bank.BankCircles = Banks[cnt].BankCircles;
                    banks.push(bank);
                }
                if ($cookies.get("Role") == "1") {
                    allBanks = banks;
                }
                else {

                    var bankid = parseInt($cookies.get("BankId"));
                    var updateBankList = banks.filter(function (x) {
                        return x.BankId === bankid
                    });
                    allBanks = updateBankList;
                    $('#allBanks').val($cookies.get("BankId"));


                }
                $('#selBank').append($('<option>', { value: 0, text: '--Select Bank--' }));
                console.log(allBanks);
                for (let i = 0; i < allBanks.length; i++) {
                    $('#selBank').append($('<option>', {
                        value: allBanks[i].BankId,
                        text: allBanks[i].BankName
                    }));
                }


                $('.loader').hide();
            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }
        });
        return banks;
    }

    $('#selBank').on('change', function () {
        $('#selState').empty();

        Bankid = $('#selBank').val();
        $('#selState').append($('<option>', { value: 0, text: '--Select State--' }));
        $('#selCity').empty();
        $('#selCity').append($('<option>', { value: 0, text: '--Select City--' }));
        $('#selDistrict').empty();
        $('#selDistrict').append($('<option>', { value: 0, text: '--Select District--' }));
        $('#bankBranch').empty();
        $('#bankBranch').append($('<option>', { value: 0, text: '--Select Branch--' }));
        if (Bankid == 0) {
            $timeout(function () { $scope.divbtnState = true; }, 100);
        }
        else {
            $timeout(function () { $scope.divbtnState = false; }, 100);
        }
        $scope.Bankid = Bankid;

        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=State&BankId=" + Bankid;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {

                if (Banks.length > 0) {

                }
                else {

                    $timeout(function () { $scope.divbtnCity = true; }, 100);
                    $timeout(function () { $scope.divbtnDistrict = true; }, 100);
                    $('#selCity').empty();
                    $('#selCity').append($('<option>', { value: 0, text: '--Select City--' }));
                    $('#selDistrict').empty();
                    $('#selDistrict').append($('<option>', { value: 0, text: '--Select District--' }));



                }

                for (let i = 0; i < Banks.length; i++) {
                    $('#selState').append($('<option>', {
                        value: Banks[i].CircleId,
                        text: Banks[i].CircleName
                    }));
                }
            },
            error: function (err) {

            }
        });
        return banks;
    });
    $('#selState').on('change', function () {
       $('#selCity').empty();
        StateId = $('#selState').val();
        $('#selCity').append($('<option>', { value: 0, text: '--Select City--' }));
        $('#selDistrict').empty();
        $('#selDistrict').append($('<option>', { value: 0, text: '--Select District--' }));
        $('#bankBranch').empty();
        $('#bankBranch').append($('<option>', { value: 0, text: '--Select Branch--' }));
        if (StateId == 0) {
            $timeout(function () { $scope.divbtnCity = true; }, 100);
        }
        else {
            $timeout(function () { $scope.divbtnCity = false; }, 100);
        }
        $scope.StateId = StateId;
        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=City&StateId=" + StateId;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {

                for (let i = 0; i < Banks.length; i++) {
                    $('#selCity').append($('<option>', {
                        value: Banks[i].ZoneId,
                        text: Banks[i].ZoneName
                    }));
                }
            },
            error: function (err) {

            }
        });
        return banks;
    });
    $('#selCity').on('change', function () {
        $('#selDistrict').empty();
        var districtId = $('#selCity').val();
        $('#selDistrict').append($('<option>', { value: 0, text: '--Select District--' }));
        $('#bankBranch').empty();
        $('#bankBranch').append($('<option>', { value: 0, text: '--Select Branch--' }));
        if (districtId == 0) {
            $timeout(function () { $scope.divbtnDistrict = true; }, 100);
        }
        else {
            $timeout(function () { $scope.divbtnDistrict = false; }, 100);
        }

        $scope.CityID = districtId;
        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=District&districtId=" + districtId;
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {

                for (let i = 0; i < Banks.length; i++) {
                    $('#selDistrict').append($('<option>', {
                        value: Banks[i].RegionId,
                        text: Banks[i].RegionName
                    }));
                }
            },
            error: function (err) {

            }
        });
        return banks;
    });
   
    $scope.userRole = $cookies.get("Role");
    if ($scope.userRole == "1") {
        $scope.hidebtnDiv = true;
    }
    else {
        $scope.hidebtnDiv = false;
    }
    $scope.redirectToDashBoard = function () {
        $window.open('http://bcregistrybi.herokuapp.com/public/question/77b859e4-c700-4040-b9f0-d8d3f59b334a');
    };
    $scope.redirectToTopList= function () {
        $window.open('https://bcregistrybi.herokuapp.com/question/11');
    };
});