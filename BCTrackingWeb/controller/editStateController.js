﻿bctrackApp.controller("editStateController", function ($scope, $state, $cookies) {
    $scope.divhome = false;
    $scope.edithome = true;
    $scope.divcircle = true;
    $scope.divprofile = true;
    $scope.divsetting = true;
    $scope.displayTab = function (fn) {
        if (fn == "home") {
            $scope.edithome = false;
            $scope.divhome = false;
            $scope.divcircle = true;
            $scope.divprofile = true;
            $scope.divsetting = true;
        }
        if (fn == "circle") {
            $scope.edithome = true;
            $scope.divhome = true;
            $scope.divcircle = false;
            $scope.divprofile = true;
            $scope.divsetting = true;
        }
        if (fn == "profile") {
            $scope.edithome = true;
            $scope.divhome = true;
            $scope.divcircle = true;
            $scope.divprofile = false;
            $scope.divsetting = true;
        }
        if (fn == "setting") {
            $scope.edithome = true;
            $scope.divhome = true;
            $scope.divcircle = true;
            $scope.divprofile = true;
            $scope.divsetting = false;
        }
    }
  
    $scope.btnedit = 'Edit';
    $scope.divVIEW = false;
    $scope.divEdit = true;

    $scope.editView = function () {
        
        if ($scope.btnedit == "Edit") {
            $scope.btnedit = 'View';
            $scope.divVIEW = true;
            $scope.divEdit = false;
        }
        else {
            $scope.btnedit = 'Edit';
            $scope.divVIEW = false;
            $scope.divEdit = true;
        }

    }
    var b = $state.params.stateId;
    var stateId = '';
    if (b != undefined)
        stateId = parseInt(b);
    else
        stateId = 0;

    var errorMessage = "";

    $scope.districtsList = [];
    $scope.citiesList = [];
    $scope.talukaslist = [];
    $scope.villagesList = [];
    var districts = [];
    var cities = [];
    var talukas = [];
    var villages = [];



    $scope.deleteState = function () {

        if (confirm("Do you want to delete?")) {

            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: "api/DeleteBcHandler.ashx?mode=state&stateid=" + $state.params.stateId,
                async: false,
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                success: function () {
                    window.location.href = "#/statelist";

                    alert('State deleted successfully.');

                },
                error: function (xmlHttpRequest, errorString, errorMessage) {

                    window.location.href = "#/statelist";
                 
                    alert('State deleted successfully.');
                }
            });
        }
        return false;
    }


    function getParameterByName(name) //courtesy Artem
    {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else {
            if ((results[1].indexOf('?')) > 0)
                return decodeURIComponent(results[1].substring(0, results[1].indexOf('?')).replace(/\+/g, " "));
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    $scope.addDistrict = function () {
        var district = new Object();
        district.DistrictName = $('#txtDistrictName').val();
        $scope.districtsList.push(district)
        districts.push(district);
        $('#txtDistrictName').val('');
        PopulateDistricts();
    }

    $scope.removeDistrict = function (index) {
        districts.splice(index, 1);
        $scope.districtsList.splice(index, 1);
    }

    $scope.removeCities = function (index) {
        cities.splice(index, 1);
        $scope.citiesList.splice(index, 1);
    }
    $scope.removeTalukas = function (index) {
        talukas.splice(index, 1);
        $scope.talukaslist.splice(index, 1);
    }
    $scope.removeVillage = function (index) {
        villages.splice(index, 1);
        $scope.villagesList.splice(index, 1);
    }



    $scope.addCities = function () {
        
        var city = new Object();
        city.CityName = $('#txtCityName').val();
        var districtName = '';
        districtName = $('#selectDistrictNameForCity :selected').text();
        if (districtName === '---Select District---')
            districtName = ' ';
        city.DistrictName = districtName;
        $scope.citiesList.push(city)
        cities.push(city);
        $('#txtCityName').val('');

    }


    $scope.addTalukas = function () {
        
        var taluka = new Object();
        taluka.TalukaName = $('#txtTalukaName').val();
        var districtName = '';
        districtName = $('#selectDistrictNameForTaluka :selected').text();
        if (districtName === '---Select District---')
            districtName = ' ';
        taluka.DistrictName = districtName;
        $scope.talukaslist.push(taluka)
        talukas.push(taluka);
        PopulateTalukas();
        $('#txtTalukaName').val('');

    }

    $scope.addVilages = function () {
         
        var village = new Object();
        village.VillageName = $('#txtVillageName').val();
        var talukaName = '', districtName = '';
        talukaName = $('#selectTalukaNameForVillage :selected').text();
        districtName = $('#selectDistrictNameForVillage :selected').text();
        if (talukaName === '---Select Taluka---')
            talukaName = ' ';
        village.TalukaName = talukaName;
        if (districtName === '---Select District---')
            districtName = ' ';
        village.DistrictName = districtName;
        $scope.villagesList.push(village)
        villages.push(village);
        $('#txtVillageName').val('');


    }



    function PopulateDistricts() {
        $('#selectDistrictNameForCity').empty();
        $('#selectDistrictNameForVillage').empty();
        $('#selectDistrictNameForTaluka').empty();
        $('#selectDistrictNameForCity').append($('<option>', {
            value: 0,
            text: '---Select District---'
        }));
        $('#selectDistrictNameForVillage').append($('<option>', {
            value: 0,
            text: '---Select District---'
        }));
        $('#selectDistrictNameForTaluka').append($('<option>', {
            value: 0,
            text: '---Select District---'
        }));
        for (var i = 0; i < districts.length; i++) {
            $('#selectDistrictNameForCity').append($('<option>', {
                value: 0,
                text: districts[i].DistrictName
            }));
            $('#selectDistrictNameForVillage').append($('<option>', {
                value: 0,
                text: districts[i].DistrictName
            }));
            $('#selectDistrictNameForTaluka').append($('<option>', {
                value: 0,
                text: districts[i].DistrictName
            }));
        }
    }
    function PopulateTalukas() {
        $('#selectTalukaNameForVillage').empty();
        $('#selectTalukaNameForVillage').append($('<option>', {
            value: 0,
            text: '---Select Taluka---'
        }));
        for (var i = 0; i < talukas.length; i++) {
            $('#selectTalukaNameForVillage').append($('<option>', {
                value: 0,
                text: talukas[i].TalukaName
            }));
        }
    }




    function validate() {
        errorMsg = '';
        var name = $('#txtStateName').val();
        var result = true;
        if (name == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter State Name.";
            result = false;
        }
       

        return result;
    }
    $scope.addState = function () {
         
        if (validate() == true) {
            let collectedDistricts = [];
            //  for (let i = 0; i < districts.length; i++) {
            let district = new Object();
            district.StateId = stateId;
            district.DistrictList = $scope.districtsList;
            district.SubDistrict = $scope.subDistrictList;
            district.Villages = $scope.villagesList;
            //for (let j = 0; j < cities.length; j++) {
            //    if (cities[j].DistrictName === districts[i].DistrictName) {
            //        let city = new Object();
            //        city.CityName = cities[j].CityName;
            //        city.DistrictName = districts[i].DistrictName;
            //        district.Cities.push(city);
            //    }
            //}
            //for (let j = 0; j < talukas.length; j++) {
            //    if (talukas[j].DistrictName === districts[i].DistrictName) {
            //        let taluka = new Object();
            //        taluka.DistrictName = districts[i].DistrictName;
            //        taluka.TalukaName = talukas[j].TalukaName;
            //        district.Talukas.push(taluka);
            //    }
            //}
            //for (let j = 0; j < villages.length; j++) {
            //    let village = new Object();
            //    if (villages[j].DistrictName === districts[i].DistrictName) {
            //        village.DistrictName = districts[i].DistrictName;
            //        village.VillageName = villages[j].VillageName;
            //        village.TalukaName = villages[j].TalukaName;
            //        district.Villages.push(village);
            //    }
            //}
            //for (let j = 0; j < district.Talukas.length; j++) {
            //    district.Talukas[j].Villages = [];
            //    for (let k = 0; k < villages.length; k++) {
            //        if (villages[k].TalukaName === district.Talukas[j].TalukaName) {
            //            let village = new Object();
            //            village.TalukaName = district.Talukas[j].TalukaName;
            //            village.VillageName = villages[k].VillageName;
            //            district.Talukas[j].Villages.push(village);
            //        }
            //    }
            //}
            collectedDistricts.push(district);
            //}
            var state = new Object();
            state.StateId = stateId;

            let capturedStateName = $('#txtStateName').val();
            if (capturedStateName != null)
                state.StateName = capturedStateName;

            state.Districts = collectedDistricts;

            let requestUrl = 'api/InsertUpdateStateHander.ashx';
            $.ajax({
                url: requestUrl,
                datatype: 'json',
                data: { "state": JSON.stringify(state) },
                method: 'POST',
                async: false,
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                success: function () {

                    //    alert('State information saved successfully.');
                    //  window.location.href = "#/statelist";
                },
                error: function (xhr, errorString, errorMessage) {
                    //   alert('State information saved successfully.');
                    //  window.location.href = "#/statelist";
                }

            });


        }

        else {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
    }
    //$scope.addState = function () {
         
    //    if (validate() == true) {
    //        let collectedDistricts = [];
    //        for (let i = 0; i < districts.length; i++) {
    //            let district = new Object();
    //            district.StateId = stateId;
    //            district.DistrictName = districts[i].DistrictName;
    //            district.Cities = [];
    //            district.Talukas = [];
    //            district.Villages = [];
    //            for (let j = 0; j < cities.length; j++) {
    //                if (cities[j].DistrictName === districts[i].DistrictName) {
    //                    let city = new Object();
    //                    city.CityName = cities[j].CityName;
    //                    city.DistrictName = districts[i].DistrictName;
    //                    district.Cities.push(city);
    //                }
    //            }
    //            for (let j = 0; j < talukas.length; j++) {
    //                if (talukas[j].DistrictName === districts[i].DistrictName) {
    //                    let taluka = new Object();
    //                    taluka.DistrictName = districts[i].DistrictName;
    //                    taluka.TalukaName = talukas[j].TalukaName;
    //                    district.Talukas.push(taluka);
    //                }
    //            }
    //            for (let j = 0; j < villages.length; j++) {
    //                let village = new Object();
    //                if (villages[j].DistrictName === districts[i].DistrictName) {
    //                    village.DistrictName = districts[i].DistrictName;
    //                    village.VillageName = villages[j].VillageName;
    //                    village.TalukaName = villages[j].TalukaName;
    //                    district.Villages.push(village);
    //                }
    //            }
    //            for (let j = 0; j < district.Talukas.length; j++) {
    //                district.Talukas[j].Villages = [];
    //                for (let k = 0; k < villages.length; k++) {
    //                    if (villages[k].TalukaName === district.Talukas[j].TalukaName) {
    //                        let village = new Object();
    //                        village.TalukaName = district.Talukas[j].TalukaName;
    //                        village.VillageName = villages[k].VillageName;
    //                        district.Talukas[j].Villages.push(village);
    //                    }
    //                }
    //            }
    //            collectedDistricts.push(district);
    //        }
    //        var state = new Object();
    //        state.StateId = stateId;

    //        let capturedStateName = $('#txtStateName').val();
    //        if (capturedStateName != null)
    //            state.StateName = capturedStateName;

    //        state.Districts = collectedDistricts;

    //        let requestUrl = 'api/InsertUpdateStateHander.ashx';
    //        $.ajax({
    //            url: requestUrl,
    //            datatype: 'json',
    //            data: { "state": JSON.stringify(state) },
    //            method: 'POST',
    //            async: false,
    //            headers: {
    //                "userId": sessionStorage.getItem("userId"),
    //                "TokenId": sessionStorage.getItem("TokenId")
    //            },
    //            success: function () {
                   
    //            //    alert('State information saved successfully.');
    //              //  window.location.href = "#/statelist";
    //            },
    //            error: function (xhr, errorString, errorMessage) {
    //             //   alert('State information saved successfully.');
    //              //  window.location.href = "#/statelist";
    //            }
              
    //        });


    //    }

    //    else {
    //        ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    //    }
    //}

    $scope.addBranch = function () {

        var branch = new Object();
        branch.BranchName = $('#txtBranchName').val();

        branch.Address = $('#txtBranchAddress').val();
        branch.RegionId = $('#selBranchRegion').val();
        branch.StateId = $('#selBranchState').val();
        branch.DistrictId = $('#selBranchDistrict').val();
        branch.TalukaId = $('#selBranchTaluka').val();
        branch.CityId = $('#selBranchCity').val();
        branch.VillageId = $('#selBranchVillage').val();
        branch.PinCode = $('#txtBranchPinCode').val();
        branch.ContactNumber = $('#txtBranchContact').val();
        branch.Email = $('#txtBranchEmail').val();
        var RegionName = $("#selBranchRegion option:selected").text();
        branches.push(branch);

        for (var r = 0; r < regions.length; r++) {
            var reg = regions[r];
            if (reg.RegionName == RegionName) {
                reg.RegionBranches.push(branch);

            }

        }
        var s = $scope.circles;
        $scope.brancheslist.push(branch);


    };
    $scope.removeBranch = function (index, RegionName) {

        for (var r = 0; r < regions.length; r++) {
            var reg = regions[r];
            if (reg.RegionName == RegionName) {
                reg.RegionBranches.push(branch);

            }

        }
        $scope.brancheslist.splice(index, 1);
    }


    function ShowDialog(message, height, width, title, padding, icon, showOkButton, link) {

        $("#helloModalMessage").html(message);

        $('#helloModal').modal('show');

        if (showOkButton == false) {
            $("#closemodalbutton").show();
            $("#redirectbutton").hide();
        }
        else {
            if (link != '')
                $("#redirectbutton").attr("href", link);
            $("#closemodalbutton").hide();
            $("#redirectbutton").show();
        }

        //$.Dialog({
        //    overlay: true,
        //    shadow: true,
        //    flat: true,
        //    title: title,
        //    icon: icon,
        //    content: '',
        //    width: width,
        //    padding: padding,
        //    height: height,//600,400
        //    onShow: function (_dialog) {
        //        var content = _dialog.children('.content');
        //        content.html(message);
        //        $.Metro.initInputs();
        //    }
        //});
    }

    //$scope.getStateDetails = function () {
        
    //    //talk to the DB first to get all the details of the state with stateId    
    //    if (stateId != '') {
    //        /*$('.loader').show();*/
    //        $.ajax({
    //            type: 'POST',
    //            dataType: 'json',
    //            contentType: 'application/json',
    //            url: 'api/GetBCHandler.ashx?mode=state&stateId=' + stateId,
    //            async: false,
    //            headers: {
    //                "userId": sessionStorage.getItem("userId"),
    //                "TokenId": sessionStorage.getItem("TokenId")
    //            },
    //            success: function (state) {
    //                 
    //                var statename = state[0].StateName
    //                $scope.lblStateName = state[0].StateName;
    //                $scope.txtStateName = state[0].StateName;
    //                $('#txtStateName').val(state[0].StateName);

    //                for (let i = 0; i < state.length; i++) {
    //                    console.log(JSON.stringify(state[i]) + '\n');
    //                    if (state[i].Districts != null) {
    //                        for (let j = 0; j < state[i].Districts.length; j++) {
    //                            let tempDistrict = new Object();
    //                            tempDistrict.DistrictName = state[i].Districts[j].DistrictName;
    //                            districts.push(tempDistrict);
    //                            $scope.districtsList.push(tempDistrict);

    //                            for (let k = 0; k < state[i].Districts[j].Cities.length; k++) {
    //                                let tempCity = new Object();
    //                                tempCity.CityName = state[i].Districts[j].Cities[k].CityName;
    //                                tempCity.DistrictName = state[i].Districts[j].DistrictName;
    //                                cities.push(tempCity);

    //                                $scope.citiesList.push(tempCity);

    //                            }

    //                            for (let k = 0; k < state[i].Districts[j].Talukas.length; k++) {
    //                                let tempTaluka = new Object();
    //                                tempTaluka.TalukaName = state[i].Districts[j].Talukas[k].TalukaName;
    //                                tempTaluka.DistrictName = state[i].Districts[j].DistrictName;
    //                                talukas.push(tempTaluka);

    //                                $scope.talukaslist.push(tempTaluka);

    //                            }

    //                            for (let k = 0; k < state[i].Districts[j].Villages.length; k++) {
    //                                let tempVillage = new Object();
    //                                tempVillage.DistrictName = state[i].Districts[j].DistrictName;
    //                                tempVillage.VillageName = state[i].Districts[j].Villages[k].VillageName;
    //                                tempVillage.TalukaName = state[i].Districts[j].Villages[k].TalukaName;
    //                                villages.push(tempVillage);
    //                                $scope.villagesList.push(tempVillage);


    //                            }
    //                        }
    //                    }
    //                }
    //                PopulateDistricts();
    //                PopulateTalukas();
    //            },
    //            error: function (XmlHttpRequest, errorString, errorMessage) {
    //                alert(errorMessage);
    //            }
    //        });
    //    }
    //}

})