﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="AddBank1.aspx.cs" Inherits="BCTrackingWeb.AddBank1" %>
<html>
    <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>BC Tracking System</title>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
       <script src="Angularjs/angular15.js"></script>
   <script src="Angularjs/MainModule.js"></script>
    <script src="Angularjs/addBankController.js"></script>

</head>
    <div class="main-page"  ng-app="BCTRACK" ng-controller="addBankController" ng-init="GetStates()" ng-clock>
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-12">
                    <h2 class="title">Add Bank </h2>

                </div>


                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                       <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="BankList.aspx"> Bank List</a></li>
                        <li><a href="AddBank.aspx">Add Bank</a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">

                                </div>
                            </div>
                            <div class="panel-body">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home"
                                                                              role="tab" data-toggle="tab"><label
                                            for="exampleInputEmail1">Bank Details</label> </a></li>
                                            <li role="presentation"><a href="#circle" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Circle
                                    </label> </a></li>
                                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Branches
                                    </label> </a></li>


                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content bg-white p-15">
                                    <div role="tabpanel" class="tab-pane active" id="home">


                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel">

                                                        <div class="panel-body p-20">
                                                            <div class="panel-body">

                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class=" control-label">Name</label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtName" placeholder="Name"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                </div>


                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class="control-label">Address</label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight "
                                                                               id="txtAddress" placeholder="Address"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class="control-label">Contact</label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtContact" placeholder="Contact"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <label for="fileImage" class="control-label">
                                                                               Email</label>
                                                                    </div>
                                                                    <div class="col-sm-9 form-group">
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtEmail" placeholder="Email"
                                                                               parsley-trigger="change" required>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                    </div>
                                                   
                                                    <!-- /.panel -->
                                                </div>
                                            </div>
                                        </div>


                                    </div>


                                    <div role="tabpanel" class="tab-pane" id="circle">


                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    
                                                    <div class="panel">
                                                        <div class="panel-heading">
                                                            <div class="panel-title">
                                                                Circles
                                                            </div>
                                                        </div>
                                                        <div class="panel-body p-20">

                                                            <div class="panel-body">
                                                                <div class="row">


                                                                    <div class="col-sm-12">
                                                                        <strong>Circle Name</strong>
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtCircleName"
                                                                               placeholder="Circle Name" />
                                                                               
                                                                    </div>


                                                                </div>
                                                                <div class="row pull-right" id="Div2">
                                                                    <div class="col-sm-12">
                                                                        <input type="button"
                                                                               class="btn btn-primary  btn-sm form-control"
                                                                               ng-click="addCircle()"
                                                                               ng-hide=""
                                                                               value="Add More  " id="Button2">
                                                                    </div>
                                                                </div>
                                                                <br/>
                                                                <br/>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <table id="" class="table ">
                                                                            <tr
                                                                            ">
                                                                            <td><strong>Circle</strong>
                                                                            </td>
                                                                            <td><strong>Remove</strong></td>
                                                                            </tr>
                                                                            <tr ng-repeat="item in circles">
                                                                                <td>{{item.CircleName}}</td>

                                                                                <td>
                                                                                    <div ng-click="removeCircle($index)"
                                                                                         class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>


                                                            </div>


                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                    </div>
                                                    <div class="panel">
                                                        <div class="panel-heading">
                                                            <div class="panel-title">
                                                                Zones
                                                            </div>
                                                        </div>
                                                        <div class="panel-body p-20">

                                                            <div class="panel-body">
                                                                <div class="row">

                                                                    <div class="col-sm-6">
                                                                        <strong>Zone Name</strong>
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtZoneName" placeholder="Zone Name">
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <strong>Circle</strong>
                                                                     
                                                                             <select type="text" class="form-control formcontrolheight" id="selZoneCircle"></select>
                                                                    </div>


                                                                </div>
                                                                <div class="row pull-right" id="Div2">
                                                                    <div class="col-sm-12">
                                                                        <input type="button"
                                                                               class="btn btn-primary  btn-sm form-control"
                                                                               ng-click="addZone()"
                                                                               ng-hide=""
                                                                               value="Add More  " id="Button2">
                                                                    </div>
                                                                </div>
                                                                <br/>
                                                                <br/>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <table id="" class="table ">
                                                                            <tr
                                                                            ">
                                                                            <td><strong>Zone</strong>
                                                                            </td>
                                                                            <td><strong>Circle</strong>
                                                                            </td>
                                                                            <td><strong>Remove</strong></td>
                                                                            </tr>
                                                                            <tr ng-repeat="item in zoneList ">
                                                                                <td>{{item.Zone}}</td>
                                                                                <td>{{item.cName}}</td>
                                                                                <td>
                                                                                    <div ng-click="removeZone($index,item.cName)"
                                                                                         class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>


                                                            </div>


                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                    </div>
                                                    <div class="panel">
                                                        <div class="panel-heading">
                                                            <div class="panel-title">
                                                                Regions
                                                            </div>
                                                        </div>
                                                        <div class="panel-body p-20">

                                                            <div class="panel-body">
                                                                <div class="row">


                                                                    <div class="col-sm-6">
                                                                        <strong>Region Name</strong>
                                                                        <input type="text"
                                                                               class="form-control formcontrolheight"
                                                                               id="txtRegionName"
                                                                               placeholder="Region Name">
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <strong>Zone</strong>
                                                                       <select type="text" class="form-control formcontrolheight" id="selRegionZone"></select>
                                                                    </div>


                                                                </div>
                                                                <div class="row pull-right" id="Div2">
                                                                    <div class="col-sm-12">
                                                                        <input type="button"
                                                                               class="btn btn-primary  btn-sm form-control"
                                                                               ng-click="addRegion()"
                                                                               ng-hide="!regionList "
                                                                               value="Add More  " id="Button2">
                                                                    </div>
                                                                </div>
                                                                <br/>
                                                                <br/>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <table id="" class="table ">
                                                                            <tr
                                                                            ">
                                                                            <td><strong>Region</strong>
                                                                            </td>
                                                                            <td><strong>Zone</strong>
                                                                            </td>
                                                                            <td><strong>Remove</strong></td>
                                                                            </tr>
                                                                            <tr ng-repeat="item in regionList ">
                                                                                <td>{{item.region}}</td>
                                                                                <td>{{item.zone}}</td>
                                                                                <td>
                                                                                    <div ng-click="removeRegion($index,item.zone)"
                                                                                         class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>


                                                            </div>


                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                    </div>
                                                    <!-- /.panel -->
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="profile">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <strong>Branch Name</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchName"
                                                                                   placeholder="Branch Name">
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>Region</strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchRegion"></select>
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>IFSC Code</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchIFSCCode"
                                                                                   placeholder="IFSC Code">
                                                                        </div>


                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <strong>Address</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchAddress"
                                                                                   placeholder="Branch Address">
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>State</strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchState"></select>
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>District</strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchDistrict"></select>
                                                                        </div>


                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <strong>Taluka</strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchTaluka"></select>
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>City</strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchCity"></select>
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>Village</strong>
                                                                            <select type="text"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selBranchVillage"></select>
                                                                        </div>


                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <strong>Pin Code</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchPinCode"
                                                                                   placeholder="Pin Code">
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>Contact</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight "
                                                                                   id="txtBranchContact"
                                                                                   placeholder="Contact">
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>Email</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtBranchEmail"
                                                                                   placeholder="Email">
                                                                        </div>


                                                                    </div>
                                                                    <div class="row pull-right" id="Div2">
                                                                        <div class="col-sm-12">
                                                                            <input type="button"
                                                                                   class="btn btn-primary  btn-sm form-control"
                                                                                   ng-click="addBranch()"
                                                                                   ng-hide="!brancheslist "
                                                                                   value="Add More  " id="Button2">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                    <br/>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Branch</strong>
                                                                                </td>
                                                                                <td><strong>Region</strong></td>
                                                                                <td><strong> Address</strong></td>
                                                                                <td><strong>Contact</strong></td>
                                                                                <td><strong>Email</strong></td>
                                                                                <td><strong>Remove</strong></td>
                                                                                </tr>
                                                                                <tr ng-repeat="item in brancheslist ">
                                                                                    <td>{{item.BranchName}}</td>
                                                                                    <td>{{item.RegionName}}</td>
                                                                                     <td>{{item.Address}}</td>
                                                                                <td>{{item.ContactNumber}}</td>
                                                                               <td>{{item.Email}}</td>
                                                                           
                                                                                    <td>
                                                                                        <div ng-click="removeBranch($index,item.RegionName)"
                                                                                             class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>
                                    <div class="formmargin center-block">
                    <div class="center-block">
                        <button type="submit"  ng-click="addBank()" class="btn btn-success center-block">Submit</button>
                    </div>
                </div>

                                </div>

                             
                    
                
                                <!-- /.src-code -->

                            </div>
                              
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>
      
        <script src="ot/js/jquery/jquery-2.2.4.min.js"></script>
        

   <%-- <script src="js/jquery-1.12.2.min.js"></script>--%>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/parsley.min.js" defer></script>
    <script src="js/main.js"></script>
    <script src="js/banks.js"></script>

    </html>