﻿using SBI.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCTrackingWeb
{
    public partial class Options : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Util.IsUserLoggedIn())
                Response.Redirect("Login.aspx?ReturnUrl=" + Page.AppRelativeVirtualPath.Replace("~/", ""));
        }
    }
}