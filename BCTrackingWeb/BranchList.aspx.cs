﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using BCTrackingBL;
using BCEntities;


namespace BCTrackingWeb
{
    public partial class BranchList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetBranch()
        {
            // Paging parameters:
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);

            // Sorting parameters
            var iSortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var iSortDir = HttpContext.Current.Request.Params["sSortDir_0"];
            // Search parameters
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToLower();
            int totalRecords = 0;
            if (System.Web.HttpContext.Current.Session["UserInfo"] != null) ;
            var userId = HttpContext.Current.Request.Params["adminuserId"].ToLower();

            var leadList = Common.GetBranch(iDisplayStart, iDisplayLength, iSortCol, iSortDir, sSearch, out totalRecords, userId,"","","1");

            Func<Branch, object> order = p =>
            {
                if (iSortCol == 0)
                {
                    return p.BranchId;
                }

                else if (iSortCol == 1)
                {
                    return p.BranchName;
                }
                else if (iSortCol == 2)
                {
                    return p.ContactNumber;
                }
                else if (iSortCol == 3)
                {
                    return p.IFSCCode;
                }              
                else if (iSortCol == 4)
                {
                    return p.Address;
                }



                return p.BranchId;
            };

            //    // Define the order direction based on the iSortDir parameter
            if ("desc" == iSortDir)
            {
                leadList = leadList.OrderByDescending(order).ToList<Branch>();
            }
            else
            {
                leadList = leadList.OrderBy(order).ToList<Branch>();
            }
            var finalResult = leadList
                    .Select(p => new[] { p.BranchId.ToString(), p.BranchName, p.IFSCCode, p.ContactNumber, p.Address});

            int count = finalResult.Count();

            // prepare an anonymous object for JSON serialization
            var result = new
            {
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = finalResult
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            return json;
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string GetBranchFilter()
        {
            // Paging parameters:
            var iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            var iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);

            // Sorting parameters
            var iSortCol = int.Parse(HttpContext.Current.Request.Params["iSortCol_0"]);
            var iSortDir = HttpContext.Current.Request.Params["sSortDir_0"];
            // Search parameters
            var sSearch = HttpContext.Current.Request.Params["sSearch"].ToLower();
            int totalRecords = 0;
            if (System.Web.HttpContext.Current.Session["UserInfo"] != null) ;
            var userId = HttpContext.Current.Request.Params["adminuserId"].ToLower();
            var BankId = HttpContext.Current.Request.Params["adminBankId"].ToLower();
            var DistrictId = HttpContext.Current.Request.Params["DistrictId"].ToLower();
            var iType = HttpContext.Current.Request.Params["iType"].ToLower();

            var leadList = Common.GetBranch(iDisplayStart, iDisplayLength, iSortCol, iSortDir, sSearch, out totalRecords, userId, BankId, DistrictId, iType);

            Func<Branch, object> order = p =>
            {
                if (iSortCol == 0)
                {
                    return p.BranchId;
                }

                else if (iSortCol == 1)
                {
                    return p.BranchName;
                }
                else if (iSortCol == 2)
                {
                    return p.ContactNumber;
                }
                else if (iSortCol == 3)
                {
                    return p.IFSCCode;
                }
                else if (iSortCol == 4)
                {
                    return p.Address;
                }



                return p.BranchId;
            };

            //    // Define the order direction based on the iSortDir parameter
            if ("desc" == iSortDir)
            {
                leadList = leadList.OrderByDescending(order).ToList<Branch>();
            }
            else
            {
                leadList = leadList.OrderBy(order).ToList<Branch>();
            }
            var finalResult = leadList
                    .Select(p => new[] { p.BranchId.ToString(), p.BranchName, p.IFSCCode, p.ContactNumber, p.Address });

            int count = finalResult.Count();

            // prepare an anonymous object for JSON serialization
            var result = new
            {
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                aaData = finalResult
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            return json;
        }
    }
}