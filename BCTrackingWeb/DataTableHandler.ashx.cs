﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCEntities;
using Newtonsoft.Json;
namespace BCTrackingWeb
{
    /// <summary>
    /// Summary description for DataTableHandler
    /// </summary>
    public class DataTableHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

           

            string bcData = context.Request["bcorr"];
            bcData = HttpUtility.UrlDecode(bcData);
            BankCorrespondence bc = JsonConvert.DeserializeObject<BankCorrespondence>(bcData);

            string rtnData = context.Request["allCerts"];
            rtnData = HttpUtility.UrlDecode(rtnData);
            List<BCCertifications> certs = JsonConvert.DeserializeObject<List<BCCertifications>>(rtnData);
            bc.Certifications = certs;


            BCTrackingBL.BankBL bl = new BCTrackingBL.BankBL();
            //lb.Insert(bc);

            //BankCorrespondence bc = JsonConvert.DeserializeObject<BankCorrespondence>(rtnData);


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}