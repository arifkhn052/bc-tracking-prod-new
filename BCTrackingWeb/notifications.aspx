﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="notifications.aspx.cs" Inherits="BCTrackingWeb.notifications" %>
<div>
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-6">
                    <h2 class="title">List of Notification</h2>
                </div>

                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="#" ui-sref="notifications">Notification List</a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">


                    <!-- /.col-md-6 -->

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">
                                </div>
                            </div>
                            <div class="panel-body p-20">

                                <table id="tblNotificationS" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Notification Type</th>
                                            <th>Notification Text</th>
                                            <th>Notification Date</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>


                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-8 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>


    <%--<script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/getbc.js" type="text/javascript"></script>--%>
    <%--<script>

        $(document).ready(function () {



            var leadTable = $('#tblNotificationS').dataTable({
                "oLanguage": {
                    "sZeroRecords": "No records to display",
                    "sSearch": "Search "
                },
                "sDom": 'T<"clear">lfrtip',
                "tableTools": {

                    "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
                },
                "iDisplayLength": 15,
                "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                "bSortClasses": false,
                "bStateSave": false,
                "bPaginate": true,
                "bAutoWidth": false,
                "bProcessing": true,
                "bServerSide": true,
                "bDestroy": true,
                "sAjaxSource": "notifications.aspx/getNotification",
                "columnDefs": [                  
                   
                
                ],
                "bDeferRender": true,
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success":
                                    function (msg) {
                                        var json = jQuery.parseJSON(msg.d);
                                        fnCallback(json);
                                        $("#tblNotificationS").show();
                                    }
                    });
                }
            });
            //  leadTable.fnSetFilteringDelay(300);
        });

        function deleteUser(stateid) {
            
            if (confirm("Do you want to delete?")) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: "DeleteBcHandler.ashx?mode=&corporateId=" + corporateId,
                    async: false,
                    success: function () {
                        window.location.reload();

                    }
                });

            }
            return false;
        }
    </script>--%>

