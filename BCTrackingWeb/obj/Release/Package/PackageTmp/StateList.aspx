﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mainMaster.Master" AutoEventWireup="true" CodeBehind="StateList.aspx.cs" Inherits="BCTrackingWeb.StateList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <title>BC Tracking System</title>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="main-page">
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-6">
                    <h2 class="title">List of State</h2>

                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 right-side">
                    <a href="AddStateInfo.aspx" class="btn bg-black" role="button"><i class="fa fa-plus"></i> Add State</a>
                </div>
                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="home.aspx"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="StateList.aspx"> State List</a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">


                    <!-- /.col-md-6 -->

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">

                                </div>
                            </div>
                            <div class="panel-body p-20">

                                <table id="tblStateList" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>State</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>

                                
                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-8 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>
     <%--<div class="container page-content">
        <div class="loader"></div>
        <div>
            <ul class="breadcrumbs mini">
                <li>
                    <a href="Home.aspx">
                        <span class="icon mif-home"></span>
                    </a>
                </li>
                <li>
                    <a href="StateList.aspx">
                        <span>State List</span>
                    </a>
                </li>
            </ul>
        </div>
        <br>
        <table id="tblStateList" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>State</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        <br>
            
        <div class="formmargin center-block">
            <div class="col-sm-2">
                <a href="AddStateInfo.aspx" class="btn btn-default center-block">Add State</a>
            </div>
        </div>
    </div>--%> 

    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script type="text/javascript" src="js/parsley.min.js" defer></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/states.js"></script>
</asp:Content>
