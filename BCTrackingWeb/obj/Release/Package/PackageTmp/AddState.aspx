﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="AddState.aspx.cs" Inherits="BCTrackingWeb.AddState" %>

    <%--<div class="main-page" >--%>
        <div class="container-fluid"  ng-app="BCTRACK" ng-controller="addStateController"ng-clock>
            <div class="row page-title-div">
                <div class="col-md-12">
                    <h2 class="title">Add State </h2>

                </div>


                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                      <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="StateList.aspx"> State List</a></li>
                        <li><a href="AddStateInfo.aspx">Add State Info</a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">

                                </div>
                            </div>
                            <div class="panel-body">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home"
                                                                              role="tab" data-toggle="tab"><label
                                            for="exampleInputEmail1">State Details </label> </a></li>
                                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">City
                                        Details
                                    </label> </a></li>
                                    <li role="presentation"><a href="#message" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Taluka
                                        Details
                                    </label> </a></li>
                                    <li role="presentation"><a href="#setting" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Village
                                        Details
                                    </label> </a></li>


                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content bg-white p-15">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <strong>State Name</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtStateName"
                                                                                   ng-model="txtStateName"
                                                                                   placeholder="State Name"
                                                                                   parsley-trigger="change"  required />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Districts
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">


                                                                                <div class="col-sm-12">
                                                                                    <strong>District Namee</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtDistrictName"
                                                                                           ng-model="txtDistrictName"
                                                                                           placeholder="District Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>


                                                                            </div>
                                                                            <div class="row pull-right" id="Div2">
                                                                                <div class="col-sm-12">
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addDistrict()"
                                                                                           ng-disabled="!txtDistrictName"
                                                                                           value="Add More  "
                                                                                           id="Button2">
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr
                                                                                        ">
                                                                                        <td><strong>District
                                                                                            Name</strong>
                                                                                        </td>
                                                                                        <td><strong>Remove</strong></td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in districtsList ">
                                                                                            <td>{{item.DistrictName}}</td>

                                                                                            <td>
                                                                                                <div ng-click="removeDistrict($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="profile">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">

                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Cities
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">


                                                                                <div class="col-sm-6">
                                                                                    <strong>City Name</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtCityName"
                                                                                           ng-model="txtCityName"
                                                                                           placeholder="City Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <strong>District</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectDistrictNameForCity"></select>
                                                                                </div>


                                                                            </div>
                                                                            <div class="row pull-right" id="Div2">
                                                                                <div class="col-sm-12">
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addCities()"
                                                                                            ng-disabled="!txtCityName"
                                                                                           value="Add More  "
                                                                                           id="Button2">
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr>
                                                                                            <td><strong>City
                                                                                                Name</strong>
                                                                                            <td><strong>District
                                                                                                Name</strong>
                                                                                            </td>
                                                                                            <td><strong>Remove</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in citiesList ">
                                                                                            <td>{{item.CityName}}</td>
                                                                                              <td>{{item.DistrictName}}</td>
                                                                                           
                                                                                            <td></td>
                                                                                            <td>
                                                                                                <div ng-click="removeCities($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="message">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">

                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Talukas
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">


                                                                                <div class="col-sm-6">
                                                                                    <strong>Taluka Name</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtTalukaName"
                                                                                           ng-model="txtTalukaName"
                                                                                           placeholder="Taluka Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <strong>District</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectDistrictNameForTaluka"></select>
                                                                                </div>


                                                                            </div>
                                                                            <div class="row pull-right" id="Div2">
                                                                                <div class="col-sm-12">
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addTalukas()"
                                                                                           ng-disabled="!txtTalukaName "
                                                                                           value="Add More  "
                                                                                           id="Button2">
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr>
                                                                                            <td><strong>Taluka
                                                                                                Name</strong>
                                                                                            <td><strong>District
                                                                                                Name</strong>
                                                                                            </td>
                                                                                            <td><strong>Remove</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in talukaslist">
                                                                                            <td>{{item.TalukaName}}</td>
                                                                                            <td>{{item.DistrictName}}</td>
                                                                                            <td>
                                                                                                <div ng-click="removeTalukas($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="setting">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">

                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Villages
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">


                                                                                <div class="col-sm-4">
                                                                                    <strong>Villages Name</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtVillageName"
                                                                                           ng-model="txtVillageName"
                                                                                           placeholder="Village Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <strong>District</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectDistrictNameForVillage"></select>
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <strong>Taluka</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectTalukaNameForVillage"></select>
                                                                                </div>


                                                                            </div>
                                                                            <div class="row pull-right" id="Div2">
                                                                                <div class="col-sm-12">
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addVilages()"
                                                                                           ng-disabled="!txtVillageName "
                                                                                           value="Add More  "
                                                                                           id="Button2">
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr>
                                                                                            <th>Village Name</th>
                                                                                            <th>Taluka Name</th>
                                                                                            <th>District Name</th>

                                                                                            <td><strong>Remove</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in villagesList ">
                                                                                            <td>{{item.VillageName}}</td>
                                                                                         <td>{{item.TalukaName}}</td>
                                                                                         <td>{{item.VillageName}}</td>
                                                                                            <td>
                                                                                                <div ng-click="removeVillage($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>

                                           <div class="formmargin center-block">
                    <div class="center-block">
                        <button type="submit" ng-click="addState()" class="btn btn-success center-block">Submit</button>
                    </div>
                </div>
                                </div>


                                <!-- /.src-code -->

                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

   <%-- </div>--%>


    
<%--    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/parsley.min.js" defer></script>
    <script src="js/main.js"></script>
    <script src="js/states.js"></script>--%>

