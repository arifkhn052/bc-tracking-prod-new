﻿
$(document).ready(function () {
  
    var d = new Date();


    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
        (('' + month).length < 2 ? '0' : '') + month + '-' +
        (('' + day).length < 2 ? '0' : '') + day;
    $("#txtmarginmatrix").prop("readonly", true);
    $("#txtmarginmatrix").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '2010: 3000' });
    $("#txtisdadate").prop("readonly", true);
    $("#txtisdadate").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '2010: 3000' });
    $("#txtecbmaturity").prop("readonly", true);
    $("#txtecbmaturity").datepicker({
        dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true,
        yearRange: '2010: 5000'
    });


  //  $("#txtfwdrs").prop("readonly", true);
    $("#txtfwdrs").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '2010: 3000' });
 //   $("#txtderivativers").prop("readonly", true);
    $("#txtderivativers").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '2010: 3000' });
    $("#txtboardResolution").prop("readonly", true);
    $("#txtboardResolution").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '2010: 3000' });

    $('#datetimepicker10 input').on("keydown", function (e) { e.preventDefault(); });
    
    $("#DivAllocattype").hide();
    $("#DivTmo").hide();
    $("#DivRtmo").hide();
   
    $("#chkselfAllocate").change(function () {


        $.ajax({
            url: 'tCustomer.aspx/GetUserDetails',
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
               
                var checkbox = $('#chkselfAllocate').is(':checked');
                $.each(data, function () {
                    $.each(this, function (k, v) {
                        
                        if (k == "RTMU Head" || k == "CTMU Head" && checkbox == false)
                        {
                            $("#DivAllocattype").show();

                        }
                        else
                        {
                            $("#DivAllocattype").hide();
                            $("#DivRtmo").hide();
                            $("#DivTmo").hide();
                            $("#DdlTmo").html('');
                            $('#DdlTmo').append('<option value="">' + 'select' + '</option>');
                            $("#DdlRTMU").html('');
                            $('#DdlRTMU').append('<option value="">' + 'select' + '</option>');


                        }
                        if (k == "TMO" && checkbox == false)
                        {
                            $("#DivRtmo").show();
                            Getrtmu();
                           
                           
                        }
                        else
                        {
                            $("#DivRtmo").hide();
                            $("#DdlRTMU").html('');
                            $('#DdlRTMU').append('<option value="">' + 'select' + '</option>');

                        }

                    });
                });
               
            }
        });



      

    });
    $("#rbtmo").change(function () {
        var checkbox = $('#rbtmo').is(':checked');
        if (checkbox = true)
        {
            $("#DivTmo").show();
            $("#DivRtmo").hide();
            $("#DdlRTMU").html('');
            $('#DdlRTMU').append('<option value="">' + 'select' + '</option>');
            GetTMO();
        }
        


    });

    $("#rbRtmo").change(function () {
        var checkbox = $('#rbRtmo').is(':checked');
        if (checkbox = true) {
            $("#DivTmo").hide();
            $("#DivRtmo").show();
            $("#DdlTmo").html('');
            $('#DdlTmo').append('<option value="">' + 'select' + '</option>');
            Getrtmu();
        }
       

    });

   
  
   
    
});
function validateisnumber(id) {
    
    var value = "#" + id;


    $(value).on("keypress keyup blur", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }

    });
}


GetTMO = function () {
    $.ajax({
        url: 'tCustomer.aspx/GetTMO',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {
                    $('#DdlTmo').append('<option value="' + k + '">' + v + '</option>');

                });
            });
        }
    });
}

Getrtmu = function () {
    $.ajax({
        url: 'tCustomer.aspx/GetRtrmo',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            
            $.each(data, function () {
                $.each(this, function (k, v) {
                    $('#DdlRTMU').append('<option value="' + k + '">' + v + '</option>');

                });
            });
        }
    });
}

$("#ddlsector").change(function () {
    $("#divsubsectorother").hide();

    
    $('#ddlsubsector').html('');
   
    var sector_id = $("#ddlsector").val();
    $.ajax({
        url: 'tCustomer.aspx/GetSubSector',
        type: "POST",
        dataType: "json",
        data: "{'sector_id': '" + sector_id + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
           
            $('#ddlsubsector').append('<option value="">' + 'Select' + '</option>');
           
            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#ddlsubsector').append('<option value="' + k + '">' + v + '</option>');
                });
            });
            $('#ddlsubsector').append('<option value="Other">' + 'Other' + '</option>');
        }
    });
});
$("#divsubsectorother").hide();
$("#ddlsubsector").change(function () {
    
    var subsector_id = $("#ddlsubsector").val();
    if (subsector_id == "Other")
    {
        $("#divsubsectorother").show();
     
    }
    else
    {
        $("#divsubsectorother").hide();
    }
   
});
$("#DivOtherRM").hide();
$("#txtrmname").change(function () {
   
    var rmname = $("#txtrmname").val();
    if (rmname == "other") {
        $("#DivOtherRM").show();
        $("#divphnoetelephne").hide();
        

    }
    else {
        $("#DivOtherRM").hide();
        $("#divphnoetelephne").show();
    }

});
$("#logout").click(function () {
    

    alert();
   
    

});



//GetYear = function () {
//    
//    $.ajax({
//        url: 'tCustomer.aspx/GetYear',
//        type: "POST",
//        dataType: "json",
//        contentType: "application/json; charset=utf-8",
//        async: false,
//        success: function (data) {

//            $.each(data, function () {
//                $.each(this, function (k, v) {
//                    $('#performance_year').append('<option value="' + k + '">' + v + '</option>');

//                });
//            });
//        }
//    });
//}




