﻿//$("#A7").click(function () {
//    window.location.href = "Home.aspx";
//});

$(document).ready(function () {
    var emp_id = $("#hdnEmpId").val();
    GetUserDetail(emp_id);

    $("#divreason").hide();
    $("#divaddflagproc1").hide();
    $("#divflagstatus").hide();
    $("#NewDivcif").hide();
    Getleadlist();

    var leadstatus = $("#hdnStatus").val();
    tabing();
    $("#UL2").hide();
    var Pid = getParameterByName('pid');

    if (Pid != "1") {

        $("#UL2").hide();
        if (leadstatus == "Disbursed") {
            $("#DDLStatus").prop("disabled", true);
            $("#chckMeeting").prop("disabled", true);

            // hide controls
            if ($("#hdnStaffType").val() != 'Admin') {
                $("#DDLNewEnhance").prop("disabled", true);
                $("#Addproduct").prop("disabled", true);
                $("#txtDateOfAction").prop("disabled", true);
                $("#txtComments").prop("disabled", true);
                $("#btnSubmit").prop("disabled", true);
            } else {
                $("#DDLNewEnhance").prop("disabled", false);
                $("#Addproduct").prop("disabled", false);
                $("#txtDateOfAction").prop("disabled", false);
                $("#txtComments").prop("disabled", false);
                $("#btnSubmit").prop("disabled", false);
            }

        } else {
            $("#DDLStatus").prop("disabled", false);
            $("#chckMeeting").prop("disabled", false);


        }
    } else {
        if (leadstatus != "To Initiate") {
            $("#UL2").hide();
            $("#lblMsg").show();
        } else {
            $("#lblMsg").hide();
            $("#UL2").show();
        }
    }
    //NumbersToWords(1911111111);

    //alert(num2words.numberToWords(300000000000).replace("Zero",""));
    GetProductList();
 
    GetBranches();
    GetTMO();
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
        (('' + month).length < 2 ? '0' : '') + month + '-' +
        (('' + day).length < 2 ? '0' : '') + day;

    $("#txtDateOfAction").prop("readonly", true);
    $("#txtDateOfAction").datepicker({
        dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '2010: 3000'
    });

    $('#dpDateOfAction input').on("keydown", function (e) {
        e.preventDefault();
    });

    $("#txtAddMeetingMeetDate").prop("readonly", true);
    $('#txtAddMeetingMeetDate')
        .datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '2010: 3000'
        });
    $('#divaddMeetingMeetDate input').on("keydown", function (e) {
        e.preventDefault();
    });

    $("#txtNextAppointment").prop("readonly", true);
    $("#txtNextAppointment").datepicker({
        dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '2010: 3000'
    });

    $('#divNextAppointment input').on("keydown", function (e) {
        e.preventDefault();
    });

    $("#txtAddMeetingMeetDate").prop("readonly", true);
    $('#txtAddMeetingMeetDate')
        .datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '2010: 3000'
        });

    $("#txtAddMeetingNextAppointmentDate").prop("readonly", true);
    $('#txtAddMeetingNextAppointmentDate').datepicker({
        dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: '2010: 3000'
    });
    $('#divaddMeetingNextAppointmentDate input').on("keydown", function (e) {
        e.preventDefault();
    });
    $("#_page_lead_details").show();

    $('#txtFromTime').timepicker({ 'minTime': '12:30:pm', 'maxTime': '10:00:am', });
    $('#txtToTime').timepicker();

    $('#txtFromTimes').timepicker();
    $('#txtToTimes').timepicker();

})




function GetUserDetail(EmpId) {
    
    $.ajax({
        url: 'AddUser.aspx/GetUserDetail',
        type: "POST",
        dataType: "json",
        data: "{'empId': '" + EmpId + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
           
            var user = data.d;
            if (user["userrole_id"] == 1)
                $("#tabProcess").hide();
            else
                $("#tabProcess").show();

            //if (user["id"] == null) {
            //    ShowDialog("User does not exist.", 100, 300, 'Message', 10, '<span class="icon-info"></span>', true);
            //}
            //else {

            //    // $("#txtEmpId").val(user["employee_id"]);
            //    $("#DDLCircle").val(user["circle"]);
            //    GetNetwork();
            //    $("#DDLNetwork").val(user["network"]);
            //    GetBranch();
            //    $("#DDLBranch").val(user["branch_id"].trim());

            //    if (user["staff_type"] == "Sale Staff") {
            //        $("#divSelfAllocate").show();
            //        $("#chkselfAllocate").prop('checked', true);
            //        $("#DDLSalesType").val(user["saletype"]);

            //    }
            //    else {
            //        $("#divSelfAllocate").hide();
            //        if (user["staff_type"] == "Admin") {
            //            GetUserList();
            //            $("#trEnterAs").show();
            //        }
            //    }
            //}
        }
    });
}
$("#divTMU").hide();
$("#divFlagBranch").hide();
$("#divother").hide();
$("#divTMU1").hide();
$("#divFlagBranch1").hide();
$("#divother1").hide();
$("#divTMU2").hide();
$("#divFlagBranch2").hide();
$("#divother2").hide();

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function GetProductList() {
    $.ajax({
        url: 'AddLead.aspx/GetProductList',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {


                    $('#DDLProductSold0').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductSold1').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductSold2').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductSold3').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductSold4').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductSold5').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductSold6').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductSold7').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductSold8').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLProductSold9').append('<option value="' + v + '">' + k + '</option>');
                });
            });
        }
    });
}

function GetBranches() {
    $.ajax({
        url: 'AddLead.aspx/GetBranch',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {
                //    $('#DDLBRANCH').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLBRANCH1').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLBRANCH2').append('<option value="' + v + '">' + k + '</option>');
                    $('#DddlBranchMeeting').append('<option value="' + v + '">' + k + '</option>');
                    $('#DddlBranchMeetings').append('<option value="' + v + '">' + k + '</option>');
                    
                  
                });
            });
        }
    });
}
function GetTMO() {
    $.ajax({
        url: 'AddLead.aspx/GetTMO',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {
                    $('#DDLTMU').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLTMU1').append('<option value="' + v + '">' + k + '</option>');
                    $('#DDLTMU2').append('<option value="' + v + '">' + k + '</option>');
                    $('#DdlTmoMeeting').append('<option value="' + v + '">' + k + '</option>');
                    
                 
                });
            });
        }
    });
}





$("#DDLFLAGFOR").change(function () {
    
   
    var ddlflagfor = $("#DDLFLAGFOR").val();
    if (ddlflagfor == "RTMU") {
        $("#divFlagBranch").show();
        $("#divTMU").hide();
        $("#divother").hide();
        Getrtmu();
    }
   
    if (ddlflagfor == "TMO") {
        $("#divTMU").show();
        $("#divother").hide();
        $("#divFlagBranch").hide();
    }
   
    if (ddlflagfor == "Other") {
        $("#divother").show();
        $("#divTMU").hide();
        $("#divFlagBranch").hide();
    }
    
    
});

$("#DDLFLAGFOR1").change(function () {
   
    var ddlflagfor = $("#DDLFLAGFOR1").val();
    if (ddlflagfor == "RTMU") {
        $("#divFlagBranch1").show();
        $("#divTMU1").hide();
        $("#divother1").hide();
        Getrtmu();
    }

    if (ddlflagfor == "TMO") {
        $("#divTMU1").show();
        $("#divother1").hide();
        $("#divFlagBranch1").hide();
     
    }

    if (ddlflagfor == "Other") {
        $("#divother1").show();
        $("#divTMU1").hide();
        $("#divFlagBranch1").hide();
    }


});



Getrtmu = function () {
    $('#DDLBRANCH').html('');
    
    $.ajax({
        url: 'tCustomer.aspx/GetRtrmo',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            $('#DDLBRANCH').append('<option value="">' + 'Select' + '</option>');
            $.each(data, function () {
                $.each(this, function (k, v) {
                    $('#DDLBRANCH1').append('<option value="' + k + '">' + v + '</option>');
                    $('#DDLBRANCH').append('<option value="' + k + '">' + v + '</option>');
                });
            });
        }
    });
}
$("#DDLFLAGFOR2").change(function () {
    
    var ddlflagfor = $("#DDLFLAGFOR2").val();
    if (ddlflagfor == "Branch") {
        $("#divFlagBranch2").show();
        $("#divTMU2").hide();
        $("#divother2").hide();
        
        Getrtmu();
    }

    if (ddlflagfor == "TMO") {
        $("#divTMU2").show();
        $("#divother2").hide();
        $("#divFlagBranch2").hide();
    }

    if (ddlflagfor == "Other") {
        $("#divother2").show();
        $("#divTMU2").hide();
        $("#divFlagBranch2").hide();
    }
    if (ddlflagfor == "") {
        $("#divother2").hide();
        $("#divTMU2").hide();
        $("#divFlagBranch2").hide();
    }


});


$(document).on('change', '[type=checkbox]', function (e) {
    var a = '#txt' + $(this).attr('id');
    if (a.indexOf('Type') > 0) {
        $(a).toggle();
    }
   
});


//$(document).on('click', "#btnAddProduct", function (e) {
//    $("input[type='checkbox']:checked").each(function (e) {
//        var txtBoxId = '#txt' + $(this).attr('id');
//        var txtBoxVal = $(txtBoxId).val();

//        if (txtBoxId.indexOf('Type') > 0) {
//            var leadId = getParameterByName('Customer_id');
//            if (txtBoxId.indexOf('Type1') > 0 && txtBoxVal == "") {
//                ShowDialog('Please fill the values required for the product selected', 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
//                return false;
//            }
//            $.ajax({
//                url: 'TrackLead.aspx/SaveCrossSellProductSold',
//                type: "POST",
//                dataType: "json",
//                data: "{'product': '" + $(this).attr('id') + "','value': '" + txtBoxVal + "','leadId' : '" + leadId + "'}",
//                contentType: "application/json; charset=utf-8",
//                success: function (data) {
//                    var messageContent = "<center><h1>Thank You</h1><h6>" + data.d + " </h6>" +
//                        "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
//                    ShowDialog(messageContent, 100, 300, 'Message', 10, '<span class="icon-info"></span>', true, window.location.href);
//                }
//            });
//        }
//    });
//});



var errorMessage = '';

$("#DDLStatus").change(function () {
    
  
    var leadId = getParameterByName('lead_id');
    var ddlstatus = $("#DDLStatus").val();
   
    $.ajax({
        url: 'TrackLead.aspx/CheckPrevStageDate',
        type: "POST",
        dataType: "json",
        data: "{'leadId': '" + leadId + "','stage' : '" + $("#DDLStatus option:selected").text() + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
           
            $("#hdnPrevStage").val(data.d["Stage"]);
            $("#hdnPrevActionDate").val(data.d["date"]);
        }
    });
    var state = $("#hdnPrevStage").val();
    if (state == 'dummy stage') {
    }

  
    $("#chckMeeting").prop('checked', false);
    $("#divNextAppointmentDate").hide();
    
    if (ddlstatus == "0") {
        $("#NewDivcif").hide();
        $("#divreason").hide();
        $("#divConverted").hide();
        $("#divaddflagproc1").hide();
        $("#divflagstatus").hide();
        $("#divNewEnhancement").hide();
    }
    if (ddlstatus == "1") {
        $("#NewDivcif").hide();
        $("#divreason").hide();
        $("#divConverted").show();
        $("#divaddflagproc1").hide();
        $("#divflagstatus").hide();
        $("#divNewEnhancement").hide();
    }
    if (ddlstatus == "2") {
        $("#NewDivcif").hide();
        $("#divreason").show();
        $("#divConverted").hide();
        $("#divaddflagproc1").hide();
        $("#divflagstatus").hide();
        $("#divNewEnhancement").hide();
    }
    if (ddlstatus == "3") {
        $("#NewDivcif").hide();
        $("#divreason").hide();
        $("#divConverted").hide();
        $("#divaddflagproc1").hide();
        $("#divflagstatus").hide();
        $("#divNewEnhancement").hide();
    }
    if (ddlstatus == "4") {
        $("#NewDivcif").show();
        $("#divreason").hide();
     //   $("#divConverted").hide();
        $("#divaddflagproc1").hide();
        $("#divflagstatus").hide();
        $("#divNewEnhancement").hide();
    }
    if (ddlstatus == "5") {
        $("#NewDivcif").hide();
        $("#divreason").hide();
        $("#divConverted").hide();
        $("#divaddflagproc1").show();
        $("#divflagstatus").show();
        $("#divNewEnhancement").hide();
    }
  
    

});

$("#chckMeeting").change(function () {
    $("#divNextAppointmentDate").toggle(this.checked);
});

var errorMsg;

function validate() {
 
    
    var status = $("#DDLStatus").val();
    var addMeeting = $("#chckMeeting").val();

    var cifno = $("#txtnewcifno").val();

    var newEnhance = $("#DDLNewEnhance").val();

    var productSold0 = $("#DDLProductSold0").val();
    var account0 = $("#txtAccount0").val();
    var amount0 = $("#txtAmount0").val();


    var productSold1 = $("#DDLProductSold1").val();
    var account1 = $("#txtAccount1").val();
    var amount1 = $("#txtAmount1").val();


    var currentStatus = $("#currentStatus").val();

    var nextAppointmentDate = $("#txtNextAppointment").val();

    var actionDate = $("#txtDateOfAction").val();

    var comment = $("#txtComments").val();

    //var reason = $("#textreason").val();



    errorMsg = '';
    var result = true;

    if (status == "4" && cifno == "") {
        errorMsg = errorMsg + "<br/>Enter cif no";
        $("#divCifno").addClass("form-group has-error");
        result = false;
    }
    //if (status == "2" && reason == "") {


    //    errorMsg = errorMsg + "<br/>Enter the reason";
    //    $("#textreason").addClass("form-group has-error");
    //    result = false;
    //} else {
    //    $("#textreason").removeClass("form-group has-error");

    //}
   

    if (status == "-1") {
        errorMsg = errorMsg + "<br/>Please select status.";
        $("#divStatus").addClass("form-group has-error");
        result = false;
    } else {
        $("#divStatus").removeClass("form-group has-error");
    }

 
    if (status > 3 && productSold0 == "-1") {
        errorMsg = errorMsg + "<br/>Please select product.";
        $("#divProductSold0").addClass("form-group has-error");
        result = false;
    } else {
        $("#divProductSold0").removeClass("form-group has-error");
    }

    if ( status == "1" && (amount0 == "" )) {
        errorMsg = errorMsg + "<br/>Please enter amount.";
        $("#abc").addClass("form-group has-error");
        result = false;
    } else {
        $("#abc").removeClass("form-group has-error");
    }

    if (status == "1" && (account0 == "" )) {
        errorMsg = errorMsg + "<br/>Please enter quantity ";
        $("#divAccount0").addClass("form-group has-error");
        result = false;
    } 
    else {
        $("#divAccount0").removeClass("form-group has-error");
    }



    if (status > 3 && productSold1 != "-1" && status == 8 ) {
        errorMsg = errorMsg + "<br/>Please select product.";
        $("#divAccount1").addClass("form-group has-error");
        result = false;
    } else {
        $("#divAccount1").removeClass("form-group has-error");
    }

    if (actionDate == "") {
        errorMsg = errorMsg + "<br/>Please select date.";
        $("#divActionDate").addClass("form-group has-error");
        result = false;
    } else {
        $("#divActionDate").removeClass("form-group has-error");
        var aDate = new Date(actionDate.substring(6, 10) + '-' + actionDate.substring(3, 5) + '-' + actionDate.substring(0, 2));
        var bDate = new Date($("#hdnPrevActionDate").val().substring(6, 10) + '-' + $("#hdnPrevActionDate").val().substring(3, 5) + '-' + $("#hdnPrevActionDate").val().substring(0, 2))



        errorMessage = "";
        if (aDate <= bDate) {
            result = false;
            $("#divActionDate").removeClass("form-group has-error");
            errorMessage = $("#DDLStatus option:selected").text() + " Date should be greater than " + $("#hdnPrevStage").val() + " date";
        }

    }

   

   

    if (comment == "") {
        errorMsg = errorMsg + "<br/>Please enter comment.";
        $("#divComments").addClass("form-group has-error");
        result = false;
    } else {
        $("#divComments").removeClass("form-group has-error");
    }
    return result;

}

$("#Addproduct").click(function () {

    for (i = 3; i < 11; i++) {
        var a = (i - 1);
        if ($("#DDLProductSold" + a).val() == "-1") {
            $("#divproduct" + i).show();
            break;
        }

    }

});

function reply_click(clicked_id) {
    hideDelDiv(clicked_id);
};

function hideDelDiv(id) {
    var idVar = id.substring(9);
    $("#DDLProductSold" + (idVar - 1)).find('option:first').attr('selected', 'selected');
   
  
    $("#txtAccount" + (idVar - 1)).val('');
    $("#txtAmount" + (idVar - 1)).val('');
    $("#divproduct" + idVar).hide();

};

$("#btnSubmit").click(function () {
   
   
    if (validate() == true) {
        
        var procproduct1;
        var productSold1 = $("#DDLProductSold1").val();
        if (productSold1 == null) {
            procproduct = -1;
        }
        else {
            procproduct = productSold1;

        }
        var leadId = getParameterByName('lead_id');

        var status = $("#DDLStatus option:selected").text();
        var cifnumber = $("#txtnewcifno").val();
        var reason = $("#textreason").val();

        var newEnhance = $("#DDLNewEnhance").val();

        var productSold0 = $("#DDLProductSold0").val();
        var account0 = $("#txtAccount0").val();
        var amount0 = $("#txtAmount0").val();


      
        var account1 = $("#txtAccount1").val();
        var amount1 = $("#txtAmount1").val();


        var productSold2 = $("#DDLProductSold2").val();
        var account2 = $("#txtAccount2").val();
        var amount2 = $("#txtAmount2").val();

        var productSold3 = $("#DDLProductSold3").val();
        var account3 = $("#txtAccount3").val();
        var amount3 = $("#txtAmount3").val();


        var productSold4 = $("#DDLProductSold4").val();
        var account4 = $("#txtAccount4").val();
        var amount4 = $("#txtAmount4").val();


        var productSold5 = $("#DDLProductSold5").val();
        var account5 = $("#txtAccount5").val();
        var amount5 = $("#txtAmount5").val();


        var productSold6 = $("#DDLProductSold6 ").val();
        var account6 = $("#txtAccount6").val();
        var amount6 = $("#txtAmount6").val();


        var productSold7 = $("#DDLProductSold7 ").val();
        var account7 = $("#txtAccount7").val();
        var amount7 = $("#txtAmount7").val();


        var productSold8 = $("#DDLProductSold8 ").val();
        var account8 = $("#txtAccount8").val();
        var amount8 = $("#txtAmount8").val();


        var productSold9 = $("#DDLProductSold9 ").val();
        var account9 = $("#txtAccount9").val();
        var amount9 = $("#txtAmount9").val();

      
        var flag_for = $("#DDLFLAGFOR ").val();
        var flag_branch = $("#DDLBRANCH ").val();
        var flag_tmu = $("#DDLTMU ").val();        
        var flag_other = $("#txtotheremail ").val();
        var flag_description = $("#txtflagdesgrption").val();


        var nextAppointmentDate = $("#txtNextAppointment").val();

        var actionDate = $("#txtDateOfAction").val();
        var addMeeting = $('input[id="chckMeeting"]:checked').length > 0 ? 'on' : 'off';

        $.ajax({
            url: 'TrackCustomerLead.aspx/SaveProcessDetails',
            type: "POST",
            dataType: "json",
            data: "{'leadId': '" + leadId + "','status': '" + status + "','chkMeeting' : '" + addMeeting + "','nextAppointment' : '" + nextAppointmentDate +
                "','newEnhance' : '" + newEnhance +
                "','product0' : '" + productSold0 + "','account0' : '" + account0 + "','amount0' : '" + amount0 + "','product1' : '" + procproduct + "','account1' : '" + account1 + "','amount1' : '" + amount1 +
                "','product2' : '" + productSold2 + "','account2' : '" + account2 + "','amount2' : '" + amount2 + "','product3' : '" + productSold3 + "','account3' : '" + account3 + "','amount3' : '" + amount3 +
                "','product4' : '" + productSold4 + "','account4' : '" + account4 + "','amount4' : '" + amount4 + "','product5' : '" + productSold5 + "','account5' : '" + account5 + "','amount5' : '" + amount5 +
                "','product6' : '" + productSold6 + "','account6' : '" + account6 + "','amount6' : '" + amount6 + "','product7' : '" + productSold7 + "','account7' : '" + account7 + "','amount7' : '" + amount7 +
                "','product8' : '" + productSold8 + "','account8' : '" + account8 + "','amount8' : '" + amount8 + "','product9' : '" + productSold9 + "','account9' : '" + account9 + "','amount9' : '" + amount9 +
                "','actionDate' : '" + actionDate + "','comments' : '" + $("#txtComments").val() + "','reason' : '" + $("#textreason").val() + "','flag_for' : '" + $("#DDLFLAGFOR").val() + "','flag_branch' : '" + $("#DDLBRANCH").val() + "','flag_tmu' : '" + $("#DDLTMU").val() + "','flag_other' : '" + $("#txtotheremail").val() + "','flag_description' : '" + $("#txtflagdesgrption").val() + "','cifNo' : '" + cifnumber + 
                "','Plain' : '" + $('#DDLPlain').val() + "','Swaps' : '" + $('#DDLSwaps').val() +
                         "','Plain1' : '" + $('#DDLPlain1').val() + "','Swaps1' : '" + $('#DDLSwaps1').val() +
                            "','Plain2' : '" + $('#DDLPlain2').val() + "','Swaps2' : '" + $('#DDLSwaps2').val() +
                               "','Plain3' : '" + $('#DDLPlain3').val() + "','Swaps3' : '" + $('#DDLSwaps3').val() +
                                  "','Plain4' : '" + $('#DDLPlain4').val() + "','Swaps4' : '" + $('#DDLSwaps4').val() +
                                     "','Plain5' : '" + $('#DDLPlain5').val() + "','Swaps5' : '" + $('#DDLSwaps5').val() +
                                        "','Plain6' : '" + $('#DDLPlain6').val() + "','Swaps6' : '" + $('#DDLSwaps6').val() +
                                           "','Plain7' : '" + $('#DDLPlain7').val() + "','Swaps7' : '" + $('#DDLSwaps7').val() +
                                              "','Plain8' : '" + $('#DDLPlain8').val() + "','Swaps8' : '" + $('#DDLSwaps8').val() +
                                                "','Plain9' : '" + $('#DDLPlain9').val() + "','Swaps9' : '" + $('#DDLSwaps9').val() +

                         "'}",
        
        
   
            contentType: "application/json; charset=utf-8",
            success: function (data) {


                var messageContent = "<center><h1>Thank You</h1><h6>" + data.d + " </h6>" +
                    "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                ShowDialog(messageContent, 100, 300, 'Message', 10, '<span class="icon-info"></span>', true, window.location.href);
            }
        });

    } else {
        ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }

});

function ValidateMeeting() {

    var meetingDate = $("#txtAddMeetingMeetDate").val();

    var result = true;

    if (meetingDate == "") {
        $("#divaddMeetingMeetDate").addClass("form-group has-error");
        result = false;
    } else {
        $("#divaddMeetingMeetDate").removeClass("form-group has-error");
    }

    return result;
}


$("#btnAddMeeting").click(function () {
    if (ValidateMeeting() == true) {
        var leadId = getParameterByName('lead_id');
        var meetingDate = $("#txtAddMeetingMeetDate").val();

        var nextAppointmentDate = $("#txtAddMeetingNextAppointmentDate").val();

        //var actionDate = $("#txtDateOfAction").val();\
        if (meetingDate != "")
            meetingDate = meetingDate.substring(6, 10) + '-' + meetingDate.substring(3, 5) + '-' + meetingDate.substring(0, 2);

        if (nextAppointmentDate != "")
            nextAppointmentDate = nextAppointmentDate.substring(6, 10) + '-' + nextAppointmentDate.substring(3, 5) + '-' + nextAppointmentDate.substring(0, 2);

        $.ajax({
            url: 'TrackLead.aspx/SaveMeetingDetails',
            type: "POST",
            dataType: "json",
            data: "{'meetingDate': '" + meetingDate + "','nextAppointment': '" + nextAppointmentDate + "','comments': '" + $("#txtMeetingDescription").val() + "','leadId': '" + leadId + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                ShowDialog(data.d, 100, 300, 'Message', 10, '<span class="icon-info"></span>', true, window.location.href);
            }
        });

    } else {
        ShowDialog("Please enter all the values correctly.", 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }

});

//$("#txtAmount0").focusout(function (event) {

//    var num2words = new NumberToWords();
//    num2words.setMode("indian");
//    if (event.which == 13) {
//        event.preventDefault();
//    }
//    $("#lblAmount0").html(replaceAll("Zero", "", num2words.numberToWords($("#txtAmount0").val())));
//});

//$("#txtAmount1").focusout(function (event) {
//    var num2words = new NumberToWords();
//    num2words.setMode("indian");
//    if (event.which == 13) {
//        event.preventDefault();
//    }
//    $("#lblAmount1").html(replaceAll("Zero", "", num2words.numberToWords($("#txtAmount1").val())));
//});



//function NumbersToWords(inputNumber) {
//    var inputNo = inputNumber;

//    if (inputNo == 0)
//        return "Zero";

//    var numbers = new Array(4);
//    var first = 0;
//    var u, h, t;
//    var sb = '';

//    if (inputNo < 0) {
//        sb += "Minus ";
//        inputNo = -inputNo;
//    }

//    var words0 = ["", "One ", "Two ", "Three ", "Four ",
//        "Five ", "Six ", "Seven ", "Eight ", "Nine "
//    ];
//    var words1 = ["Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
//        "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen "
//    ];
//    var words2 = ["Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
//        "Seventy ", "Eighty ", "Ninety "
//    ];
//    var words3 = ["Thousand ", "Lakh ", "Crore "];

//    numbers[0] = parseInt(inputNo % 1000); // units
//    numbers[1] = parseInt(inputNo / 1000);
//    numbers[2] = parseInt(inputNo / 100000);
//    numbers[1] = parseInt(numbers[1] - 100 * numbers[2]); // thousands
//    numbers[3] = parseInt(inputNo / 10000000); // crores
//    numbers[2] = parseInt(numbers[2] - 100 * numbers[3]); // lakhs

//    for (var i = 3; i > 0; i--) {
//        if (numbers[i] != 0) {
//            first = i;
//            break;
//        }
//    }

//    for (var i = first; i >= 0; i--) {
//        if (numbers[i] == 0) continue;
//        u = parseInt(numbers[i] % 10); // ones
//        t = parseInt(numbers[i] / 10);
//        h = parseInt(numbers[i] / 100); // hundreds
//        t = parseInt(t - 10 * h); // tens
//        if (h > 0) sb += words0[h] + "Hundred ";
//        if (u > 0 || t > 0) {
//            if (h > 0 || i == 0) sb += "and ";
//            if (t == 0)
//                sb += words0[u];
//            else if (t == 1)
//                sb += words1[u];
//            else
//                sb += words2[t - 2] + words0[u];
//        }
//        if (i != 0) sb += words3[i - 1];
//    }
//    //alert(sb);
//    return sb;
//}









$("#liDeleteLead").click(function () {

    var leadid = $("#hdnLeadId").val();
    var holderid = $('#hdnAssignId').val();
    var holderemail = $('#hdnEmployeeid').val();
    var checkstr = confirm('Are you sure you want to delete this lead?');
    if (checkstr == true) {
        $.ajax({
            url: 'TrackLead.aspx/LeadDelete',
            type: "POST",
            dataType: "json",
            data: "{'leadId': '" + leadid + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d == "Try again. Lead deletion failed.")
                    ShowDialog(data.d, 400, 600, 'Message', 10, '<span class="icon-info"></span>', false);
                else {
                    var messageContent = "<center><h1>Thank You</h1><hr> <p><h2>" + leadid + " </h2></p><br/>" +
                        "<p><h6>" + data.d + " </h6></p>" +
                        "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                    ShowDialog(messageContent, 400, 600, 'Message', 10, '<span class="icon-info"></span>', true);
                }

            }
        });
    } else {
        return false;
    }


});


$("#divplain").hide();
$("#divSwaps").hide();
$("#divplain1").hide();
$("#divSwaps1").hide();
$("#divplain2").hide();
$("#divSwaps2").hide();
$("#divplain3").hide();
$("#divSwaps3").hide();
$("#divplain4").hide();
$("#divSwaps4").hide();
$("#divplain5").hide();
$("#divSwaps5").hide();
$("#divplain6").hide();
$("#divSwaps6").hide();
$("#divplain7").hide();
$("#divSwaps7").hide();
$("#divplain8").hide();
$("#divSwaps8").hide();
$("#divplain9").hide();
$("#divSwaps9").hide();


$("#DDLProductSold0").change(function () {
    var Product_id = $('#DDLProductSold0').val()
    if (Product_id == '1025') {
        $("#divplain").show();
    }
    else {
        $("#divplain").hide();
        $("#divSwaps").hide();
    }
});
$("#DDLPlain").change(function () {
    
    var Product_id = $('#DDLPlain').val()
    if (Product_id == 'Swaps') {
        $("#divSwaps").show();
    }
    else {
        $("#divSwaps").hide();
    }
});


$("#DDLProductSold1").change(function () {
    var Product_id = $('#DDLProductSold1').val()
    if (Product_id == '1025') {
        $("#divplain1").show();
    }
    else {
        $("#divplain1").hide();
        $("#divSwaps1").hide();
    }
});
$("#DDLPlain1").change(function () {
    var Product_id = $('#DDLPlain1').val()
    if (Product_id == 'Swaps') {
        $("#divSwaps1").show();
    }
    else {
        $("#divSwaps1").hide();
    }
});

$("#DDLProductSold2").change(function () {
    var Product_id = $('#DDLProductSold2').val()
    if (Product_id == '1025') {
        $("#divplain2").show();
    }
    else {
        $("#divplain2").hide();
        $("#divSwaps2").hide();
    }
});
$("#DDLPlain2").change(function () {
    var Product_id = $('#DDLPlain2').val()
    if (Product_id == 'Swaps') {
        $("#divSwaps2").show();
    }
    else {
        $("#divSwaps2").hide();
    }
});

$("#DDLProductSold3").change(function () {
    var Product_id = $('#DDLProductSold3').val()
    if (Product_id == '1025') {
        $("#divplain3").show();
    }
    else {
        $("#divplain3").hide();
        $("#divSwaps3").hide();
    }
});
$("#DDLPlain3").change(function () {
    var Product_id = $('#DDLPlain3').val()
    if (Product_id == 'Swaps') {
        $("#divSwaps3").show();
    }
    else {
        $("#divSwaps3").hide();
    }
});



$("#DDLProductSold4").change(function () {
    var Product_id = $('#DDLProductSold4').val()
    if (Product_id == '1025') {
        $("#divplain4").show();
    }
    else {
        $("#divplain4").hide();
        $("#divSwaps4").hide();
    }
});
$("#DDLPlain4").change(function () {
    var Product_id = $('#DDLPlain4').val()
    if (Product_id == 'Swaps') {
        $("#divSwaps4").show();
    }
    else {
        $("#divSwaps4").hide();
    }
});

$("#DDLProductSold5").change(function () {
    var Product_id = $('#DDLProductSold5').val()
    if (Product_id == '1025') {
        $("#divplain5").show();
    }
    else {
        $("#divplain5").hide();
        $("#divSwaps5").hide();
    }
});
$("#DDLPlain5").change(function () {
    var Product_id = $('#DDLPlain5').val()
    if (Product_id == 'Swaps') {
        $("#divSwaps5").show();
    }
    else {
        $("#divSwaps5").hide();
    }
});

$("#DDLProductSold6").change(function () {
    var Product_id = $('#DDLProductSold6').val()
    if (Product_id == '1025') {
        $("#divplain6").show();
    }
    else {
        $("#divplain6").hide();
        $("#divSwaps6").hide();
    }
});
$("#DDLPlain6").change(function () {
    var Product_id = $('#DDLPlain6').val()
    if (Product_id == 'Swaps') {
        $("#divSwaps6").show();
    }
    else {
        $("#divSwaps6").hide();
    }
});

$("#DDLProductSold7").change(function () {
    var Product_id = $('#DDLProductSold7').val()
    if (Product_id == '1025') {
        $("#divplain7").show();
    }
    else {
        $("#divplain7").hide();
        $("#divSwaps7").hide();
    }
});
$("#DDLPlain7").change(function () {
    var Product_id = $('#DDLPlain7').val()
    if (Product_id == 'Swaps') {
        $("#divSwaps7").show();
    }
    else {
        $("#divSwaps7").hide();
    }
});

$("#DDLProductSold8").change(function () {
    var Product_id = $('#DDLProductSold8').val()
    if (Product_id == '1025') {
        $("#divplain8").show();
    }
    else {
        $("#divplain8").hide();
        $("#divSwaps8").hide();
    }
});
$("#DDLPlain8").change(function () {
    var Product_id = $('#DDLPlain8').val()
    if (Product_id == 'Swaps') {
        $("#divSwaps8").show();
    }
    else {
        $("#divSwaps8").hide();
    }
});

$("#DDLProductSold9").change(function () {
    var Product_id = $('#DDLProductSold9').val()
    if (Product_id == '1025') {
        $("#divplain9").show();
    }
    else {
        $("#divplain9").hide();
        $("#divSwaps9").hide();
    }
});
$("#DDLPlain9").change(function () {
    var Product_id = $('#DDLPlain9').val()
    if (Product_id == 'Swaps') {
        $("#divSwaps9").show();
    }
    else {
        $("#divSwaps9").hide();
    }
});


var tabing = function () {
    
    var nav = $(".tabs"),
        slideCon = $(".frames");
    slideCon.find(".frame").hide();
    var id = nav.find(".selected").attr("href");
    $(id).show();
    $('a[href*="' + "#" + id + '"]').addClass("selected");
    nav.on("click", "li a", function (e) {
        e.preventDefault();
        var $this = $(this);
        var parentItem = $($this.attr("href"));
        var currentTab = $this.attr("href");
        if (parentItem.is(":visible")) {
            return;
        } else {
            slideCon.find(".frame").hide();
            parentItem.show();
            $this.addClass("selected").closest("li").siblings().find("a").removeClass("selected");
            var leadId = getParameterByName('lead_id');
            if (currentTab == "#_page_process") {
                $.ajax({
                    url: 'EditCustomer.aspx/GetProcessDetails',
                    type: "POST",
                    dataType: "json",
                    data: "{'leadId': '" + leadId + "'}",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (data) {
                        
                        var obj = JSON.parse(data.d);
                        console.log(obj);
                        if ('CurrentStatus' in obj) {
                            $("#DDLStatus option:contains(" + obj['CurrentStatus'] + ")").attr('selected', 'selected');
                            $("#currentStatus").val(obj['CurrentStatus']);
                        }

                        $("#divproduct2").hide();
                        //if ('new_enhance' in obj) {
                        //    $("#DDLNewEnhance option:contains(" + obj['new_enhance'] + ")").attr('selected', 'selected');
                        //    $("#DDLNewEnhance").val(obj['new_enhance']);
                        //}
                        if ($("#DDLStatus").val() > 3) {
                            //$("#divNewEnhancement").show();
                        }
                        var status = obj['CurrentStatus'];
                        if (status == 'Open For Action')
                            $("#divConverted").show();
                        if (status == 'Flagged Lead')
                            $("#divaddflagproc1").show();
                       
                        
                        $("#NewDivcif").hide();
                        $("#txtreason").hide();
                        if ('ProductSold0' in obj)

                            

                            $("#DDLProductSold0").val(obj['id0']);

                        if ('ac_no0' in obj)
                            $("#txtAccount0").val(obj['account0']);
                        if ('amount0' in obj)
                            $("#txtAmount0").val(obj['amount0']);

                        if ('ProductSold1' in obj)
                            $("#divproduct2").show();
                            $("#DDLProductSold1").val(obj['id1']);
                        if ('ac_no1' in obj)
                            $("#txtAccount1").val(obj['account1']);
                        if ('amount1' in obj)
                            $("#txtAmount1").val(obj['amount1']);

                        if ('ProductSold2' in obj) {
                            $("#divproduct3").show();
                            $("#DDLProductSold2").val(obj['id2']);
                        }
                        if ('ac_no2' in obj)
                            $("#txtAccount2").val(obj['account2']);
                        if ('amount2' in obj)
                            $("#txtAmount2").val(obj['amount2']);

                        if ('ProductSold3' in obj) {
                            $("#divproduct4").show();
                            $("#DDLProductSold3").val(obj['id3']);
                        }
                        if ('ac_no3' in obj)
                            $("#txtAccount3").val(obj['account3']);
                        if ('amount3' in obj)
                            $("#txtAmount3").val(obj['amount3']);

                        if ('ProductSold4' in obj) {
                            $("#divproduct5").show();
                            $("#DDLProductSold4").val(obj['id4']);
                        }
                        if ('ac_no4' in obj)
                            $("#txtAccount4").val(obj['account4']);
                        if ('amount4' in obj)
                            $("#txtAmount4").val(obj['amount4']);

                        if ('ProductSold5' in obj) {
                            $("#divproduct6").show();
                            $("#DDLProductSold5").val(obj['id5']);
                        }
                        if ('ac_no5' in obj)
                            $("#txtAccount5").val(obj['account5']);
                        if ('amount5' in obj)
                            $("#txtAmount5").val(obj['amount5']);

                        if ('ProductSold6' in obj) {
                            $("#divproduct7").show();
                            $("#DDLProductSold6").val(obj['id6']);
                        }
                        if ('ac_no6' in obj)
                            $("#txtAccount6").val(obj['account6']);
                        if ('amount6' in obj)
                            $("#txtAmount6").val(obj['amount6']);

                        if ('ProductSold7' in obj) {
                            $("#divproduct8").show();
                            $("#DDLProductSold7").val(obj['id7']);
                        }
                        if ('ac_no7' in obj)
                            $("#txtAccount7").val(obj['account7']);
                        if ('amount7' in obj)
                            $("#txtAmount7").val(obj['amount7']);

                        if ('ProductSold8' in obj) {
                            $("#divproduct9").show();
                            $("#DDLProductSold8").val(obj['id8']);
                        }
                        if ('ac_no8' in obj)
                            $("#txtAccount8").val(obj['account8']);
                        if ('amount8' in obj)
                            $("#txtAmount8").val(obj['amount8']);

                        if ('ProductSold9' in obj) {
                            $("#divproduct10").show();
                            $("#DDLProductSold9").val(obj['id9']);
                        }
                        if ('ac_no9' in obj)
                            $("#txtAccount9").val(obj['account9']);
                        if ('amount9' in obj)
                            $("#txtAmount9").val(obj['amount9']);
                        

                        if (obj['ProductSold0'] == 'Derivative') {
                            $("#divplain").show();
                            $('#DDLPlain').val(obj['Plain0']);
                        }
                        else {
                            $("#divplain").hide();
                        }
                        if (obj['Plain0'] == 'Swaps')
                            {
                            $("#divSwaps").show();
                            $('#DDLSwaps').val(obj['Swaps0']);
                        }
                        else {
                            $("#divSwaps").hide();
                        }
                        // 
                        if (obj['ProductSold1'] == 'Derivative') {
                            $("#divplain1").show();
                            $('#DDLPlain1').val(obj['Plain1']);
                        }
                        else {
                            $("#divplain1").hide();
                        }
                        if (obj['Plain1'] == 'Swaps') {
                            $("#divSwaps1").show();
                            $('#DDLSwaps1').val(obj['Swaps1']);
                        }
                        else {
                            $("#divSwaps1").hide();
                        }
                        // 
                        if (obj['ProductSold2'] == 'Derivative') {
                            $("#divplain2").show();
                            $('#DDLPlain2').val(obj['Plain2']);
                        }
                        else {
                            $("#divplain2").hide();
                        }
                        if (obj['Plain2'] == 'Swaps') {
                            $("#divSwaps2").show();
                            $('#DDLSwaps2').val(obj['Swaps2']);
                        }
                        else {
                            $("#divSwaps2").hide();
                        }
                        //
                        if (obj['ProductSold3'] == 'Derivative') {
                            $("#divplain3").show();
                            $('#DDLPlain3').val(obj['Plain3']);
                        }
                        else {
                            $("#divplain3").hide();
                        }
                        if (obj['Plain3'] == 'Swaps') {
                            $("#divSwaps3").show();
                            $('#DDLSwaps3').val(obj['Swaps3']);
                        }
                        else {
                            $("#divSwaps3").hide();
                        }
                        //
                        if (obj['ProductSold4'] == 'Derivative') {
                            $("#divplain4").show();
                            $('#DDLPlain4').val(obj['Plain4']);
                        }
                        else {
                            $("#divplain4").hide();
                        }
                        if (obj['Plain4'] == 'Swaps') {
                            $("#divSwaps4").show();
                            $('#DDLSwaps4').val(obj['Swaps4']);
                        }
                        else {
                            $("#divSwaps4").hide();
                        }
                        //
                        if (obj['ProductSold5'] == 'Derivative') {
                            $("#divplain5").show();
                            $('#DDLPlain5').val(obj['Plain5']);
                        }
                        else {
                            $("#divplain5").hide();
                        }
                        if (obj['Plain5'] == 'Swaps') {
                            $("#divSwap5").show();
                            $('#DDLSwaps5').val(obj['Swaps5']);
                        }
                        else {
                            $("#divSwaps5").hide();
                        }
                        //
                        if (obj['ProductSold6'] == 'Derivative') {
                            $("#divplain6").show();
                            $('#DDLPlain6').val(obj['Plain6']);
                        }
                        else {
                            $("#divplain6").hide();
                        }
                        if (obj['Plain6'] == 'Swaps') {
                            $("#divSwaps6").show();
                            $('#DDLSwaps6').val(obj['Swaps6']);
                        }
                        else {
                            $("#divSwaps6").hide();
                        }
                        //

                        if (obj['ProductSold7'] == 'Derivative') {
                            $("#divplain7").show();
                            $('#DDLPlain7').val(obj['Plain7']);
                        }
                        else {
                            $("#divplain7").hide();
                        }
                        if (obj['Plain7'] == 'Swaps') {
                            $("#divSwaps7").show();
                            $('#DDLSwaps7').val(obj['Swaps7']);
                        }
                        else {
                            $("#divSwaps7").hide();
                        }
                        //
                        if (obj['ProductSold8'] == 'Derivative') {
                            $("#divplain8").show();
                            $('#DDLPlain8').val(obj['Plain8']);
                        }
                        else {
                            $("#divplain8").hide();
                        }
                        if (obj['Plain8'] == 'Swaps') {
                            $("#divSwaps8").show();
                            $('#DDLSwaps8').val(obj['Swaps8']);
                        }
                        else {
                            $("#divSwaps8").hide();
                        }
                        //
                        if (obj['ProductSold9'] == 'Derivative') {
                            $("#divplain9").show();
                            $('#DDLPlain9').val(obj['Plain9']);
                        }
                        else {
                            $("#divplain9").hide();
                        }
                        if (obj['Plain9'] == 'Swaps') {
                            $("#divSwaps9").show();
                            $('#DDLSwaps9').val(obj['Swaps9']);
                        }
                        else {
                            $("#divSwaps9").hide();
                        }
                       

                       

                      

                        if ('action_date' in obj)
                            $("#hdnPrevActionDate").val(obj['action_date']);


                        if (status == "Lead Completed") {
                            $("#NewDivcif").show();
                            if ($("#txtnewcifno").val(obj['cif_no']) == "" || $("#txtnewcifno").val(obj['cif_no']) == null) {
                                $("#txtnewcifno").prop('disabled', false);
                            } else {
                                $("#txtnewcifno").val(obj['cif_no']);
                                $("#txtnewcifno").prop('disabled', false);
                            }
                            if ($("#hdnStatus").val() == "Disbursed") {
                                $("#txtnewcifno").prop('disabled', true);
                            } else {
                                $("#txtnewcifno").prop('disabled', false);
                            }
                        } else {
                            $("#NewDivcif").hide();

                        }

                        if (status == "Lost lead") {
                            $("#divreason").show();
                            $("#textreason").val(obj['reason']);
                           
                        }
                        else {
                            $("#divreason").hide();
                        }
                    }
                });
            } else if (currentTab == "#_page_feed") {
                var feedTable = $('#tblFeed').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 25,
                    "aLengthMenu": [
                        [25, 30, 45, 60],
                        [25, 30, 45, 60]
                    ],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true, //TODO :- Sorting searching to happen on DB side.
                    "bDestroy": true,
                    "sAjaxSource": "TrackCustomerLead.aspx/GetFeed",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblFeed").show();
                            }
                        });
                    }
                });
            }
            else if (currentTab == "#_page_flag_list") {
                var feedTable = $('#tblflaglist').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 25,
                    "aLengthMenu": [
                        [25, 30, 45, 60],
                        [25, 30, 45, 60]
                    ],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true, //TODO :- Sorting searching to happen on DB side.
                    "bDestroy": true,
                    "sAjaxSource": "TrackCustomerLead.aspx/GetFlag",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblflaglist").show();
                            }
                        });
                    }
                });
            }

            
            else if (currentTab == "#_page_products") {
                var productTable = $('#tblProduct').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [
                        [25, 50, 100, 200, 300],
                        [25, 50, 100, 200, "All"]
                    ],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "TrackCustomerLead.aspx/GetProduct",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblProduct").show();
                            }
                        });
                    }
                });

                var crossproductTable = $('#tblCrossSellProduct').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [
                        [25, 50, 100, 200, 300],
                        [25, 50, 100, 200, "All"]
                    ],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "TrackLead.aspx/GetCrossSellProductSold",
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        aoData["leadId"] = leadId;
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblCrossSellProduct").show();
                            }
                        });
                    }
                });

                //
            }
            else if (currentTab == "#_page_schedulemeetinglist") {
              
                var leadTable = $('#tblscedulemeeting').dataTable({
                    "oLanguage": {
                        "sZeroRecords": "No records to display",
                        "sSearch": "Search "
                    },
                    "sDom": 'T<"clear">lfrtip',
                    "tableTools": {

                        "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
                    },
                    "iDisplayLength": 15,
                    "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                    "bSortClasses": false,
                    "bStateSave": false,
                    "bPaginate": true,
                    "bAutoWidth": false,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bDestroy": true,
                    "sAjaxSource": "TrackCustomerLead.aspx/GetScheduleMeetingList",
                    "columnDefs": [
                    {
                        "orderable": false,
                        "targets": -1,
                        "render": function (data, type, full, meta) {

                            return '<a  href="ModifyMeeting.aspx?Meetingid=' + full[4] + '">Edit</a>';

                        }
                    },
                      {
                          "orderable": false,
                          "targets": -2,
                          "render": function (data, type, full, meta) {

                              return '<a target="_blank" href="CustomercallReport.aspx?Meetingid=' + full[4] + '">View</a>';

                          }
                      },

                { orderable: true, "targets": -2 }


        ],
                    "bDeferRender": true,
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success":
                                        function (msg) {
                                            var json = jQuery.parseJSON(msg.d);
                                            fnCallback(json);
                                            $("#tblscedulemeeting").show();
                                        }
                        });
                    }
                });
            }
            else if (currentTab == "#_page_meeting") {
                var ddlmeetingstatus = 'Pending for review';
                GetMeetingbystatus(ddlmeetingstatus)
                
             
            }
            else if (currentTab == "#_page_add_product") {
                $.ajax({
                    url: 'TrackLead.aspx/GetCrossSellProducts',
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (data) {
                        var content = '<h1 class="tab-head">Add Cross Sell Product</h1><div style="margin-left:15px;margin-bottom:20px;">';
                        $.each(data, function () {
                            $.each(this, function (k, v) {
                                content += '<label class="inline-block"><input type="checkbox" id="' + v + '" /><span class="check" style="margin-left:1em"></span>' + k + '</label>';
                                if (v.indexOf('Type1') > 0) {
                                    content += '<input id="txt' + v + '" style="margin-left:5em;display:none;" placeholder="a/c no" autofocus required />'
                                }
                                content += '<br/><br/>';
                            });
                        });

                        content += '<input type="button" class="btn btn-primary btn-sm" value="Add Product" id="btnAddProduct" /></div>';
                     //   $("#_page_add_product").html(content);
                    }
                });
            }

        }




    });


}


function NumberToWords() {

    var units = ["Zero", "One", "Two", "Three", "Four", "Five", "Six",
        "Seven", "Eight", "Nine", "Ten"
    ];
    var teens = ["Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen",
        "Sixteen", "Seventeen", "Eighteen", "Nineteen", "Twenty"
    ];
    var tens = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty",
        "Seventy", "Eighty", "Ninety"
    ];

    var othersIndian = ["Thousand", "Lakh", "Crore"];

    var othersIntl = ["Thousand", "Million", "Billion", "Trillion"];

    var INDIAN_MODE = "indian";
    var INTERNATIONAL_MODE = "international";
    var currentMode = INDIAN_MODE;

    var getBelowHundred = function (n) {
        if (n >= 100) {
            return "greater than or equal to 100";
        };
        if (n <= 10) {
            return units[n];
        };
        if (n <= 20) {
            return teens[n - 10 - 1];
        };
        var unit = Math.floor(n % 10);
        n /= 10;
        var ten = Math.floor(n % 10);
        var tenWord = (ten > 0 ? (tens[ten] + " ") : '');
        var unitWord = (unit > 0 ? units[unit] : '');
        return tenWord + unitWord;
    };

    var getBelowThousand = function (n) {
        if (n >= 1000) {
            return "greater than or equal to 1000";
        };
        var word = getBelowHundred(Math.floor(n % 100));

        n = Math.floor(n / 100);
        var hun = Math.floor(n % 10);
        word = (hun > 0 ? (units[hun] + " Hundred ") : '') + word;

        return word;
    };

    return {
        numberToWords: function (n) {
            if (isNaN(n)) {
                return "Not a number";
            };

            var word = '';
            var val;

            val = Math.floor(n % 1000);
            n = Math.floor(n / 1000);

            word = getBelowThousand(val);

            if (this.currentMode == INDIAN_MODE) {
                othersArr = othersIndian;
                divisor = 100;
                func = getBelowHundred;
            } else if (this.currentMode == INTERNATIONAL_MODE) {
                othersArr = othersIntl;
                divisor = 1000;
                func = getBelowThousand;
            } else {
                throw "Invalid mode - '" + this.currentMode + "'. Supported modes: " + INDIAN_MODE + "|" + INTERNATIONAL_MODE;
            };

            var i = 0;
            while (n > 0) {
                if (i == othersArr.length - 1) {
                    word = this.numberToWords(n) + " " + othersArr[i] + " " + word;
                    break;
                };
                val = Math.floor(n % divisor);
                n = Math.floor(n / divisor);
                if (val != 0) {
                    word = func(val) + " " + othersArr[i] + " " + word;
                };
                i++;
            };
            return word;
        },
        setMode: function (mode) {
            if (mode != INDIAN_MODE && mode != INTERNATIONAL_MODE) {
                throw "Invalid mode specified - '" + mode + "'. Supported modes: " + INDIAN_MODE + "|" + INTERNATIONAL_MODE;
            };
            this.currentMode = mode;
        }
    }
}

function validateflag()
{
    
    var result = true;
    var flagfor1 = $("#DDLFLAGFOR1").val();
    var flagdescription = $("#flagdescription").val();

    if (flagfor1=="") {
        errorMsg = errorMsg + "<br/>select flag for";
        $("#textreason").addClass("form-group has-error");
        result = false;
    } else {
        $("#divFlagFor1").removeClass("form-group has-error");

    }

    if (flagdescription == "") {
        errorMsg = errorMsg + "<br/>Enter flag description";
        $("#txtflagdesgrption").addClass("form-group has-error");
        result = false;
    } else {
        $("#divflagDesction1").removeClass("form-group has-error");

    }
   
    return result;
}

$("#btnSubmitFlag").click(function () {
    

    if (validateflag() == true) {
        var leadId = getParameterByName('lead_id');


        $.ajax({
            url: 'TrackCustomerLead.aspx/SaveAddFlag',
            type: "POST",
            dataType: "json",
            data: "{'leadId': '" + leadId + "','flag_for' : '" + $("#DDLFLAGFOR1").val() + "','flag_branch' : '" + $("#DDLBRANCH1").val() + "','flag_tmu' : '" + $("#DDLTMU1").val() + "','flag_other' : '" + $("#txtotheremail1").val() + "','flag_description' : '" + $("#txtflagdesgrption1").val() + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {


                var messageContent = "<center><h1>Thank You</h1><h6>" + data.d + " </h6>" +
                    "<h3 class='color-green'>Have A Great Day &#9786;</h3></center><br/><br/>";
                ShowDialog(messageContent, 100, 300, 'Message', 10, '<span class="icon-info"></span>', true, window.location.href);
            }
        });

    } else {

        ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
    }

});

$("#DivRexisting").hide();
$("#DivRelatedNew").hide();
$("#DivBranch").hide();
$("#DivTmo").hide();
$("#DivOther").hide();
$("#DivBranch1").hide();
$("#DivTmo1").hide();
$("#DivOther1").hide();

$("#DivCustomerLead").hide();
$("#DivSchedule").hide();
$("#divtmomeeting").hide()
$('#chckMeeting').prop('checked', true);

$("#DDLMettingwith").change(function () {
    

    var meetingwith = $("#DDLMettingwith").val();
    if (meetingwith == 'Existing Customer') {
        $("#DivRexisting").show();
        $("#DivRelatedNew").hide();
        ExistCustomerlist();
    }
    if (meetingwith == 'New Customer') {
        $("#DivRexisting").hide();
        $("#DivRelatedNew").show();
        NewCustomerList();
    }
    if (meetingwith == 'General Meeting') {
        $("#DivRexisting").hide();
        $("#DivRelatedNew").hide();
        $("#DivCustomerLead").hide();
        AllCusomerlist();
    }

});
$("#DdlMeetingwithPerson").change(function () {
    

    var meetingwith = $("#DdlMeetingwithPerson").val();
    if (meetingwith == "Branch") {
        $("#DivBranch").show();
        $("#DivTmo").hide();
        $("#DivOther").hide();

    }
    if (meetingwith == "TMO") {
        $("#DivBranch").hide();
        $("#DivTmo").show();
        $("#DivOther").hide();
    }
    if (meetingwith == "Other") {
        $("#DivBranch").hide();
        $("#DivTmo").hide();
        $("#DivOther").show();
    }

});

$("#DDLFLAGFOR1").change(function () {
   

    var meetingwith = $("#DDLFLAGFOR1").val();
    if (meetingwith == "Branch") {
        $("#DivBranch1").show();
        $("#DivTmo1").hide();
        $("#DivOther1").hide();
      

    }
    if (meetingwith == "TMU") {
        $("#DivBranch1").hide();
        $("#DivTmo1").show();
        $("#DivOther1").hide();
     
    }
    if (meetingwith == "Other") {
        $("#DivBranch1").hide();
        $("#DivTmo1").hide();
        $("#DivOther1").show();
    }

});

$("#DdlCustomer").change(function () {
    

    var Customer = $("#DdlCustomer").val();
    var MeetingWith = $("#DDLMettingwith").val();
    var RelateExisting = $("#DDLRelateExisting").val();
    var DDLRealtedNew = $("#DDLRealtedNew").val();


   
    if (Customer !=  "" && MeetingWith == 'General Meeting') {
        $("#DivCustomerLead").show();


    }
  
    if (Customer != "" && RelateExisting == 'General Meeting') {
        $("#DivCustomerLead").show();
    }
    if (Customer != "" && RelateExisting == 'Related to Lead Meeting') {
        $("#DivCustomerLead").hide();
    }


    if (Customer != "" && DDLRealtedNew == 'General Meeting') {
        $("#DivCustomerLead").hide();
    }
    if (Customer != "" && DDLRealtedNew == 'Lead Meeting ') {
        $("#DivCustomerLead").show();
    }



});

ExistCustomerlist = function () {
    $("#DdlCustomer").html('');
    $.ajax({
        url: 'TrackCustomerLead.aspx/ExistGetCustomeList',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            $('#DdlCustomer').append('<option value="">' + 'select' + '</option>');
            $.each(data, function () {
                $.each(this, function (k, v) {
                  

                    $('#DdlCustomer').append('<option value="' + k + '">' + v + '</option>');

                });


            });
        }
    });
}
NewCustomerList = function () {
    $("#DdlCustomer").html('');
  
    $.ajax({
        url: 'TrackCustomerLead.aspx/NewCustomerList',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            $('#DdlCustomer').append('<option value="">'+'select'+'</option>');
            $.each(data, function () {
                $.each(this, function (k, v) {
               
                   
                    $('#DdlCustomer').append('<option value="' + k + '">' + v + '</option>');

                });


            });
        }
    });
}
AllCusomerlist = function () {
    $("#DdlCustomer").html('');
    $("#DdlCustomer").html('select');
    $.ajax({
        url: 'TrackCustomerLead.aspx/AllCusomerlist',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {
            $('#DdlCustomer').append('<option value="">' + 'select' + '</option>');
            $.each(data, function () {
                $.each(this, function (k, v) {
                   

                    $('#DdlCustomer').append('<option value="' + k + '">' + v + '</option>');

                });


            });
        }
    });
}


/// meeting \\\
$("#divMeets").hide();
$("#divMeet").hide();
$("#DivBranch").hide();
$("#DivBranchs").hide();
$("#DivOther").hide();
$("#DivOthers").hide();
$("#DivCustomerLead").hide();
$("#DivCustomerLeads").hide();

$("#ddlSMeetingType").change(function () {
    
    
    var ddlSMeetingType = $("#ddlSMeetingType").val();
    if (ddlSMeetingType == "Customer") {
        $("#DivBranch").hide();
        $("#DivOther").hide();
        $("#divMeet").show();
    }
    if (ddlSMeetingType == "Branch") {
        $("#DivBranch").show();
        $("#DivOther").hide();
        $("#divMeet").hide();
        $("#DdlCustomer").html('');
    }
    if (ddlSMeetingType == "Other") {
        $("#DivBranch").hide();
        $("#DivOther").show();
        $("#divMeet").hide();
        $("#DdlCustomer").html('');
    }

        if (ddlSMeetingType == "TFCPC Meeting") {
            $("#DivBranch").hide();
            $("#DivOther").hide();
            $("#divMeet").hide();
            $("#DdlCustomer").html('');
        }
        if (ddlSMeetingType == "P-Review Meeting") {
            $("#DivBranch").hide();
            $("#DivOther").hide();
            $("#divMeet").hide();
            $("#DdlCustomer").html('');
        }
    

    });

$("#ddlSMeetingTypes").change(function () {
    
    //1
        var ddlSMeetingType = $("#ddlSMeetingTypes").val();
        if (ddlSMeetingType == "Customer") {
            $("#DivBranchs").hide();
            $("#DivOthers").hide();
            $("#divMeets").show();
        }
        if (ddlSMeetingType == "Branch") {
            $("#DivBranchs").show();
            $("#DivOthers").hide();
            $("#divMeets").hide();

        }
        if (ddlSMeetingType == "Other") {
            $("#DivBranchs").hide();
            $("#DivOthers").show();
            $("#divMeets").hide();

        }
        if (ddlSMeetingType == "TFCPC Meeting") {
            $("#DivBranchs").hide();
            $("#DivOthers").hide();
            $("#divMeets").hide();

        }
        if (ddlSMeetingType == "P-Review Meeting") {
            $("#DivBranchs").hide();
            $("#DivOthers").hide();
            $("#divMeets").hide();

        }

    });

    $("#DdlMeetingWith").change(function () {
    
        var DdlMeetingWith = $("#DdlMeetingWith").val();
        if (DdlMeetingWith == "Related to Lead Meeting") {
            $("#DivCustomerLead").show();

        }
        if (DdlMeetingWith == "General Meeting") {
            $("#DivCustomerLead").hide();

        }

    });

    $("#DdlMeetingWiths").change(function () {
    
        var DdlMeetingWith = $("#DdlMeetingWiths").val();
        if (DdlMeetingWith == "Related to Lead Meeting") {
            $("#DivCustomerLeads").show();

        }
        if (DdlMeetingWith == "General Meeting") {
            $("#DivCustomerLeads").hide();

        }

    });

    $("#ddlmeetingstatus").change(function () {
    
        var ddlmeetingstatus = $("#ddlmeetingstatus").val();
        GetMeetingbystatus(ddlmeetingstatus)
    });


    function GetMeetingbystatus(ddlmeetingstatus)
    {
    
        var leadTable = $('#tblMeeting').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "TrackCustomerLead.aspx/GetMeetingList",
            "columnDefs": [
                    {
                        "orderable": false,
                        "targets": -1,
                        "render": function (data, type, full, meta) {

                            return '<a  href="ModifyMeeting.aspx?Meetingid=' + full[4] + '">Edit</a>';

                        }
                    },
                      {
                          "orderable": false,
                          "targets": -2,
                          "render": function (data, type, full, meta) {

                              return '<a target="_blank" href="CustomercallReport.aspx?Meetingid=' + full[4] + '">View</a>';

                          }
                      },

                { orderable: true, "targets": -2 }


            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["ddlmeetingstatus"] = ddlmeetingstatus;

                //DDLBranch
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblMeeting").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);
    }

    Getleadlist = function () {
        $("#DdlCustomer").html('');
        $("#DdlCustomer").html('select');
        $.ajax({
            url: 'EditCustomer.aspx/GetLead',
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data) {
                $('#DdlCustomer').append('<option value="">' + 'select' + '</option>');
                $.each(data, function () {
                    $.each(this, function (k, v) {
                   
                        $('#Meetingleadid').append('<option value="' + k + '">' + v + '</option>');
                        $('#Meetingleadids').append('<option value="' + k + '">' + v + '</option>');

                    });


                });
            }
        });
    }


    function validateisnumber(id) {
    
        var value = "#" + id;


        $(value).on("keypress keyup blur", function (event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }

        });
    }

