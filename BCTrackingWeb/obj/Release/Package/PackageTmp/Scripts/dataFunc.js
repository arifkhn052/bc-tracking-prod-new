﻿$(document).ajaxError(function (e, xhr) {

    if (xhr.status == 401)
        window.location = "home.aspx";
    if (xhr.status == 500) {
        $("#smsSuccess").removeClass("popUp-success").addClass("popUp-success").find("#dynTxt").html("Please try to login again.");
        //  alert("There was some error. Please try again later.");
        window.location = "home.aspx";
    }
    else if (xhr.status == 403)
        alert("You have no enough permissions to request this resource.");
});

$(document).ready(function () {
    var setThresholdVisit = null;


    //console.log('start');
    getRoles();
    getCircles();
    PopulateHomePageParameters();


    $("input[name=address]").click(function () {
        console.log('chk');
        populateSmsThreshold();
    });

    $(".logoContainer").click(function () {
        window.location.href = "rmmain.aspx";
    });
    $("#select-all").click(function (e) {

        $('#circleOption input:checkbox').prop('checked', this.checked);
    });
    $("#send").click(function () {
        console.log('send');

        var modeChkId = getModeChkId();
        var monthChkId = getMonthChkId();
        monthChkId = monthChkId.slice(0, -1);
        var emailTextCustomMessage = $("#emailTextCustomMessage").val();
        var smsTextCustomMessage = $("#smsTextCustomMessage").val();
        var emailTextCustomString = $("#emailTextCustomString").val();
        var smsTextCustomString = $("#smsTextCustomString").val();

        var isCustomMessageSMS = false;
        var isCustomMessageEmail = false
        if (emailTextCustomMessage.length > 0) {
            isCustomMessageEmail = true;
        }
        if (smsTextCustomMessage.length > 0) {
            isCustomMessageSMS = true;
        }

        var parameter = $(".subList ul li.highlight").html();
        var perf = $(".subtabData ul li.active").data('alerttype');
        if (perf == undefined)
            perf = '';
        var customMessage = $("#customMessageInput").text();
        var staffType = $(".secMenu ul li.active > .secTab").html();

        var jsonParams = JSON.stringify({
            "parameter": parameter,
            "performance": perf,
            "isCustomMessageSMS": isCustomMessageSMS,
            "isCustomMessageEmail": isCustomMessageEmail,
            "emailTextCustomMessage": emailTextCustomMessage,
            "smsTextCustomMessage": smsTextCustomMessage,
            "emailTextCustomString": emailTextCustomString,
            "smsTextCustomString": smsTextCustomString,
            "staffType": staffType,
            "monthChkId": monthChkId,
            "salutationIndicator": $("input[name=address]:checked").attr('id')

        });
        //console.log(jsonParams);
        if (modeChkId.indexOf("SMS") > -1) {
            $.ajax({
                type: 'POST',
                url: 'VijayLineMain.aspx/send_sms',
                data: jsonParams,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (msg) {
                    //console.log(msg.d);
                    $("#smsSuccess").removeClass("popUp-success").addClass("popUp-success").find("#dynTxt").html("SMS is sent");
                    $(".rmv").show();
                    popUp("#smsLayout", "#smsSuccess");

                }
            });

        }
        if (modeChkId.indexOf("Email") > -1) {

            $.ajax({
                type: 'POST',
                url: 'VijayLineMain.aspx/send_email',
                data: jsonParams,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (msg) {
                    $("#smsSuccess").removeClass("popUp-success").addClass("popUp-success").find("#dynTxt").html("Email is sent");
                    $(".rmv").show();
                    popUp("#smsLayout", "#smsSuccess");

                }
            });

        }
    });

    $("#customMessageLi").click(function () {
        $("#customMessageInput").parent().toggle();
        //  $("#customMessageLi").toggle();
        var modeChkId = getModeChkId();
        console.log(modeChkId);
        $("#noOptionSelectedText").html("");

        if (modeChkId.indexOf("SMS,Email,") > -1) {
            $("#emailTextCustomMessage").parent().show();
            $("#smsTextCustomMessage").parent().show();

        } else if (modeChkId.indexOf("SMS,") > -1) {
            $("#emailTextCustomMessage").parent().hide();
            $("#smsTextCustomMessage").parent().show();

        } else
            if (modeChkId.indexOf("Email,") > -1) {
                $("#emailTextCustomMessage").parent().show();
                $("#smsTextCustomMessage").parent().hide();

            }

            else {
                $("#emailTextCustomMessage").parent().hide();
                $("#smsTextCustomMessage").parent().hide();
                $("#noOptionSelectedText").html("<br/> Please choose SMS/Email.");
            }
    });

    $("#customMessageOk").click(function () {
        console.log('send');
        var modeChkId = getModeChkId();

        var circleChkId = '';
        $('.checkbox1:checked').each(function () {
            circleChkId += $(this).val() + ",";
        });
        circleChkId = circleChkId.slice(0, -1);


        var emailTextCustomMessage = $("#emailTextCustomMessage").val();
        var smsTextCustomMessage = $("#smsTextCustomMessage").val();

        var parameter = "Custom Message";

        var staffType = $(".secMenu ul li.active > .secTab").html();

        var jsonParams = JSON.stringify({
            "parameter": parameter,
            "emailTextCustomMessage": emailTextCustomMessage,
            "smsTextCustomMessage": smsTextCustomMessage,
            "staffType": staffType,
            "circleParam": circleChkId,
            "modeChkId": modeChkId

        });
        //console.log(jsonParams);
        $.ajax({
            type: 'POST',
            url: 'VijayLineMain.aspx/SendCustomMessage',
            data: jsonParams,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (msg) {
                //console.log(msg.d);
                $("#smsSuccess").removeClass("popUp-success").addClass("popUp-success").find("#dynTxt").html("Message is sent");
                $(".rmv").show();
                popUp("#smsLayout", "#smsSuccess");

            }
        });



    });



    $("#displaydata").click(function () {

        var chkId = '';
        $('.checkbox1:checked').each(function () {
            chkId += $(this).val() + ",";
        });
        chkId = chkId.slice(0, -1);
        chkId = '';
        $('.monthCheckbox:checked').each(function () {
            chkId += $(this).val() + ",";
        });
        chkId = chkId.slice(0, -1);


    });
    var oldSelectedValue = "";

    $("[id$='-tooltip'], #setThresholdSideBar,#nextBtn").on('change click', function (e) {
        console.log($(this).find('.tooltip').text());
        console.log('oldvalue' + oldSelectedValue);

        console.log('removed');

        $("#customMessageInput").parent().toggle();
        var modeChkId = getModeChkId();
        if (modeChkId.indexOf("SMS") > -1) {
            $("#emailTextCustomString").parent().hide();
            $("#smsTextCustomString").parent().show();
        }
        if (modeChkId.indexOf("Email") > -1) {
            $("#emailTextCustomString").parent().show();
            $("#smsTextCustomString").parent().hide();
        }
        if (modeChkId.indexOf("SMS,Email,") > -1) {
            $("#emailTextCustomString").parent().show();
            $("#smsTextCustomString").parent().show();
        }

        var monthChkId = getMonthChkId();
        monthChkId = monthChkId.slice(0, -1);
        var perf = getPerformanceClass();
        console.log(perf);
        // 
        if (modeChkId == '' || $(".subList ul li.highlight").html() == '') {
            e.preventDefault();
            return false;
        }

        if ((monthChkId == '' || perf == '' || perf == undefined) && CheckParameter() == false) {
            e.preventDefault();
            return false;
        }
        if (CheckParameter1() == false && monthChkId == '') {
            e.preventDefault();
            return false;
        }
        //  
        console.log('removed');
        $(".setThreshold-table").remove();
        $(".setThreshold-tbl-wrap").html('');
        $(".setThreshold-tbl-wrap").html("<div class = 'table-holder'><table class='setThreshold-table'></table></div>");

        //$(".setThreshold-table").remove();



        $(".setThreshold-tbl-wrap").attr('max-height', '280px');
        var parameter = $(".subList ul li.highlight").html();

        switch (parameter) {
            case 'Input Parameters':

                DisplayInputParameters();
                break;
            case 'Financial Performance':

                DisplayFP();
                break;
            case 'SUD':
                $("#sud").show();

                DisplaySUD();
                break;
            case 'League Score':
                DisplayLeagueScore();
                break;
            case 'Hero Board':
                console.log('hB');
                $("#heroBoard").show();
                DisplayHeroBoard();
                break;
            case 'Zero Board':
                $("#zeroBoard").show();

                DisplayZeroBoard();
                break;
            default:
                break;
        }

        function DisplayInputParameters() {
            var data = {};
            data.meetings = $("#meetings-tooltip").val();
            data.risk = $("#risk-tooltip").val();
            data.tat = $("#tat-tooltip").val();
            PopulateGridItemsInputParameters(data);
        }

        function DisplayFP() {
            var data = {};
            data.sanctions = $("#sanctions-tooltip").val();
            data.disbursal = $("#disbursal-tooltip").val();
            PopulateGridItemsFinancialPerformance(data);
        }

        function DisplayHeroBoard() {
            var data = {};
            data.hero = $("#hero-tooltip").val();
            PopulateGridItemsHeroBoard(data);
        }

        function DisplayZeroBoard() {
            var data = {};
            //data.zero = $("#zero-tooltip").val();
            PopulateGridItemsZeroBoard(data);


        }

        function DisplayLeagueScore() {
            var monthChkId = getMonthChkId();
            monthChkId = monthChkId.slice(0, -1);
            //console.log(monthChkId);
            var data = {};
            data.leagueScore = $("#league-tooltip").val();


            PopulateGridItemsLeagueScore(data);
        }
        function DisplaySUD() {
            // var monthChkId = getMonthChkId();
            // monthChkId = monthChkId.slice(0, -1);
            //console.log(monthChkId);
            var data = {};
            data.sud = $("#sud-tooltip").val();


            PopulateGridItemsSUD(data);
        }


        $(window).trigger('resize');


    });
    $(".disabled").click(function (e) {


        e.preventDefault();
        return false;
    });


    $(".highlight").click(function () {

        var chkId = '';
        $('.checkbox1:checked').each(function () {
            chkId += $(this).val() + ",";
        });
        chkId = chkId.slice(0, -1);
        //console.log(chkId);
    });

    $('body:not(#welcomeMsg label)').click(function () {
        if (event.target.className === 'welcome-main-menu'
            || event.target.className === 'welcome-user-name'
            || event.target.className === 'welcome-menu'
            || event.target.className === 'Log-Out-Button'
            || event.target.className.indexOf('welcome-container') >= 0) {
            return;
        }
        $('.welcome-main-menu').hide();
        // $("#welcomeMsg").removeClass('solid-menu-border').addClass('dummy-menu-border');
    });

    $("#welcomeMsg").click(function () {
        $('.welcome-main-menu').toggle();
        // $("#welcomeMsg").addClass('solid-menu-border').removeClass('dummy-menu-border');
    });

    $(".welcome-menu").click(function () {
        $('.welcome-main-menu').show();
        //  $("#welcomeMsg").addClass('solid-menu-border').removeClass('dummy-menu-border');
    });


});




function getCircles() {

    $.ajax({
        type: 'POST',
        url: 'VijayLineMain.aspx/GetCircles',
        data: '{ }',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {

            //console.log(msg.d);
            $.each(msg.d, function (index, value) {
                $("#circleOption").append("    <li class='categoryList'><input type='checkbox' class ='checkbox1' value='" + value + "' id='" +
                   value + "' name='check' />" + "<label for='squared1'>" + value + "</label> </li>");
            });

            //console.log($(".checkbox1").length);
            if ($(".checkbox1").length == 1) {
                $(".checkbox1").prop('checked', true);
            }

        }
    });



}

function getRoles() {
    window.SBICommon.sendAjaxCall({
        url: 'VijayLineMain.aspx/GetRoles',
        data: {},
        requestType: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {
            //console.log(msg.d.staff);
            $(".secMenu ul li").removeClass("active");
            var ind = $(".secTab").filter(function () {
                //console.log($(this).text().trim());
                return $.trim($(this).text()) == msg.d.staff;
            }).parent().next().addClass('active');


            if (msg.d.login_Type == 'Reporting1') {
                $(".circleSection").show();
                $('.secMenu ul li').first().addClass('active');
            }

            if (msg.d.login_Type == 'Reporting2') {
                $('.secMenu ul li').first().addClass('disabled').addClass('permdisabled');
                $('.secMenu ul li').first().next().addClass('active');
            }
            if (msg.d.login_Type !== 'Reporting1') {
                $(".circleSection").hide();
            }

            if(msg.d.login_Type != 'Reporting2')
            $('.secMenu ul li:lt(' + $('.secMenu ul li.active').index() + ')').addClass('disabled').addClass('permdisabled');

        },
        error: function (message) {
            console.log(message.statusText);
        },
        showLoader: true
        , complete: function (xhr, data) {
            console.log(xhr.status);
            if (xhr.status == 401)
                window.location.href = '/rmmain.aspx';
        }
    });
}


function disableAlertSection() {
    //    console.log('dis');
    $(".subtabData ul li").removeClass('active').addClass("disabled").removeClass("highperf").removeClass("lowperf");
    //  $(".subtabData ul li").removeClass('active').addClass("disabled");


}
function enableAlertSection() {
    //  console.log('en');
    $(".monthCheckbox").removeAttr("disabled");
    $(".subtabData ul li").removeClass("disabled");
    $(".secMenu ul").children().slice(0, 2).removeClass("disabled");

}

function disableRoleMenuSection() {
    //console.log('dis1');
    $(".secMenu ul").children().slice(0, 2).removeClass('active').addClass("disabled");
}

function enableRoleMenuSection() {
    //console.log('en1');

    $(".secMenu ul li").not("permdisabled").removeClass("disabled");
    $(".secMenu ul").children().slice(0, 2).not("permdisabled").removeClass("disabled");
}

function disableMenu() {

    var permDisabledIndex = $(".secMenu ul.permdisabled").last().index();
    if (permDisabledIndex == -1)
        permDisabledIndex = 0;
    if (getParameter() == 'Hero Board') {
        //  console.log(CheckParameter() + 'chk param')
        disableAlertSection();
        $(".secMenu ul").children().slice(permDisabledIndex, 6).removeClass("disabled");
        $(".secMenu ul").children().slice(0, 2).removeClass('active').addClass("disabled");
        $(".monthCheckbox").attr("disabled", false);
        return false;
    }
    if (getParameter() == 'Zero Board') {
        //    console.log(CheckParameter() + 'chk param')
        disableAlertSection();
        $(".secMenu ul").children().slice(permDisabledIndex, 6).removeClass("disabled");
        $(".secMenu ul").children().slice(0, 3).removeClass('active').addClass("disabled");
        $(".monthCheckbox").attr("disabled", false);
        return false;
    }
    if (getParameter() == 'League Score') {
        //      console.log(CheckParameter() + 'chk param')
        $(".secMenu ul").children().slice(permDisabledIndex, 6).removeClass("disabled");
        $(".secMenu ul").children().slice(3, 6).removeClass('active').addClass("disabled"); //disable RM & RMMERMSE
        $(".monthCheckbox").attr("disabled", false);
        enableAlertSection();
        return false;
    }
    if (getParameter() == 'SUD') {
        //      console.log(CheckParameter() + 'chk param') // $("input.group1").removeAttr("disabled");
        enableRoleMenuSection();
        $(".monthCheckbox:checked").attr('checked', false);
        $(".monthCheckbox").attr("disabled", true);
        disableAlertSection();
        return false;
    }
    else {
        enableAlertSection();
        enableRoleMenuSection();
    }
}

function PopulateHomePageParameters() {
    $(".subList ul li").remove();
    window.SBICommon.sendAjaxCall({
        url: 'VijayLineMain.aspx/GetParameters',
        data: '{ }',
        requestType: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {
            $.each(msg.d, function (index, value) {
                $(".subList ul").append("<li >" + value + "</li>");

            });

            $(".subList ul li").on('click', function () {
                //console.log($(this).text());
                $(".subList ul li").removeClass("highlight");
                $(this).addClass("highlight");
                $(this).siblings().removeClass("highlight");
                disableMenu();

            });

            $(".subtabData ul li").on('click', function () {
                var buttonInndex = $(this).index();
                $(this).siblings().removeClass("highperf").removeClass("lowperf").removeClass("customperf").removeClass("active");


                switch (buttonInndex) {
                    case 0:
                        $(this).addClass("highperf active");
                        break;
                    case 1:
                        $(this).addClass("lowperf active");
                        break;
                    case 2:
                        $(this).addClass("customperf active");
                        break;
                    default:
                        break;
                }
                disableMenu();
            });

            $(".subtabData1 ul li").on('click', function () {
                $(this).toggleClass("customperf active");
            });
        },
        error: function (message) {
            console.log(message.statusText);
        },
        showLoader: true
        , complete: function (xhr, data) {
            console.log(xhr.status);
            if (xhr.status == 401)
                window.location.href = '/rmmain.aspx';
        }
    });
}

function CheckParameter() {
    console.log(getParameter());
    if (getParameter() == 'Hero Board' || getParameter() == 'Zero Board' || getParameter() == 'SUD')
        return true;
    else
        return false;
}


function CheckParameter1() {
    console.log(getParameter());
    if (getParameter() == 'SUD')
        return true;
    else
        return false;
}

function removeTable() {

    $(".setThreshold-table").remove();
    $(".setThreshold-tbl-wrap").html('');
    $(".setThreshold-tbl-wrap").html("<div class = 'table-holder'><table class='setThreshold-table'></table></div>");

}
function PopulateGridItemsInputParameters(data) {

    var headervalue = $(".secMenu ul li.active > .secTab").html();
    if ((headervalue == '' || headervalue == undefined)) {
        return false;
    }
    var circleChkId = '';
    $('.checkbox1:checked').each(function () {
        circleChkId += $(this).val() + ",";
    });
    circleChkId = circleChkId.slice(0, -1);
    //console.log(circleChkId);
    var monthChkId = '';
    $('.monthCheckbox:checked').each(function () {
        monthChkId += $(this).val() + ",";
    });
    monthChkId = monthChkId.slice(0, -1);
    //console.log(monthChkId);
    data.staffType = $(".secMenu ul li.active > .secTab").html();

    var perf = getPerformanceClass();
    perf = perf.replace(" active", "");

    data.perf = perf;

    data.circle = circleChkId;
    data.month = monthChkId;

    var jsonParams = JSON.stringify({
        "meetings": data.meetings, "risk": data.risk,
        "tat": data.tat,
        "circleParam": data.circle, "month": data.month,
        "perf": data.perf, "staffType": data.staffType
    });



    window.SBICommon.sendAjaxCall({
        url: 'VijayLineMain.aspx/PopulateGridItemsInputParameters',
        data: jsonParams,
        requestType: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {
            //console.log(msg.d);
            removeTable();

            var header = '<thead><tr><th>Name</th><th>' + getCircleHeader() + '</th>' +
            '<th>Meetings/RMSE (% target)</th>' +
            '<th>RMSE Risk Mitigation%</th>' +
            '<th>RMSE TAT</th>' +
            '<th>Meetings/RMME (% target)</th>' +
            '<th>RMME Risk Mitigation%</th>' +
            '<th>RMME TAT</th>' +
            '</tr></thead>';
            if (data.staffType == 'RMSE') {
                header = header.replace(/\/RM/g, '-RM');
                header = '<thead><tr><th>Name</th><th>' + getCircleHeader() + '</th>' +
                '<th>Meetings/RMSE (% target)</th>' +
                '<th>RMSE Risk Mitigation%</th>' +
                '<th>RMSE TAT</th>' +
                '</tr></thead>';
            }
            else if (data.staffType == 'RMME') {
                header = header.replace(/\/RM/g, '-RM');
                header = '<thead><tr><th>Name</th><th>' + getCircleHeader() + '</th>' +
                '<th>Meetings/RMME (% target)</th>' +
                '<th>RMME Risk Mitigation%</th>' +
                '<th>RMME TAT</th>' +
                '</tr></thead>';
            }

            $(".setThreshold-table").html(header);
            $.each(msg.d, function (index, value) {
                if (data.staffType == 'RMSE') {
                    $(".setThreshold-table").append("<tr><td>" + value.personName + "</td><td>" + value.fullnetwork
                        + "</td><td>" +
                        value.MeetingsRmse + "</td><td>" + value.RmseRiskMitigatedSanctions + "</td><td>" + value.RmseTAT + "</td>"
                        + "</tr>");
                }
                else if (data.staffType == 'RMME') {
                    $(".setThreshold-table").append("<tr><td>" + value.personName + "</td><td>" + value.fullnetwork
                        + "</td>"
                        + "<td>" + value.MeetingsRmme + "</td>"
                        + "<td>" + value.RmmeRiskMitigatedSanctions + "</td>"
                        + "<td>" + value.RmmeTAT + "</td></tr>");
                }
                else {
                    $(".setThreshold-table").append("<tr><td>" + value.personName + "</td><td>" + value.fullnetwork
                        + "</td><td>" +
                        value.MeetingsRmse + "</td><td>" + value.RmseRiskMitigatedSanctions + "</td><td>" + value.RmseTAT + "</td>"
                        + "<td>" + value.MeetingsRmme + "</td>"
                        + "<td>" + value.RmmeRiskMitigatedSanctions + "</td>"
                        + "<td>" + value.RmmeTAT + "</td></tr>");
                }
            });
            $('.setThreshold-tbl-wrap').fixedHeader();
            //removeBigTable();
            ToggleSendButton();

        },

        showLoader: true
        , complete: function (xhr, data) {
            console.log(xhr.status);
            if (xhr.status == 401)
                window.location.href = '/rmmain.aspx';
        }
    });

}


function PopulateGridItemsFinancialPerformance(data) {
    var headervalue = $(".secMenu ul li.active > .secTab").html();
    if ((headervalue == '' || headervalue == undefined)) {
        return false;
    }
    var circleChkId = '';
    $('.checkbox1:checked').each(function () {
        circleChkId += $(this).val() + ",";
    });
    circleChkId = circleChkId.slice(0, -1);
    var monthChkId = getMonthChkId();
    monthChkId = monthChkId.slice(0, -1);
    var perf = getPerformanceClass();
    perf = perf.replace(" active", "");
    data.perf = perf;

    data.circle = circleChkId;
    data.month = monthChkId;
    data.staffType = $(".secMenu ul li.active > .secTab").html();
    var jsonParams = JSON.stringify({
        "sanctions": data.sanctions, "disbursal": data.disbursal,
        "circleParam": data.circle, "month": data.month,
        "staffType": data.staffType, "perf": perf
    });
    window.SBICommon.sendAjaxCall({
        url: 'VijayLineMain.aspx/PopulateGridItemsFinancialPerformance',
        data: jsonParams,
        requestType: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {
            console.log(msg);
            removeTable();
            var header = ' <thead><tr><th>Name</th>'
                + '<th>' + getCircleHeader() +
                '</th> <th>Sanction/RMSE (% of target)</th>'
                + '<th>Sanction/RMME (% of target)</th>'
             + '<th>Total sanction/Sale Staff<br/> (% of target)</th>'
                + '<th>Disbursal/RMSE (% of target)</th>'
                + '<th>Disbursal/RMME (% of target)</th>'
                + '<th>Total disbursal/Sale Staff<br/> (% of target)</th>';
            if (data.staffType == 'RMSE') {
                header = header.replace(/\/RM/g, '-RM');
                header = ' <thead><tr><th>Name</th>'
                + '<th>' + getCircleHeader() +
                '</th> <th>Sanction/RMSE (% of target)</th>'
                + '<th>Disbursal/RMSE (% of target)</th>'
            }
            else if (data.staffType == 'RMME') {
                header = header.replace(/\/RM/g, '-RM');
                header = ' <thead><tr><th>Name</th>'
                + '<th>' + getCircleHeader() + '</th>'
                + '<th>Sanction/RMME (% of target)</th>'
                + '<th>Disbursal/RMME (% of target)</th>';
            }
            $(".setThreshold-table").html(header);

            $.each(msg.d, function (index, value) {
                if (data.staffType == 'RMSE') {
                    $(".setThreshold-table").append(
                        "<tr><td>" + value.personName + "</td>"
                        + "<td>" + value.fullnetwork + "</td>"
                      + " <td>" + value.TotalSancPercent + "</td>"
                      + " <td>" + value.TotalDisbPerRmsePercent + "</td>"
                     + "</tr>");
                }
                else if (data.staffType == 'RMME') {
                    $(".setThreshold-table").append(
                        "<tr><td>" + value.personName + "</td>"
                        + "<td>" + value.fullnetwork + "</td>"
                      + " <td>" + value.TotalSancPercent + "</td>"
                      + " <td>" + value.TotalDisbPercent + "</td>"
                     + "</tr>");
                }
                else {
                    $(".setThreshold-table").append(
                        "<tr><td>" + value.personName + "</td>"
                        + "<td>" + value.fullnetwork + "</td>"
                      + " <td>" + value.TotalSancPerRmsePercent + "</td>"
                      + " <td>" + value.TotalSancPerRmmePercent + "</td>"
                      + " <td>" + value.TotalSancPercent + "</td>"
                      + " <td>" + value.TotalDisbPerRmsePercent + "</td>"
                      + " <td>" + value.TotalDisbPerRmmePercent + "</td>"
                      + " <td>" + value.TotalDisbPercent + "</td>"
                     + "</tr>");
                }
            });

            $('.setThreshold-tbl-wrap').fixedHeader();
            //removeBigTable();
            ToggleSendButton();
        },
        showLoader: true

    });

}

function PopulateGridItemsHeroBoard(data) {
    var headervalue = $(".secMenu ul li.active > .secTab").html();
    if ((headervalue == '' || headervalue == undefined)) {
        return false;
    }
    var circleChkId = '';
    $('.checkbox1:checked').each(function () {
        circleChkId += $(this).val() + ",";
    });
    circleChkId = circleChkId.slice(0, -1);
    //console.log(circleChkId);
    var monthChkId = '';
    $('.monthCheckbox:checked').each(function () {
        monthChkId += $(this).val() + ",";
    });
    monthChkId = monthChkId.slice(0, -1);
    //console.log(monthChkId);   

    data.circle = circleChkId;
    data.month = monthChkId;
    data.staffType = $(".secMenu ul li.active > .secTab").html();

    var jsonParams = JSON.stringify({
        "topN": '10',
        "circleParam": data.circle, "month": data.month,
        "staffType": data.staffType
    });

    //console.log(jsonParams);


    window.SBICommon.sendAjaxCall({
        url: 'VijayLineMain.aspx/PopulateHeroBoard',
        data: jsonParams,
        requestType: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {
            //console.log(msg.d);
            removeTable();
            $(".setThreshold-table").html(' <thead><tr><th>Name</th><th>' + getCircleHeader() + '</th>' +
                     '   <th>Email</th> <th>Phone No.</th></tr></thead>');
            $.each(msg.d, function (index, value) {
                $(".setThreshold-table").append("<tr><td>" + value.name + "</td><td>" + value.fullnetwork + "</td><td>" +
                    value.emailId + "</td><td>" + value.phno + "</td></tr>");
            });
            $('.setThreshold-tbl-wrap').fixedHeader();
            //removeBigTable();
            //addBigTable()
            ToggleSendButton();

        },

        showLoader: true
        , complete: function (xhr, data) {
            console.log(xhr.status);
            if (xhr.status == 401)
                window.location.href = '/rmmain.aspx';
        }
    });


}


function PopulateGridItemsSUD(data) {

    var headervalue = $(".secMenu ul li.active > .secTab").html();
    if ((headervalue == '' || headervalue == undefined)) {

        return false;
    }
    //  
    var circleChkId = '';
    $('.checkbox1:checked').each(function () {
        circleChkId += $(this).val() + ",";
    });
    circleChkId = circleChkId.slice(0, -1);
    //console.log(circleChkId);

    monthChkId = 'prevMonth,currMonth';
    //console.log(monthChkId);
    data.perf = 'highperf';

    data.circle = circleChkId;
    data.month = monthChkId;
    data.staffType = $(".secMenu ul li.active > .secTab").html();

    var jsonParams = JSON.stringify({
        "sud": data.sud,
        "circleParam": data.circle,
        "month": data.month,
        "perf": data.perf,
        "staffType": data.staffType
    });


    window.SBICommon.sendAjaxCall({
        url: 'VijayLineMain.aspx/PopulateGridItemsSUD',
        data: jsonParams,
        requestType: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {
            //console.log(msg.d);
            //console.log(msg.d);
            removeTable();

            $(".setThreshold-table").html(' <thead><tr><th>Name</th><th>' + getCircleHeader() + '</th> <th>SUD</th>' +
                  '   </tr></thead>');
            $.each(msg.d, function (index, value) {

                $(".setThreshold-table").append("<tr><td>" + value.personName + "</td><td>" + value.fullnetwork + "</td><td>" +
                    value.SUDValue + "</td></tr>");
            });
            $('.setThreshold-tbl-wrap').fixedHeader();
            //removeBigTable();
            ToggleSendButton();

        },

        showLoader: true
        , complete: function (xhr, data) {
            console.log(xhr.status);
            if (xhr.status == 401)
                window.location.href = '/rmmain.aspx';
        }
    });


}
function PopulateGridItemsLeagueScore(data) {

    var headervalue = $(".secMenu ul li.active > .secTab").html();
    if ((headervalue == '' || headervalue == undefined)) {

        return false;
    }
    var circleChkId = '';
    $('.checkbox1:checked').each(function () {
        circleChkId += $(this).val() + ",";
    });
    circleChkId = circleChkId.slice(0, -1);
    //console.log(circleChkId);
    var monthChkId = getMonthChkId();
    monthChkId = monthChkId.slice(0, -1);
    //console.log(monthChkId);
    var perf = getPerformanceClass();
    perf = perf.replace(" active", "");
    //console.log(perf);
    data.perf = perf;

    data.circle = circleChkId;
    data.month = monthChkId;
    data.staffType = $(".secMenu ul li.active > .secTab").html();

    var jsonParams = JSON.stringify({
        "leagueScore": data.leagueScore,
        "circleParam": data.circle,
        "month": data.month,
        "perf": data.perf,
        "staffType": data.staffType
    });


    window.SBICommon.sendAjaxCall({
        url: 'VijayLineMain.aspx/PopulateGridItemsLeagueScore',
        data: jsonParams,
        requestType: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {
            //console.log(msg.d);
            removeTable();
            //console.log(msg.d);
            $(".setThreshold-table").html(' <thead><tr><th>Name</th><th>' + getCircleHeader() + '</th>' +
                     '   <th>RMME/RMSE Count</th> <th>Score</th></tr></thead>');
            $.each(msg.d, function (index, value) {

                $(".setThreshold-table").append("<tr><td>" + value.personName + "</td><td>" + value.fullnetwork + "</td><td>" +
                    value.rmsermmecount + "</td><td>" + value.score + "</td></tr>");


            });
            $('.setThreshold-tbl-wrap').fixedHeader();
            ////removeBigTable();



            ToggleSendButton();

        },

        showLoader: true
        , complete: function (xhr, data) {
            console.log(xhr.status);
            if (xhr.status == 401)
                window.location.href = '/rmmain.aspx';
        }
    });


}

function PopulateGridItemsZeroBoard(data) {
    var headervalue = $(".secMenu ul li.active > .secTab").html();
    if ((headervalue == '' || headervalue == undefined)) {

        return false;
    }
    var circleChkId = '';
    $('.checkbox1:checked').each(function () {
        circleChkId += $(this).val() + ",";
    });
    circleChkId = circleChkId.slice(0, -1);
    //console.log(circleChkId);
    var monthChkId = getMonthChkId();
    monthChkId = monthChkId.slice(0, -1);
    //console.log(monthChkId);
    data.circle = circleChkId;
    data.month = monthChkId;
    data.staffType = $(".secMenu ul li.active > .secTab").html();


    var jsonParams = JSON.stringify({
        "circleParam": circleChkId,
        "month": data.month,
        "staffType": data.staffType
    });

    window.SBICommon.sendAjaxCall({
        url: 'VijayLineMain.aspx/PopulateGridItemsZeroBoard',
        data: jsonParams,
        requestType: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {
            //console.log(msg.d);
            removeTable();
            // $(".setThreshold-tbl-wrap").attr('max-height', '425px');
            var rmse = 'RMSEs'
            if (data.staffType == 'RM') {
                
                rmse = 'RMSEs';
                $(".setThreshold-table").html(' <thead><tr><th>' + getCircleHeader() + '</th><th>' + rmse + '</th>' +
                     '   <th>RM Name</th> <th>RM Mobile</th></tr></thead>');
            } else if (data.staffType == 'RMSE') {
                rmse = 'RMSE';
                $(".setThreshold-table").html(' <thead><tr><th>' + getCircleHeader() + '</th><th>' + rmse + '</th>' +
                     '   <th>RMSE Mobile</th></tr></thead>');
            }
            else if (data.staffType == 'RMME') {
                rmse = 'RMME';
                $(".setThreshold-table").html(' <thead><tr><th>' + getCircleHeader() + '</th><th>' + rmse + '</th>' +
                     '  <th>RMME Mobile</th></tr></thead>');
            }



            //$(".setThreshold-table").html(' <thead><tr><th>'+getCircleHeader()+'</th><th>'+rmse+'</th>' +
            //         '   <th>RM Name</th> <th>RM Mobile</th></tr></thead>');
            $.each(msg.d, function (index, value) {
                if (data.staffType == 'RM') {
                    $(".setThreshold-table").append("<tr><td>" + value.fullnetwork + "</td><td>" +
                        value.rmse + "</td><td>" + value.rmname + "</td><td>" + value.rmmobile + "</td></tr>");
                }
                else {
                    $(".setThreshold-table").append("<tr><td>" + value.fullnetwork + "</td><td>" +
                        value.rmse + "</td><td>" + value.rmmobile + "</td></tr>");
                }
            });


            $('.setThreshold-tbl-wrap').fixedHeader();


            //removeBigTable();
            //addBigTable()
            ToggleSendButton();
        },

        showLoader: true
        , complete: function (xhr, data) {
            console.log(xhr.status);
            if (xhr.status == 401)
                window.location.href = '/rmmain.aspx';
        }
    });

}


function ToggleSendButton() {

    console.log('length-' + $(".setThreshold-table tbody tr").length);
    if ($(".setThreshold-table tbody tr").length == undefined || $(".setThreshold-table tbody tr").length == 0) {
        $(".setThreshold-tbl-wrap").hide();
        console.log('Preview');
        $(".prevew-text").html('');
        $("#smsSuccess").removeClass("popUp-success").addClass("popUp-success").find("#dynTxt").html("No results found!");

        $(".rmv").hide();
        popUp("#smsLayout", "#smsSuccess");
        $(".nextBtn").hide();
        $("#previewSideBar").hide();
    } else {
        $(".setThreshold-tbl-wrap").show();
        $(".nextBtn").show();
        $("#previewSideBar").show();
        populateSmsThreshold("true");
    }

    if ($(".headertable thead").length > 1) {
        console.log('del first');
        $(".headertable thead").first().remove();

    }

    $(window).trigger('resize');
}

function removeBigTable() {
    var tableWrapper = $(".setThreshold-table").closest(".setThreshold-tbl-wrap");
    if (tableWrapper.hasClass("big-table")) {
        tableWrapper.removeClass("big-table");
    }
}

function addBigTable() {
    $(".setThreshold-table").closest(".setThreshold-tbl-wrap").addClass("big-table");
}


function getCircleHeader() {
    var staff = $(".secMenu ul li.active > .secTab").html();
    var type = "Circle";
    if (staff == "CGM")
        type = "Circle";
    if (staff == "GM")
        type = "Network";
    if (staff == "DGM")
        type = "Zone";
    if (staff == "RM")
        type = "Region";
    if (staff == "RMSE" || staff == "RMME")
        type = "Region";
    return type;
}

function getMonthChkId() {

    var monthChkId = '';
    $('.monthCheckbox:checked').each(function () {
        monthChkId += $(this).val() + ",";
    });
    return monthChkId;
}
function getModeChkId() {
    var modeChkId = '';
    $('.modeCheckbox:checked').each(function () {
        modeChkId += $(this).val() + ",";
    });
    return modeChkId;
}
function getPerformanceAlertType() {
    return $(".subtabData ul li.active").data('alerttype');
}
function getPerformanceClass() {
    return $(".subtabData ul li.active").attr('class');
}
function getParameter() {
    return $(".subList ul li.highlight").html();
}


$(".disabled").click(function (e) {
    e.preventDefault();
    return false;
});
$(".permdisabled").click(function (e) {
    e.preventDefault();
    return false;
});

$('#emailTextCustomString').on("keyup", function (e) { populateSmsThreshold(); });
$('#smsTextCustomString').on("keyup", function (e) { populateSmsThreshold(); });


function populateSmsThreshold(ispreview) {

    var modeChkId = getModeChkId();
    var monthChkId = getMonthChkId();
    monthChkId = monthChkId.slice(0, -1);
    var emailTextCustomMessage = $("#emailTextCustomMessage").val();
    var smsTextCustomMessage = $("#smsTextCustomMessage").val();
    var emailTextCustomString = $("#emailTextCustomString").val();
    var smsTextCustomString = $("#smsTextCustomString").val();

    var isCustomMessageSMS = false;
    var isCustomMessageEmail = false
    if (emailTextCustomMessage.length > 0) {
        isCustomMessageEmail = true;
    }
    if (smsTextCustomMessage.length > 0) {
        isCustomMessageSMS = true;
    }

    var parameter = $(".subList ul li.highlight").html();
    var perf = $(".subtabData ul li.active").data('alerttype');
    if (perf == undefined)
        perf = '';
    var customMessage = $("#customMessageInput").text();
    var staffType = $(".secMenu ul li.active > .secTab").html();
    // console.log('Address-'+$("input[name=address]:checked").attr('id'));

    var jsonParams = JSON.stringify({
        "parameter": parameter,
        "performance": perf,
        "isCustomMessageSMS": isCustomMessageSMS,
        "isCustomMessageEmail": isCustomMessageEmail,
        "emailTextCustomMessage": emailTextCustomMessage,
        "smsTextCustomMessage": smsTextCustomMessage,
        "emailTextCustomString": emailTextCustomString,
        "smsTextCustomString": smsTextCustomString,
        "staffType": staffType,
        "monthChkId": monthChkId,
        "salutationIndicator": $("input[name=address]:checked").attr('id')
    });



    $.ajax({
        type: 'POST',
        url: 'VijayLineMain.aspx/GetSMSNEmailText',
        data: jsonParams,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {
            var msgsms = ''
            
            msgsms = msg.d[0].toString();

            console.log(msgsms.replace("?", " -"));
            if (msgsms == undefined || msgsms == '') {
                $(".prevew-text").html('');
            }
            else {
                $(".prevew-text").html(msgsms.split('?').join("-"));
            }
        }
    });
}

//jquery plugin for fixed header
(function ($) {
    $.fn.fixedHeader = function () {
        var $table = $('.setThreshold-table');
        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.table-holder');
            }
        });

        //alert($(this).attr('class'));
        //$this = $(this);
        //var tableHeader = $this.find('thead');
        //var tableHeaderTable = $this.find('.headertable');
        //var tableBodyDiv = $this.find('.table-body');
        //var tableBody = tableBodyDiv.find('tbody')
        //var tableRow = tableBody.find('tr:first');
        //var columnCount = tableHeader.find('th').length;
        //var curWidth;
        //var i;
        //tableHeader.remove();
        //tableHeaderTable.append(tableHeader);
        //// Adjust the width of thead cells when window resizes
        //$(window).resize(function () {
        //    for (i = 1; i <= columnCount; i++) {
        //        curWidth = tableRow.find('td:nth-child(' + i + ')').width();
        //        tableHeader.find('th:nth-child(' + i + ')').width(curWidth);
        //        tableRow.find('td:nth-child(' + i + ')').width(curWidth);
        //    }
        //}).resize(); // Trigger resize handler    */                                                
    };
}(jQuery));

