﻿$(document).ready(function () {
    //$("#dtReviewDate").datepicker({ minDate: -100, maxDate: "+0D" });
    //$("#dtReviewDate").datepicker('setDate', new Date());
    //$("#dtReviewDate").datepicker("option", "dateFormat", "dd/mm/yy");
    GetCircle();
    //$('#dtReviewDate').datepicker();
    //$('#txtDesc').focus();
});

$("#DDLCircle").change(function () {
    $("#DDLNetwork").html('<option value="-1">Select</option>');
    GetNetwork();
});

$("#btnAddReview").click(function () {
    if (validate() == true) {
        var fileName = "";
        var fakeFilePath = $('#fileUpload').val();
        if (fakeFilePath != null && fakeFilePath != "") {
            var fileName = /[^\\]*$/.exec(fakeFilePath)[0];
            //alert(fileName);
        }
        $.ajax({
            url: 'AddReview.aspx/AddNewReview',
            type: "POST",
            dataType: "json",
            data: "{'dtReviewDate': '" + $('#dtReviewDate').val() + "','strReviewType': '" + $("#ddlReviewType").val() + "','strCircle' : '" + $('#DDLCircle').val() +
                "','strNetwork' : '" + $("#DDLNetwork").val() + "','strPresentationName' : '" + fileName + "','strReviewText' : '" + $('#txtDesc').val().trim() + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                ShowDialog(data.d, 100, 300, 'Message', 10, '<span class="icon-info"></span>',true);
            }
        });
    }
    else {
        ShowDialog("Please enter all the values correctly.", 100, 300, 'Message', 10, '<span class="icon-info"></span>',false);
    }
});

function validate() {
    var reviewType = $("#ddlReviewType").val();
    var circle = $("#DDLCircle").val();
    var network = $("#DDLNetwork").val();
    var dtReviewDate = $('#dtReviewDate').val();
    var desc = $('#txtDesc').val().trim();
    //alert($('#fileUpload').val());
    var result = true;

    if (reviewType == "-1") {
        $("#divReviewType").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divReviewType").removeClass("form-group has-error");
    }
    if (circle == "-1") {
        $("#divCircle").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divCircle").removeClass("form-group has-error");
    }
    if (network == "-1") {
        $("#divNetwork").addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divNetwork").removeClass("form-group has-error");
    }
    if (dtReviewDate == "") {
        $('#divReviewDate').addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divReviewDate").removeClass("form-group has-error");
    }
    if (desc == "") {
        $('#divDescription').addClass("form-group has-error");
        result = false;
    }
    else {
        $("#divDescription").removeClass("form-group has-error");
    }
    return result;
}

function GetCircle() {
    $.ajax({
        url: 'AddReview.aspx/GetCircleName',
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLCircle').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}

function GetNetwork() {
    $.ajax({
        url: 'AddReview.aspx/GetNetwork',
        type: "POST",
        dataType: "json",
        data: "{'circle': '" + $("#DDLCircle").val() + "'}",
        contentType: "application/json; charset=utf-8",
        async: false,
        success: function (data) {

            $.each(data, function () {
                $.each(this, function (k, v) {

                    $('#DDLNetwork').append('<option value="' + v + '">' + v + '</option>');
                });
            });
        }
    });
}