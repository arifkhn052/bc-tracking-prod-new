﻿bctrackApp.controller("homeController", function ($scope, $state, $cookies) {
 
    $scope.getData = function () {
      
            $('.loader').show();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                url: "api/GetBankHandler.ashx?mode=Home",
                async: false,
                success: function (bankreport) {

                    $scope.bcCount = bankreport[0].bcCount;
                    $scope.bankCount = bankreport[0].bankCount;
                    $scope.branchCount = bankreport[0].branchCount;
                   
                },
                error: function (err) {
                    alert(err);
                    $('.loader').hide();
                }
            });
    }
});