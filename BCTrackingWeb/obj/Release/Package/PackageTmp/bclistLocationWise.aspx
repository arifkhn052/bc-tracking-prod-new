﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="bclistLocationWise.aspx.cs" Inherits="BCTrackingWeb.bclistLocationWise" %>



    <div class="container-fluid">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">List of Location wise BC's</h2>

            </div>
        

            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href=""><span>Location wise BC's</span></a></li>

                </ul>
            </div>
            
            <!-- /.col-md-6 -->
        </div>
        <div class="col-md-6 pull-right" style="margin-right:-15%;margin-top:15px" ng-show="hidebtnDiv">
             
                                        <button type="button" style="background-color: #e15349;"  ng-click="redirectToDashBoard()" class="btn btn-primary btn-lg">
                                            DashBoard
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </button>
                  
                                        <button type="button"  ng-click="redirectToTopList()" class="btn btn-primary btn-lg">
                                             List of Count
                                             <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </button>

            </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <section class="section" ng-init="ListOfLocationWIse();GetStates()">
        <div class="container-fluid">

            <div class="row">


                <!-- /.col-md-6 -->

                <div class="col-md-12">
                    <div class="panel">

                        <div class="panel-body p-20">
                            <div class="panel-body">

                                 <div class="formmargin">


                                    <div class="col-md-3">
                                        <strong>State</strong>
                                        <select runat="server" class="form-control"
                                            id="selAddressState">
                                            
                                        </select>

                                    </div>
                                    <div class="col-md-3">

                                        <strong>District</strong>
                                      
                                            <select class="form-control"
                                                id="selAddressDistrict">
                                            </select>
                                      

                                    </div>

                                    <div class="col-md-3">
                                        <strong>Sub District</strong>
                                        <select class="form-control"
                                            id="selAddresssubDistrict">
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <strong>Villages</strong>
                                     
                                            <select class="form-control"
                                                id="selAddressVillage">
                                            </select>
                                        
                                    </div>

                                </div>


                            </div>

                            <div class="panel-body">

                                <div class="formmargin">

                                    <div class="col-md-3 pull-right">
                                        <strong></strong>
                                        <button type="button" class="btn btn-success" ng-click="ListOfLocationWIse()">Show List</button>
                                    </div>






                                </div>


                            </div>
                            <hr />
                            <table id="tblbclistLocationWise" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                           <th>BC Code</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                       
                                        <th>Certificates</th>
                                        <th>Villages</th>
                                        <th>Action</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->


                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.section -->

