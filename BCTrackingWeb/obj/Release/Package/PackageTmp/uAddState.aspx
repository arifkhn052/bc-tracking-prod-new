﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uAddState.aspx.cs" Inherits="BCTrackingWeb.uAddState" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
      <div  >
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-12">
                    <h2 class="title">Add State </h2>

                </div>


                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                      <li><a href="#"  ui-sref="home"><i class="fa fa-home"></i> Home</a></li>
                        <li><a ui-sref="statelist"> State List</a></li>
                        <li><a href="#" ui-sref="addstate">Add State Info</a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">

                                </div>
                            </div>
                            <div class="panel-body">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a  ng-click="displayTab('home')"  aria-controls="home"
                                                                              role="tab" data-toggle="tab"><label
                                            for="exampleInputEmail1">State Details </label> </a></li>
                                    <li role="presentation"><a ng-click="displayTab('circle')"  aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Sub District
                                        Details
                                    </label> </a></li>
                                  <%--  <li role="presentation"><a ng-click="displayTab('profile')"  aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Taluka
                                        Details
                                    </label> </a></li>--%>
                                    <li role="presentation"><a ng-click="displayTab('setting')"  aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Village
                                        Details
                                    </label> </a></li>


                                </ul>

                                <!-- Tab panes -->
                              
                                    <div role="tabpanel" class="tab-pane active" ng-hide="divhome">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <strong>State Code</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtStateCode"
                                                                                   ng-model="txtStateCode"
                                                                                   placeholder="State Code"
                                                                                   parsley-trigger="change"  required />
                                                                        </div>
                                                                           <div class="col-sm-6">
                                                                            <strong>State Name</strong>
                                                                            <input type="text"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtStateName"
                                                                                   ng-model="txtStateName"
                                                                                   placeholder="State Name"
                                                                                   parsley-trigger="change"  required />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Districts
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">


                                                                                <div class="col-sm-6">
                                                                                    <strong>District Code</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtDistrictCode"
                                                                                           ng-model="txtDistrictCode"
                                                                                           placeholder="District Code"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>

                                                                                <div class="col-sm-6">
                                                                                    <strong>District Name</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtDistrictName"
                                                                                           ng-model="txtDistrictName"
                                                                                           placeholder="District Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>

                                                                            </div>
                                                                            <div class="row pull-right" id="Div2">
                                                                                <div class="col-sm-12">
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addDistrict()"
                                                                                           ng-disabled="!txtDistrictName"
                                                                                           value="Add More  "
                                                                                           id="Button2">
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr
                                                                                        ">
                                                                                        <td><strong>District
                                                                                            Code</strong>
                                                                                        </td>
                                                                                               <td><strong>District
                                                                                            Name</strong>
                                                                                        </td>
                                                                                        <td><strong>Remove</strong></td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in districtsList ">
                                                                                             <td>{{item.DistrictCode}}</td>
                                                                                            <td>{{item.DistrictName}}</td>

                                                                                            <td>
                                                                                                <div ng-click="removeDistrict($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" ng-hide="divcircle">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">

                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Sub District
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">


                                                                                <div class="col-sm-4">
                                                                                    <strong>Sub District Code</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtsubdistrictCode"
                                                                                           ng-model="txtsubdistrictCode"
                                                                                           placeholder="Sub District Code"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>
                                                                                 <div class="col-sm-4">
                                                                                    <strong>Sub District Name</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtsubdistrictName"
                                                                                           ng-model="txtsubdistrictName"
                                                                                           placeholder="Sub District Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>
                                                                             
                                                                                <div class="col-sm-4">
                                                                                    <strong>District Name</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectDistrictNameForSubDistrict"></select>
                                                                                </div>


                                                                                <div class="row pull-right" id="Div2">
                                                                                    
                                                                                <div class="col-sm-12">
                                                                                  <label for="" style="color:white">.</label>
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addSubDistrict()"
                                                                                            ng-disabled="!txtsubdistrictName"
                                                                                           value="Add More"
                                                                                           id="Button3"/> 
                                                                                </div>


                                                                               

                                                                            </div>
                                                                       
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr>
                                                                                            <td><strong>Sub District
                                                                                                Code</strong>
                                                                                            <td><strong>Sub District
                                                                                                Name</strong>
                                                                                            <td><strong>District
                                                                                                Name</strong>
                                                                                            </td>
                                                                                            <td><strong>Remove</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in subDistrictList">
                                                                                            <td>{{item.SubDistrictCode}}</td>
                                                                                            <td>{{item.SubDistrictName}}</td>
                                                                                              <td>{{item.DistrictName}}</td>
                                                                                           
                                                                                            <td></td>
                                                                                            <td>
                                                                                                <div ng-click="removeSubDistrict($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>
                                    <%--<div role="tabpanel" class="tab-pane" ng-hide="divprofile">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">

                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Talukas
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">


                                                                                <div class="col-sm-6">
                                                                                    <strong>Taluka Name</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtTalukaName"
                                                                                           ng-model="txtTalukaName"
                                                                                           placeholder="Taluka Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <strong>District</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectDistrictNameForTaluka"></select>
                                                                                </div>


                                                                            </div>
                                                                            <div class="row pull-right" id="Div2">
                                                                                <div class="col-sm-12">
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addTalukas()"
                                                                                           ng-disabled="!txtTalukaName "
                                                                                           value="Add More  "
                                                                                           id="Button2">
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr>
                                                                                            <td><strong>Taluka
                                                                                                Name</strong>
                                                                                            <td><strong>District
                                                                                                Name</strong>
                                                                                            </td>
                                                                                            <td><strong>Remove</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in talukaslist">
                                                                                            <td>{{item.TalukaName}}</td>
                                                                                            <td>{{item.DistrictName}}</td>
                                                                                            <td>
                                                                                                <div ng-click="removeTalukas($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>--%>
                                    <div role="tabpanel" class="tab-pane" ng-hide="divsetting">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-body p-20">

                                                                <div class="panel">
                                                                    <div class="panel-heading">
                                                                        <div class="panel-title">
                                                                            Villages
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-body p-20">

                                                                        <div class="panel-body">
                                                                            <div class="row">

                                                                                  <div class="col-sm-4">
                                                                                    <strong>Villages Code</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtVillageCode"
                                                                                           ng-model="txtVillageCode"
                                                                                           placeholder="Village Code"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <strong>Villages Name</strong>
                                                                                    <input type="text"
                                                                                           class="form-control formcontrolheight"
                                                                                           id="txtVillageName"
                                                                                           ng-model="txtVillageName"
                                                                                           placeholder="Village Name"
                                                                                           parsley-trigger="change"
                                                                                            >
                                                                                </div>
                                                                                <div class="col-sm-4">
                                                                                    <strong> Sub District</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectSubDistrictNameForVillage"></select>
                                                                                </div>
                                                                              <%--  <div class="col-sm-4">
                                                                                    <strong>Taluka</strong>
                                                                                    <select type="text"
                                                                                            class="form-control formcontrolheight"
                                                                                            id="selectTalukaNameForVillage"></select>
                                                                                </div>--%>


                                                                            </div>
                                                                            <div class="row pull-right" id="Div2">
                                                                                <div class="col-sm-12">
                                                                                    <input type="button"
                                                                                           class="btn btn-primary  btn-sm form-control"
                                                                                           ng-click="addVilages()"
                                                                                           ng-disabled="!txtVillageName "
                                                                                           value="Add More  "
                                                                                           id="Button2">
                                                                                </div>
                                                                            </div>
                                                                            <br/>
                                                                            <br/>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <table id="" class="table ">
                                                                                        <tr>
                                                                                            <th>Village Code</th>
                                                                                            <th>Village Name</th>
                                                                                            <th>District Name</th>
                                                                                            

                                                                                            <td><strong>Remove</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr ng-repeat="item in villagesList ">
                                                                                             <td>{{item.VillageCode}}</td>
                                                                                            <td>{{item.VillageName}}</td>
                                                                                        
                                                                                         <td>{{item.subdistrictName}}</td>
                                                                                            <td>
                                                                                                <div ng-click="removeVillage($index)"
                                                                                                     class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>


                                                                        </div>


                                                                        <!-- /.col-md-12 -->
                                                                    </div>
                                                                </div>

                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>

                                           <div class="formmargin center-block">
                    <div class="center-block">
                        <button type="button" ng-click="addState()" class="btn btn-success center-block">Submit</button>
                    </div>
                </div>


                                <!-- /.src-code -->

                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>


<%--    
    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/parsley.min.js" defer></script>
    <script src="js/main.js"></script>
    <script src="js/states.js"></script>--%>
    </form>
</body>
</html>
