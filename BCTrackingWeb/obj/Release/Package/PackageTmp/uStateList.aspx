﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uStateList.aspx.cs" Inherits="BCTrackingWeb.uStateList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BC TRACKING</title>

</head>
<body>
    <form id="form1" runat="server">
     <div>
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-6">
                    <h2 class="title">List of State</h2>

                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 right-side">
                    <a href="#" ui-sref="addstate" class="btn bg-black" role="button"><i class="fa fa-plus"></i> Add State</a>
                </div>
                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="#"  ui-sref="home"><i class="fa fa-home"></i> Home</a></li>
                     <%--   <li><a href="#"  ui-sref="statelist" > State List</a></li>--%>
                            <li><a href="#" ui-sref="editState({ stateId: 4})" > State List</a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">


                    <!-- /.col-md-6 -->

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">

                                </div>
                            </div>
                            <div class="panel-body p-20">

                                <table class="display" clientidmode="Static"  id="tblLeadList">
                            <thead>
                                <tr>
                                 <th class="text-left">StateId</th>
                                     <th class="text-left">State Name</th>                                 
                                    <th class="text-left">Edit</th>
                                <%--    <th class="text-left">View</th>--%>
                                </tr>
                            </thead>
                                    <tbody></tbody>
                        </table>

                                
                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-8 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>
      
      
   </form>
   
</body>
</html>