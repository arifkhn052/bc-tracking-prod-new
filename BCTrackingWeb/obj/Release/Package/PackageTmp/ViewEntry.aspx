﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mainMaster.Master" AutoEventWireup="true" CodeBehind="ViewEntry.aspx.cs" Inherits="BCTrackingWeb.ViewEntry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
          <title>BC Tracking System</title>       
  <style>
      .img-circle {
    border-radius: 50%;
        width: 75px;
}
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div  ng-init="GetBanks();getBcDetails();GetStates();datetimeBind();getProduct()" ng-clock>
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-9">
                    <h2 class="title">View Business Correspondents Single Entry </h2>

                </div>
           
                  <div class="col-md-3">
                    <img src="http://bctrackingprod.blob.core.windows.net/dev-photos/{{ImagePath}}" class="img-circle" alt="User Avatar" class="img-responsive">

                </div>
              
                                        
                                         

                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="#"  ui-sref="home"><i class="fa fa-home"></i> Home</a></li>
                    <%--    <li><a ui-sref="all">Single Entry List</a></li>--%>
                         <li><a >View Single Entry</a></li>


                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">
                                     
                                     <h4>{{lblName}}</h4>  
                                     <h6>{{lblEmail}}, {{lblPhone1}}</h6>  
                                              <h6>{{bankMessage}} </h6>  
                                     
                                      
                                          
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel">                                                       
                                                                                                     
                                                        <div class="panel-body p-20">
                                                             <div class="panel-body" ng-hide="divEdit">
                                                         
                                                                <div class="row">
                                                                     <div class="col-sm-2">
                                                                        <label for="fileImage" class=" control-label">Name </label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtName"
                                                                             required    placeholder="Name"
                                                                               parsley-trigger="change">
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                      <label for="fileImage" class="control-label">Date
                                                                            Of Birth</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                   <%--       <input runat="server" type="date" required
                                                                               class="form-control" id="txtDOB"
                                                                                 placeholder="Date of Birth">--%>
                                                                          <input type='text' class="form-control" id="txtDOB"  placeholder="dd/mm/yyyy" autocomplete="off" />
                                                                    </div>

                                                                </div>  
                                                                 

                                                                <div class="row">
                                                                    <div class="col-sm-2">
                                                                        <label for="fileImage" class="control-label">Gender</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                       <select runat="server" class="form-control"
                                                                                  id="selGender">
                                                                            <option value="">Select</option>
                                                                            <option value="Male">Male</option>
                                                                            <option value="Female">Female</option>
                                                                            <option value="Other">Other
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                     <label for="txtFather"
                                                                           class="col-sm-2 control-label">Upload
                                                                            Image</label>
                                                                    <div class="col-sm-4 form-group">
                                                                          <input type="file" id="fileImage" runat="server"
                                                                               class="uploaderCss"
                                                                               accept='.png,.jpg,.gif,.jpeg'>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtGender"
                                                                           class="col-sm-2 control-label">Physically
                                                                        Handicapped</label>
                                                                    <div class="col-sm-4 form-group">
                                                                        <input type="radio" id="radHandicapNo"
                                                                               name="Handicap" value="No" checked/>&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;&nbsp;

                                                                        <input type="radio" id="radHandicapYes"
                                                                               name="Handicap" value="Yes"/>&nbsp;&nbsp;Yes

                                                                    </div>
                                                                            <label for="txtFather"
                                                                           class="col-sm-2 control-label">Father
                                                                        Name</label>
                                                                    <div class="col-sm-4 form-group">
                                                                           <input runat="server" type="text"
                                                                               class="form-control" id="txtFather"
                                                                               placeholder="Name">

                                                                    </div>

                                                                </div>
                                                             

                                                                <div class="row">
                                                                    <label for="txtSpouse"
                                                                           class="col-sm-2 control-label">Spouse
                                                                        Name</label>
                                                                    <div class="col-sm-4 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtSpouse"
                                                                               placeholder="Name">
                                                                    </div>
                                                                         <label for="selCategory"
                                                                           class="col-sm-2 control-label">Category</label>
                                                                    <div class="col-sm-4 form-group">
                                                                        <select runat="server" class="form-control"
                                                                                id="selCategory">
                                                                            <option value="General">General</option>
                                                                            <option value="OBC">OBC</option>
                                                                            <option value="SC">SC</option>
                                                                            <option value="ST">ST</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                            

                                                                


                                                                <div class="row">
                                                                    <label for="txtPhone1"
                                                                           class="col-sm-2 control-label">Contact
                                                                        Number</label>
                                                                    <div class="col-sm-4 form-group">
                                                                        <input runat="server" type="text" required
                                                                               class="form-control" id="txtPhone1"
                                                                               placeholder="Contact Number">
                                                                    </div>
                                                                       <label for="txtPhone1"
                                                                           class="col-sm-2 control-label">Contact
                                                                        Number2</label>
                                                                        <div class="col-sm-4 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtPhone2"
                                                                               placeholder="Contact Number 2">
                                                                    </div>
                                                                   
                                                                </div>
                                                                      
                                                                
                                                                <div class="row">
                                                                             <label for="txtEmail"
                                                                           class="col-sm-2 control-label">Contact
                                                                        Number3</label>
                                                                          <div class="col-sm-4 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="Text4"
                                                                               placeholder="Contact Number 3">
                                                                    </div>
                                                                    <label for="txtEmail"
                                                                           class="col-sm-2 control-label">Email</label>
                                                                    <div class="col-sm-4 form-group">
                                                                        <input runat="server" type="email"
                                                                               class="form-control" id="Email1" required
                                                                               placeholder="Email">
                                                                    </div>
                                                                </div> 
                                                            <%--    <div class="row">
                                                                             <label for="txtEmail"
                                                                           class="col-sm-2 control-label">Contact Person</label>
                                                                          <div class="col-sm-4 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtContactPerson"
                                                                               placeholder="Contact Person">
                                                                    </div>
                                                                    <label for="txtEmail"
                                                                           class="col-sm-2 control-label">Contact Designation</label>
                                                                    <div class="col-sm-4 form-group">
                                                                        <input runat="server" type="email"
                                                                               class="form-control" id="txtContactDesignation" 
                                                                               placeholder="Contact Designation">
                                                                    </div>
                                                                </div>--%>
                                                                <div class="row">
                                                                             <label for="txtEmail"
                                                                           class="col-sm-2 control-label">No of Complaint</label>
                                                                          <div class="col-sm-4 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtNoofComplaint"
                                                                               placeholder="No of Complaint">
                                                                    </div>
                                                                  
                                                                </div>
                                                                

                                                                </div>
                                                            <div class="panel-body" ng-hide="divVIEW">
                                                         
                                                                <div class="row">
                                                                     <div class="col-sm-2">
                                                                        <label for="fileImage" class=" control-label">Name</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                           <label for="fileImage" class=" control-label">{{lblName}}</label>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                      <label for="fileImage" class="control-label">Date
                                                                            Of Birth</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                        <label for="fileImage" class="control-label">{{lblDOB}}</label>
                                                                    </div>
                                                                </div>   

                                                                <div class="row">
                                                                    <div class="col-sm-2">
                                                                        <label for="fileImage" class="control-label">Gender</label>
                                                                    </div>
                                                                    <div class="col-sm-4 form-group">
                                                                           <label for="fileImage" class="control-label">{{lblGender}}</label>
                                                                   
                                                                    </div>
                                                                     <label for="txtFather"
                                                                           class="col-sm-2 control-label">
                                                                            Image</label>
                                                                    <div class="col-sm-4 form-group">
                                                                       
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtGender"
                                                                           class="col-sm-2 control-label">Physically
                                                                        Handicapped</label>
                                                                    <div class="col-sm-4 form-group">
                                                                       <label for="txtGender"
                                                                           class="col-sm-2 control-label">{{lblhand}}</label>

                                                                

                                                                    </div>
                                                                            <label for="txtFather"
                                                                           class="col-sm-2 control-label">Father
                                                                        Name</label>
                                                                    <div class="col-sm-4 form-group">
                                                                         <label for="txtFather"
                                                                           class="col-sm-2 control-label">{{lblFather}}</label>
                                                                   

                                                                    </div>

                                                                </div>
                                                             

                                                                <div class="row">
                                                                    <label for="txtSpouse"
                                                                           class="col-sm-2 control-label">Spouse
                                                                        Name</label>
                                                                    <div class="col-sm-4 form-group">
                                                                          <label for="txtSpouse"
                                                                           class="col-sm-2 control-label">{{lblSpouse}}</label>
                                                                  
                                                                    </div>
                                                                         <label for="selCategory"
                                                                           class="col-sm-2 control-label">Category</label>
                                                                    <div class="col-sm-4 form-group">
                                                                       <label for="txtSpouse"
                                                                           class="col-sm-2 control-label">{{lblCategory}}</label>
                                                                    </div>
                                                                </div>

                                                            

                                                                <div class="row">
                                                                    <label for="txtPhone1"
                                                                           class="col-sm-2 control-label">Contact
                                                                        Number</label>
                                                                    <div class="col-sm-4 form-group">
                                                                           <label for="txtSpouse"
                                                                           class="col-sm-2 control-label">{{lblPhone1}}</label>
                                                                    </div>
                                                                       <label for="txtPhone1"
                                                                           class="col-sm-2 control-label">Contact
                                                                        Number2</label>
                                                                        <div class="col-sm-4 form-group">
                                                                        <label for="txtSpouse"
                                                                           class="col-sm-2 control-label">{{lblPhone2}}</label>
                                                                    </div>
                                                                   
                                                                </div>
                                                                    <div class="row">
                                                                             <label for="txtEmail"
                                                                           class="col-sm-2 control-label">Contact
                                                                        Number3</label>
                                                                          <div class="col-sm-4 form-group">
                                                                            <label for="txtSpouse"
                                                                           class="col-sm-2 control-label">{{lblPhone3}}</label>
                                                                    </div>
                                                                      
                                                                    <label for="txtEmail"
                                                                           class="col-sm-2 control-label">Email</label>
                                                                    <div class="col-sm-4 form-group">
                                                                           <label for="txtSpouse"
                                                                           class="col-sm-2 control-label">{{lblEmail}}</label>
                                                                    </div>
                                                                </div>   

<%--                                                                <div class="row">
                                                                             <label for="txtEmail"
                                                                           class="col-sm-2 control-label">Contact Person</label>
                                                                          <div class="col-sm-4 form-group">
                                                                            <label for="lblContact"
                                                                           class="col-sm-2 control-label">{{lblcontactperoson}}</label>
                                                                    </div>
                                                                      
                                                                    <label for="txtEmail"
                                                                           class="col-sm-2 control-label">Contact Designation</label>
                                                                    <div class="col-sm-4 form-group">
                                                                           <label for="txtSpouse"
                                                                           class="col-sm-2 control-label">{{lblcontactdesignaton}}</label>
                                                                    </div>
                                                                </div>--%>

                                                                <div class="row">
                                                                             <label for="txtEmail"
                                                                           class="col-sm-2 control-label">No of Complaint</label>
                                                                          <div class="col-sm-4 form-group">
                                                                            <label for="lblContact"
                                                                           class="col-sm-2 control-label">{{lblNoofComplaint}}</label>
                                                                    </div>
                                                                
                                                                </div>

                                                                </div>
                                                           

                                                  
                                                            <div class="panel-body" ng-hide="divEdit">
                                                         
                                                           

                                                          

                                                            </div>
                                    <div  class="row pull-right">                                     
                                    <div class="col-sm-6" ng-hide="divAllocateBtn">
                                          <a class="btn btn-danger btn-sm" ng-click="allocateBc()">
                                           <i class="fa fa-arrow-right"  aria-hidden="true"></i>&nbsp;Allocate</a>                                      
                                    </div>
                                   


                                    <div class="col-sm-12">
                                        <a  class="btn btn-primary btn-sm"  href="#/editallCorresponds/{{bcid}}" id="tabedit">
                                            <span class="fa fa-pencil-square-o" aria-hidden="true"></span>&nbsp;Edit </a>
                                        <a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/{{bcid}}" >
                                            <i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Blacklist</a>
                                        <a class="btn btn-danger  btn-sm"    href="#/chnageStatusallCorresponds/{{bcid}}" >
                                            <i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Terminate</a>
                                    </div>
                                </div>
                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                      
                                                        
                                            
                                                    </div>
                                                   
                                                    <!-- /.panel -->
                                                </div>
                                            </div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a ng-click="displayTab('home')" aria-controls="home"
                                                                              role="tab" data-toggle="tab"><label
                                            for="exampleInputEmail1"> Identity Details</label> </a></li>
                                    <li role="presentation"><a ng-click="displayTab('circle')" aria-controls="profile" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Qualification</label> </a></li>
                                    <li role="presentation"><a ng-click="displayTab('profile')" aria-controls="messages" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Previous Experience  </label> </a></li>
                                    <li role="presentation"><a ng-click="displayTab('setting')" aria-controls="messages" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">BC Type and Workings</label> </a></li>
                                       <li role="presentation"><a ng-click="displayTab('message')" aria-controls="messages" role="tab"
                                                               data-toggle="tab"><label for="exampleInputEmail1">Device Details</label> </a></li>


                                </ul>

                                <!-- Tab panes -->
                               
                                    <div role="tabpanel" class="tab-pane" ng-hide="divhome">


                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel">                                                    
                                                        <div class="panel-body p-20">
                                                            <div class="panel-body">                                                           

                                                                

                                                                <div class="row" >
                                                                    <label for="txtAadharCard"
                                                                           class="col-sm-4 control-label">Aadhar
                                                                        Card</label>
                                                                   <%-- <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text" disabled
                                                                               class="form-control" id="txtAadharCard"
                                                                               placeholder="Aadhar Card Number">
                                                                         
                                                                    </div>--%>
                                                                     <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{AadharCard}}</label>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtPanCard"
                                                                           class="col-sm-4 control-label">Pan
                                                                        Card</label>
                                                                    <%--<div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtPanCard"
                                                                               placeholder="Pan Card Number">
                                                                        
                                                                    </div>--%>
                                                                     <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{PanCard}}</label>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtVoterId"
                                                                           class="col-sm-4 control-label">Voter Id
                                                                        Card</label>
                                                                  <%--  <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtVoterId"
                                                                               placeholder="Voter Id Card">
                                                                        
                                                                    </div>--%>
                                                                     <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{VoterCard}}</label>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtDriverLic"
                                                                           class="col-sm-4 control-label">Drivers
                                                                        License</label>
                                                                 <%--   <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtDriverLic"
                                                                               placeholder="Drivers License">
                                                                    </div>--%>
                                                                      <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{DriverLicense}}</label>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtNregaCard"
                                                                           class="col-sm-4 control-label">NREGA
                                                                        Card</label>
                                                                  <%--  <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtNregaCard"
                                                                               placeholder="NREGA Card">
                                                                    </div>--%>
                                                                     <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{NregaCard}}</label>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtRationCard"
                                                                           class="col-sm-4 control-label">Passport 
                                                                        Number</label>
                                                              <%--      <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtRationCard"
                                                                               placeholder="Ration Card">
                                                                    </div>--%>
                                                                     <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{RationCard}}</label>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="selAddressState"
                                                                           class="col-sm-4 control-label">Address
                                                                        State</label>
                                                                <%--    <div class="col-sm-9 form-group">
                                                                        <select runat="server" class="form-control"
                                                                                id="selAddressState">
                                                                        </select>
                                                                    </div>--%>
                                                                       <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{State[0].StateName}} ({{State[0].StateCode}})</label>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="selAddressDistrict"
                                                                           class="col-sm-4 control-label">Address
                                                                        District</label>
                                                          <%--          <div class="col-sm-9 form-group">
                                                                        <select class="form-control"
                                                                                id="selAddressDistrict">
                                                                        </select>
                                                                    </div>--%>
                                                                    <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{District[0].DistrictName}}  ({{District[0].DistrictCode}})</label>
                                                                </div>


                                                                <div class="row">
                                                                    <label for="txtAddressSubDistrict"
                                                                           class="col-sm-4 control-label">Address Sub
                                                                        District</label>
                                                                  <%--  <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control"
                                                                               id="txtAddressSubDistrict"
                                                                               placeholder="Address Sub District">
                                                                    </div>--%>
                                                                     <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{Subdistrict[0].SubDistrictName}}  ({{Subdistrict[0].SubDistrictCode}})</label>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="selAddressCity"
                                                                           class="col-sm-4 control-label">Address
                                                                        Villages</label>
                                                            <%--        <div class="col-sm-9 form-group">
                                                                        <select class="form-control"
                                                                                id="selAddressCity">
                                                                        </select>
                                                                    </div>--%>
                                                                     <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{City[0].VillageName}}  ({{City[0].VillageCode}})</label>
                                                                </div>

                                                                 <div class="row">
                                                                    <label for="txtRationCard"
                                                                           class="col-sm-4 control-label">Passport
                                                                        Number</label>
                                                              <%--      <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtRationCard"
                                                                               placeholder="Ration Card">
                                                                    </div>--%>
                                                                     <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{PinCode}}</label>
                                                                </div>
                                                               
                                                                 <div class="row">
                                                                    <label for="txtAddressArea"
                                                                           class="col-sm-4 control-label">Address
                                                                        Area</label>
                                        <%--                            <div class="col-sm-9 form-group">
                            <textarea runat="server" class="form-control" id="txtAddressArea"
                                      placeholder="Address Area"/>
                                                                    </div>--%>
                                                                     <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{Area}}</label>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtPinCode"
                                                                           class="col-sm-4 control-label">Pin
                                                                        Code</label>
                                                                 <%--   <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtPinCode"
                                                                               placeholder="Pin Code">
                                                                    </div>--%>
                                                                         <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{PinCode}}</label>
                                                                </div>

                                                                <div class="row">
                                                                    <label for="SelAlternateOccupation"
                                                                           class="col-sm-4 control-label">Alternate
                                                                        Occupation
                                                                        Type</label>
                                                                 <%--   <div class="col-sm-9 form-group">
                                                                        <select class="form-control"
                                                                                id="SelAlternateOccupation"
                                                                                runat="server">
                                                                             <option value="">--Select-- </option>
                                                                            <option value="Government">Government
                                                                            </option>
                                                                            <option value="Public Sector">Public
                                                                                Sector
                                                                            </option>
                                                                            <option value="Private">Private</option>
                                                                            <option value="Self Employed">Self
                                                                                Employed
                                                                            </option>
                                                                            <option value="Any Other">Any Other</option>


                                                                        </select>
                                                                       
                                                                    </div>--%>
                                                                       <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{AlternateOccupationType}}</label>

                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtAlternateOccupationDtl"
                                                                           class="col-sm-4 control-label">Alternate
                                                                        Occupation
                                                                        Detail</label>
                                                        <%--            <div class="col-sm-9 form-group">
                            <textarea runat="server" class="form-control" id="txtAlternateOccupationDtl"
                                      placeholder="Alternate Occupation Detail"/>
                                                                    </div>--%>
                                                                     <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{AlternateOccupationDetail}}</label>
                                                            
                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtUniqueId"
                                                                           class="col-sm-4 control-label">Account no</label>
                                                                 <%--   <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control" id="txtUniqueId"
                                                                               placeholder="Unique Identification Number">
                                                                    </div>--%>
                                                                     <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{UniqueIdentificationNumber}}</label>

                                                                </div>

                                                                <div class="row">
                                                                    <label for="txtBankReferenceNumber"
                                                                           class="col-sm-4 control-label">Bank Reference
                                                                        Number</label>
                                                                <%--    <div class="col-sm-9 form-group">
                                                                        <input runat="server" type="text"
                                                                               class="form-control"
                                                                               id="txtBankReferenceNumber"
                                                                               placeholder="Bank Reference Number">
                                                                    </div>--%>
                                                                       <label for="lbladhaar"
                                                                           class="col-sm0 control-label">{{BankReferenceNumber}}</label>

                                                                </div>
                                                            </div>


                                                            <!-- /.col-md-12 -->
                                                        </div>
                                                    </div>
                                                    <!-- /.panel -->
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div role="tabpanel" class="tab-pane" ng-hide="divcircle">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                    Bc Certification
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row" style="display:none">
                                                                        <div class="col-sm-3">
                                                                            <strong>Date of Passing</strong>
                                                                     <%--       <input runat="server" type="date"
                                                                                   class="form-control"
                                                                                   id="txtPassingDate"
                                                                                    ng-model="txtPassingDate"
                                                                                   placeholder="Date of Passing">--%>

                                                                              <input type='text' class="form-control"   ng-model="txtPassingDate" id="txtPassingDate"  placeholder="dd/mm/yyyy" autocomplete="off" />
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <strong>Institute Name</strong>
                                                                            <select runat="server" class="form-control"
                                                                                    id="SelInstitute"
                                                                                    datatextfield="InstituteName"
                                                                                    datavaluefield="InstituteName"
                                                                                    
                                                                                >
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <strong>Course</strong>
                                                                            <select runat="server" class="form-control"
                                                                                    id="SelCourse"
                                                                                    datatextfield="Course"
                                                                                    datavaluefield="Course">
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3">

                                                                            <strong>Grades</strong>
                                                                            <input runat="server" type="text"
                                                                                   class="form-control" id="txtGrades"
                                                                                   placeholder="Grades">
                                                                        </div>
                                                                    </div>
                                                                     <div class="row">
                                                                         <div class="col-sm-6" ng-hide="divOtherrInstitute">

                                                                            <strong>Other Institute Name</strong>
                                                                            <input runat="server" type="text"
                                                                                   class="form-control" id="Text1"
                                                                                   placeholder="Other Institute Name">
                                                                        </div>
                                                                         <div class="col-sm-6" ng-hide="divOtherCourse">

                                                                            <strong>Other Course</strong>
                                                                            <input runat="server" type="text"
                                                                                   class="form-control" id="Text2"
                                                                                   placeholder="Other Course">
                                                                        </div>
                                                                         </div>
                                                                    <div class="row pull-right" id="Div2" style="display:none">
                                                                        <div class="col-sm-12">
                                                                            <input type="button"
                                                                                   class="btn btn-primary  btn-sm form-control"
                                                                                   ng-click="AddMoreCerts()"
                                                                                   ng-disabled="!txtPassingDate "
                                                                                   value="Add More  " id="Button2">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                    <br/>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Date of Passing</strong>
                                                                                </td>
                                                                                <td><strong>Institute Name</strong></td>
                                                                                <td><strong> Course</strong></td>
                                                                                <td><strong>Grades</strong></td>
                                                                           <%--     <td><strong>Remove</strong></td>--%>
                                                                                </tr>
                                                                                <tr ng-repeat="item in certsList ">
                                                                                    <td>{{item.DateOfPassing}}</td>
                                                                                    <td>{{item.InstituteName}}</td>
                                                                                     <td>{{item.CourseName}}</td>
                                                                                     <td>{{item.Grade}}</td>
                                                                                 <%--   <td>
                                                                                        <div ng-click="removeCerts($index)"
                                                                                             class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                        </div>
                                                                                    </td>--%>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                    Educational Qualifications
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row">


                                                                        <div class="col-sm-6">
                                                                            <strong>Qualification</strong>
                                                                          <%--  <select runat="server"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selQualification">
                                                                                <option value="SSC">SSC</option>
                                                                                <option value="HSC">HSC</option>
                                                                                <option value="Graduate">Graduate
                                                                                </option>
                                                                                <option value="Others">Others</option>
                                                                            </select>--%>
                                                                             <label for="lbladhaar"
                                                                           class="col-sm-2 control-label">{{Qualification}}</label>
                                                                        </div>


                                                                        <div class="col-sm-6">
                                                                            <strong>If Other Qualification</strong>
                                                                    <%--        <textarea runat="server"
                                                                                      class="form-control"
                                                                                      id="txtOtherQualification"
                                                                                      placeholder="Other Qualification"/>--%>
                                                                            <label for="lbladhaar"
                                                                           class="col-sm-2 control-label">{{OtherQualification}}</label>
                                                                        </div>


                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        
                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" ng-hide="divprofile">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                      
                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                        
                                                                  Previous Experience as BC (if Any)
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row" style="display:none">


                                                                        <div class="col-sm-2">
                                                                            <strong>Bank Name</strong>
                                                                               <select  runat="server" class="form-control"
                                                                                id="selPreviousExpBank">
                                                                        </select>
                                                                        </div>


                                                                        <div class="col-sm-2">
                                                                            <strong>Branch</strong>
                                                                            <select runat="server"
                                                                                    class="form-control formcontrolheight"
                                                                                    id="selPreviousExpBranch"
                                                                                    datatextfield="BranchName"
                                                                                    datavaluefield="BranchId">
                                                                            </select>
                                                                        </div>


                                                                        <div class="col-sm-2">
                                                                            <strong>From Date</strong>
                                                                         <%--   <input runat="server" type="date"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtFromDateExp"
                                                                                   placeholder="Experience From Date">--%>

                                                                              <input type='text' class="form-control" id="txtFromDateExp"  placeholder="dd/mm/yyyy" autocomplete="off" />
                                                                        </div>


                                                                        <div class="col-sm-2">

                                                                            <strong>To Date</strong>
                                                                         <%--   <input runat="server" type="date"
                                                                                   class="form-control formcontrolheight"
                                                                                   id="txtToDateExp"
                                                                                   placeholder="Experience To Date">--%>
                                                                             <input type='text' class="form-control" id="txtToDateExp"  placeholder="dd/mm/yyyy" autocomplete="off" />
                                                                        </div>
                                                                        <div class="col-sm-4">

                                                                            <strong>Reasons for Leaving </strong>
                                                                            <textarea runat="server"
                                                                                      class="form-control"
                                                                                      id="txtReasons"
                                                                                      placeholder="Reasons"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row pull-right" id="Div2" style="display:none" >
                                                                        <div class="col-sm-12">
                                                                            <input type="button"
                                                                                   class="btn btn-primary  btn-sm form-control"
                                                                                   ng-click="AddMoreExperience()"
                                                                                 
                                                                                   value="Add More  " id="Button2">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                    <br/>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Bank Name</strong></td>
                                                                                <td><strong>Branch</strong></td>
                                                                                <td><strong> From Date</strong></td>
                                                                                <td><strong>To Date</strong></td>
                                                                                <td><strong>Reasons for Leaving</strong></td>
                                                                             <%--   <td><strong>Remove</strong></td>--%>
                                                                                </tr>
                                                                                <tr ng-repeat="item in expsList">
                                                                                    <td>{{item.BankName}}</td>
                                                                                 <td>{{item.BranchName}}</td>
                                                                                   <td>{{item.FromDate}}</td>
                                                                                    <td>{{item.ToDate}}</td>
                                                                                  <td>{{item.Reason}}</td>
                                                                                  <%--  <td>
                                                                                        <div ng-click="removeExperience($index)"
                                                                                             class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                        </div>
                                                                                    </td>--%>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>

                                    </div>
                                    <div role="tabpanel" class="tab-pane" ng-hide="divsetting">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                          <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                   Corporate Association / Business Correspondent Allocation

                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row" style="display:none">


                                                                        <div class="col-sm-6">
                                                                            <strong>Corporate List</strong>
                                                                          <select runat="server" style="display:none" class="form-control formcontrolheight" id="selCorporate" datatextfield="CorporateName" datavaluefield="CorporateId">
                            </select> <label for="lbladhaar"
                                                                       
                                                        
                                                                        </div>


                                                                        <div class="col-sm-6">
                                                                            <strong>Allocated</strong>
                                                                         <input type="radio" id="radAllocatedNo" name="Allocated" value="No"  checked />&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;&nbsp;
                           
                    <input type="radio" id="radAllocatedYes" name="Allocated" value="Yes" />&nbsp;&nbsp;Yes
                                                                        </div>
                                                                    </div>                                                                   
                                                                <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Corporate List</strong></td>
                                                                                <td><strong>AllocatedAllocated</strong></td>    
                                                                                                     </tr>
                                                                                <tr ng-repeat="item in connsList ">
                                                                                    <td>{{CorporateId}}</td>
                                                                                   <td>{{isAllocated}}</td>
                                                                                   
                                                                                 
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>
                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                   Associated Bank Detail 
                                                                    <br /> {{bankMessage1}}
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row" style="display:none">


                                                                        <div class="col-sm-3">
                                                                            <strong>Appointment Date</strong>
                                                                        <%-- <input runat="server" type="date" class="form-control formcontrolheight" id="txtAppointmentDate" placeholder="Appointment Date"--%>>
                                                                              <input type='text' class="form-control" id="txtAppointmentDate"  placeholder="dd/mm/yyyy" autocomplete="off" />
                                                                        </div>


                                                                        <div class="col-sm-3">
                                                                            <strong>IFSC Code</strong>
                                                                           <input runat="server" type="text" class="form-control formcontrolheight" id="ifscCode" placeholder="IFSC Code">
                                                                        </div>


                                                                        <div class="col-sm-3">
                                                                            <strong>Bank Name</strong>
                                                                       <%--  <select runat="server" class="form-control formcontrolheight" id="selAllocationBank" datatextfield="BankName" datavaluefield="BankId">
                                </select>--%>
                                                                            <select class="form-control formcontrolheight"  
                                                                                       id="selAllocationBank">
                                                                             </select>
                                                                        </div>


                                                                        <div class="col-sm-3">

                                                                            <strong>Branch</strong>
                                                                         <select runat="server" class="form-control formcontrolheight" id="selAllocationBranch" datatextfield="BranchName" datavaluefield="BranchId">
                                </select>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    
                                                           <div class="row" ng-hide="divAllocate">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Appointment Date</strong></td>
                                                                                <td><strong>IFSC Code</strong></td>   
                                                                                     <td><strong>Bank Name</strong></td>   
                                                                                      <td><strong>Branch</strong></td>      
                                                                                                     </tr>
                                                                                <tr ng-repeat="item in connsList ">
                                                                                    <td>{{AppointmentDate}}</td>
                                                                                   <td>{{AllocationIFSCCode}}</td>
                                                                                     <td>{{AllocationBankId}}</td>
                                                                                   <td>{{AllocationBranchId}}</td>
                                                                                   
                                                                                 
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>
                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                 BC Type and Workings

                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row" style="display:none">


                                                                        <div class="col-sm-4">
                                                                            <strong>Type of Bank</strong>
                                                                             <div class="col-sm-10">
                                <select runat="server" class="form-control formcontrolheight" id="selBCType">
                                     <option value="">--Select-- </option>
                                    <option value="Individual">Individual</option>
                                    <option value="Corporate">Corporate</option>
                                    <option value="Fixed">Fixed</option>
                                    <option value="Mobile">Mobile</option>
                                    <option value="Both">Both</option>

                                </select>
                            </div>
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>Working Days</strong>
                                                                      <input runat="server" type="text" class="form-control formcontrolheight" id="txtWorkingDays" placeholder="Working Days">
                                                                        </div>
                                                                          <div class="col-sm-4">
                                                                            <strong>Working Hours</strong>
                                                                           <input runat="server" type="text" class="form-control formcontrolheight" id="txtWorkingHours" placeholder="Working Hours">
                            </select>
                                                                        </div>
                                                                    </div>                                                                   
                                                                
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Type of Bank</strong></td>
                                                                                <td><strong>Working Days</strong></td>   
                                                                                     <td><strong>Working Hours</strong></td>   
                                                                                    
                                                                                                     </tr>
                                                                                <tr ng-repeat="item in connsList ">
                                                                                    <td>{{BCType}}</td>
                                                                                   <td>{{WorkingDays}}</td>
                                                                                     <td>{{WorkingHours}}</td>
                                                                                 
                                                                                   
                                                                                 
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <%--<div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                               Primary Location (For Fixed Bank)
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row" style="display:none">
                                                                        <div class="col-sm-4">
                                                                            <strong>SSA</strong>
                                                                           <input type="text" class="form-control formcontrolheight" id="txtPostalAddress" runat="server" placeholder="Postal Address">
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <strong>Village Code</strong>
                                                                        <input type="text" class="form-control formcontrolheight" id="txtVillageCode1" runat="server" placeholder="Enter Village Code">
                                                                        </div>
                                                                         <div class="col-sm-4">
                                                                            <strong>Village Detail</strong>
                                                                       <textarea name="villageDetail" id="txtVillageDetail1" runat="server" class="form-control" rows="3" placeholder="Village Detail"></textarea>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Postal Address</strong></td>
                                                                                <td><strong>Village Code</strong></td>   
                                                                                     <td><strong>Village Detail</strong></td>   
                                                                                    
                                                                                                     </tr>
                                                                                <tr ng-repeat="item in connsList ">
                                                                                    <td>{{PLPostalAddress}}</td>
                                                                                   <td>{{PLVillageCode}}</td>
                                                                                     <td>{{PLVillageDetail}}</td>
                                                                                 
                                                                                   
                                                                                 
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                     <div class="row" style="display:none">
                                                                        <div class="col-sm-4">
                                                                            <strong>State</strong>
                                                                             <select class="form-control formcontrolheight" id="selPLState" runat="server">
                                </select>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <strong>Taluk</strong>
                                                                       <select class="form-control formcontrolheight" id="selPLTaluk" runat="server">
                                </select>
                                                                        </div>
                                                                         <div class="col-sm-4">
                                                                            <strong>Pin Code</strong>
                                                                      <input type="text" class="form-control formcontrolheight" runat="server" id="txtPLPinCode" placeholder="Pin Code">
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>State</strong></td>
                                                                                <td><strong>Taluk</strong></td>   
                                                                                     <td><strong>Pin Code</strong></td>   
                                                                                    
                                                                                                     </tr>
                                                                                <tr ng-repeat="item in connsList ">
                                                                                    <td>{{PLStateId}}</td>
                                                                                   <td>{{PLTaluk}}</td>
                                                                                     <td>{{PLVillageDetail}}</td>
                                                                                 
                                                                                   
                                                                                 
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>--%>
                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                Primary Location (For Fixed Bank)<span>&nbsp;&nbsp;&nbsp;<strong>SSA</strong><label for=""></label></span>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                   
                                                                    <br/>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr>
                                                                                <td style="display:none"><strong>SSA</strong></td>
                                                                                <td><strong>State</strong></td>
                                                                                <td><strong>District</strong></td> 
                                                                                <td><strong>Sub District</strong></td>
                                                                                <td><strong> Villages Code</strong></td>                                                                             
                                                                              
                                                                                </tr>
                                                                                <tr ng-repeat="item in ssaList">
                                                                                     <td style="display:none">{{item.SSA}}</td>
                                                                                    <td>{{item.StateName}}</td>
                                                                                     <td>{{item.DistrictName}}</td>
                                                                                    <td>{{item.SubDistricttName}}</td>
                                                                                     <td>{{item.Village}}</td>
                                                                                   
                                                                                    <td>
                                                                                        
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>

                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                               Other Working Area
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row" style="display:none"> 


                                                                        <div class="col-sm-6">
                                                                            <strong>Village Code</strong>
                                                                          <input runat="server" ng-model="txtOperationVillageCode" type="text" class="form-control formcontrolheight" id="txtOperationVillageCode" placeholder="Village Code">
                                                                        </div>


                                                                        <div class="col-sm-6">
                                                                            <strong>Village Detail</strong>
                                                                          <textarea runat="server" class="form-control" id="txtOperationVillageDetail" placeholder="Village Detail" />
                                                                        </div>


                                       
                                                                        
                                                                    </div>
                                                                    <div class="row pull-right" id="Div2" style="display:none">
                                                                        <div class="col-sm-12">
                                                                            <input type="button"
                                                                                   class="btn btn-primary  btn-sm form-control"
                                                                                   ng-click="AddMoreOpAreas()"
                                                                                   ng-disabled="!txtOperationVillageCode "
                                                                                   value="Add More  " id="Button2">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                    <br/>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Village Code</strong></td>
                                                                                <td><strong>Village Detail</strong></td>                                                                              
                                                                            <%--    <td><strong>Remove</strong></td>--%>
                                                                                </tr>
                                                                                <tr ng-repeat="item in areasList">
                                                                                    <td>{{item.VillageCode}}</td>
                                                                                     <td>{{item.VillageDetail}}</td>
                                                                                   
                                                                                   
                                                                                  <%--  <td>
                                                                                        <div ng-click="removeOpAreas($index)"
                                                                                             class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                        </div>
                                                                                    </td>--%>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>
                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>

                                    </div>
                                      <div role="tabpanel" class="tab-pane" ng-hide="divmessage">
                                        <section class="section">
                                            <div class="container-fluid">

                                                <div class="row">

                                                    <div class="col-md-12">


                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                               Device Details
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row" style="display:none">


                                                                        <div class="col-sm-6">
                                                                            <strong>Given On</strong>
                                                                         <%-- <input runat="server" type="date" ng-model="txtGivenOn" class="form-control formcontrolheight" id="txtGivenOn" placeholder="dd-mm-yyyy">--%>
                                                                              <input type='text' class="form-control" id="txtGivenOn"  placeholder="dd/mm/yyyy" autocomplete="off" />
                                                                        </div>


                                                                        <div class="col-sm-6">
                                                                            <strong>Device Name</strong>
                                                                          <select runat="server" ng-model="selDeviceName" class="form-control formcontrolheight" id="selDeviceName">
                                    <option value="">--Select-- </option>
                                                                               <option value="POS">POS</option>
                                    <option value="Micro ATM">Micro ATM</option>
                                    <option value="USSD Phones">USSD Phones</option>
                                    <option value="KIOSK">KIOSK</option>
                                    <option value="VSAT">VSAT</option>
                                </select>
                                                                        </div>


                                       
                                                                        
                                                                    </div>
                                                                    <div class="row pull-right" id="Div2" style="display:none">
                                                                        <div class="col-sm-12">
                                                                            <input type="button"
                                                                                   class="btn btn-primary  btn-sm form-control"
                                                                                   ng-click="AddMoreDevices()"
                                                                                   ng-disabled="!selDeviceName "
                                                                                   value="Add More  " id="Button2">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                    <br/>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Given On</strong></td>
                                                                                <td><strong>Device Name</strong></td>  
                                                                                     <td><strong>Device Code</strong></td>                                                                               
                                                                            <%--    <td><strong>Remove</strong></td>--%>
                                                                                </tr>
                                                                                <tr ng-repeat="item in devicesList ">
                                                                                    <td>{{item.GivenOn}}</td>
                                                                                    <td>{{item.Device}}</td>     
                                                                                      <td>{{item.DeviceCode}}</td>                                                                                
                                                                                   
                                                                                 <%--   <td>
                                                                                        <div ng-click="removeDevices($index)"
                                                                                             class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                        </div>
                                                                                    </td>--%>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>
                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                               Connectivity Details
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row" style="display:none">


                                                                        <div class="col-sm-4">
                                                                            <strong>Connectivity Type</strong>
                                                                              <select runat="server" ng-model="selConnType" class="form-control formcontrolheight" id="selConnType">
                                  <option value="">--Select-- </option>
                                    <option value="LandLine">LandLine</option>
                                    <option value="Mobile">Mobile</option>
                                    <option value="USAT">USAT</option>

                                </select>
                                                                        </div>


                                                                        <div class="col-sm-4">
                                                                            <strong>Provider</strong>
                                                                            <input runat="server" type="text" class="form-control formcontrolheight" id="txtProvider" placeholder="Provider">
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <strong>Number</strong>
                                                                           <input runat="server" type="text" class="form-control formcontrolheight" id="txtNumber" placeholder="Number">
                                                                        </div>


                                       
                                                                        
                                                                    </div>
                                                                    <div class="row pull-right" id="Div2" style="display:none">
                                                                        <div class="col-sm-12">
                                                                            <input type="button"
                                                                                   class="btn btn-primary  btn-sm form-control"
                                                                                   ng-click="AddMoreConnectivity()"
                                                                                   ng-disabled="!selConnType "
                                                                                   value="Add More  " id="Button2">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                    <br/>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Connectivity Type</strong></td>
                                                                                <td><strong>Provider</strong></td>    
                                                                                      <td><strong>Number</strong></td>                                                                               
                                                                               <%-- <td><strong>Remove</strong></td>--%>
                                                                                </tr>
                                                                                <tr ng-repeat="item in connsList ">
                                                                                    <td>{{item.ConnectivityMode}}</td>
                                                                                   <td>{{item.ConnectivityProvider}}</td>
                                                                                    <td>{{item.ContactNumber}}</td>
                                                                                  <%--  <td>
                                                                                        <div ng-click="removeConnectivity($index)"
                                                                                             class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></span>&nbsp;Remove
                                                                                        </div>
                                                                                    </td>--%>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>
                                                        <div class="panel">
                                                            <div class="panel-heading">
                                                                <div class="panel-title">
                                                                Products/Services Offered
                                                                </div>
                                                            </div>
                                                            <div class="panel-body p-20">

                                                                <div class="panel-body">
                                                                    <div class="row" style="display:none">


                                                                        <div class="col-sm-3">
                                                                            <strong>Product Group<br /></strong>
                                                                          <select runat="server" class="form-control formcontrolheight" id="selProductsOffered">
                                </select>
                                                                        </div>


                                                                        <div class="col-sm-3">
                                                                            <strong>Minimum Cash Handling Limit</strong>
                                                                         <input runat="server" type="text" required class="form-control formcontrolheight" id="txtMinCash" placeholder="Minimum Cash Handling Limit">
                                                                        </div>
                                                                        
                                                                        <div class="col-sm-3">
                                                                            <strong>Commission 1</strong>
                                                                       <input runat="server" type="text" required class="form-control formcontrolheight" id="txtMonthlyFixed" placeholder="Commission 1">
                                                                        </div>
                                                                        
                                                                        <div class="col-sm-3">
                                                                            <strong>Commission 2</strong>
                                                                        <input runat="server" type="text" required class="form-control formcontrolheight" id="txtMonthlyVariable" placeholder="Commission 2">
                                                                        </div>


                                       
                                                                        
                                                                    </div>
                                                           <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <table id="" class="table ">
                                                                                <tr
                                                                                ">
                                                                                <td><strong>Product Offered</strong></td>
                                                                                <td><strong>Minimum Cash Handling Limit</strong></td>    
                                                                                      <td><strong>Commission 1</strong></td>                                                                               
                                                                                <td><strong>Commission 2</strong></td>
                                                                                </tr>
                                                                                <tr ng-repeat="item in connsList ">
                                                                                    <td>{{ProductsOffered}}</td>
                                                                                   <td>{{MinimumCashHandlingLimit}}</td>
                                                                                    <td>{{MonthlyFixedRenumeration}}</td>
                                                                                      <td>{{MonthlyVariableRenumeration}}</td>
                                                                                 
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <!-- /.col-md-12 -->
                                                            </div>
                                                        </div>
                                                        <!-- /.panel -->
                                                    </div>
                                                    <!-- /.col-md-6 -->


                                                    <!-- /.col-md-8 -->
                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.container-fluid -->
                                        </section>

                                    </div>
                                           <div class="formmargin center-block">
                    <div class="center-block">
                      <%--  <button type="button" ng-click="addSingleEntry()" class="btn btn-success center-block">Update</button>--%>
                    </div>
                </div>
                                    
                             


                                <!-- /.src-code -->

                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    </div>
    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
<%--    <script src="js/metro.min.js"></script>--%>
    <script src="js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            var t = $('#example').DataTable({
                "paging": false,
                "ordering": false,
                "info": false,
                "searching": false
            });


            $("#villageCode").on('blur', function () {
                $("#villageDetail").text('village 1, district 1, state');
            }).on('focus', function () {
                $("#villageDetail").text('');
            });


            $('#addMoreButton').on('click', function () {
                t.row.add([
                    $("#villageCode").val(),
                    $("#villageDetail").text(),
                    'delete'
                ]).draw(false);
                $("#villageCode").val("");
                $("#villageDetail").text("");
            });

            $("#ifscCode").on('blur', function () {
                $("#bankName").val("1");
                $("#bankCircle").val("1");
                $("#bankState").val("2");
                $("#bankZone").val("3");
                $("#bankRegion").val("4");
                $("#bankCategory").val("5");
                $("#bankBranch").val("5");
            }).on('focus', function () {
                $("#bankName").val("0");
                $("#bankCircle").val("0");
                $("#bankState").val("0");
                $("#bankZone").val("0");
                $("#bankRegion").val("0");
                $("#bankCategory").val("0");
                $("#bankBranch").val("0");
            });
        });
    </script>
    <!--<script src="js/report.js" type="text/javascript"></script>-->
    <script src="js/bc.js" type="text/javascript"></script>
    <script src="js/main.js" type="text/javascript"></script>
</asp:Content>
