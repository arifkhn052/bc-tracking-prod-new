﻿'use strict';

bctrackApp.service('authenticationService', [
    '$http',
    '$q',
    '$cookies',
    function ($http, $q, $cookies) {
        var saveUserDataInCookie = function (userId, TokenId) {
            $cookies.put('logged', 1, { path: '/' });
            $cookies.put('userId', userId, { path: '/' });
            $cookies.put('TokenId', TokenId, { path: '/' });

        };

        var logoutUserInCookie = function () {
            $cookies.put('logged', { path: '/' });
            $cookies.put('userId', { path: '/' });
            $cookies.put('TokenId', { path: '/' });
            $cookies.put('Role', { path: '/' });
            $cookies.put('BankId', { path: '/' });
           

        };
        return {
            authenticateUser: function (user) {
                debugger;
                var deferred = $q.defer();
                $http.post("api/auth.ashx", { userId: user.userId, password: user.password })
               .then(function (response) {
                   console.log(response);
                   var dataReceived = response.data;
                   if (dataReceived.hasOwnProperty("TokenId"))
                   {
                       saveUserDataInCookie(dataReceived.userId,dataReceived.TokenId);
                   }
                   
                   deferred.resolve(dataReceived);

               });
                return deferred.promise;
            },

            getLoggedInUser: function () {
             
                var logged = $cookies.get('logged') || 0;
                if (logged == 1) {
                    var obj = {
                        'userId': $cookies.get('userId'),
                        'TokenId': $cookies.get('TokenId'),

                    };
                    return angular.copy(obj);
                } else {
                    return false;
                }
            },
            logout: function () {
                var defer = $q.defer();
                var logged = $cookies.get('logged') || 0;
                if (logged == 1) {
                    $http.post('api/LogoutHandler.ashx', {
                        'userId': $cookies.get('userId'),
                        'TokenId': $cookies.get('TokenId'),
                    }).then(function () {
                        logoutUserInCookie();
                        defer.resolve();
                    }, function () {
                        logoutUserInCookie();
                        defer.resolve();
                    });
                } else {
                    defer.resolve();
                }
                return defer.promise;
            },
        }
    }
]);
