﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="BCListAll.aspx.cs" Inherits="BCTrackingWeb.BCListAll" %>
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-6">
                    <h2 class="title">List All</h2>

                </div>
                <!-- /.col-md-6 -->
              
                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="BCListAll.aspx">  List All</a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">


                    <!-- /.col-md-6 -->

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">

                                </div>
                            </div>
                            <div class="panel-body p-20">

                                 <table id="tblBCList" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Aadhar Card</th>
                    <th>Certificates</th>
                    <th>Villages</th>
                    <th>Action</th>
        
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>

                                
                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-8 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

    
   
   <%-- <br/><br /><br /><br /><br />

    <div class="container page-content">

            
        <div class="loader"></div>

        <div>
            <ul class="breadcrumbs mini">
                <li><a href="Home.aspx"><span class="icon mif-home"></span></a></li>
                <li><a href="bcListIndex.aspx"><span>Business Correspondent List</span></a></li>
                <li><a href="BCListAll.aspx"><span>List All</span></a></li>

            </ul>
        </div>
        <br>
        <div class="">
            <button class="btn btn-md btn-primary">Export</button>
        </div>
        <table id="tblBCList" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Aadhar Card</th>
                    <th>Certificates</th>
                    <th>Villages</th>
                    <th>Action</th>
        
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>


        <br>
    </div>--%>

