﻿$(document).ready(function () {

    var fullurl = $(location).attr('href');

    var b = fullurl.match(/bankId=([^&]+)/);
    var bankId = 0;
    if (b != null)
        bankId = b[1];

    $('form').parsley();
    
    var defaultOption = '--Select--';
    var defaultOptionValue = -1;


    $('select').each(function()
    {
        //alert($(this).attr("id"));
        $(this).prepend($('<option>', {
            value: -1,
            text: defaultOption
        }));
    });       

    var circles = [];
    var t2 = $('#tblCircles').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "searching": true
    });

    var zones = [];
    var t3 = $('#tblZones').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "searching": true
    });


    var regions = [];
    var t4 = $('#tblRegions').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "searching": true
    });


    var branches = [];
    var t5 = $('#tblBranches').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "searching": true
    });


  
    //if (bankId != 0) {
    //    $('.loader').show();
    //    $.ajax({
    //        type: 'POST',
    //        dataType: 'json',
    //        contentType: 'application/json',
    //        url: "GetBCHandler.ashx?mode=Bank&bankId=" + bankId,
    //        async: false,
    //        success: function (bank) {
    //            $('#txtName').val(bank.BankName);
    //            $('#txtAddress').val(bank.Address);
    //            $('#txtContact').val(bank.ContactNumber);
    //            $('#txtEmail').val(bank.Email);

    //            if (bank.BankCircles != null) {
    //                for (var c = 0; c < bank.BankCircles.length; c++) {
    //                    var circle = bank.BankCircles[c];
    //                    circles.push(circle);
    //                    t2.row.add([circle.CircleName,
    //                  '<td><button id="btnDeleteCircle" circle=' + circle.CircleName + ' class="btn btn-danger">Delete</button></td>'
    //                    ]).draw(false);

    //                    if (circle.CircleZones != null) {
    //                        for (var z = 0; z < circle.CircleZones.length; z++) {
    //                            var zone = circle.CircleZones[z];
    //                            zones.push(zone);
    //                            t3.row.add([zone.ZoneName, circle.CircleName,
    //                           '<td><button id="btnDeleteZone" zone=' + zone.ZoneName + ' class="btn btn-danger">Delete</button></td>'
    //                            ]).draw(false);

    //                            if (zone.ZoneRegions != null) {
    //                                for (var r = 0; r < zone.ZoneRegions.length; r++) {
    //                                    var region = zone.ZoneRegions[r];
    //                                    regions.push(region);
    //                                    t4.row.add([region.RegionName, zone.ZoneName,
    //                                    '<td><button id="btnDeleteRegion" region=' + region.RegionName + ' class="btn btn-danger">Delete</button></td>'
    //                                    ]).draw(false);


    //                                    if (region.RegionBranches != null) {
    //                                        for (var b = 0; b < region.RegionBranches.length; b++) {
    //                                            var branch = region.RegionBranches[b];
    //                                            branches.push(branch);
    //                                            AddBranch(branch, region.RegionName);
    //                                        }

    //                                    }
    //                                }

    //                            } // if (zone.ZoneRegions
    //                        }
    //                    }
    //                }

    //            }

    //            PopulateCircles();
    //            PopulateZones();
    //            PopulateRegions();


    //        }
    //    });
    //}



    function GetBanks() {

        var requrl = "GetBCHandler.ashx?mode=Bank";

        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            async: false,
            success: function (Banks) {
                //console('Got it!');
                
                for (var cnt = 0; cnt < Banks.length; cnt++) {

                    $('#selBank').append($('<option>', {
                        value: Banks[cnt].BankId,
                        text: Banks[cnt].BankName
                    }));

                }

                $('.loader').hide();

            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }

        });

    }


    if ($('#tblBankList').length > 0) {

        var t1 = $('#tblBankList').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": true
        });


        var mode = "Bank";
        if (bankId != '')
            mode += "One";



        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: "GetBCHandler.ashx?mode=" + mode + "&bankId=" + bankId,
            async: false,
            success: function (Banks) {

                for (var cnt = 0; cnt < Banks.length; cnt++) {
                    var bank = Banks[cnt];

                    t1.row.add([bank.BankId, bank.BankName, bank.Address, bank.ContactNumber + '<br />' + bank.Email,
                        '<td><a href="EditBank.aspx?bankId=' + bank.BankId + '" class="btn btn-success btn-rounded icon-only"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>',
                        '<td><input type="button"  id="deletebank" class="btn btn-danger btn-sm btn-labeled btn-rounded" value="Delete">'

                    ]).draw(false);

                }

                $('.loader').hide();

            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }

        });
    }
  
    $('#deletebank').on('click', function () {
        
    
        var bankId = id;
        //$.ajax({
        //    type: 'POST',
        //    dataType: 'json',
        //    contentType: 'application/json',
        //    url: "DeleteBcHandler.ashx?mode=Bank&bankId=" + bankId,
        //    async: false,
        //    success: function (user) {
        //        window.location.reload();

        //    }
        //});

    });
    function getParameterByName(name) //courtesy Artem
    {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else {
            if ((results[1].indexOf('?')) > 0)
                return decodeURIComponent(results[1].substring(0, results[1].indexOf('?')).replace(/\+/g, " "));
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
   

  

    $('#btnAddMoreCircles').on('click', function () {
        
        var circle = new Object();
        circle.CircleName = $('#txtCircleName').val(); 
        circle.CircleZones = [];
        t2.row.add([circle.CircleName,                     
                     '<td><input type="button" class="btn btn-danger" value="Delete">'
        ]).draw(false);

        circles.push(circle);
        PopulateCircles();
    });
  

    $('#tblCircles').on('click', 'tr', function () {
        
        var row = $(this).find('td:first').text();       
        var index = functiontofindIndexByKeyValue(circles, "CircleName", row);
        var s = index + 1;
        $('#tblCircles tr:eq(' + s + ')').remove();
         
        circles.splice(0, index);

    });

    function functiontofindIndexByKeyValue(arraytosearch, key, valuetosearch) {

        for (var i = 0; i < arraytosearch.length; i++) {

            if (arraytosearch[i][key] == valuetosearch) {
                return i;
            }
        }
        return null;
    }
    
    $('#btnAddMoreZones').on('click', function () {
        
        var zone = new Object();
        zone.ZoneName = $('#txtZoneName').val();
        zone.CircleId = $('#selZoneCircle').val();
        zone.ZoneRegions = [];

        var circleName = $("#selZoneCircle option:selected").text();

        t3.row.add([zone.ZoneName, circleName,
                    '<td><button id="btnDeleteZone" zone=' + zone.ZoneName + ' class="btn btn-danger">Delete</button></td>'
        ]).draw(false);

        zones.push(zone);
        PopulateZones();

        //Add to relevant Cirlce
        for(var c=0; c<circles.length; c++)
        {
            var circ = circles[c];
            if (circ.CircleName == circleName)
            {
                circ.CircleZones.push(zone);

            }
               
        }


    });


   
    $('#btnAddMoreRegions').on('click', function () {
        var region = new Object(); Populatezone
        region.RegionName = $('#txtRegionName').val();
        region.ZoneId = $("#selRegionZone").val();
        region.RegionBranches = [];
        var zoneName = $("#selRegionZone option:selected").text();


        regions.push(region);
        PopulateRegions();

        for (var z = 0; z < zones.length; z++) {
            var zon = zones[z];
            if (zon.ZoneName == zoneName) {
                zon.ZoneRegions.push(region);

            }

        }

    });


   

    $('#btnAddMoreBranches').on('click', function () {
        var branch = new Object();
        branch.BranchName = $('#txtBranchName').val();
       // branch.Region = $('#selBranchRegion').val();
        branch.Address = $('#txtBranchAddress').val();
        branch.RegionId = $('#selBranchRegion').val();
        branch.StateId = $('#selBranchState').val();
        branch.DistrictId = $('#selBranchDistrict').val();
        branch.TalukaId = $('#selBranchTaluka').val();
        branch.CityId = $('#selBranchCity').val();
        branch.VillageId = $('#selBranchVillage').val();
        branch.PinCode = $('#txtBranchPinCode').val();
        branch.ContactNumber = $('#txtBranchContact').val();
        branch.Email = $('#txtBranchEmail').val();
        var RegionName = $("#selBranchRegion option:selected").text();

        AddBranch(branch, RegionName);

        branches.push(branch);


        for (var r = 0; r < regions.length; r++) {
            var reg = regions[r];
            if (reg.RegionName == RegionName) {
                reg.RegionBranches.push(branch);

            }

        }

    });



    function PopulateCircles()
    {
        
        $('#selZoneCircle').empty();
        for(var c=0;c<circles.length;c++)
        {
              $('#selZoneCircle').append($('<option>', {
                  value: 0,
                  text: circles[c].CircleName
            }));
        }
    }


    function PopulateZones() {
        $('#selRegionZone').empty();
        for (var c = 0; c < zones.length; c++) {
            $('#selRegionZone').append($('<option>', {
                value: 0,
                text: zones[c].ZoneName
            }));


        }
    }

    function PopulateRegions() {
        $('#selBranchRegion').empty();
        for (var c = 0; c < regions.length; c++) {
            $('#selBranchRegion').append($('<option>', {
                value: 0,
                text: regions[c].RegionName
            }));


        }
    }

   
    function AddBranch(branch, RegionName)
    {
        var addr = branch.Address;
        if (branch.DistrictId != '' && branch.DistrictId != defaultOptionValue)
            addr += '<br/>' + $("#selBranchDistrict option:selected").text();
        if (branch.CityId != '' && branch.CityId!=defaultOptionValue)
            addr += '<br/>' + $("#selBranchCity option:selected").text();
        else if (branch.VillageId != '' && branch.VillageId != defaultOptionValue)
            addr += '<br/>' + $("#selBranchVillage option:selected").text();
        addr += '<br/>' + branch.PinCode;


        t5.row.add([branch.BranchName, RegionName, addr, branch.ContactNumber, branch.Email,
                   '<td><button id="btnDeleteBranch" branch=' + branch.BranchName + ' class="btn btn-danger">Delete</button></td>'
        ]).draw(false);


    }



    $('#btnSubmitBank').on('click', function () {
        
        var bank = new Object();
        bank.BankId = bankId;
        bank.BankName = $('#txtName').val();
        bank.Address = $('#txtAddress').val();
        bank.ContactNumber = $('#txtContact').val();
        bank.Email = $('#txtEmail').val();
        bank.BankCircles = circles;

        $.ajax({
            url: "InsertUpdateBankHandler.ashx",
            method: "POST",
            datatype: "json",
            data: { "bank": JSON.stringify(bank) },
            success: function ()
            {
                alert('Bank Saved!');
            },
            error: function (xmlHttpRequest, errorString, errorMessage) {
                alert(errorMessage);
            }
        });
    });

  


 
});