﻿

bctrackApp.controller("viewSingleEntryController", function ($scope, $state, $timeout, $cookies) {
    
    $scope.divAllocateBtn = true;
    $scope.divAllocate = true;

    $scope.bcid = $state.params.BankCorrespondId
    if($cookies.get("Role")=="1")
    { 

    }
   
    $scope.allBanks = [];
    let allBanks = new Array();
    $scope.GetaBnks = function () {
        var requestUrl = 'api/getBankHandler.ashx?mode=Bank';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            url: requestUrl,
            async: false,
            success: function (bankList) {
          
                if (bankList.length != 0) {
                    for (let i = 0; i < bankList.length; i++) {
                        let bank = new Object();
                        bank.BankId = bankList[i].BankId;
                        bank.BankName = bankList[i].BankName;
                        bank.BankCircles = bankList[i].BankCircles;
                        bank.Branches = bankList[i].Branches;
                        allBanks.push(bank);
                    }
                }
                $scope.allBanks = allBanks
                if (allBanks.length != 0) {
                    $('#selPreviousExpBank').append($('<option>', {
                        value: '-1',
                        text: '--Select--'
                    }));
                    for (let i = 0; i < allBanks.length; i++) {

                        $('#selPreviousExpBank').append($('<option>', {
                            value: allBanks[i].BankId,
                            text: allBanks[i].BankName
                        }));

                    }

                    $('#selAllocationBank').append($('<option>', {
                        value: '-1',
                        text: '--Select--'
                    }));
                    for (let i = 0; i < allBanks.length; i++) {

                        $('#selAllocationBank').append($('<option>', {
                            value: allBanks[i].BankId,
                            text: allBanks[i].BankName
                        }));

                    }

                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allBanks;
    }

    $('#selPreviousExpBank').on('change', function () {
     
        $('#selPreviousExpBranch').empty();
        $('#selPreviousExpBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        var selectedBankId = $('#selPreviousExpBank :selected').val();
        if (selectedBankId != 0) {
            let bank = getBank(allBanks, selectedBankId);
            let bankCircles = null, bankBranches = null;
            if (bank.BankCircles != null)
                bankCircles = bank.BankCircles;
            if (bank.Branches != null)
                bankBranches = bank.Branches;
            if (bankCircles != null) {
                for (let i = 0; i < bankBranches.length; i++) {
                    $('#selPreviousExpBranch').append($('<option>', { value: bankBranches[i].BranchId, text: bankBranches[i].BranchName }));
                }
            }
        }
    });
    $('#selAllocationBank').on('change', function () {
    
        $('#selAllocationBranch').empty();
        $('#selAllocationBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        var selectedBankId = $('#selAllocationBank :selected').val();
        if (selectedBankId != 0) {
            let bank = getBank(allBanks, selectedBankId);
            let bankCircles = null, bankBranches = null;
            if (bank.BankCircles != null)
                bankCircles = bank.BankCircles;
            if (bank.Branches != null)
                bankBranches = bank.Branches;
            if (bankCircles != null) {
                for (let i = 0; i < bankBranches.length; i++) {
                    $('#selAllocationBranch').append($('<option>', { value: bankBranches[i].BranchId, text: bankBranches[i].BranchName }));
                }
            }
        }
    });
    function getBank(bankList, bankId) {

        let bank = new Object();
        for (let i = 0; i < bankList.length; i++) {
            if (bankList[i].BankId == bankId) {
                bank = bankList[i];
                break;
            }
        }
        return bank;
    }

    $scope.getProduct = function () {

        let corporates = new Array();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: 'api/GetBCHandler.ashx?mode=userProductS',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (product) {

                if (product.length != 0) {
                    $('#selProductsOffered').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < product.length; i++) {
                        $('#selProductsOffered').append($('<option>', {
                            value: product[i].productId,
                            text: product[i].productName
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('Failed. ' + errorMessage);
            }
        });
        return corporates;
    }
    $scope.datetimeBind = function () {
        //tabing();
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        var firstDay = '01/' + mm + '/' + yyyy;
        $("#txtDOB").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtDOB").val(firstDay);

        $("#txtPassingDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtPassingDate").val(today);
        $scope.txtPassingDate = today;
        $("#txtFromDateExp").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtFromDateExp").val(today);

        $("#txtToDateExp").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtToDateExp").val(today);

        $("#txtAppointmentDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtAppointmentDate").val(today);

        $("#txtGivenOn").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtGivenOn").val(today);

        $("#txtDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtDate").val(today);



    }
    $scope.deleteBc = function () {

        if (confirm("Do you want to delete?")) {

            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                url: "api/DeleteBcHandler.ashx?mode=BANKc&bcId=" + $state.params.BankCorrespondId,
                async: false,
                success: function () {


                },
                error: function (xmlHttpRequest, errorString, errorMessage) {

                    window.location.href = "#/all";

                    alert('BC deleted successfully.');
                }
            });
        }
        return false;
    }


    $scope.divhome = true;
    $scope.divcircle = true;
    $scope.divprofile = true;
    $scope.divsetting = true;
    $scope.divmessage = true;

    $scope.displayTab = function (fn) {
        if (fn == "home") {

            $scope.divhome = false;
            $scope.divcircle = true;
            $scope.divprofile = true;
            $scope.divsetting = true;
            $scope.divmessage = true;
        }
        if (fn == "circle") {

            $scope.divhome = true;
            $scope.divcircle = false;
            $scope.divprofile = true;
            $scope.divsetting = true;
            $scope.divmessage = true;
        }
        if (fn == "profile") {

            $scope.divhome = true;
            $scope.divcircle = true;
            $scope.divprofile = false;
            $scope.divsetting = true;
            $scope.divmessage = true;
        }
        if (fn == "setting") {

            $scope.divhome = true;
            $scope.divcircle = true;
            $scope.divprofile = true;
            $scope.divsetting = false;
            $scope.divmessage = true;
        }
        if (fn == "message") {

            $scope.divhome = true;
            $scope.divcircle = true;
            $scope.divprofile = true;
            $scope.divsetting = true;
            $scope.divmessage = false;
        }
    }









    $scope.btnedit = 'Edit';
    $scope.divVIEW = false;
    $scope.divEdit = true;
    var states;
    var districts;
    $scope.editView = function () {

        if ($scope.btnedit == "Edit") {
            $scope.btnedit = 'View';
            $scope.divVIEW = true;
            $scope.divEdit = false;
        }
        else {
            $scope.btnedit = 'Edit';
            $scope.divVIEW = false;
            $scope.divEdit = true;
        }

    }

    var b = $state.params.BankCorrespondId;
    var bcId = '';
    if (b != undefined)
        bcId = parseInt(b);


    var baseControlName = '';
    var errorMessage = "";
    var certs = [];
    $scope.certsList = [];
    var exps = [];
    $scope.expsList = [];
    var areas = [];
    $scope.areasList = [];
    var devices = [];
    $scope.devicesList = [];
    var conns = [];
    $scope.connsList = [];
    var bcorrespondene = new Object();

    function getParameterByName(name) //courtesy Artem
    {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else {
            if ((results[1].indexOf('?')) > 0)
                return decodeURIComponent(results[1].substring(0, results[1].indexOf('?')).replace(/\+/g, " "));
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    $scope.AddMoreCerts = function () {
   
        var institueName = '';
        var Course = '';
        var cert = new Object();
        //  cert.DateOfPassing = $('#txtPassingDate').val();
        var from = $("#txtPassingDate").val().split("/");
        newdate = from[2] + "-" + from[1] + "-" + from[0];
        cert.DateOfPassing = newdate
        //   cert.InstituteId = $('#' + baseControlName + 'SelInstitute :selected').text();
        //    cert.CourseId = $('#' + baseControlName + 'SelCourse').val();
        var Institute_Name = $('#' + baseControlName + 'SelInstitute :selected').text();
        if (Institute_Name == "Other") {
            cert.InstituteName = $("#Text1").val();
            institueName = $("#Text1").val();
            $timeout(function () { $scope.divOtherrInstitute = true; }, 100);
        }
        else {
            cert.InstituteName = $('#' + baseControlName + 'SelInstitute :selected').text();
            institueName = $('#' + baseControlName + 'SelInstitute :selected').text();

        }

        var Course_Name = $('#' + baseControlName + 'SelInstitute :selected').text();
        if (Course_Name == "Other") {
            cert.CourseName = $("#Text2").val();
            Course = $("#Text2").val();
            $timeout(function () { $scope.divOtherCourse = true; }, 100);
        }
        else {
            debugger;
            cert.CourseName = $('#' + baseControlName + 'SelCourse :selected').text();
            var Course = $('#' + baseControlName + 'SelCourse :selected').text();

        }



        cert.Grade = $('#' + baseControlName + 'txtGrades').val();
        certs.push(cert);


        $scope.certsList.push({
            "DateOfPassing": cert.DateOfPassing,
            "InstituteName": institueName,
            "CourseName": Course,
            "Grade": cert.Grade

        });
        debugger;
        $('#' + baseControlName + 'txtPassingDate').val("");
        $('#' + baseControlName + 'SelInstitute').val("");
        $('#' + baseControlName + 'SelCourse').val("");
        $('#' + baseControlName + 'txtGrades').val("");
    };
    $scope.divOtherrInstitute = true;
    $scope.divOtherCourse = true;

    $('#SelInstitute').on('change', function () {
   
        var institueName = $('#' + baseControlName + 'SelInstitute :selected').text();
        if (institueName == "Other") {
            $timeout(function () { $scope.divOtherrInstitute = false; }, 100);


        }
        else {
            $timeout(function () { $scope.divOtherrInstitute = true; }, 100);

        }

    });

    $('#SelCourse').on('change', function () {
        debugger;
        var institueName = $('#' + baseControlName + 'SelCourse :selected').text();
        if (institueName == "Other") {
            $timeout(function () { $scope.divOtherCourse = false; }, 100);

        }
        else {
            $timeout(function () { $scope.divOtherCourse = true; }, 100);

        }

    });
    $scope.removeCerts = function (index) {
        certs.splice(index, 1);
        $scope.certsList.splice(index, 1);
    }

    $scope.AddMoreExperience = function () {

        var exp = new Object();
        exp.BankId = $('#' + baseControlName + 'selPreviousExpBank').val();
        exp.Branchid = $('#' + baseControlName + 'selPreviousExpBranch').val();
        // exp.FromDate = $('#' + baseControlName + 'txtFromDateExp').val();
        var frmDate = new Date($('#' + baseControlName + 'txtFromDateExp').val());
        var dtString = frmDate.getFullYear() + '-' + (frmDate.getMonth() + 1) + '-' + frmDate.getDate();
        exp.FromDate = $('#txtFromDateExp').val()
        var to_Date = new Date($('#' + baseControlName + 'txtToDateExp').val());
        var dtToString = to_Date.getFullYear() + '-' + (to_Date.getMonth() + 1) + '-' + to_Date.getDate();
        exp.ToDate = $('#txtToDateExp').val()
        //exp.ToDate = $('#' + baseControlName + 'txtToDateExp').val();
        exp.Reason = $('#' + baseControlName + 'txtReasons').val();
        exps.push(exp);
        $scope.expsList.push({
            "BankId": exp.BankId,
            "Branchid": exp.Branchid,
            "BankName": $('#' + baseControlName + 'selPreviousExpBank :selected').text(),
            "BranchName": $('#' + baseControlName + 'selPreviousExpBranch :selected').text(),
            "FromDate": exp.FromDate,
            "ToDate": exp.ToDate,
            "Reason": exp.Reason

        });
        $('#' + baseControlName + 'selPreviousExpBank').val("");
        $('#' + baseControlName + 'selPreviousExpBranch').val("");
        $('#' + baseControlName + 'txtFromDateExp').val("");
        $('#' + baseControlName + 'txtToDateExp').val("");
        $('#' + baseControlName + 'txtReasons').val("");

    };


    $scope.removeExperience = function (index) {
        exps.splice(index, 1);
        $scope.expsList.splice(index, 1);
    }

    $scope.AddMoreOpAreas = function () {
        var area = new Object();
        area.VillageCode = $('#' + baseControlName + 'txtOperationVillageCode').val();
        area.VillageDetail = $('#' + baseControlName + 'txtOperationVillageDetail').val();
        areas.push(area);
        $scope.areasList.push(area);

        $('#' + baseControlName + 'txtOperationVillageCode').val("");
        $('#' + baseControlName + 'txtOperationVillageDetail').val("");

    };
    $scope.removeOpAreas = function (index) {
        areas.splice(index, 1);
        $scope.areasList.splice(index, 1);
    }

    $scope.AddMoreDevices = function () {
        var device = new Object();
        device.GivenOn = $('#txtGivenOn').val();
        device.Device = $('#' + baseControlName + 'selDeviceName').val();
        devices.push(device);
        $scope.devicesList.push({
            "GivenOn": device.GivenOn,
            "Device": $('#' + baseControlName + 'selDeviceName :selected').text()
        })
        $('#' + baseControlName + 'txtGivenOn').val("");
        $('#' + baseControlName + 'selDeviceName').val("");

    };

    $scope.removeDevices = function (index) {
        devices.splice(index, 1);
        $scope.devicesList.splice(index, 1);
    }

    $scope.AddMoreConnectivity = function (index) {
        var conn = new Object();
        conn.ConnectivityMode = $('#' + baseControlName + 'selConnType').val();
        conn.ConnectivityProvider = $('#' + baseControlName + 'txtProvider').val();
        conn.ContactNumber = $('#' + baseControlName + 'txtNumber').val();
        conns.push(conn);
        $scope.connsList.push(conn);


        $('#' + baseControlName + 'selConnType').val("");
        $('#' + baseControlName + 'txtProvider').val("");
        $('#' + baseControlName + 'txtNumber').val("");

    };
    $scope.removeConnectivity = function (index) {
        conns.splice(index, 1);
        $scope.connsList.splice(index, 1);
    }




    function validate() {

        errorMsg = '';

        var email = $('#txtEmail').val();
        var name = $('#txtName').val();
        var phone = $('#txtPhone1').val();
        var dob = $('#txtDOB').val();
        var MinCash = $('#txtMinCash').val();
        var MonthlyFixed = $('#txtMonthlyFixed').val();
        var MonthlyVariable = $('#txtMonthlyVariable').val();
        var AadharCard = $('#txtAadharCard').val();


        var result = true;
        if (name == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Name.";
            result = false;
        }
        if (AadharCard == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Aadhar Card.";
            result = false;
        }

        if (dob == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select Date of Birth";
            result = false;
        }
        if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
            $('#divPhone').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Valid First Mobile Number.";
            result = false;
        }
        else {
            $("#divPhone").removeClass("form-group has-error");
        }
        if (!IsEmail(email)) {
            $('#txtEmail').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter valid email Id.";
            result = false;
        }
        else {
            $("#divEmail").removeClass("form-group has-error");
        }






        if (MinCash == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Minimum Cash Handling Limit.";
            result = false;
        }
        if (MonthlyFixed == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Monthly Fixed Renumeration.";
            result = false;
        }
        if (MonthlyVariable == "") {
            $('#divName').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Monthly Variable Renumeration.";
            result = false;
        }


        return result;
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    $scope.addSingleEntry = function () {

        if (validate() == true) {
            bcorrespondene.Name = $('#' + baseControlName + 'txtName').val();
            //Handle Duplicate Image Name Issue

            //bcorrespondene.ImagePath = $('#' + baseControlName + 'fileImage.PostedFile.FileName').val();


            //append ts to img name
            //    var fileName = $('#' + baseControlName + 'fileImage').val();
            var d = new Date();
            //    var ts = '_' + d.getDate().toString() + '-' + (d.getMonth() + 1).toString() + '-' + d.getFullYear().toString() + '-' + d.getHours().toString() + '-' + d.getMinutes().toString();
            var fileName = $('#' + baseControlName + 'fileImage').val().replace(/^.*[\\\/]/, '');
            var currFile = $('#' + baseControlName + 'fileImage')[0].files[0];
            var fieldId = $('#' + baseControlName + 'fileImage').attr('id');

            bcorrespondene.ImagePath = fileName;

            bcorrespondene.Gender = $('#' + baseControlName + 'selGender').val();
            var from = $("#txtDOB").val().split("/");
            newdate = from[2] + "-" + from[1] + "-" + from[0];

            bcorrespondene.DOB = newdate;
            bcorrespondene.FatherName = $('#' + baseControlName + 'txtFather').val();
            bcorrespondene.SpouseName = $('#' + baseControlName + 'txtSpouse').val();
            bcorrespondene.Category = $('#' + baseControlName + 'selCategory').val();
            if ($('#' + baseControlName + 'radHandicapNo').checked)
                bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapNo').val()
            else
                bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapYes').val()

            // bcorrespondene.Handicap = $('#' + baseControlName + 'radHandicapNo.Checked ? radHandicapNo).val() : radHandicapYes').val();
            bcorrespondene.PhoneNumber1 = $('#' + baseControlName + 'txtPhone1').val();
            bcorrespondene.PhoneNumber2 = $('#' + baseControlName + 'txtPhone2').val();
            bcorrespondene.PhoneNumber3 = $('#' + baseControlName + 'txtPhone3').val();
            bcorrespondene.Email = $('#' + baseControlName + 'txtEmail').val();
            bcorrespondene.AadharCard = $('#' + baseControlName + 'txtAadharCard').val();
            bcorrespondene.PanCard = $('#' + baseControlName + 'txtPanCard').val();
            bcorrespondene.VoterCard = $('#' + baseControlName + 'txtVoterId').val();
            bcorrespondene.DriverLicense = $('#' + baseControlName + 'txtDriverLic').val();
            bcorrespondene.NregaCard = $('#' + baseControlName + 'txtNregaCard').val();
            bcorrespondene.RationCard = $('#' + baseControlName + 'txtRationCard').val();
            bcorrespondene.State = $('#' + baseControlName + 'selAddressState').val();
            bcorrespondene.City = $('#' + baseControlName + 'selAddressCity').val();
            bcorrespondene.District = $('#' + baseControlName + 'selAddressDistrict').val();
            bcorrespondene.Subdistrict = $('#' + baseControlName + 'txtAddressSubDistrict').val();
            bcorrespondene.Area = $('#' + baseControlName + 'txtAddressArea').val();
            bcorrespondene.PinCode = $('#' + baseControlName + 'txtPinCode').val();
            bcorrespondene.AlternateOccupationType = $('#' + baseControlName + 'SelAlternateOccupation').val();
            bcorrespondene.AlternateOccupationDetail = $('#' + baseControlName + 'txtAlternateOccupationDtl').val();
            bcorrespondene.UniqueIdentificationNumber = $('#' + baseControlName + 'txtUniqueId').val();
            bcorrespondene.BankReferenceNumber = $('#' + baseControlName + 'txtBankReferenceNumber').val();
            bcorrespondene.Qualification = $('#' + baseControlName + 'selQualification').val();

            bcorrespondene.OtherQualification = $('#' + baseControlName + 'txtOtherQualification').val();
            bcorrespondene.CorporateId = $('#' + baseControlName + 'selCorporate').val();


            bcorrespondene.Allocation = $('#' + baseControlName + 'radAllocatedNo').checked ? "No" : "Yes";
            bcorrespondene.isAllocated = (bcorrespondene.Allocation == 'Yes');
            if (bcorrespondene.Allocation == 'Yes') {
                var from = $("#txtAppointmentDate").val().split("/");
                newdates = from[2] + "-" + from[1] + "-" + from[0];

                bcorrespondene.AppointmentDate = newdates;
                bcorrespondene.AllocationIFSCCode = $('#' + baseControlName + 'ifscCode').val();

                bcorrespondene.AllocationBankId = $('#selAllocationBank').val();
                bcorrespondene.AllocationBranchId = $('#selAllocationBranch').val();

                bcorrespondene.BCType = $('#' + baseControlName + 'selBCType').val();
                bcorrespondene.WorkingDays = $('#' + baseControlName + 'txtWorkingDays').val();
                bcorrespondene.WorkingHours = $('#' + baseControlName + 'txtWorkingHours').val();
                bcorrespondene.PLPostalAddress = $('#' + baseControlName + 'txtPostalAddress').val();
                bcorrespondene.PLVillageCode = $('#' + baseControlName + 'txtVillageCode1').val();
                bcorrespondene.PLVillageDetail = $('#' + baseControlName + 'txtVillageDetail1').val();
                bcorrespondene.PLStateId = $('#' + baseControlName + 'selPLState').val();
                //bcorrespondene.PLDistrictId = $('#' + baseControlName + 'selPLDistrict').val();
                bcorrespondene.PLTaluk = $('#' + baseControlName + 'selPLTaluk').val();
                bcorrespondene.PLPinCode = $('#' + baseControlName + 'txtPLPinCode').val();
                bcorrespondene.MinimumCashHandlingLimit = $('#' + baseControlName + 'txtMinCash').val();
                bcorrespondene.MonthlyFixedRenumeration = $('#' + baseControlName + 'txtMonthlyFixed').val();
                bcorrespondene.MonthlyVariableRenumeration = $('#' + baseControlName + 'txtMonthlyVariable').val();
            }



            $.ajax({
                url: "api/InsertUpdateHandler.ashx",
                method: "POST",
                datatype: 'json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                data: {
                    "bcorr": JSON.stringify(bcorrespondene), "allCerts": JSON.stringify(certs), "allExps": JSON.stringify(exps),
                    "allAreas": JSON.stringify(areas), "allDevices": JSON.stringify(devices), "allConns": JSON.stringify(conns)
                },
                success: function (data) {
                    //handle img upload on btn click
                    var fileName = $('#' + baseControlName + 'fileImage').val().replace(/^.*[\\\/]/, '');
                    var currFile = $('#' + baseControlName + 'fileImage')[0].files[0];
                    var fieldId = $('#' + baseControlName + 'fileImage').attr('id');
                    var xhr = new XMLHttpRequest();
                    var uploadData = new FormData();
                    var url = 'FileUploader.ashx';
                    uploadData.append('file', currFile);
                    xhr.open("POST", url, true);
                    xhr.send(uploadData);
                    alert('Data Updated successfully.');
                    window.location.href = "#/all";
                    //  $("#getCodeModal").modal('show');
                    //alert('posted');                
                }
            });

        }

        else {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
    }

    $scope.getBcDetails = function () {

        if (bcId != '') {
            $('.loader').show();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                url: "api/GetBCHandler.ashx?mode=One&bcId=" + bcId,
                async: false,
                success: function (bankc) {
                    //console('Got it!');
                    if (bankc != null) {
                      
                        bcorrespondene = bankc;
                        //Set Controls
                        $scope.ImagePath = bankc.ImagePath;
                        $scope.lblName = bankc.Name;
                        $scope.lblGender = bankc.Gender;
                        $scope.lblDOB = bankc.dateOfBirth;
                        $('#' + baseControlName + 'txtName').val(bankc.Name);
                        $('#' + baseControlName + 'selGender').val(bankc.Gender);

                        var dt = new Date(bankc.DOB);
                        //       var dtString = dt.toDateString("YYYY-MM-DD");
                        var dtString = dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate();

                        $('#txtDOB').val(bankc.dateOfBirth);

                        $scope.lblFather = bankc.FatherName;
                        $('#' + baseControlName + 'txtFather').val(bankc.FatherName);
                        $scope.lblSpouse = bankc.SpouseName;
                        $('#' + baseControlName + 'txtSpouse').val(bankc.SpouseName);
                        $('#' + baseControlName + 'selCategory').val(bankc.Category);
                        $scope.lblCategory = bankc.Category;
                        if (bankc.Handicap == 'Yes') {
                            $scope.lblhand = "Yes";
                            $('#' + baseControlName + 'radHandicapYes').checked = true;
                        }
                        else {
                            $scope.lblhand = "No";
                            $('#' + baseControlName + 'radHandicapNo').checked = true;
                        }
                        //$scope.lblcontactperoson = bankc.ContactPerson;
                        //$scope.lblcontactdesignaton = bankc.ContactDesignation;
                        $scope.lblNoofComplaint = bankc.NoofComplaint;

                        //$('#' + baseControlName + 'txtContactPerson').val(bankc.ContactPerson);
                        //$('#' + baseControlName + 'txtContactDesignation').val(bankc.ContactDesignation);
                        $('#' + baseControlName + 'txtNoofComplaint').val(bankc.NoofComplaint);

                        $scope.lblPhone1 = bankc.PhoneNumber1;
                        $scope.lblPhone2 = bankc.PhoneNumber2;
                        $scope.lblPhone3 = bankc.PhoneNumber3;
                        $scope.lblEmail = bankc.Email;
                        $('#' + baseControlName + 'txtPhone1').val(bankc.PhoneNumber1);
                        $('#' + baseControlName + 'txtPhone2').val(bankc.PhoneNumber2);
                        $('#' + baseControlName + 'txtPhone3').val(bankc.PhoneNumber3);
                        $('#' + baseControlName + 'txtEmail').val(bankc.Email);
                        $scope.AadharCard = bankc.AadharCard;
                     
                        $('#' + baseControlName + 'txtAadharCard').val(bankc.AadharCard);
                        $('#' + baseControlName + 'txtPanCard').val(bankc.PanCard);
                        $scope.PanCard = bankc.PanCard;
                        $('#' + baseControlName + 'txtVoterId').val(bankc.VoterCard);
                        $scope.VoterCard = bankc.VoterCard;
                        $('#' + baseControlName + 'txtDriverLic').val(bankc.DriverLicense);
                        $scope.DriverLicense = bankc.DriverLicense;
                        $('#' + baseControlName + 'txtNregaCard').val(bankc.NregaCard);
                        $scope.NregaCard = bankc.NregaCard;
                        $('#' + baseControlName + 'txtRationCard').val(bankc.RationCard);
                        $scope.RationCard = bankc.RationCard;
                        $('#' + baseControlName + 'selAddressState').val(bankc.State);
                       $scope.GetStates(bankc.State);
                       $scope.GetDistrict(bankc.State, bankc.District);
                       $scope.GetSubDistrict(bankc.District, bankc.Subdistrict);
                       $scope.GetVillages(bankc.Subdistrict, bankc.Village);
                        
                        $('#' + baseControlName + 'selAddressCity').val(bankc.City);
                       // $scope.City = bankc.City;
                        $('#' + baseControlName + 'selAddressDistrict').val(bankc.District);


                        //$scope.District = bankc.District;
                        //$scope.GetSubDistricts(bankc.District);
                        $('#' + baseControlName + 'txtAddressSubDistrict').val(bankc.Subdistrict);
                       
                        // $scope.Subdistrict GetSubDistrict = bankc.Subdistrict;
                        $('#' + baseControlName + 'txtAddressArea').val(bankc.Area);
                        $scope.Area = bankc.Area;
                        $('#' + baseControlName + 'txtPinCode').val(bankc.PinCode);
                        $scope.PinCode = bankc.PinCode;
                        $('#' + baseControlName + 'SelAlternateOccupation').val(bankc.AlternateOccupationType);
                        $scope.AlternateOccupationType = bankc.AlternateOccupationType;
                        $('#' + baseControlName + 'txtAlternateOccupationDtl').val(bankc.AlternateOccupationDetail);
                        $scope.AlternateOccupationDetail = bankc.AlternateOccupationDetail;
                        $('#' + baseControlName + 'txtUniqueId').val(bankc.UniqueIdentificationNumber);
                        $scope.UniqueIdentificationNumber = bankc.UniqueIdentificationNumber;
                        $('#' + baseControlName + 'txtBankReferenceNumber').val(bankc.BankReferenceNumber);
                        $scope.BankReferenceNumber = bankc.BankReferenceNumber;
                        $('#' + baseControlName + 'selQualification').val(bankc.Qualification);
                        $scope.Qualification = bankc.Qualification;
                        $('#' + baseControlName + 'txtOtherQualification').val(bankc.OtherQualification);
                        $scope.OtherQualification = bankc.OtherQualification;
                        $('#' + baseControlName + 'selCorporate').val(bankc.CorporateId);
                        $scope.CorporateId = bankc.CorporateId;
                        if (bankc.isAllocated)
                        {
                            $scope.isAllocated = bankc.isAllocated;
                            $('#radAllocatedYes').prop('checked', true);
                        }
                          
                        else {
                            $scope.isAllocated = bankc.isAllocated;
                            $('#radAllocatedNo').prop('checked', true);
                            $('#divAllocationDetails').hide();
                        }

                        if (bankc.AppointmentDate != null && bankc.AppointmentDate != undefined) {
                            var dt2 = new Date(bankc.AppointmentDate);
                            var dt2String = dt2.getFullYear() + '-' + (dt2.getMonth() + 1) + '-' + dt2.getDate();
                            $scope.AppointmentDate = dt2String;
                            $('#' + baseControlName + 'txtAppointmentDate').val(dtString);
                        }


                        $('#' + baseControlName + 'ifscCode').val(bankc.AllocationIFSCCode);
                        $scope.AllocationIFSCCode = bankc.AllocationIFSCCode;
                        $('#selAllocationBank').val(bankc.AllocationBankId);
                        $scope.AllocationBankId = bankc.AllocationBankId;

                         
                        var bankList =  $scope.allBanks.filter(function (x) {
                            return x.BankId === $scope.AllocationBankId
                        });

                        if( $scope.AllocationBankId==0)
                        {
                            $timeout(function () { $scope.divAllocate = true; }, 100);
                            $timeout(function () { $scope.divAllocateBtn = false; }, 100);
                            $scope.bankMessage = "Not Allocated ";
                            $scope.bankMessage1 = "Not Allocated";
                        }
                        else
                        {
                            
                            $timeout(function () { $scope.divAllocate = false; }, 100);
                            $timeout(function () { $scope.divAllocateBtn = true; }, 100);
                          //  $scope.bankMessage = " Allocated To :- " + bankList[0].BankName + "Bank";
                          //  $scope.AllocationBankId = bankList[0].BankName;
                          
                        }
                     
                       
                        $scope.AllocationBranchId = bankc.AllocationBranchId;
                        $('#selAllocationBranch').val(bankc.AllocationBranchId);

                        $('#' + baseControlName + 'selBCType').val(bankc.BCType);
                        $scope.BCType = bankc.BCType;
                        $('#' + baseControlName + 'txtWorkingDays').val(bankc.WorkingDays);
                        $scope.WorkingDays = bankc.WorkingDays;
                        $('#' + baseControlName + 'txtWorkingHours').val(bankc.WorkingHours);
                        $scope.WorkingHours = bankc.WorkingHours;
                        $('#' + baseControlName + 'txtPostalAddress').val(bankc.PLPostalAddress);
                        $scope.PLPostalAddress = bankc.PLPostalAddress;
                        $('#' + baseControlName + 'txtVillageCode1').val(bankc.PLVillageCode);
                        $scope.PLVillageCode = bankc.PLVillageCode;
                        $('#' + baseControlName + 'txtVillageDetail1').val(bankc.PLVillageDetail);
                        $scope.PLVillageDetail = bankc.PLVillageDetail;
                        $('#' + baseControlName + 'selPLState').val(bankc.PLStateId);
                        $scope.PLStateId = bankc.PLStateId;
                        //$('#' + baseControlName + 'selPLDistrict').val(bankc.PLDistrictId);
                        $('#' + baseControlName + 'selPLTaluk').val(bankc.PLTaluk);
                        $scope.PLTaluk = bankc.PLTaluk;
                        $('#' + baseControlName + 'txtPLPinCode').val(bankc.PLPinCode);
                        $('#' + baseControlName + 'selProductsOffered').val(bankc.ProductsOffered);
                        $('#' + baseControlName + 'txtMinCash').val(bankc.MinimumCashHandlingLimit);
                        $('#' + baseControlName + 'txtMonthlyFixed').val(bankc.MonthlyFixedRenumeration);
                        $('#' + baseControlName + 'txtMonthlyVariable').val(bankc.MonthlyVariableRenumeration);

                        $scope.ProductsOffered = bankc.ProductsOffered;
                        $scope.MinimumCashHandlingLimit = bankc.MinimumCashHandlingLimit;
                        $scope.MonthlyFixedRenumeration = bankc.MonthlyFixedRenumeration;
                        $scope.MonthlyVariableRenumeration = bankc.MonthlyVariableRenumeration;


                        $('#lblName').html(bankc.Name);

                        if (bankc.Certifications != null) {
                            $scope.certsList = bankc.Certifications;
                            for (var c = 0; c < bankc.Certifications.length; c++) {
                                var cert = bankc.Certifications[c];
                                certs.push(cert);


                            }
                        }
                        if (bankc.PreviousExperience != null) {
                            for (var c = 0; c < bankc.PreviousExperience.length; c++) {
                                $scope.expsList = bankc.PreviousExperience;
                                var exp = bankc.PreviousExperience[c];
                                exps.push(exp);

                            }

                        }


                        if (bankc.OperationalAreas != null) {
                            $scope.areasList = bankc.OperationalAreas;
                            for (var c = 0; c < bankc.OperationalAreas.length; c++) {
                                var oprarea = bankc.OperationalAreas[c];
                                areas.push(oprarea);
                            }
                        }


                        if (bankc.Devices != null) {
                            $scope.devicesList = bankc.Devices;
                            for (var c = 0; c < bankc.Devices.length; c++) {
                                var device = bankc.Devices[c];
                                devices.push(device);

                            }
                        }


                        if (bankc.ConnectivityDetails != null) {
                            $scope.connsList = bankc.ConnectivityDetails;
                            for (var c = 0; c < bankc.ConnectivityDetails.length; c++) {
                                var conn = bankc.ConnectivityDetails[c];
                                conns.push(conn);

                            }
                        }

                        if (bankc.Products != null) {
                            for (var c = 0; c < bankc.Products.length; c++) {
                                var prod = bankc.AddProducts[c];
                                prods.push(prod);
                                AddProducts(t7, prod.minCashHandlingLimit, prod.MonthlyFixedRenumeration, prod.MonthlyVariableRenumeration);
                                //prods.push(bankc.Products[c]);
                                //AddProducts(t7, bankc.ProductName, bankc.MinimumCashHandlingLimit, bankc.MonthlyFixedRenumeration, bankc.MonthlyVariableRenumeration);
                            }
                        }


                    }

                    $('.loader').hide();

                },
                error: function (err) {
                    alert(err);
                    $('.loader').hide();
                }
            });
        }
    }



    $scope.GetStates = function (id) {

        var allStates = new Array();
        var requestUrl = 'api/state.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        let state = new Object();
                        state.StateId = stateList[i].StateId;
                        state.StateName = stateList[i].StateName;
                        state.StateCode = stateList[i].StateCode;
                        state.StateCircles = stateList[i].StateCircles;
                        state.Branches = stateList[i].Branches;
                        allStates.push(state);
                    }
                }
                states = stateList;
                if (allStates.length != 0) {
                   
                    $scope.State = allStates.filter(function (x) {
                        return x.StateId === parseInt(id)
                    });

                }

            },
            error: function (xhr, errorString, errorMessage) {
                //   alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allStates;
    };
  $scope.GetDistrict= function (stateid,distid) {
        $('#selAddressDistrict').empty();

        var allDistrict = new Array();
        var requestUrl = 'api/Districts.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"stateId": "' + stateid + '"}'
           ,
            async: false,
            success: function (districtList) {
                if (districtList.length != 0) {
                    for (let i = 0; i < districtList.length; i++) {
                        let district = new Object();
                        district.DistrictCode = districtList[i].DistrictCode;
                        district.DistrictName = districtList[i].DistrictName;
                        district.DistrictId = districtList[i].DistrictId;

                        allDistrict.push(district);
                    }
                }
                // states = districtList;
                if (allDistrict.length != 0) {
                
                    $scope.District = allDistrict.filter(function (x) {
                        return x.DistrictId === parseInt(distid)
                    });
                }
            },
            error: function (xhr, errorString, errorMessage) {
               // alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;
        //let allDistricts = getDistricts($('#selPLState').val(), states);
        //if (allDistricts.length >= 1) {
        //    $('#selPLDistrict').empty();
        //    $('#selPLDistrict').append($('<option>', { value: 0, text: 'Select District' }));
        //    for (let i = 0; i < allDistricts.length; i++) {
        //        $('#selPLDistrict').append($('<option>', { value: allDistricts[i].DistrictId, text: allDistricts[i].DistrictName + '(' + allDistricts[i].DistrictCode + ')' }));
        //    }
        //}
    };
  $scope.GetSubDistrict = function (distid, subdistid) {
        // $scope.Subdistrict GetSubDistrict
        var allSubDistrict = new Array();
        var requestUrl = 'api/GetSubDistrict.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"distirctId": "' + distid + '"}'
           ,
            async: false,
            success: function (subdistrictList) {
                if (subdistrictList.length != 0) {
                    for (let i = 0; i < subdistrictList.length; i++) {
                        let subdistrict = new Object();
                        subdistrict.SubDistrictCode = subdistrictList[i].SubDistrictCode;
                        subdistrict.SubDistrictName = subdistrictList[i].SubDistrictName;
                        subdistrict.SubDistrictId = subdistrictList[i].SubDistrictId;

                        allSubDistrict.push(subdistrict);
                    }
                }
                // states = districtList;
                if (allSubDistrict.length != 0) {
                    
                    $scope.Subdistrict = allSubDistrict.filter(function (x) {
                        return x.SubDistrictId === parseInt(subdistid)
                    });
                   
                }
            },
            error: function (xhr, errorString, errorMessage) {
               // alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allSubDistrict;
        // 
        //let subDistrict = new Array();
        //let distrcts = getDistricts($('#selPLState').val(), states);
        //subDistrict = getCities($('#selPLDistrict').val(), distrcts);
        //console.log(JSON.stringify(subDistrict));
        //$('#selPLSubDistrict').empty();
        //$('#selPLSubDistrict').append($('<option>', {
        //    value: 0,
        //    text: 'Select Sub District'
        //}));
        //if (subDistrict.length >= 1) {
        //    for (let i = 0; i < subDistrict.length; i++) {
        //        $('#selPLSubDistrict').append($('<option>', { value: subDistrict[i].SubDistrictId, text: subDistrict[i].SubDistrictName + '(' + subDistrict[i].SubDistrictCode + ')' }));
        //    }
        //}
    };

    //////////////////////////////////------------------------------- Villagee--------
  $scope.GetVillages = function (subdistid, villageid) {

        var allVillage = new Array();
        var requestUrl = 'api/GetVillage.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")

            },
            data: '{"subDistrictId": "' + subdistid + '"}'
           ,
            async: false,
            success: function (villageList) {
                if (villageList.length != 0) {
                    
                    for (let i = 0; i < villageList.length; i++) {
                        let village = new Object();
                        village.VillageCode = villageList[i].VillageCode;
                        village.VillageName = villageList[i].VillageName;
                        village.VillageId = villageList[i].VillageId;

                        allVillage.push(village);
                    }
                }
                $scope.allVillageList = allVillage;
                // states = districtList;
                if (allVillage.length != 0) {
                    $scope.City = allVillage.filter(function (x) {
                         
                        return x.VillageId === parseInt(villageid)
                    });
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the village list could not be populated!\n' + errorMessage);
            }
        });
        return allVillage;
        // 
        //var StateId = parseInt($('#selPLState').val());
        //var DistrictId = parseInt($('#selPLDistrict').val());
        //var SubDistrictId = parseInt($('#selPLSubDistrict').val());

        //var distrcts = states.filter(function (x) {
        //    return x.StateId === StateId;
        //});

        //var cities = distrcts[0].Districts.filter(function (x) {
        //    return x.DistrictId === DistrictId;
        //});


        //var village = cities[0].Villages


        //$('#selPLdVillages').empty();
        //$('#selPLdVillages').append($('<option>', { value: 0, text: 'Select Village' }));
        //if (village.length >= 1) {
        //    for (i = 0; i < village.length; i++) {
        //        $('#selPLdVillages').append($('<option>', { value: village[i].VillageCode, text: village[i].VillageName + '(' + village[i].VillageCode + ')' }));
        //    }
        //}
    };


    function ShowDialog(message, height, width, title, padding, icon, showOkButton, link) {

        $("#helloModalMessage").html(message);

        $('#helloModal').modal('show');

        if (showOkButton == false) {
            $("#closemodalbutton").show();
            $("#redirectbutton").hide();
        }
        else {
            if (link != '')
                $("#redirectbutton").attr("href", link);
            $("#closemodalbutton").hide();
            $("#redirectbutton").show();
        }

        //$.Dialog({
        //    overlay: true,
        //    shadow: true,
        //    flat: true,
        //    title: title,
        //    icon: icon,
        //    content: '',
        //    width: width,
        //    padding: padding,
        //    height: height,//600,400
        //    onShow: function (_dialog) {
        //        var content = _dialog.children('.content');
        //        content.html(message);
        //        $.Metro.initInputs();
        //    }
        //});
    }

    $('#btnSubmitBlackList').on('click', function () {

        var bc = new Object();
        bc.BankCorrespondId = bcId; //From url
        bc.IsBlackListed = true;
        var from = $("#txtDate").val().split("/");
        newdate = from[2] + "-" + from[1] + "-" + from[0];

        bc.BlacklistDate = newdate;
        bc.BlackListReason = $('#txtReason').val();
        bc.BlackListApprovalNumber = $('#txtApprovalNumber').val();

        $.ajax({
            url: "api/InsertUpdateHandler.ashx?mode=BlackList",
            method: "POST",
            datatype: "json",
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            data: { "bcorr": JSON.stringify(bc) },
            success: function () {
                alert('BC Successfully BlackListed.')
                window.location.href = "#/all";

            },
            error: function (xmlHttpRequest, errorString, errorMessage) {
                alert('BC Successfully BlackListed.')
                window.location.href = "#/all";
            }
        });
    });

    $('#btnSubmitChangeStatus').on('click', function () {

        var bc = new Object();
        bc.BankCorrespondId = bcId; //From url
        bc.Status = 'Inactive';
        bc.CurrentStatusSince = $('#txtDate').val();
        bc.StatusChangeReason = $('#txtReason').val();

        $.ajax({
            url: "api/InsertUpdateHandler.ashx?mode=ChangeStatus",
            method: "POST",
            datatype: "json",
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            data: { "bcorr": JSON.stringify(bc) },
            success: function () {
                alert('BC Terminated successfully.');
                window.location.href = "#/all";
            },
            error: function (xmlHttpRequest, errorString, errorMessage) {
                alert('BC Terminated successfully.');
                window.location.href = "#/all";
            }
        });
    });

    //$scope.GetStates = function () {

    //    var allStates = new Array();
    //    var requestUrl = 'api/GetBCHandler.ashx?mode=state';
    //    $.ajax({
    //        type: 'POST',
    //        dataType: 'json',
    //        headers: {
    //            "userId": sessionStorage.getItem("userId"),
    //            "TokenId": sessionStorage.getItem("TokenId")
    //        },
    //        contentType: 'application/json',
    //        url: requestUrl,
    //        async: false,
    //        success: function (stateList) {
    //            if (stateList.length != 0) {
    //                for (let i = 0; i < stateList.length; i++) {
    //                    let state = new Object();
    //                    state.StateId = stateList[i].StateId;
    //                    state.StateName = stateList[i].StateName;
    //                    state.StateCircles = stateList[i].StateCircles;
    //                    state.Branches = stateList[i].Branches;
    //                    allStates.push(state);
    //                }
    //            }
    //            states = stateList;
    //            if (allStates.length != 0) {
    //                $('#selAddressState').append($('<option>', { value: 0, text: '--Select--' }));
    //                $('#selPLState').append($('<option>', { value: 0, text: '--Select--' }));
    //                for (let i = 0; i < allStates.length; i++) {
    //                    $('#selAddressState').append($('<option>', {
    //                        value: allStates[i].StateId,
    //                        text: allStates[i].StateName
    //                    }));
    //                }
    //            }
    //            if (allStates.length != 0) {
    //                for (let i = 0; i < allStates.length; i++) {
    //                    $('#selPLState').append($('<option>', {
    //                        value: allStates[i].StateId,
    //                        text: allStates[i].StateName
    //                    }));
    //                }
    //            }

    //        },
    //        error: function (xhr, errorString, errorMessage) {
    //            alert('For some reason, the bank list could not be populated!\n' + errorMessage);
    //        }
    //    });
    //    return allStates;
    //}

    $('#selAddressState').on('change', function () {

        let allDistricts = getDistricts($('#selAddressState').val(), states);
        if (allDistricts.length >= 1) {
            $('#selAddressDistrict').empty();
            $('#selAddressDistrict').append($('<option>', { value: 0, text: 'Select District' }));
            for (let i = 0; i < allDistricts.length; i++) {
                $('#selAddressDistrict').append($('<option>', { value: allDistricts[i].DistrictId, text: allDistricts[i].DistrictName }));
            }
        }
    });

    function getDistricts(stateId, stateList) {
        let districts = new Array();
        if (stateList.length != 0) {
            for (let i = 0; i < stateList.length; i++) {
                if (stateId == stateList[i].StateId) {
                    districts = stateList[i].Districts;
                    break;
                }
            }
        }
        return districts;
    }

    $('#selAddressDistrict').on('change', function () {
        let cities = new Array();
        let distrcts = getDistricts($('#selAddressState').val(), states);
        cities = getCities($('#selAddressDistrict').val(), distrcts);
        console.log(JSON.stringify(cities));
        $('#selAddressCity').empty();
        $('#selAddressCity').append($('<option>', {
            value: 0,
            text: 'Select City'
        }));
        if (cities.length >= 1) {
            for (let i = 0; i < cities.length; i++) {
                $('#selAddressCity').append($('<option>', { value: cities[i].CityId, text: cities[i].CityName }));
            }
        }
    });

    function getCities(districtId, districtList) {
        let cities = new Array();
        if (districtList.length != 0) {
            for (let i = 0; i < districtList.length; i++) {
                if (districtList[i].DistrictId == districtId) {
                    cities = districtList[i].Cities;
                    break;
                }
            }
        }
        return cities;
    }

    $('#selPLState').on('change', function () {
        let talukas = getAllTalukas($('#selPLState').val(), states);
        $('#selPLTaluk').empty();
        $('#selPLTaluk').append($('<option>', { value: 0, text: 'Select Taluka' }));
        if (talukas.length >= 1) {
            for (i = 0; i < talukas.length; i++) {
                $('#selPLTaluk').append($('<option>', { value: talukas[i].TalukaId, text: talukas[i].TalukaName }));
            }
        }
    });

    function getAllTalukas(stateId, stateList) {
        let talukas = new Array();
        var stateObject = function (stateIdNumber) {
            var stateOb;
            for (let i = 0; i < stateList.length; i++) {
                if (stateList[i].StateId == stateId) {
                    stateOb = stateList[i];
                    break;
                }
            }
            return stateOb;
        }(stateId);
        if (stateObject != null) {
            for (let i = 0; i < stateObject.Districts.length; i++) {
                for (let j = 0; j < stateObject.Districts[i].Talukas.length; j++) {
                    var t = new Object();
                    t.TalukaId = stateObject.Districts[i].Talukas[j].TalukaId;
                    t.TalukaName = stateObject.Districts[i].Talukas[j].TalukaName;
                    talukas.push(t);
                }
            }
        }
        return talukas;
    }
    $scope.allocateBc = function () {
         
        $scope.bankid = 0;
        if (confirm("Do you want to allocate BC?")) {

            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: '{ "bankId": "' + $scope.bankid + '","bcId": "' + $scope.bcid + '"}',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                url: "api/AllocateBCHandler.ashx",
                async: false,
                success: function () {
                    alert('BC Allocated successfully.');

                },
                error: function (xmlHttpRequest, errorString, errorMessage) {

                  

                    alert('BC Allocated successfully.');
                }
            });
        }
        return false;
    }
});