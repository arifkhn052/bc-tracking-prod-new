﻿bctrackApp.controller("reportListController", function ($scope, $cookies, $window) {
    $scope.datetimeBind = function () {
        //tabing();
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        var firstDay = '01/' + mm + '/' + yyyy;
        $("#dtStartDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: firstDay
        });
        $("#dtStartDate").val(firstDay);

        $("#dtEndDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#dtEndDate").val(today);
    }

    $scope.getListAsOndate =function()
    {
        startDate = $('#dtStartDate').val();
        endDate = $('#dtEndDate').val();

        var leadTable = $('#tblListAsOnDate').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "listason.aspx/getListAsOnDate",
            "columnDefs": [

                

            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["startDate"] = startDate;
                aoData["endDate"] = endDate;
                aoData["adminuserid"] = $cookies.get("userId"),
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblListAsOnDate").show();
                                }
                });
            }
        });
          leadTable.fnSetFilteringDelay(300);
    }
  
   
    $scope.getListAsOndatefilter = function () {
        startDate = $('#dtStartDate').val();
        endDate = $('#dtEndDate').val();

        var leadTable = $('#tblListAsOnDate').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 5000], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,

            "sAjaxSource": "listason.aspx/getListAsOnDatefilter",
            "columnDefs": [
              
            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["startDate"] = startDate;
                aoData["endDate"] = endDate;
                aoData["adminuserid"] = $cookies.get("userId"),
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblListAsOnDate").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);

    }


    $scope.getCorporateBc =function()
    {
        var stateid = 0;
        var districtid = 0;
        var subdistrictid = 0;
        var villageId = 0;
        CorporateId = $('#selCorporate').val();
        var leadTable = $('#tblCorporateBC').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "CorporateBC.aspx/getBCbyCorporate",
            "columnDefs": [

                {
                    "orderable": false,
                    "targets": -1,
                    "render": function (data, type, full, meta) {
                        return '';//'<a class="btn btn-danger  btn-sm"    href="#/chnageStatusallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Terminate</a>';
                    }
                },
              {
                  "orderable": false,
                  "targets": -2,
                  "render": function (data, type, full, meta) {

                      if (full[6] == "True")
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i> Blacklisted</a>';
                      else
                          return '';//'<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Blacklist</a>';
                  }
              },
                {
                    "orderable": false,
                    "targets": -3,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-primary  btn-sm"   href="#/viewallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>';
                    }
                },
                  {
                      "orderable": false,
                      "targets": -4,
                      "render": function (data, type, full, meta) {
                          return '<a class="btn btn-primary  btn-sm"   href="#/editallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Edit</a>';
                      }
                  }
            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["stateid"] = stateid,
                aoData["districtid"] = districtid,
                aoData["subdistrictid"] = subdistrictid,
                aoData["villageId"] = villageId,
                aoData["adminuserid"] = $cookies.get("userId"),
                   aoData["corporateId"] = CorporateId
                
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblCorporateBC").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);

    }
    //bind state
    $scope.GetStates = function () {

        //  $('#selAddressState').empty();
        $('#selAddressState').empty();
        $('#selAddressDistrict').empty();

        $('#selAddresssubDistrict').empty();

        $('#selAddressVillage').empty();

        $('#selAddressState').append($('<option>', { value: 0, text: '--Select State--' }));
        $('#selAddressDistrict').append($('<option>', { value: 0, text: '--Select District--' }));
        $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select Sub District--' }));
        $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select Village--' }));


        var allStates = new Array();
        var requestUrl = 'api/state.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        let state = new Object();
                        state.StateId = stateList[i].StateId;
                        state.StateName = stateList[i].StateName;
                        state.StateCode = stateList[i].StateCode;
                        state.StateCircles = stateList[i].StateCircles;
                        state.Branches = stateList[i].Branches;
                        allStates.push(state);
                    }
                }
                states = stateList;
                if (allStates.length != 0) {
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selAddressState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }
                if (allStates.length != 0) {
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selAddressState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }

            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        //   return allStates;
    }

    //bind district
    $('#selAddressState').on('change', function () {

        $('#selAddressDistrict').empty();
        $('#selAddresssubDistrict').empty();
        $('#selAddressVillage').empty();

        $('#selAddressDistrict').append($('<option>', { value: 0, text: '--Select District--' }));
        $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select Sub District--' }));
        $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select Village--' }));
        var allDistrict = new Array();
        var requestUrl = 'api/Districts.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"stateId": "' + $('#selAddressState').val() + '"}'
           ,
            async: false,
            success: function (districtList) {
                if (districtList.length != 0) {
                    for (let i = 0; i < districtList.length; i++) {
                        let district = new Object();
                        district.DistrictCode = districtList[i].DistrictCode;
                        district.DistrictName = districtList[i].DistrictName;
                        district.DistrictId = districtList[i].DistrictId;

                        allDistrict.push(district);
                    }
                }
                // states = districtList;
                if (allDistrict.length != 0) {

                      for (let i = 0; i < allDistrict.length; i++) {
                        $('#selAddressDistrict').append($('<option>', {
                            value: allDistrict[i].DistrictId,
                            text: allDistrict[i].DistrictName + '(' + allDistrict[i].DistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        // return allDistrict;

    });
    // bind subdistrict
    $('#selAddressDistrict').on('change', function () {
        $('#selAddresssubDistrict').empty();
        $('#selAddressVillage').empty();
        $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select Sub District--' }));
        $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select Village--' }));

        var allSubDistrict = new Array();
        var requestUrl = 'api/GetSubDistrict.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"distirctId": "' + $('#selAddressDistrict').val() + '"}'
           ,
            async: false,
            success: function (subdistrictList) {
                if (subdistrictList.length != 0) {
                    for (let i = 0; i < subdistrictList.length; i++) {
                        let subdistrict = new Object();
                        subdistrict.SubDistrictCode = subdistrictList[i].SubDistrictCode;
                        subdistrict.SubDistrictName = subdistrictList[i].SubDistrictName;
                        subdistrict.SubDistrictId = subdistrictList[i].SubDistrictId;

                        allSubDistrict.push(subdistrict);
                    }
                }
                // states = districtList;
                if (allSubDistrict.length != 0) {
                    for (let i = 0; i < allSubDistrict.length; i++) {
                        $('#selAddresssubDistrict').append($('<option>', {
                            value: allSubDistrict[i].SubDistrictId,
                            text: allSubDistrict[i].SubDistrictName + '(' + allSubDistrict[i].SubDistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        // return allDistrict;

    });
    //bind village
    $('#selAddresssubDistrict').on('change', function () {
        $('#selAddressVillage').empty();
        $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select Village--' }));

        var allVillage = new Array();
        var requestUrl = 'api/GetVillage.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")


            },
            data: '{"subDistrictId": "' + $('#selAddresssubDistrict').val() + '"}'
           ,
            async: false,
            success: function (villageList) {
                if (villageList.length != 0) {
                    for (let i = 0; i < villageList.length; i++) {
                        let village = new Object();
                        village.VillageCode = villageList[i].VillageCode;
                        village.VillageName = villageList[i].VillageName;
                        village.VillageId = villageList[i].VillageId;

                        allVillage.push(village);
                    }
                }
                $scope.allVillageList = allVillage;

                if (allVillage.length != 0) {
                    for (let i = 0; i < allVillage.length; i++) {
                        $('#selAddressVillage').append($('<option>', {
                            value: allVillage[i].VillageId,
                            text: allVillage[i].VillageName + '(' + allVillage[i].VillageCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the village list could not be populated!\n' + errorMessage);
            }
        });
        // return allVillage;

    });

   
    var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    var origYearFrom = 2017;
    var origMonthFrom = 0;
    var range = [];
    var secondrange = [];
    var monthRange = 1;
    var apptMode = 'AppointmentMode';
    var createMode = 'CreateMode';
    blackListMode = 'BlackList';
    var yearTo = 2017;
    let allBanks = new Array();

    var fullurl = $(location).attr('href');

    var c = fullurl.match(/cId=([^&]+)/);
    var cId = '';
    if (c != null)
        cId = c[1];

    var t2 = '';
    if ($('#tblBCList').length > 0) {
        t2 = $('#tblBCList').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": true,
            dom: 'Bfrtip',
            buttons: ['excel']
        });

        var mode = '';
        var url = $(location).attr('href');
        if (url.indexOf('BCListAll') > 0 || url.indexOf('bclistStatus') > 0)
            mode = 'All';
        else if (url.indexOf('bclistBankWise') > 0 || url.indexOf('bclistLocationWise') > 0)
            mode = 'Allocated';
        else if (url.indexOf('bclistUnallocated') > 0)
            mode = 'Unallocated';

        var bcList = [];
        var allCorporates;
        bcList = GetBC(mode, '');
        FillTable(t2);
        GetCorporates();
      
     
    }
    var tblBranchesLastUpdated = $('#tblBranchesLastUpdated').DataTable({
        'paging': true,
        'ordering': true,
        'info': false,
        'searching': false
    });

    // allCorporates = GetCorporates();
    // if (allCorporates != null) {
    //     debugger;
    //     $('#selCorporate').empty();
    //     $('#selCorporate').append($('<option>', { value: 0, text: '--Select--' }));

    //    for (let i = 0; i < allCorporates.length; i++) {
            
    //        $('#selCorporate').append($('<option>', { value: allCorporates[i].CorporateId, text: allCorporates[i].CorporateName }));
    //    }
    //}
    function GetBC(mode, filter) {

        var requrl = "api/GetBCHandler.ashx?mode=" + mode;
        if (filter != '')
            requrl += filter;

        var bcl = [];

        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            contentType: 'application/json',
            url: requrl,
            async: false,
            success: function (BankCorrs) {
                bcl = BankCorrs;
            }
        });

        return bcl;
    }
    function FillTable(table) {

        table.clear().draw();

        for (var cnt = 0; cnt < bcList.length; cnt++) {
            var bankc = bcList[cnt];


            var certs = '';
            if (bankc.Certifications != null) {
                for (var j = 0; j < bankc.Certifications.length; j++)
                    certs += bankc.Certifications[j].CourseName;
            }

            var villages = '';
            if (bankc.OperationalAreas != null) {
                for (var k = 0; k < bankc.OperationalAreas.length; k++)
                    villages += bankc.OperationalAreas[k].VillageCode;
            }
            var black = "BlackList";
            var glp = 'ban-circle';
            if (bankc.IsBlackListed) {
                black = "WhiteList";
                glp = 'ok-circle';
            }

            var actionRow = '<a href="SingleEntry.aspx?bc=' + bankc.BankCorrespondId + '&mode=Edit" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="SingleEntry.aspx?bc=' + bankc.BankCorrespondId + '&mode=View"  title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="BlackListBC.aspx?bc=' + bankc.BankCorrespondId + '&mode=' + black + '" title="' + black + '"><span class="glyphicon glyphicon-' + glp + '" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="ChangeStatus.aspx?bc=' + bankc.BankCorrespondId + '" title="ChangeStatus"><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span></a>';

            table.row.add([bankc.BankCorrespondId, bankc.Name, bankc.PhoneNumber1, bankc.AadharCard, certs, villages, actionRow
            ]).draw(false);

        }

    }

    $('#btnListAsOnDate').on('click', function () {
        let date = $('#createFromDate').val();
        let filter = "&createFromDate=" + date;
        filter = getCommonFilter(filter);
        let mode = 'Allocated';
        let bcList = GetBC(mode, filter);
        if (bcList != null) {
            console.log(JSON.stringify(bcList));
            for (let i = 0; i < bcList.length; i++) {
                t2.row.add([bcList[i].BankCorrespondId, bcList[i].Name, bcList[i].PhoneNumber1, bcList[i].UniqueIdentificationNumber, bcList[i].Certificates, bcList[i].Villages]).draw(false);
            }
        }
        else {
            alert('No data!');
        }
    });
    $scope.GetCorporates=function() {
        let corporates = new Array();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: 'api/GetBCHandler.ashx?mode=Corporate',
            async: false,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            success: function (corpList) {
                $('#selCorporate').empty();
                $('#selCorporate').append($('<option>', { value: 0, text: '--Select--' }));
                console.log("corpList", corpList);
                for (let i = 0; i < corpList.length; i++) {
                    $('#selCorporate').append($('<option>', { value: corpList[i].CorporateId, text: corpList[i].CorporateName }));

                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('Failed. ' + errorMessage);
            }
        });
       // return corporates;
    }
    function getCommonFilter(filter) {
        let bankId = $('#allBanks :selected').val();
        let bankCircle = $('#bankCircle :selected').val();
        let bankZone = $('#bankZone :selected').val();
        let bankRegion = $('#bankRegion :selected').val();
        let bankBranch = $('#bankBranch :selected').val();
        let category = $('#bankCategory :selected').val();
        let ssa = $('#bankSsa :selected').val();
        let state = $('#bankState :selected').val();

        if (bankId != 0 && bankId !== 'Select Bank')
            filter += "&bankId=" + bankId;
        if (bankCircle != 0)
            filter += "&bankCircleId=" + bankCircle;
        if (bankZone != 0)
            filter += "&bankZoneId=" + bankZone;
        if (bankRegion != 0)
            filter += "&bankRegionId=" + bankRegion;
        if (category != 0 && category !== 'Select Category')
            filter += "&bankCategory=" + category;
        if (ssa !== 'Select SSA' && ssa != 0)
            filter += "&bankSsa=" + ssa;
        if (state != 0)
            filter += "&stateId=" + state;

        return filter;
    }
    $scope.GetBanks = function () {

        var requestUrl = 'api/getBankHandler.ashx?mode=Bank';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            contentType: 'application/json',
            url: requestUrl,
            async: false,
            success: function (bankList) {
                if (bankList.length != 0) {
                    for (let i = 0; i < bankList.length; i++) {
                        let bank = new Object();
                        bank.BankId = bankList[i].BankId;
                        bank.BankName = bankList[i].BankName;
                        bank.BankCircles = bankList[i].BankCircles;
                        bank.Branches = bankList[i].Branches;
                        allBanks.push(bank);
                    }
                }

                if (allBanks.length != 0) {
                    for (let i = 0; i < allBanks.length; i++) {
                        $('#allBanks').append($('<option>', {
                            value: allBanks[i].BankId,
                            text: allBanks[i].BankName
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allBanks;
    }
  
    $('#allBanks').on('change', function () {
        $('#bankCircle').empty();
        $('#bankBranch').empty();
        $('#bankCircle').append($('<option>', { value: 0, text: 'Select Circle' }));
        $('#bankBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        var selectedBankId = $('#allBanks :selected').val();
        if (selectedBankId != 0) {
            let bank = getBank(allBanks, selectedBankId);
            let bankCircles = null, bankBranches = null;
            if (bank.BankCircles != null)
                bankCircles = bank.BankCircles;
            if (bank.Branches != null)
                bankBranches = bank.Branches;
            if (bankCircles != null) {
                for (let i = 0; i < bankCircles.length; i++) {
                    $('#bankCircle').append($('<option>', { value: bankCircles[i].CircleId, text: bankCircles[i].CircleName }));
                }
            }
        }
    });
    function getBank(bankList, bankId) {
        
        let bank = new Object();
        for (let i = 0; i < bankList.length; i++) {
            if (bankList[i].BankId == bankId) {
                bank = bankList[i];
                break;
            }
        }
        return bank;
    }
    //Given a bank's circle, list all of it's zones
    $('#bankCircle').on('change', function () {
        let selectedBankId = $('#allBanks :selected').val();
        let selectedBank = getBank(allBanks, selectedBankId);
        let selectedCircleId = $('#bankCircle :selected').val();
        $('#bankZone').empty();
        $('#bankZone').append($('<option>', { value: 0, text: 'Select Zone' }));
        if (selectedBank != null) {
            if (selectedBank.BankCircles != null) {
                let zones = getZones(selectedBank.BankCircles, selectedCircleId);
                if (zones != null) {
                    for (let i = 0; i < zones.length; i++) {
                        $('#bankZone').append($('<option>', { value: zones[i].ZoneId, text: zones[i].ZoneName }));
                    }
                }
            }
        }
    });
    function getZones(circleList, circleId) {
        let zones = new Array();
        for (let i = 0; i < circleList.length; i++) {
            if (circleList[i].CircleId == circleId) {
                if (circleList[i].CircleZones != null) {
                    zones = circleList[i].CircleZones;
                    break;
                }
            }
        }
        return zones;
    }
    //Given a bank's region, list all of it's branches under that region
    $('#bankRegion').on('change', function () {
        $('#bankBranch').empty();
        $('#bankBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        let selectedBankId = $('#allBanks :selected').val();
        let selectedCircleId = $('#bankCircle :selected').val();
        let selectedZoneId = $('#bankZone :selected').val();
        let selectedRegionId = $('#bankRegion :selected').val();
        let bank = getBank(allBanks, selectedBankId);
        if (bank != null) {
            if (bank.BankCircles != null) {
                let zones = getZones(bank.BankCircles, selectedCircleId);
                if (zones != null) {
                    let regions = getRegions(zones, selectedZoneId);
                    if (regions != null) {
                        let branches = getBranches(regions, selectedRegionId);
                        if (branches != null) {
                            for (let i = 0; i < branches.length; i++) {
                                $('#bankBranch').append($('<option>', { value: branches[i].BranchId, text: branches[i].BranchName }));
                            }
                        }
                    }
                }
            }
        }
    });
    function getRegions(zoneList, zoneId) {
        let regions = new Array();
        for (let i = 0; i < zoneList.length; i++) {
            if (zoneList[i].ZoneId == zoneId) {
                if (zoneList[i].ZoneRegions != null) {
                    regions = zoneList[i].ZoneRegions;
                    break;
                }
            }
        }
        return regions;
    }
    //Given a bank's zone, list all of it's regions
    $('#bankZone').on('change', function () {
        let selectedZoneId = $('#bankZone :selected').val();
        let selectedBankId = $('#allBanks :selected').val();
        let selectedCircleId = $('#bankCircle :selected').val();
        let bank = getBank(allBanks, selectedBankId);
        $('#bankRegion').empty();
        $('#bankRegion').append($('<option>', { value: 0, text: 'Select Region' }));
        if (bank != null) {
            if (bank.BankCircles != null) {
                let zones = getZones(bank.BankCircles, selectedCircleId);
                if (zones != null) {
                    let regions = getRegions(zones, selectedZoneId);
                    if (regions != null) {
                        for (let i = 0; i < regions.length; i++) {
                            $('#bankRegion').append($('<option>', { value: regions[i].RegionId, text: regions[i].RegionName }));
                        }
                    }
                }
            }
        }
    });
    function getBranches(regionList, regionId) {
        let branches = new Array();
        for (let i = 0; i < regionList.length; i++) {
            if (regionId == regionList[i].RegionId) {
                if (regionList[i].RegionBranches != null) {
                    branches = regionList[i].RegionBranches;
                    break;
                }
            }
        }
        return branches;
    }
    $('#btnListCorporateBCs').on('click', function () {
        let date = $('#txtCreateDate').val();
        let corporateId = $('#selCorporate').val();
        let filter = "&createFromDate=" + date + "&corporateId=" + corporateId;
        filter = getCommonFilter(filter);
    });

    $scope.userRole = $cookies.get("Role");
    if ($scope.userRole == "1") {
        $scope.hidebtnDiv = true;
    }
    else {
        $scope.hidebtnDiv = false;
    }
    $scope.redirectToDashBoard = function () {
        $window.open('http://bcregistrybi.herokuapp.com/public/question/1f8d860c-fa70-4904-8f92-bc9738c51d9d');
    };
    $scope.redirectToTopList = function () {
        $window.open('https://bcregistrybi.herokuapp.com/question/6');
    };
});