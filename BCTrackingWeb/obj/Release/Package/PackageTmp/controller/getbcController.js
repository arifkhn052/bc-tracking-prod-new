﻿bctrackApp.controller("getbcController", function ($scope, $cookies) {
    $scope.getBcBranchWise =function ()
    {
        var leadTable = $('#tblBranchList').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "BCListAlls.aspx/getBcAllCorresponds",
            "columnDefs": [

                {
                    "orderable": false,
                    "targets": -1,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-danger  btn-sm"    href="#/chnageStatusallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Terminate</a>';
                    }
                },
              {
                  "orderable": false,
                  "targets": -2,
                  "render": function (data, type, full, meta) {

                      if (full[6] == "True")
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i> Blacklisted</a>';
                      else
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Blacklist</a>';
                  }
              },
                {
                    "orderable": false,
                    "targets": -3,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-primary  btn-sm"   href="#/viewallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>';
                    }
                },
                  {
                      "orderable": false,
                      "targets": -4,
                      "render": function (data, type, full, meta) {
                          return '<a class="btn btn-primary  btn-sm"   href="#/editallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Edit</a>';
                      }
                  }

            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["adminuserid"] = $cookies.get("userId"),
                
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblBranchList").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);

    }

    $scope.getBcBranchWiseFilter = function () {
         
        var  url;
        let bankId = $('#allBanks').val();
        let branchId = $('#bankBranch').val();
        var itype;

        if (bankId != "Select Bank") {
            itype = 1;
            url = "bclistBankWise.aspx/getBcByBrnachFilter";
         
        }
        
        if (branchId != "0") {
            itype = 2;
            url = "bclistBankWise.aspx/getBcByBrnachFilter";
        }
        if (bankId == "Select Bank" && branchId == "0") {
          
            url = "BCListAlls.aspx/getBcAllCorresponds";
        }
        if (bankId != "Select Bank" && branchId != "0") {
            itype = 3;
            url = "bclistBankWise.aspx/getBcByBrnachFilter";
        }

        

        var leadTable = $('#tblBranchList').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": url,
            "columnDefs": [
 {
     "orderable": false,
     "targets": -1,
     "render": function (data, type, full, meta) {
         return '<a class="btn btn-danger  btn-sm"    href="#/chnageStatusallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Terminate</a>';
     }
 },
              {
                  "orderable": false,
                  "targets": -2,
                  "render": function (data, type, full, meta) {

                      if (full[6] == "True")
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i> Blacklisted</a>';
                      else
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Blacklist</a>';
                  }
              },
                {
                    "orderable": false,
                    "targets": -3,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-primary  btn-sm"   href="#/viewallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>';
                    }
                },
                  {
                      "orderable": false,
                      "targets": -4,
                      "render": function (data, type, full, meta) {
                          return '<a class="btn btn-primary  btn-sm"   href="#/editallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Edit</a>';
                      }
                  }

            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["itype"] = itype;
                aoData["bankId"] = bankId;
                aoData["branchId"] = branchId;
                aoData["adminuserid"] = $cookies.get("userId"),
              

                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblBranchList").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);

    }


    //-----------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------List Location wise---------------------------------------

    $scope.getListOfLocationWIse = function () {
         
        selState = $('#selState').val();
        selPLState = $('#selPLState').val();
        txtPincode = $('#txtPincode').val();

        var leadTable = $('#tblbclistLocationWise').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "BCListAlls.aspx/getBCLocationWise",
            "columnDefs": [

                {
                    "orderable": false,
                    "targets": -1,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-danger  btn-sm"    href="#/chnageStatusallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> Terminate</a>';
                    }
                },
              {
                  "orderable": false,
                  "targets": -2,
                  "render": function (data, type, full, meta) {

                      if (full[6] == "True")
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i> Blacklisted</a>';
                      else
                          return '<a class="btn btn-danger  btn-sm"    href="#/blacklistallCorresponds/' + full[0] + '" ><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Blacklist</a>';
                  }
              },
                {
                    "orderable": false,
                    "targets": -3,
                    "render": function (data, type, full, meta) {
                        return '<a class="btn btn-primary  btn-sm"   href="#/viewallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>';
                    }
                },
                  {
                      "orderable": false,
                      "targets": -4,
                      "render": function (data, type, full, meta) {
                          return '<a class="btn btn-primary  btn-sm"   href="#/editallCorresponds/' + full[0] + '" > <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Edit</a>';
                      }
                  }

            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData["userId"] = $cookies.get("userId");
              
                aoData["selState"] = selState;
                aoData["Taluk"] = selPLState;
                aoData["txtPincode"] = txtPincode;

                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblbclistLocationWise").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);


    }
    


    var fullurl = $(location).attr('href');

    var c = fullurl.match(/cId=([^&]+)/);
    var cId = '';
    if (c != null)
        cId = c[1];

    var t2 = '';

    if ($('#tblBCList').length > 0) {
        t2 = $('#tblBCList').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": true,
            dom: 'Bfrtip',
            buttons: ['excel']
        });

        var mode = '';
        var url = $(location).attr('href');
        if (url.indexOf('BCListAll') > 0 || url.indexOf('bclistStatus') > 0)
            mode = 'All';
        else if (url.indexOf('bclistBankWise') > 0 || url.indexOf('bclistLocationWise') > 0)
            mode = 'Allocated';
        else if (url.indexOf('bclistUnallocated') > 0)
            mode = 'Unallocated';

        var bcList = [];
    
        bcList = GetBC(mode, '');
        FillTable(t2);
      
      //  GetStates();
        GetCorporates();
        var allBanks;
    }

    $('#selState').on('change', function () {
         
        let talukas = getAllTalukas($('#selState').val(), allStates);
        $('#selPLState').empty();
        $('#selPLState').append($('<option>', { value: 0, text: 'Select Taluka' }));
        if (talukas.length >= 1) {
            for (i = 0; i < talukas.length; i++) {
                $('#selPLState').append($('<option>', { value: talukas[i].TalukaId, text: talukas[i].TalukaName }));
            }
        }
    });

    function getAllTalukas(stateId, stateList) {
        let talukas = new Array();
        var stateObject = function (stateIdNumber) {
            var stateOb;
            for (let i = 0; i < stateList.length; i++) {
                if (stateList[i].StateId == stateId) {
                    stateOb = stateList[i];
                    break;
                }
            }
            return stateOb;
        }(stateId);
        if (stateObject != null) {
            for (let i = 0; i < stateObject.Districts.length; i++) {
                for (let j = 0; j < stateObject.Districts[i].Talukas.length; j++) {
                    var t = new Object();
                    t.TalukaId = stateObject.Districts[i].Talukas[j].TalukaId;
                    t.TalukaName = stateObject.Districts[i].Talukas[j].TalukaName;
                    talukas.push(t);
                }
            }
        }
        return talukas;
    }

    function GetBC(mode, filter) {
        
        var requrl = "api/GetBCHandler.ashx?mode=" + mode;
        if (filter != '')
            requrl += filter;

        var bcl = [];

        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            async: false,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            success: function (BankCorrs) {
                bcl = BankCorrs;
            }
        });

        return bcl;
    }

    function FillTable(table) {
      
        table.clear().draw();

        for (var cnt = 0; cnt < bcList.length; cnt++) {
            var bankc = bcList[cnt];


            var certs = '';
            if (bankc.Certifications != null) {
                for (var j = 0; j < bankc.Certifications.length; j++)
                    certs += bankc.Certifications[j].CourseName;
            }

            var villages = '';
            if (bankc.OperationalAreas != null) {
                for (var k = 0; k < bankc.OperationalAreas.length; k++)
                    villages += bankc.OperationalAreas[k].VillageCode;
            }
            var black = "BlackList";
            var glp = 'ban-circle';
            if (bankc.IsBlackListed) {
                black = "WhiteList";
                glp = 'ok-circle';
            }

            var actionRow = '<a   href="#/viewallCorresponds/' + bankc.BankCorrespondId + '"  title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="#/viewallCorresponds/' + bankc.BankCorrespondId + '"  title="View"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="#/blacklistallCorresponds/' + bankc.BankCorrespondId + '"  title="' + black + '"><span class="glyphicon glyphicon-' + glp + '" aria-hidden="true"></span></a>';
            actionRow += '&nbsp;<a href="#/chnageStatusallCorresponds/' + bankc.BankCorrespondId + '"  title="ChangeStatus"><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span></a>';

            table.row.add([bankc.BankCorrespondId, bankc.Name, bankc.PhoneNumber1, bankc.AadharCard, certs, villages, actionRow
            ]).draw(false);

        }

    }

    $scope.GetBanks =   function() {

        let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=Bank";
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
                 
                //console('Got it!');
                for (var cnt = 0; cnt < Banks.length; cnt++) {
                    let bank = new Object();
                    bank.BankId = Banks[cnt].BankId;
                    bank.BankName = Banks[cnt].BankName;
                    bank.Branches = Banks[cnt].Branches;
                    bank.BankCircles = Banks[cnt].BankCircles;
                    banks.push(bank);
                }
                if ($cookies.get("Role") == "1")
                {
                    allBanks = banks;
                }
                else
                {
                    var bankid = parseInt($cookies.get("BankId"));
                    var updateBankList = banks.filter(function (x) {
                        return x.BankId === bankid
                    });
                    allBanks = updateBankList;
                    $('#allBanks').val($cookies.get("BankId"));
                   

                }
              
                console.log(allBanks);
                for (let i = 0; i < allBanks.length; i++) {
                    $('#selBank').append($('<option>', {
                        value: allBanks[i].BankId,
                        text: allBanks[i].BankName
                    }));
                }
                for (let i = 0; i < allBanks.length; i++) {
                    $('#allBanks').append($('<option>', {
                        value: allBanks[i].BankId,
                        text: allBanks[i].BankName
                    }));
                }

                $('.loader').hide();
            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }
        });
        return banks;
    }

  
    if ($('#tblCorporateList').length > 0) {

        var mode = "Corporate";
        if (cId != '')
            mode += "One";



        var t4 = $('#tblCorporateList').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": true
        });

        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            contentType: 'application/json',
            url: "api/GetBCHandler.ashx?mode=" + mode + "&cId=" + cId,
            async: false,
            success: function (Corporates) {
                //console('Got it!');
                if (mode == "Corporate") {
                    for (var cnt = 0; cnt < Corporates.length; cnt++) {
                        var Corporate = Corporates[cnt];
                        t4.row.add([Corporate.CorporateId, Corporate.CorporateName, Corporate.CorporateType, Corporate.Address, Corporate.ContactNumber + '<br />' + Corporate.Email,
                         '<td><a href="AddCorporate.aspx?CId=' + Corporate.CorporateId + '" class="btn btn-success btn-rounded icon-only" id="btnEditCorporate"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>',
                          '<td><input type="button" class="btn btn-danger btn-sm btn-labeled btn-rounded" value="Delete">'
                        ]).draw(false);
                    }
                }
                else if (mode == "CorporateOne") {
                    {
                        var corporate = Corporates[0];
                        //Fill the controls using the above object                  
                    }

                }
                $('.loader').hide();
            },
            error: function (err) {
                //  alert(err);
                $('.loader').hide();
            }
        });
    }
    $('#tblCorporateList').on('click', 'tr', function () {
     
        var corporateId = $(this).find('td:first').text();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            url: "api/DeleteBcHandler.ashx?mode=&corporateId=" + corporateId,
            async: false,
            success: function () {
                window.location.reload();

            }
        });


    });

    if ($('#tblNotificationList').length > 0) {

        var t4 = $('#tblNotificationList').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": true
        });


        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            url: "api/GetBCHandler.ashx?mode=Notification",
            async: false,
            success: function (nots) {
                //console('Got it!');

                for (var cnt = 0; cnt < nots.length; cnt++) {
                    var n = nots[cnt];

                    t4.row.add([n.NotificationId, n.NotificationType, n.NotificationText, n.NotificationDate, n.NotificationPriority
                    ]).draw(false);

                }

                $('.loader').hide();

            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }

        });
    }

    function getBankObject(bankList, bankId) {
        let bankObject = new Object();
        for (let i = 0; i < bankList.length; i++) {
            if (bankList[i].BankId == bankId) {
                bankObject = bankList[i];
                break;
            }
        }
        return bankObject;
    }

    function getZones(bankList, bankId, circleId) {
        let zones = new Array();
        let bankObject = getBankObject(bankList, bankId);
        if (bankObject != null) {
            let correspondingCircle = function (circles, circleId) {
                let c = new Object();
                for (let i = 0; i < circles.length; i++) {
                    if (circleId == circles[i].CircleId) {
                        c = circles[i];
                        break;
                    }
                }
                return c;
            }(bankObject.BankCircles, circleId);
            if (correspondingCircle.CircleZones != null) {
                for (let i = 0; i < correspondingCircle.CircleZones.length; i++) {
                    zones.push(correspondingCircle.CircleZones[i]);
                }
            }
        }
        return zones;
    }

    function getZones(bankList, bankId) {
        let zones = new Array();
        let bank = getBankObject(bankList, bankId);
        if (bank != null) {
            if (bank.BankCircles != null) {
                for (let i = 0; i < bank.BankCircles.length; i++) {
                    if (bank.BankCircles[i].CircleZones != null) {
                        for (let j = 0; j < bank.BankCircles[i].CircleZones.length; j++) {
                            zones.push(bank.BankCircles[i].CircleZones[j]);
                        }
                    }
                }
            }
        }
        return zones;
    }

    function getZoneRegions(zones, zoneId) {
        let regions = new Array();
        for (let i = 0; i < zones.length; i++) {
            if (zoneId == zones[i].ZoneId) {
                if (zones[i].ZoneRegions != null) {
                    for (let j = 0; j < zones[i].ZoneRegions.length; j++) {
                        regions.push(zones[i].ZoneRegions[j]);
                    }
                }
            }
        }
        return regions;
    }
    function GetCorporates() {
        let corporates = new Array();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            url: 'api/GetBCHandler.ashx?mode=Corporate',
            async: false,
            success: function (corpList) {
                for (let i = 0; i < corpList.length; i++) {
                    corporates.push(corpList[i]);
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('Failed. ' + errorMessage);
            }
        });
        return corporates;
    }
    function getCommonFilter(filter) {
        let bankId = $('#allBanks :selected').val();
        let bankCircle = $('#bankCircle :selected').val();
        let bankZone = $('#bankZone :selected').val();
        let bankRegion = $('#bankRegion :selected').val();
        let bankBranch = $('#bankBranch :selected').val();
        let category = $('#bankCategory :selected').val();
        let ssa = $('#bankSsa :selected').val();
        let state = $('#bankState :selected').val();

        if (bankId != 0 && bankId !== 'Select Bank')
            filter += "&bankId=" + bankId;
        if (bankCircle != 0)
            filter += "&bankCircleId=" + bankCircle;
        if (bankZone != 0)
            filter += "&bankZoneId=" + bankZone;
        if (bankRegion != 0)
            filter += "&bankRegionId=" + bankRegion;
        if (category != 0 && category !== 'Select Category')
            filter += "&bankCategory=" + category;
        if (ssa !== 'Select SSA' && ssa != 0)
            filter += "&bankSsa=" + ssa;
        if (state != 0)
            filter += "&stateId=" + state;

        return filter;
    }

    if ($('#tblUserList').length > 0) {

        var t5 = $('#tblUserList').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "searching": true
        });


        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            url: "api/GetUserHandler.ashx?mode=All",
            async: false,
            success: function (users) {
                //console('Got it!');

                for (var cnt = 0; cnt < users.length; cnt++) {
                    var user = users[cnt];

                    var roles = '';
                    for (var j = 0; j < user.UserRoles.length; j++) {
                        roles += user.UserRoles[j].Role;
                    }

                    t5.row.add([user.UserId, user.FirstName + ' ' + user.LastName, roles,
                     '<td><a href="AddUser.aspx?id=' + user.UserId + '" class="btn btn-success btn-rounded icon-only"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>',
                     '<td><input type="button" class="btn btn-danger btn-sm btn-labeled btn-rounded" value="Delete">'
                    ]).draw(false);

                }

                $('.loader').hide();

            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }

        });
    }
    $('#tblUserList').on('click', 'tr', function () {
   
        var userId = $(this).find('td:first').text();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            url: "api/DeleteBcHandler.ashx?mode=User&userId=" + userId,
            async: false,
            success: function (user) {
                window.location.reload();

            }
        });


    });
    $('#selBank').on('change', function () {
        let selectedBankId = $('#selBank :selected').val();
        if (selectedBankId != 0) {
            let selectedBankObject = getBankObject(allBanks, selectedBankId);
            let circles = selectedBankObject.BankCircles;
            $('#selCircle').empty();
            $('#selCircle').append($('<option>', { value: 0, text: 'Select Circle' }));
            if (circles != null && circles.length != 0) {
                for (let i = 0; i < circles.length; i++) {
                    $('#selCircle').append($('<option>', { value: circles[i].CircleId, text: circles[i].CircleName }));
                }
            }
        }
    });

    $('#selBank').on('change', function () {
        let selectedBankId = $('#selBank :selected').val();
        if (selectedBankId != 0) {
            let selectedBankObject = getBankObject(allBanks, selectedBankId);
            let branches = selectedBankObject.Branches;
            if (branches.length != 0) {
                for (let i = 0; i < branches.length; i++) {
                    $('#selBranch').append($('<option>', { value: branches[i].BranchId, text: branches[i].BranchName }));
                }
            }
        }
    });

    $('#selCircle').on('change', function () {
        let selectedBankId = $('#selBank :selected').val();
        let selectedCircleId = $('#selCircle :selected').val();
        if (selectedBankId != 0 && selectedCircleId != 0) {
            let zones = getZones(allBanks, selectedBankId, selectedCircleId);
            $('#selZone').empty();
            $('#selZone').append($('<option>', { value: 0, text: 'Select Zone' }));
            if (zones.length != 0) {
                for (let i = 0; i < zones.length; i++) {
                    $('#selZone').append($('<option>', { value: zones[i].ZoneId, text: zones[i].ZoneName }));
                }
            }
        }
    });

    $('#selBank').on('change', function () {
        let selectedBankId = $('#selBank :selected').val();
        $('#selBankZone').empty();
        $('#selBankZone').append($('<option>', { value: 0, text: 'Select Zone' }));
        let zones = getZones(allBanks, selectedBankId);
        if (zones != null) {
            for (let i = 0; i < zones.length; i++) {
                $('#selBankZone').append($('<option>', { value: zones[i].ZoneId, text: zones[i].ZoneName }));
            }
        }
    });

    $('#selBankZone').on('change', function () {
        let selectedBankZoneId = $('#selBankZone :selected').val();
        let selectedBankId = $('#selBank :selected').val();
        $('#selRegion').empty();
        $('#selRegion').append($('<option>', { value: 0, text: 'Select Region' }));
        let zones = getZones(allBanks, selectedBankId);
        if (zones != null) {
            let zoneRegions = getZoneRegions(zones, selectedBankZoneId);
            if (zoneRegions != null) {
                for (let i = 0; i < zoneRegions.length; i++) {
                    $('#selRegion').append($('<option>', { value: zoneRegions[i].RegionId, text: zoneRegions[i].RegionName }));
                }
            }
        }
        else {
            alert('No regions available!');
        }
    });

    $('#selZone').on('change', function () {
        let selectedCircleId = $('#selCircle :selected').val();
        let selectedZoneId = $('#selZone :selected').val();
        let selectedBankId = $('#selBank :selected').val();
        let zones = getZones(allBanks, selectedBankId, selectedCircleId);
        if (zones != null) {
            let selectedZoneObject = function (zones, zoneId) {
                let z = new Object();
                for (let i = 0; i < zones.length; i++) {
                    if (zoneId == zones[i].ZoneId) {
                        z = zones[i];
                        break;
                    }
                }
                return z;
            }(zones, selectedZoneId);
            $('#selRegion').empty();
            $('#selRegion').append($('<option>', { value: 0, text: 'Select Region' }));
            if (selectedZoneObject.ZoneRegions != null) {
                console.log(JSON.stringify(selectedZoneId.ZoneRegions));
                for (let i = 0; i < selectedZoneObject.ZoneRegions.length; i++) {
                    $('#selRegion').append($('<option>', { value: selectedZoneObject.ZoneRegions[i].ZoneId, text: selectedZoneObject.ZoneRegions[i].ZoneName }));
                }
            }
            else {
                alert('Regions not found!');
            }
        }
    });

    function getAllStates() {
        let allStates = new Array();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            url: 'api/GETBCHandler.ashx?mode=state',
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        allStates.push(stateList[i]);
                    }
                }
            },
            error: function (xhr, errorString, errorMsg) {
                alert(errorMsg);
            }
        });
        return allStates;
    }
    let allStates = getAllStates();
    if (allStates.length != 0) {
        for (let i = 0; i < allStates.length; i++) {
            $('#selState').append($('<option>', { value: allStates[i].StateId, text: allStates[i].StateName }));
        }
    }

    $('#btnFilterBC').on('click', function () {
        var bank = $('#allBanks').val()
        let bankCircle = $('#bankCircle :selected').val();
        let state = $('#bankState :selected').val();
        let zone = $('#bankZone :selected').val();
        let region = $('#bankRegion :selected').val();
        let category = $('#selCategory :selected').val();
        let branch = $('#bankBranch :selected').val();
        var filter;

        if (bank != 0)
            filter = "&bankId=" + bank;
        if (bankCircle != 0)
            filter += "&bankCircleId=" + bankCircle;
        if (state != 0)
            filter += "&stateId=" + state;
        if (zone != 0)
            filter += "&bankZoneId=" + zone;
        if (region != 0)
            filter += "&bankRegionId=" + region;
        if (category != 0)
            filter += "&branchCategory=" + 0;
        if (branch != 0)
            filter += "&branchid=" + branch;

        bcList = GetBC('&bankId=1030&bankCircleId=18&stateId=3&bankZoneId=24&bankRegionId=27&branchid=58', filter);
        FillTable(t2);


    });

    $('#btnListAsOnDate').on('click', function () {
        let date = $('#createFromDate').val();
        let filter = "&createFromDate=" + date;
        filter = getCommonFilter(filter);
        let mode = 'Allocated';
        let bcList = GetBC(mode, filter);
        if (bcList != null) {
            console.log(JSON.stringify(bcList));
            for (let i = 0; i < bcList.length; i++) {
                t2.row.add([bcList[i].BankCorrespondId, bcList[i].Name, bcList[i].PhoneNumber1, bcList[i].UniqueIdentificationNumber, bcList[i].Certificates, bcList[i].Villages]).draw(false);
            }
        }
        else {
            alert('No data!');
        }
    });

    $('#btnListCorporateBCs').on('click', function () {
        let date = $('#txtCreateDate').val();
        let corporateId = $('#selCorporate :selected').val();
        let filter = "&createFromDate=" + date + "&corporateId=" + corporateId;
        filter = getCommonFilter(filter);
    });


    /////////////////////////////////////////////////////////////////////////////////////// report.js \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


    //Given a bank, list all of it's circles
    $('#allBanks').on('change', function () {
         
        $('#bankCircle').empty();
        $('#bankBranch').empty();
        $('#bankCircle').append($('<option>', { value: 0, text: 'Select Circle' }));
        $('#bankBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        var selectedBankId = $('#allBanks :selected').val();
        if (selectedBankId != 0) {
            let bank = getBank(allBanks, selectedBankId);
            let bankCircles = null, bankBranches = null;
            if (bank.BankCircles != null)
                bankCircles = bank.BankCircles;
            if (bank.Branches != null)
                bankBranches = bank.Branches;
            if (bankCircles != null) {
                for (let i = 0; i < bankCircles.length; i++) {
                    $('#bankCircle').append($('<option>', { value: bankCircles[i].CircleId, text: bankCircles[i].CircleName }));
                }
            }
        }
    });

    function getBank(bankList, bankId) {
        let bank = new Object();
        for (let i = 0; i < bankList.length; i++) {
            if (bankList[i].BankId == bankId) {
                bank = bankList[i];
                break;
            }
        }
        return bank;
    }

    function getZones(circleList, circleId) {
        let zones = new Array();
        for (let i = 0; i < circleList.length; i++) {
            if (circleList[i].CircleId == circleId) {
                if (circleList[i].CircleZones != null) {
                    zones = circleList[i].CircleZones;
                    break;
                }
            }
        }
        return zones;
    }

    function getRegions(zoneList, zoneId) {
        let regions = new Array();
        for (let i = 0; i < zoneList.length; i++) {
            if (zoneList[i].ZoneId == zoneId) {
                if (zoneList[i].ZoneRegions != null) {
                    regions = zoneList[i].ZoneRegions;
                    break;
                }
            }
        }
        return regions;
    }

    function getBranches(regionList, regionId) {
        let branches = new Array();
        for (let i = 0; i < regionList.length; i++) {
            if (regionId == regionList[i].RegionId) {
                if (regionList[i].RegionBranches != null) {
                    branches = regionList[i].RegionBranches;
                    break;
                }
            }
        }
        return branches;
    }

    //Given a bank's circle, list all of it's zones
    $('#bankCircle').on('change', function () {
         
        let selectedBankId = $('#allBanks :selected').val();
        let selectedBank = getBank(allBanks, selectedBankId);
        let selectedCircleId = $('#bankCircle :selected').val();
        $('#bankZone').empty();
        $('#bankZone').append($('<option>', { value: 0, text: 'Select Zone' }));
        if (selectedBank != null) {
            if (selectedBank.BankCircles != null) {
                let zones = getZones(selectedBank.BankCircles, selectedCircleId);
                if (zones != null) {
                    for (let i = 0; i < zones.length; i++) {
                        $('#bankZone').append($('<option>', { value: zones[i].ZoneId, text: zones[i].ZoneName }));
                      

                    }
                }
            }
        }
    });

    //Given a bank's zone, list all of it's regions
    $('#bankZone').on('change', function () {
         
        let selectedZoneId = $('#bankZone :selected').val();
        let selectedBankId = $('#allBanks :selected').val();
        let selectedCircleId = $('#bankCircle :selected').val();
        let bank = getBank(allBanks, selectedBankId);
        $('#bankRegion').empty();
        $('#bankRegion').append($('<option>', { value: 0, text: 'Select Region' }));
        if (bank != null) {
            if (bank.BankCircles != null) {
                let zones = getZones(bank.BankCircles, selectedCircleId);
                if (zones != null) {
                    let regions = getRegions(zones, selectedZoneId);
                    if (regions != null) {
                        for (let i = 0; i < regions.length; i++) {
                            $('#bankRegion').append($('<option>', { value: regions[i].RegionId, text: regions[i].RegionName }));
                          
                        }
                    }
                }
            }
        }
    });

    //Given a bank's region, list all of it's branches under that region
    $('#bankRegion').on('change', function () {
         
        $('#bankBranch').empty();
        $('#bankBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        let selectedBankId = $('#allBanks :selected').val();
        let selectedCircleId = $('#bankCircle :selected').val();
        let selectedZoneId = $('#bankZone :selected').val();
        let selectedRegionId = $('#bankRegion :selected').val();
        let bank = getBank(allBanks, selectedBankId);
        if (bank != null) {
            if (bank.BankCircles != null) {
                let zones = getZones(bank.BankCircles, selectedCircleId);
                if (zones != null) {
                    let regions = getRegions(zones, selectedZoneId);
                    if (regions != null) {
                        let branches = getBranches(regions, selectedRegionId);
                        if (branches != null) {
                            for (let i = 0; i < branches.length; i++) {
                                $('#bankBranch').append($('<option>', { value: branches[i].BranchId, text: branches[i].BranchName }));
                            }
                        }
                    }
                }
            }
        }
    });
    //$scope.GetStates=  function() {
    //    var allStates = new Array();
    //    var requestUrl = 'api/GetBCHandler.ashx?mode=state';
    //    $.ajax({
    //        type: 'POST',
    //        dataType: 'json',
    //        contentType: 'application/json',
    //        url: requestUrl,
    //        async: false,
    //        headers: {
    //            "userId": sessionStorage.getItem("userId"),
    //            "TokenId": sessionStorage.getItem("TokenId")
    //        },
    //        success: function (stateList) {
                
    //            if (stateList.length != 0) {
    //                for (let i = 0; i < stateList.length; i++) {
    //                    let state = new Object();
    //                    state.StateId = stateList[i].StateId;
    //                    state.StateName = stateList[i].StateName;
    //                    state.StateCircles = stateList[i].StateCircles;
    //                    state.Branches = stateList[i].Branches;
    //                    allStates.push(state);
                      
    //                }
    //            }
    //            if (allStates.length != 0) {
    //                for (let i = 0; i < allStates.length; i++) {
    //                    $('#bankState').append($('<option>', {
    //                        value: allStates[i].StateId,
    //                        text: allStates[i].StateName
    //                    }));
    //                }


    //            }
    //        },
    //        error: function (xhr, errorString, errorMessage) {
    //            alert('For some reason, the bank list could not be populated!\n' + errorMessage);
    //        }
    //    });
    //    return allStates;
    //}

    ////////////////////////////////////////////////////////////////// getbc.js \\\\\\\\\\\\\\
    $('#allBanks').on('change', function () {
         
        $('#bankBranch').empty();
        $('#bankBranch').append($('<option>', { value: 0, text: 'Select Branch' }));
        var selectedBankId = $('#allBanks :selected').val();
        if (selectedBankId != 0) {
            let bank = getBank(allBanks, selectedBankId);
            let bankCircles = null, bankBranches = null;
            if (bank.BankCircles != null)
                bankCircles = bank.BankCircles;
            if (bank.Branches != null)
                bankBranches = bank.Branches;
            if (bankCircles != null) {
                for (let i = 0; i < bankBranches.length; i++) {
                    $('#bankBranch').append($('<option>', { value: bankBranches[i].BranchId, text: bankBranches[i].BranchName }));
                }
            }
        }
    });

});