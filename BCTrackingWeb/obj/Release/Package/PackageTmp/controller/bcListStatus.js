﻿bctrackApp.controller("bcListStatus", function ($scope, $cookies) {
    var t = $('#example').DataTable({
        dom: 'Bfrtip',
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        order: [[1, 'asc']],
        buttons: [
            'selectAll',
            'selectNone',
            {
                text: 'Send Reminder',
                action: function (e, dt, node, config) {
                    alert('Reminder Mail Sent');
                }
            }
        ]
    });



});