﻿
bctrackApp.controller("searchlogController", function ($scope, $state, $cookies) {

    console.log('searchlogController Called.');

    $scope.GetBanks = function () {
       let banks = new Array();
        var requrl = "api/getBankHandler.ashx?mode=Bank";
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },
            async: false,
            success: function (Banks) {
                for (var cnt = 0; cnt < Banks.length; cnt++) {
                    let bank = new Object();
                    bank.BankId = Banks[cnt].BankId;
                    bank.BankName = Banks[cnt].BankName;
                    bank.Branches = Banks[cnt].Branches;
                    bank.BankCircles = Banks[cnt].BankCircles;
                    banks.push(bank);
                }
                if ($cookies.get("Role") == "1") {
                    allBanks = banks;
                }
                else {
                     var bankid = parseInt($cookies.get("BankId"));
                     var updateBankList = banks.filter(function (x) {
                        return x.BankId === bankid
                        });
                     allBanks = updateBankList;
                     $('#allBanks').val($cookies.get("BankId"));
                    }
                $('#selBank').append($('<option>', { value: '-1', text: '--Select Bank--' }));
                $('#selBank').append($('<option>', { value: 'all', text: 'All' }));
                console.log(allBanks);
                for (let i = 0; i < allBanks.length; i++) {
                    $('#selBank').append($('<option>', {
                        value: allBanks[i].BankId,
                        text: allBanks[i].BankName
                    }));
                }

                $('.loader').hide();
            },
            error: function (err) {
                alert(err);
                $('.loader').hide();
            }
        });
        return banks;
    }
    var convertDate = function (usDate) {
        var dateParts = usDate.split(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
        return dateParts[3] + "-" + dateParts[2] + "-" + dateParts[1];
    }

    $scope.datetimeBind = function () {
        //tabing();
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var today = dd + '/' + mm + '/' + yyyy;
        var firstDay = '01/' + mm + '/' + yyyy;
        $("#txtDate").datepicker({
            dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: "+12m +4w",
            changeMonth: true,
            changeYear: true,
            defaultDate: today
        });
        $("#txtDate").val(today);
        

    }
  
     $("#btnsearch").prop("disabled", true);
    $("#selBank").change(function () {
        if (this.value == "-1") {
            $("#btnsearch").prop("disabled", true);
        } else if ($('#selSheet').val() == "-1") {
            $("#btnsearch").prop("disabled", true);
        } else {
            $("#btnsearch").prop("disabled", false);
        }
    });
    $("#selSheet").change(function () {
        if (this.value == "-1") {
            $("#btnsearch").prop("disabled", true);
        } else if ($('#selBank').val() == "-1") {
            $("#btnsearch").prop("disabled", true);
        } else {
            $("#btnsearch").prop("disabled", false);
        }
    });
    $scope.SearchLoges = function () {
        
        Bank_id = $('#selBank').val();
        Sheet = $('#selSheet').val();
        olddate = $('#txtDate').val();
        var Dates = convertDate(olddate);
       
        if (Bank_id == '-1' || Bank_id == undefined || Bank_id == null || Bank_id == 0) {
            Bank_id = 'all';
        }
        if (Sheet=='-1' || Sheet == undefined || Sheet == null || Sheet == 0) {
            Sheet = 'all';
        }
       
        var leadTable = $('#tblsearchLog').dataTable({
            "oLanguage": {
                "sZeroRecords": "No records to display",
                "sSearch": "Search "
            },
            "sDom": 'T<"clear">lfrtip',
            "tableTools": {

                "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
            },
            "iDisplayLength": 25,
            "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
            "bSortClasses": false,
            "bStateSave": false,
            "bPaginate": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "searchLog.aspx/getLoges",
            "columnDefs": [
            ],
            "bDeferRender": true,
            "fnServerData": function (sSource, aoData, fnCallback) {
            
                aoData["Bank_id"] = Bank_id;
                aoData["Sheet"] = Sheet;
                aoData["Dates"] = Dates;
              
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success":
                                function (msg) {
                                    var json = jQuery.parseJSON(msg.d);
                                    fnCallback(json);
                                    $("#tblsearchLog").show();
                                }
                });
            }
        });
        leadTable.fnSetFilteringDelay(300);


    }
});
