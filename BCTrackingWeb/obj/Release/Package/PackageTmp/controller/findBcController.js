﻿bctrackApp.controller("findBcController", function ($scope, $state, $timeout, $cookies) {
     
  
  
    $scope.notexists = true;
    $scope.notterminate = true;
    $scope.terminated = true;
    $scope.notexist_admin = true;
    
  //  $scope.adharcharNo = '';
    $scope.BCCodeNo = '';
    var errorMessage = "";
    function validate(type) {
        $timeout(function () { $scope.notexists = true; }, 100);
        $timeout(function () { $scope.notterminate = true; }, 100);
        $timeout(function () { $scope.terminated = true; }, 100);
        $scope.notexist_admin = true;
         
        var result = true;
        errorMsg = '';
        if (type == "bc") {
            var phone = $('#txtbccode').val();
            if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
                $('#divPhone').addClass("form-group has-error");
                errorMsg = errorMsg + "<br/>Enter valid BC Code.";
                result = false;
            }
            else {
                $("#divPhone").removeClass("form-group has-error");
            }
        }
        else {
            var phone = $('#txtretypeaadhaar').val();
            if (phone == "" || phone.length > 12 || phone.length < 12|| isNaN(phone)) {
                $('#divPhone').addClass("form-group has-error");
                errorMsg = errorMsg + "<br/>Enter valid Aadhar Number.";
                result = false;
            }
            else {
                $("#divPhone").removeClass("form-group has-error");
            }
        }

        return result;
    }
    $scope.filterValue = function ($event) {
        if (isNaN(String.fromCharCode($event.keyCode))) {
            $event.preventDefault();
        }
    };
    $scope.showhidebccode = true;
    $scope.showhideAadhar = false;
    $scope.showhideAadharBcCode = function (no) {
        if (no == 1) { $scope.showhidebccode = true; $scope.showhideAadhar = false; $('#txtretypeaadhaar').val(''); }
        else { $scope.showhidebccode = false; $scope.showhideAadhar = true; $('#txtbccode').val('') }
    };
    

    $('#btnSearch').on('click', function () {
        var type = "";
        var bccode = "";
        var adharno = "";
        if ($scope.showhidebccode == true) {
             bccode = $('#txtbccode').val();
            type = "bc";
        }
        else {
             adharno = $('#txtretypeaadhaar').val();
            type = "aadhar";
            
        }
        if (validate(type) == true) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "api/findBcHandler.ashx",
                data: '{ "bccode": "' + bccode + '","adharno": "' + $.md5(adharno) + '"}',
                dataType: "json",
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                   
                },
                success: function (data) {
                  //  Bank = sessionStorage.getItem("BankId")
                    
                    var arr = (data).split(",");
                     
                    if (arr[0] == "notexists") {
                       
                       
                        if ($cookies.get("BankId") == "-1") {
                          
                            $timeout(function () { $scope.notexist_admin = false; }, 100);

                        }
                        else {
                            //$scope.adharcharNo = arr[1];
                            $scope.BCCodeNo = arr[1];
                            $timeout(function () { $scope.notexists = false; }, 100);
                            $timeout(function () { $scope.notterminate = true; }, 100);
                            $timeout(function () { $scope.terminated = true; }, 100);
                            $timeout(function () { $scope.sub_notexistmsg = true; }, 100);
                            $timeout(function () { $scope.notexist_admin = true; }, 100);
                        }
                    }
                    else if (arr[0] == "notterminate") {
                     
                        $scope.bankname = arr[2];
                        $scope.bcid = arr[1];
                        $scope.BcName = arr[3];
                        $scope.DOB = arr[4];
                        $scope.VillageName = arr[5];
                        $scope.Bankid = arr[6];
                        $timeout(function () { $scope.notexists = true; }, 100);
                        $timeout(function () { $scope.notterminate = false; }, 100);
                        $timeout(function () { $scope.terminated = true; }, 100);
                        $timeout(function () { $scope.notexist_admin = true; }, 100);
                        if ($cookies.get("BankId") == $scope.Bankid)
                        {
                            $timeout(function () { $scope.btnviewdetails = false; }, 100);
                            
                        }
                        else
                        {
                            $timeout(function () { $scope.btnviewdetails = true; }, 100);
                            
                        }

                        // window.location.href = "#/singleentry";
                    }
                    else if (arr[0] == "terminated") {

                    
                        $scope.bcid = arr[1];
                        $scope.BcName = arr[2];
                        $scope.DOB = arr[3];
                        $scope.VillageName = arr[4];

                        $timeout(function () { $scope.notexists = true; }, 100);
                        $timeout(function () { $scope.notterminate = true; }, 100);
                        $timeout(function () { $scope.terminated = false; }, 100);
                        $timeout(function () { $scope.notexist_admin = true; }, 100);
                        //   window.location.href = "#/editallCorresponds/" + data + "";
                    }


                },
                error: function (xmlHttpRequest, errorString, errorMessage) {
                    alert(errorMessage);
                }


            });
        }
        else {
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
       
    });

  //  $("#btnSearch").hide();
    $("#errormsg").hide();
    //$("#txtaadhaar").on("focusout", function (e) {
    //    if ($(this).val() != $("#txtretypeaadhaar").val()) {
    //        $("#btnSearch").hide();
    //        $("#errormsg").hide();
    //        $timeout(function () { $scope.notexists = true; }, 100);
    //        $timeout(function () { $scope.notterminate = true; }, 100);
    //        $timeout(function () { $scope.terminated = true; }, 100);
    //        $timeout(function () { $scope.notexist_admin = true; }, 100);
    //    } else {

    //        $("#btnSearch").show();
    //        $("#errormsg").hide();
    //    }
    //});

    //$("#txtretypeaadhaar").on("keyup", function (e) {
    //    if ($("#txtaadhaar").val() != $(this).val()) {
    //        $("#btnSearch").hide();
    //        $("#errormsg").show();
    //        $timeout(function () { $scope.notexists = true; }, 100);
    //        $timeout(function () { $scope.notterminate = true; }, 100);
    //        $timeout(function () { $scope.terminated = true; }, 100);
    //        $timeout(function () { $scope.notexist_admin = true; }, 100);
    //    } else {
    //        $("#btnSearch").show();
    //        $("#errormsg").hide();
                
    //    }
    //});

});