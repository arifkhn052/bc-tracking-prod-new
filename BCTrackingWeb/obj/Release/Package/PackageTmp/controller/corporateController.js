﻿bctrackApp.controller("corporateController", function ($scope, $cookies,$timeout) {
    $.ajaxSetup({
        cache: false
    });

    var leadTable = $('#tblCorporateList').dataTable({
        "oLanguage": {
            "sZeroRecords": "No records to display",
            "sSearch": "Search "
        },
        "sDom": 'T<"clear">lfrtip',
        "tableTools": {

            "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
        },
        "iDisplayLength": 15,
        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
        "bSortClasses": false,
        "bStateSave": false,
        "bPaginate": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "CorporateList.aspx/getCorporate",
        "columnDefs": [

            //{
            //    "orderable": false,
            //    "targets": -2,
            //    "render": function (data, type, full, meta) {
            //        var i = null;
            //        
            //        if ($cookies.get("userId") == full[6] || $cookies.get("userId") == 'admin') {

            //            i = '<a class="btn btn-danger  btn-sm"    href="#/editCorporate/' + full[0] + '" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            //           // i = '<a  class="btn btn-success btn-sm btn-labeled btn-rounded"  href="#/editCorporate/' + full[0] + '" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            //        }
            //        else { i = '' }
            //        return i;
            //    }
            //},
            { orderable: true, "targets": -1 }, {
                "orderable": true,
                "targets": -1,
                "render": function (data, type, full, meta) {
                    return '<a  href="#/editCorporate/' + full[0] + '">Modify</a>';
                    //return '<a href="#" href="#/editCorporate/' + full[0] + '"><i class="fa fa-trash-o" aria-hidden="true"></a>';


                }
            }
        ],
        "bDeferRender": true,
        "fnServerData": function (sSource, aoData, fnCallback) {
            //aoData["userId"] = sessionStorage.getItem("userId"),
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success":
                            function (msg) {
                                
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                //  $("#tblLeadList").show();
                            }
            });
        }
    });
    //  leadTable.fnSetFilteringDelay(300);
    var errorMessage = "";

    function deleteUser(corporateId) {

        if (confirm("Do you want to delete?")) {

            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                url: "api/DeleteBcHandler.ashx?mode=&corporateId=" + corporateId,
                async: false,
                success: function () {
                    window.location.reload();

                }
            });

        }
        return false;
    }


    function validate() {

        errorMsg = '';

        var email = $('#txtEmail').val();
        var phone = $('#txtContactNo').val();      
        var panNo = $("#txtPanNO").val().toUpperCase();
        var corporateName = $('#txtName').val();    
        var Type = $('#Select1').val();
        

        var result = true;
        if (corporateName == "") {
            $('#divPhone').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Corporate Name.";
            result = false;
        }
        if (Type == -1) {
            $('#divPhone').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Select Type.";
            result = false;
        }


        if (phone == "" || phone.length > 10 || phone.length < 10 || isNaN(phone)) {
            $('#divPhone').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter Valid Mobile Number.";
            result = false;
        }
        else {
            $("#divPhone").removeClass("form-group has-error");
        }
        if (!IsEmail(email)) {
            $('#txtEmail').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter valid email Id.";
            result = false;
        }
        else {

        }
        if (!IsPanNumber(panNo) || panNo == "") {
            $('#txtPanNO').addClass("form-group has-error");
            errorMsg = errorMsg + "<br/>Enter valid Pan Number.";
            result = false;
        }
        else {

        }
       
      

        return result;
    }
    function IsPanNumber(number) {
        var regex = /^([A-Z]{5}\d{4}[A-Z]{1})+$/;
        return regex.test(number);
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    $scope.btnName = "Submit";

    $('#txtPanNO').on('change', function () {

        
        var pan = $('#txtPanNO').val();
        
        $.ajax({
            url: "api/GetBCHandler.ashx?mode=Pan&panno=" + pan,
            method: 'POST',
            datatype: 'json',
            headers: {
                "userId": $cookies.get("userId"),
                "TokenId": $cookies.get("TokenId")
            },

            success: function (data) {
                
                if (data.PanNo == 1) {
                    $('#txtPanNO').val('');
            alert('Pan no is already exists.')
                }
                else {

                }

            },
            error: function (xmlHttpRequest, errorString, errorMessage) {

            }
        });
    });
    $scope.filterValue = function ($event) {
        if (isNaN(String.fromCharCode($event.keyCode))) {
            $event.preventDefault();
        }
    };
 
    //////////////////////////////////////////////////////////// Add Corporate \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    $('#btnAddCorporate').on('click', function () {
      
        if (validate() == true) {
            $("#btnAddCorporate").prop("disabled", true);
            $timeout(function () { $scope.btnName = "Loading..."; }, 0);
          
            
            var corporate = new Object();
            corporate.CorporateName = $('#txtName').val();
            corporate.CorporateType = $('#Select1').val();

            corporate.Email = $('#txtEmail').val();
            corporate.Address = $('#txtAddress').val();         
            corporate.PanNo = $("#txtPanNO").val().toUpperCase();
            corporate.WebSite = $('#txtWebsite').val();

            corporate.ContactNumber = $('#txtContactNo').val();
            corporate.ContactPerson = $('#txtContactPerson').val();
            corporate.ContactDesignation = $('#txtContactDesignation').val();
            corporate.NoOfComplaint = 0;
         

            $.ajax({
                url: "api/GetCorporateHandler.ashx",
                method: 'POST',
                datatype: 'json',
                headers: {
                    "userId": $cookies.get("userId"),
                    "TokenId": $cookies.get("TokenId")
                },
                data: { 'info': JSON.stringify(corporate) },
                success: function () {
                    $scope.btnName = "Submit";
                    $("#btnAddCorporate").prop("disabled", false);
                    alert('Corporate details saved successfully.');
                    window.location.href = "#/corporatelist";
                },
                error: function (xmlHttpRequest, errorString, errorMessage) {
                    $scope.btnName = "Submit";
                    $("#btnAddCorporate").prop("disabled", false);
                    alert('Corporate details saved successfully.');
                    window.location.href = "#/corporatelist";
                }

            });
        }
        else {
           
            ShowDialog(errorMessage != '' ? errorMessage : "Please enter all the values correctly." + errorMsg, 100, 300, 'Message', 10, '<span class="icon-info"></span>', false);
        }
    });
    function ShowDialog(message, height, width, title, padding, icon, showOkButton, link) {

        $("#helloModalMessage").html(message);

        $('#helloModal').modal('show');

        if (showOkButton == false) {
            $("#closemodalbutton").show();
            $("#redirectbutton").hide();
        }
        else {
            if (link != '')
                $("#redirectbutton").attr("href", link);
            $("#closemodalbutton").hide();
            $("#redirectbutton").show();
        }

        //$.Dialog({
        //    overlay: true,
        //    shadow: true,
        //    flat: true,
        //    title: title,
        //    icon: icon,
        //    content: '',
        //    width: width,
        //    padding: padding,
        //    height: height,//600,400
        //    onShow: function (_dialog) {
        //        var content = _dialog.children('.content');
        //        content.html(message);
        //        $.Metro.initInputs();
        //    }
        //});
    }

});