﻿sendServerApp.controller("mainController", ['$scope','$timeout', function ($scope,$timeout) {
    console.log('controller loaded..');
    $scope.GetStates = function () {
    
        $('#selAddressState').empty();
        $('#selPLState').empty();
        $('#selPLDistrict').empty();

        $('#selPLSubDistrict').empty();

        $('#selPLdVillages').empty();


        var allStates = new Array();
        var requestUrl = '../api/state.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requestUrl,
            headers: {
                "userId": 'admin',
                "TokenId": 'b65b499d-a072-4b62-bbab-2b5ba6038e50'
            },
            async: false,
            success: function (stateList) {
                if (stateList.length != 0) {
                    for (let i = 0; i < stateList.length; i++) {
                        let state = new Object();
                        state.StateId = stateList[i].StateId;
                        state.StateName = stateList[i].StateName;
                        state.StateCode = stateList[i].StateCode;
                        state.StateCircles = stateList[i].StateCircles;
                        state.Branches = stateList[i].Branches;
                        allStates.push(state);
                    }
                }
                states = stateList;
                if (allStates.length != 0) {
                    $('#selAddressState').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLState').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLSubDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    $('#selPLdVillages').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selAddressState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }
                if (allStates.length != 0) {
                    for (let i = 0; i < allStates.length; i++) {
                        $('#selPLState').append($('<option>', {
                            value: allStates[i].StateId,
                            text: allStates[i].StateName + '(' + allStates[i].StateCode + ')'
                        }));
                    }
                }

            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the bank list could not be populated!\n' + errorMessage);
            }
        });
        return allStates;
    }
    $('#selAddressState').on('change', function () {
        $('#selAddressDistrict').empty();

        var allDistrict = new Array();
        var requestUrl = '../api/Districts.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": 'admin',
                "TokenId": 'b65b499d-a072-4b62-bbab-2b5ba6038e50'
            },
            data: '{"stateId": "' + $('#selAddressState').val() + '"}'
           ,
            async: false,
            success: function (districtList) {
                if (districtList.length != 0) {
                    for (let i = 0; i < districtList.length; i++) {
                        let district = new Object();
                        district.DistrictCode = districtList[i].DistrictCode;
                        district.DistrictName = districtList[i].DistrictName;
                        district.DistrictId = districtList[i].DistrictId;

                        allDistrict.push(district);
                    }
                }
                // states = districtList;
                if (allDistrict.length != 0) {


                    $('#selAddressDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allDistrict.length; i++) {
                        $('#selAddressDistrict').append($('<option>', {
                            value: allDistrict[i].DistrictId,
                            text: allDistrict[i].DistrictName + '(' + allDistrict[i].DistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;

    });

    //$('#getBCListbehalfofLngandLat').on('change', function () {
    //    var BCList = new Array();
    //    var requestUrl = '../api/BCList.ashx';
    //    $.ajax({
    //        type: 'POST',
    //        dataType: 'json',
    //        contentType: "application/json; charset=utf-8",
    //        url: requestUrl,
    //        headers: {
    //            "userId": 'admin',
    //            "TokenId": '36e58be1-9dd3-4a0b-b469-73ae94e54d4f'
    //        },
    //        data: '{"latitude": "' + + '","longitude": "' +  + '"}'
    //       ,
    //        async: false,
    //        success: function (districtList) {
    //            if (districtList.length != 0) {
                
    //            }
              
    //        },
    //        error: function (xhr, errorString, errorMessage) {
    //            alert('Some Error!\n' + errorMessage);
    //        }
    //    });
    //    return BCList;

    //});


    $('#selAddressDistrict').on('change', function () {


        $('#selAddresssubDistrict').empty();

        var allSubDistrict = new Array();
        var requestUrl = '../api/GetSubDistrict.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": 'admin',
                "TokenId": 'b65b499d-a072-4b62-bbab-2b5ba6038e50'
            },
            data: '{"distirctId": "' + $('#selAddressDistrict').val() + '"}'
           ,
            async: false,
            success: function (subdistrictList) {
                if (subdistrictList.length != 0) {
                    for (let i = 0; i < subdistrictList.length; i++) {
                        let subdistrict = new Object();
                        subdistrict.SubDistrictCode = subdistrictList[i].SubDistrictCode;
                        subdistrict.SubDistrictName = subdistrictList[i].SubDistrictName;
                        subdistrict.SubDistrictId = subdistrictList[i].SubDistrictId;

                        allSubDistrict.push(subdistrict);
                    }
                }
                // states = districtList;
                if (allSubDistrict.length != 0) {


                    $('#selAddresssubDistrict').append($('<option>', { value: 0, text: '--Select--' }));
                    for (let i = 0; i < allSubDistrict.length; i++) {
                        $('#selAddresssubDistrict').append($('<option>', {
                            value: allSubDistrict[i].SubDistrictId,
                            text: allSubDistrict[i].SubDistrictName + '(' + allSubDistrict[i].SubDistrictCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the district list could not be populated!\n' + errorMessage);
            }
        });
        return allDistrict;
        // 
        //let subDistrict = new Array();
        //let distrcts = getDistricts($('#selPLState').val(), states);
        //subDistrict = getCities($('#selPLDistrict').val(), distrcts);
        //console.log(JSON.stringify(subDistrict));
        //$('#selPLSubDistrict').empty();
        //$('#selPLSubDistrict').append($('<option>', {
        //    value: 0,
        //    text: 'Select Sub District'
        //}));
        //if (subDistrict.length >= 1) {
        //    for (let i = 0; i < subDistrict.length; i++) {
        //        $('#selPLSubDistrict').append($('<option>', { value: subDistrict[i].SubDistrictId, text: subDistrict[i].SubDistrictName + '(' + subDistrict[i].SubDistrictCode + ')' }));
        //    }
        //}
    });
    $('#selAddresssubDistrict').on('change', function () {

        $('#selAddressVillage').empty();
        var allVillage = new Array();
        var requestUrl = '../api/GetVillage.ashx';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            url: requestUrl,
            headers: {
                "userId": 'admin',
                "TokenId": 'b65b499d-a072-4b62-bbab-2b5ba6038e50'
            },
            data: '{"subDistrictId": "' + $('#selAddresssubDistrict').val() + '"}'
           ,
            async: false,
            success: function (villageList) {
                if (villageList.length != 0) {
                    for (let i = 0; i < villageList.length; i++) {
                        let village = new Object();
                        village.VillageCode = villageList[i].VillageCode;
                        village.VillageName = villageList[i].VillageName;
                        village.VillageId = villageList[i].VillageId;

                        allVillage.push(village);
                    }
                }
                $scope.allVillageList = allVillage;
                $('#selAddressVillage').append($('<option>', { value: 0, text: '--Select--' }));
                if (allVillage.length != 0) {
                    for (let i = 0; i < allVillage.length; i++) {
                        $('#selAddressVillage').append($('<option>', {
                            value: allVillage[i].VillageId,
                            text: allVillage[i].VillageName + '(' + allVillage[i].VillageCode + ')'
                        }));
                    }
                }
            },
            error: function (xhr, errorString, errorMessage) {
                alert('For some reason, the village list could not be populated!\n' + errorMessage);
            }
        });
        return allVillage;
        // 
        //var StateId = parseInt($('#selPLState').val());
        //var DistrictId = parseInt($('#selPLDistrict').val());
        //var SubDistrictId = parseInt($('#selPLSubDistrict').val());

        //var distrcts = states.filter(function (x) {
        //    return x.StateId === StateId;
        //});

        //var cities = distrcts[0].Districts.filter(function (x) {
        //    return x.DistrictId === DistrictId;
        //});


        //var village = cities[0].Villages


        //$('#selPLdVillages').empty();
        //$('#selPLdVillages').append($('<option>', { value: 0, text: 'Select Village' }));
        //if (village.length >= 1) {
        //    for (i = 0; i < village.length; i++) {
        //        $('#selPLdVillages').append($('<option>', { value: village[i].VillageCode, text: village[i].VillageName + '(' + village[i].VillageCode + ')' }));
        //    }
        //}
    });
    $("#listOfBC").hide();
    $("#detailOfBC").hide();
    $("#searchBc").show();
    $scope.showHideBCDiv = function (no) {

        if (no == 1) {

            $("#listOfBC").show();
            $("#detailOfBC").hide();
            $("#searchBc").hide();
        }
       else if (no == 2) {

            $("#listOfBC").hide();
            $("#detailOfBC").hide();
            $("#searchBc").show();
        }
        else {
            $("#listOfBC").hide();
            $("#detailOfBC").show();
            $("#searchBc").hide();
        }
    };

    $scope.showDetailOfBC = function (id) {
       

        $.ajax({
            url: "../api/GetBCHandler.ashx?mode=publicbcdetails&bcId=" + id,
            method: "POST",
            datatype: "json",
            headers: {
                "userId": 'admin',
                "TokenId": 'b65b499d-a072-4b62-bbab-2b5ba6038e50'
            },

            success: function (Bclist) {
            
                document.getElementById('lblName').innerHTML = Bclist[0].Name;
                document.getElementById('lblcontactNo').innerHTML = Bclist[0].PhoneNumber1;
                document.getElementById('lblbankName').innerHTML = Bclist[0].BankName;
                document.getElementById('lblstate').innerHTML = Bclist[0].State;
                document.getElementById('lbldistrict').innerHTML = Bclist[0].District;
                document.getElementById('lblsubdistrict').innerHTML = Bclist[0].Subdistrict;
                document.getElementById('lblproduct').innerHTML = Bclist[0].productName;
                
            },
            error: function (xmlHttpRequest, errorString, errorMessage) {

            }
        })
    };

    $(".table-body").on('click', 'tr', function () {
      
        var id = parseInt($(this).closest('tr').attr('id'));
      

            $.ajax({
                url: "../api/GetBCHandler.ashx?mode=publicbcdetails&bcId=" + id,
                method: "POST",
                datatype: "json",
                headers: {
                    "userId": 'admin',
                    "TokenId": 'b65b499d-a072-4b62-bbab-2b5ba6038e50'
                },

                success: function (Bclist) {
                
                    document.getElementById('lblName').innerHTML = Bclist[0].Name;
                    document.getElementById('lblcontactNo').innerHTML = Bclist[0].PhoneNumber1;
                    document.getElementById('lblbankName').innerHTML = Bclist[0].BankName;
                    document.getElementById('lblstate').innerHTML = Bclist[0].State;
                    document.getElementById('lbldistrict').innerHTML = Bclist[0].District;
                    document.getElementById('lblsubdistrict').innerHTML = Bclist[0].Subdistrict;
                    document.getElementById('lblproduct').innerHTML = Bclist[0].productName;
                    $("#listOfBC").hide();
                    $("#detailOfBC").show();
                    $("#searchBc").hide();

                },
                error: function (xmlHttpRequest, errorString, errorMessage) {

                }

            });
        });
}]);