﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bcBulkUpload.aspx.cs" Inherits="BCTrackingWeb.bcBulkUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BC Track</title>

</head>
<body>
    <form id="form1" runat="server">

        <div>
            <div class="container-fluid">
                <div class="row page-title-div">
                    <div class="col-md-6">
                        <h2 class="title">Bulk Upload </h2>

                    </div>


                    <!-- /.col-md-6 text-right -->
                </div>
                <!-- /.row -->
                <div class="row breadcrumb-div">
                    <div class="col-md-6">
                        <ul class="breadcrumb">
                            <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                            <li><a>Bulk Upload</a></li>

                        </ul>
                    </div>

                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

            <section class="section" ng-init="getFileStatus1();getZipStatus1()">
                <div class="container-fluid">

                    <div class="row">

                        <!-- /.col-md-6 -->

                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <div class="panel-title">

                                        <a href="Helpers/BC_BulkUploadTest.xlsx" class="btn btn-success" role="button"><i class="fa fa-download" aria-hidden="true"></i>File  Template</a>
                                      <%--  <a href="Helpers/bank-hierarchy-import.xlsx" class="btn btn-success" role="button"><i class="fa fa-download" aria-hidden="true"></i>Bank Data Template</a>--%>
                                        <a href="Helpers/Archive.zip" class="btn btn-success" role="button"><i class="fa fa-download" aria-hidden="true"></i>Village Code  Details</a>
                                        <a href="Helpers/UploadInstruction.docx" class="btn btn-success" role="button"><i class="fa fa-download" aria-hidden="true"></i>Upload Manual</a>
                                     <br />
                                        <br />
                                   
                                        <%--<a href="Helpers/BulkUpload.xlsx" class="btn btn-success" role="button"><i class="fa fa-download" aria-hidden="true"></i> Export Bank Data </a>--%>
                                    </div>
                                </div>
                                <div class="panel-body p-20">
                                      <div class="row">
                                            <div class="col-md-6">
                                            <div class="alert alert-success" ng-hide="myfILEAlert">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
               Your last file {{fileName}} has been uploaded successfully.
            </div>
                                                </div>
                                                  <div class="col-md-6">
                                    <div class="alert alert-success" ng-hide="myZipAlert">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                Your last zip {{zipName}} has been uploaded successfully.
            </div>
                                                </div>
                                          </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <div class="panel-title">
                                                            <h5> <i class="{{fileIcon}}" aria-hidden="true"></i> {{fileStatusText}}</h5>
                                                        </div>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row" ng-hide="divUploadingArea">
                                                            <div class="col-md-12">
                                                                <div class="formmargin">
                                                                    <label class="col-sm-6 control-label">Select File</label>
                                                                    <div class="col-sm-6" >
                                                                        <input type="file" id="uploadcsv">
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                        <br />
                                                        <div class="row" ng-hide="divUploadingArea">
                                                            <div class="col-md-12">
                                                                <div class="col-sm-6" ng-hide="divUploadingArea">
                                                                    <button type="button" class="btn btn-success" id="btnsubmitfile">Upload File</button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                       
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <div class="col-sm-6" ng-hide="DivCheckFileStatus">
                                                                    <button type="button" ng-click="getFileStatus()" class="btn btn-danger" id="btnfileStatus">Check File Status</button>
                                                                </div>
                                                                <div class="col-sm-6" ng-hide="DivCheckDownloadError">
                                                                    <button type="button" ng-click="getFileError()" class="btn btn-primary" id="btndowloaderror">Show Error List</button>
                                                                </div>

                                                            </div>



                                                        </div>

                                                    </div>
                                                </div>                                              
                                         
                                            </div>
                                             <div class="col-md-6">
                                                    <div class="panel">
                                                        <div class="panel-heading">
                                                            <div class="panel-title">
                                                                <h5><i class="{{zipIcon}}" aria-hidden="true"></i> {{zipStatusText}}  </h5>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">

                                                            <div class="row">
                                                            <div class="col-md-12" ng-hide="divUploadingAreaZip">
                                                                <div class="formmargin">
                                                                    <label class="col-sm-6 control-label">Select Zip File</label>
                                                                    <div class="col-sm-6" >
                                                                       <input type="file" id="uploadzip" />
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                              <br />
                                                        <div class="row">
                                                            <div class="col-md-12" ng-hide="divUploadingAreaZip">
                                                                <div class="col-sm-6" >
                                                                    <button type="button" class="btn btn-success" id="btnsubmitzip">Upload Zip</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                             <br />
                                                            <div class="row">
                                                            <div class="col-md-12">

                                                                <div class="col-sm-6" ng-hide="DivCheckZipStatus">
                                                                    <button type="button" ng-click="getZipStatus()" class="btn btn-danger" id="btnZipStatus">Check Zip Status</button>
                                                                </div>
                                                              

                                                            </div>



                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>


                                        </div>
                                    <div>
                                        <table ng-hide="mytable" cellpadding="5" border="1" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Error
                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="item in errorList">
                                                    <td>{{item.Error}}  
                                                    </td>

                                                </tr>

                                            </tbody>
                                        </table>
                                        <br />

                                    </div>

                                    <!-- /.col-md-12 -->
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-md-6 -->


                        <!-- /.col-md-8 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.section -->

        </div>

        <%--   <br/><br/><br/><br/><br/>

    <div class="container page-content">

        <div class="loader"></div>
        <div>
            <ul class="breadcrumbs mini">
                <li><a href="Home.aspx"><span class="icon mif-home"></span></a></li>
                <li><a href="AddBCIndex.aspx"><span>Add Business Correspondents</span></a></li>
                <li><a href="bulkupload.aspx"><span>Bulk Upload</span></a></li>

            </ul>
        </div>

        <br>

        <div class="example">
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-4 control-label">Select File</label>
                    <div class="col-sm-8">
                        <input type="file" id="uploadcsv">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Select Zip File</label>
                    <div class="col-sm-8">
                        <input type="file" id="imgupload"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="col-sm-4 control-label">Replace Existing</label>
                    <div class="radio col-sm-8">
                        <label>
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                            Replace the Existing Data
                        </label>
                        <label>
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                            Do not replace the existing Data
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-10">
                        <button type="submit" class="btn btn-default" id="btnsubmit">Submit</button>
                    </div>
                </div>
            </form>
        </div>

        <br>

    </div>--%>
    </form>
</body>
</html>
