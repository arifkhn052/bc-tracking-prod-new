﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="BankList.aspx.cs" Inherits="BCTrackingWeb.BankList" %>


    <%-- <div class="main-page">--%>
    <div class="container-fluid">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">List of Bank</h2>

            </div>
            <!-- /.col-md-6 -->
            
            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="#">Bank List</a></li>

                </ul>
            </div>

            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid">

            <div class="row">


                <!-- /.col-md-6 -->

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                            </div>
                        </div>
                        <div class="panel-body p-20">

                            <table id="tblBanks" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Bank</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>Edit</th>


                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->


                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.section -->

    <%-- </div>--%>





    <%--<script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/parsley.min.js" defer></script>
    <script src="js/getbc.js" type="text/javascript"></script>
    <script src="js/banks.js" type="text/javascript"></script>
      <script>

          $(document).ready(function () {



              var leadTable = $('#tblBanks').dataTable({
                  "oLanguage": {
                      "sZeroRecords": "No records to display",
                      "sSearch": "Search "
                  },
                  "sDom": 'T<"clear">lfrtip',
                  "tableTools": {

                      "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
                  },
                  "iDisplayLength": 15,
                  "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
                  "bSortClasses": false,
                  "bStateSave": false,
                  "bPaginate": true,
                  "bAutoWidth": false,
                  "bProcessing": true,
                  "bServerSide": true,
                  "bDestroy": true,
                  "sAjaxSource": "BankList.aspx/getBank",
                  "columnDefs": [

                      {
                          "orderable": false,
                          "targets": -2,
                          "render": function (data, type, full, meta) {
                              return '<a  class="btn btn-success btn-sm btn-labeled btn-rounded" href="AddCorporate.aspx?CId=' + full[0] + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                          }
                      }, { orderable: true, "targets": -1 }, {
                          "orderable": true,
                          "targets": -1,
                          "render": function (data, type, full, meta) {
                              return '<a href="#" class="btn btn-danger btn-sm btn-labeled btn-rounded"  onclick="return deleteUser(' + full[0] + ');"><i class="fa fa-trash-o" aria-hidden="true"></a>';


                          }
                      }
                  ],
                  "bDeferRender": true,
                  "fnServerData": function (sSource, aoData, fnCallback) {
                      $.ajax({
                          "dataType": 'json',
                          "contentType": "application/json; charset=utf-8",
                          "type": "GET",
                          "url": sSource,
                          "data": aoData,
                          "success":
                                      function (msg) {
                                          var json = jQuery.parseJSON(msg.d);
                                          fnCallback(json);
                                          $("#tblBanks").show();
                                      }
                      });
                  }
              });
              //  leadTable.fnSetFilteringDelay(300);
          });

          function deleteUser(stateid) {
              
              if (confirm("Do you want to delete?")) {
                  $.ajax({
                      type: 'POST',
                      dataType: 'json',
                      contentType: 'application/json',
                      url: "DeleteBcHandler.ashx?mode=&corporateId=" + corporateId,
                      async: false,
                      success: function () {
                          window.location.reload();

                      }
                  });

              }
              return false;
          }
    </script>--%>

