﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="AddCorporate1.aspx.cs" Inherits="BCTrackingWeb.AddCorporate1" %>

     <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Add Corporate </h2>
                                   
                                </div>
                           
                             
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							 <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                                   <li><a href="c1.aspx"> Corporate List</a></li>
                                          <li><a href="AddCorporate1.aspx">Add Corporate</a></li>
            							
            						</ul>
                                </div>
                              
                                <!-- /.col-md-6 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                                <div class="row">

                                  
                                    <!-- /.col-md-6 -->

                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                  
                                                </div>
                                            </div>
                                            <div class="panel-body p-20">

                                                  <div class="panel-body">
                <div class="formmargin">
                    <label  class="col-sm-2 control-label">Corporate Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control formcontrolheight" id="txtName"  placeholder="Name">
                    </div>
                </div>
                <div class="formmargin">
                    <label  class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control formcontrolheight" id="txtEmail"  placeholder="Email">
                    </div>
                </div>
                <div class="formmargin">
                    <label  class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control formcontrolheight" id="txtType"  placeholder="Type">
                    </div>
                </div>
                <div class="formmargin">
                    <label  class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control formcontrolheight" id="txtAddress"  placeholder="Address">
                    </div>
                </div>
                <div class="formmargin">
                    <label  class="col-sm-2 control-label">Contact</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control formcontrolheight" id="txtContact"  placeholder="Contact">
                    </div>
                </div>
                <div class="formmargin">
                    <input class="btn btn-success" type="submit" id="btnAddCorporate"/>
                </div>
            </div>

                                        
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                               

                                    
                                    <!-- /.col-md-8 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>

    <div class="container page-content">


        <div class="loader"></div>

        <div>
            <ul class="breadcrumbs mini">
              <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                <li><a href="CorporateList.aspx"><span>Corporate List</span></a></li>
            </ul>
        </div>
        <br>

                
        <br/>
            <div class="formmargin center-block">
           
                <div class="col-sm-2">
                    <a href="AddCorporate.aspx" class="btn btn-default center-block">Add Corporate</a>
                    <!--<button type="submit" id="btnAddBank" class="btn btn-default center-block">Submit</button>-->
                </div>
        </div>
    
    </div>
    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/bc.js" type="text/javascript"></script>

