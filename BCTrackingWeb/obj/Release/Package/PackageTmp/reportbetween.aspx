﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="reportbetween.aspx.cs" Inherits="BCTrackingWeb.reportbetween" %>


    <div class="container-fluid" ng-init="GetBanks();GetStates();datetimeBind()">
        <div class="row page-title-div">
            <div class="col-md-12">
                <h2 class="title">List Allotment Between Dates</h2>

            </div>

            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="home.aspx"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="reportbetween.html"><span>Allotment Between Dates</span></a></li>

                </ul>
            </div>

            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid">

            <div class="row">

                <div class="col-md-12">
                    <div class="panel">

                        <div class="panel-body p-20">


                            <div class="panel-body">

                                <div class="formmargin">


                                    <div class="col-md-6">
                                        <label for="exampleInputName2">Start Date</label>
                                        <input type="text" class="form-control" id="txtStartDate">
                                        <%--  <input type='text' class="form-control" id="txtStartDate"  placeholder="dd/mm/yyyy" autocomplete="off" />--%>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="exampleInputEmail2">End Date</label>
                                        <input type="text" class="form-control" id="txtEndDate">
                                        <%-- <input type='text' class="form-control" id="txtEndDate"  placeholder="dd/mm/yyyy" autocomplete="off" />--%>
                                    </div>

                                    <div class="formmargin">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <strong>State</strong>
                                                <select class="form-control formcontrolheight" id="selPLState" runat="server"></select>
                                            </div>
                                            <div class="col-sm-3">
                                                <strong>District</strong>
                                                <select class="form-control formcontrolheight" id="selPLDistrict" runat="server"></select>
                                            </div>
                                            <div class="col-sm-3">
                                                <strong>Sub District</strong>
                                                <select class="form-control formcontrolheight" id="selPLSubDistrict" runat="server"></select>

                                            </div>
                                            <div class="col-sm-3">
                                                <strong>Villages Code</strong>
                                                <select class="form-control formcontrolheight" id="selPLdVillages" runat="server"></select>



                                            </div>
                                           
                                           
                                        </div>
                                         <div class="row" style="padding:10px">
                                             <div class="col-sm-12 text-center">
                                                 <strong>.</strong>
                                                  <button id="genDataAppointDates" type="button" class="btn btn-success">Generate Data</button>

                                                  </div>

                                            </div>



                                    </div>

                                </div>


                            <hr>
                            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>

            </div>
        </div>

    </section>




    <%--<div class="container page-content">

        <div class="loader"></div>
        <div>
            <ul class="breadcrumbs mini">
                <li><a href="Home.aspx"><span class="icon mif-home"></span></a></li>
                <li><a href="Reports.html"><span>Reports</span></a></li>
                <li><a href="reportSummary.html"><span>Summary Reports</span></a></li>
                <li><a href="reportbetween.html"><span>Allotment Between Dates</span></a></li>

            </ul>
        </div>
        <br>
        <form class="form-inline">

            <div class="form-group">
                <div class="row">
                    
                </div>
            </div>

        </form>
        <hr>
        <form class="form-inline">
              <h4>Select Bank Subdivision Filter</h4>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                         <select class="form-control" id="allBanks">
                    <option>Select Bank</option>
                </select>
                    </div>
                    <div class="col-md-3">
  <select class="form-control" id="bankCircle">
                    <option value="0">Select Circle</option>
                </select>
                    </div>
                    <div class="col-md-3">
                          <select class="form-control" id="bankZone">
                    <option value="0">Select Zone</option>
                </select>
                    </div>
                    <div class="col-md-3">
                          <select class="form-control" id="bankRegion">
                    <option value="0">Select Region</option>
                </select>
                    </div>
                </div>
            </div>

        </form>
          <hr>
        <form class="form-inline">
           
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                           <select class="form-control" id="bankBranch">
                    <option value="0">Select Branch</option>
                    <!--
                <option class="dissolv" value="5">Branch1</option>
                <option class="dissolv" value="32">branch2</option>
                    -->
                </select>
                    </div>
                    <div class="col-md-3">
   <select class="form-control" id="bankCategory">
                    <option value="0">Select Category</option>
                    <option class="dissolv" value="5">Urban</option>
                    <option class="dissolv" value="32">Semi Urban</option>
                </select>
                    </div>
                    <div class="col-md-3">
                          <select class="form-control" id="bankSsa">
                    <option value="0">Select SSA</option>
                    <option class="dissolv" value="5">SSA1</option>
                    <option class="dissolv" value="32">SSA3</option>
                </select>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control" id="bankState">
                    <option value="0">Select State</option>
                </select>
                    </div>
                </div>
            </div>

        </form>

        
        <br>
        <form>
            <div class="form-group">
                <button id="genDataAppointDates" type="button" class="btn btn-success">Generate Data</button>
            </div>
        </form>
        
        <br>
    </div>--%>
    <%--<script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/getbc.js"></script>
    <script type="text/javascript" src="js/report.js"></script>--%>

