﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="BCTrackingWeb.AddUser" %>

    <%-- <div class="main-page">--%>
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Add User </h2>
                                   
                                </div>
                           
                             
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							 <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                                   <li><a ui-sref="userList"> User List</a></li>
                                          <li><a >Add User</a></li>
            							
            						</ul>
                                </div>
                              
                                <!-- /.col-md-6 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                                <div class="row">

                                  
                                    <!-- /.col-md-6 -->

                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                  
                                                </div>
                                            </div>
                                            <div class="panel-body p-20">

                                                  <div class="panel-body">

                <div class="formmargin">
                    <label message class="col-sm-2 control-label">Login Id</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control formcontrolheight" id="txtUserName" placeholder="Login Id With No Space"
                               parsley-trigger="change" required>
                    </div>
                </div>

                <div class="formmargin">
                    <label message class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control formcontrolheight" id="txtPassword" placeholder="Password"
                               parsley-trigger="change" required>
                    </div>
                </div>

                <div class="formmargin">
                    <label message class="col-sm-2 control-label">First Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control formcontrolheight" id="txtFirstName" placeholder="First Name"
                               parsley-trigger="change" required>
                    </div>
                </div>

                <div class="formmargin">
                    <label message class="col-sm-2 control-label">Last Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control formcontrolheight" id="txtLastName" placeholder="Last Name"
                               parsley-trigger="change" required>
                    </div>
                </div>
                                                       <div class="formmargin">
                    <label message class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-10">
                        <input type="text" maxlength="10"ng-keypress="filterValue($event)" class="form-control formcontrolheight" id="txtphone" placeholder="Phone"
                               parsley-trigger="change" required>
                    </div>
                </div>

                                                       <div class="formmargin">
                    <label message class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control formcontrolheight" id="txtEmail" placeholder="Email"
                               parsley-trigger="change" required>
                    </div>
                </div>
                <div class="formmargin">
                    <label message class="col-sm-2 control-label">Role</label>
                    <div class="col-sm-10">
                        <select id="selRole" ng-model="role" style="margin-bottom: 10px;" class="form-control formcontrolheight">
                            <option value="">Select</option>
                            <option value="1">Admin</option>
                            <option value="3">Bank Admin</option>
                          
                        </select>
                    </div>
                </div>
<br />
                                                      <br />
                <div class="formmargin" ng-show="role=='3'">
                    <label message class="col-sm-2 control-label">Bank</label>
                    <div class="col-sm-10">
                        <select id="selBank" class="form-control formcontrolheight">
                            <option value="">Select bank</option>
                    </select>
                    </div>
                </div>

                <div class="formmargin">
                    <div class="center-block">
                        <button type="button" id="btnSubmitUser" class="btn btn-success">Submit</button>
                           <button type="button" id="btnUpdate" class="btn btn-success">Update</button>
                           <button type="button" id="btnDelete" class="btn btn-danger">Delete</button>
                    </div>
                </div>




            </div>

                                        
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                               

                                    
                                    <!-- /.col-md-8 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                  <%--  </div>--%>
    


       <%-- <script src="js/jquery-1.12.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/metro.min.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/parsley.min.js" defer></script>
        <script src="js/main.js"></script>
        <script src="js/user.js"></script>--%>
