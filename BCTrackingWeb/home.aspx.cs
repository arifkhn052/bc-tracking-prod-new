﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using BCTrackingBL;
using BCEntities;
using SBI.Web.Helpers;


namespace BCTrackingWeb
{
    public partial class home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static string getUserMenu()
        {
           string result = "";
            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            return json;
        }
    }
}