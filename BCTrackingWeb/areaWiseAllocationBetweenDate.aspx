﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="areaWiseAllocationBetweenDate.aspx.cs" Inherits="BCTrackingWeb.areaWiseAllocationBetweenDate" %>

      <%--<div class="main-page">--%>
        <div class="container-fluid">
            <div class="row page-title-div">
                <div class="col-md-12">
                    <h2 class="title">List of Area Wise BC between Dates</h2>

                </div>

                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                       <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                         <li><a href="areaWiseAllocationBetweenDate.html"><span>Area Wise BC between Dates</span></a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">

                    <div class="col-md-12">
                        <div class="panel">

                            <div class="panel-body p-20">


                                <div class="panel-body">

                                    <div class="formmargin">


                                 <div class="col-md-3">
                         <label for="exampleInputName2">Start Date</label>
                <input type="date" class="form-control" id="txtStartDateUnAlloc">
                    </div>
                    <div class="col-md-3">
                         <label for="exampleInputEmail2">End Date</label>
                <input type="date" class="form-control" id="txtEndDateUnAlloc">
                    </div>

                                        <div class="col-md-3">
                           <label for="txtEndDate">Select Area</label>
                             <select class="form-control">
                        <option class="dissolv" value="1">Area 1</option>
                        <option class="dissolv" value="6">Area 2</option>
                        <option class="dissolv" value="9">Area 3</option>
                    </select>
                    </div>
                                          <div class="col-md-3">
                                                <label for="txtEndDate">.</label>
                     <button id="btnGenDataArea" type="button" class="btn btn-success">Generate Data</button>
  </div>
                                    
                                    </div>
                                   
                                    
                                </div>


                              <hr>
         <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>

                </div>
            </div>

        </section>


   <%-- </div>--%>

    <%--<div class="container page-content">
        
        
        <div class="loader"></div>

        <div>
            <ul class="breadcrumbs mini">
                <li><a href="Home.aspx"><span class="icon mif-home"></span></a></li>
                <li><a href="Reports.html"><span>Reports</span></a></li>
                <li><a href="areaWiseAllocationBetweenDate.html"><span>Area Wise BC between Dates</span></a></li>

            </ul>
        </div>
      <br>

        <div class="example">
               <form class="form-inline">

            <div class="form-group">
                <div class="row">
                    <div class="col-md-4">
                          <label for="txtStartDate">Start Date</label>
			    <input type="date" class="form-control" id="txtStartDate">
                    </div>
                    <div class="col-md-4">
                         <label for="txtEndDate">End Date</label>
			    <input type="date" class="form-control" id="txtEndDate">
                    </div>
                     
                </div>
            </div>
                   	 
        </form>
		
			<hr>
			<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

		</div>
         <br>

    </div>--%>

    <%--<script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/getbc.js" type="text/javascript"></script>
    <script src="js/report.js" type="text/javascript"></script>--%>

