﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="editProduct.aspx.cs" Inherits="BCTrackingWeb.editProduct" %>


       <div class="container-fluid" ng-init="getProduct()">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">Edit Product</h2>
            </div>
        </div>

        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a ui-sref="addproduct">Add Product</a></li>
                     <li><a ui-sref="">Edit Product</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                            </div>
                        </div>
                        <div class="panel-body p-20">
                            <div class="panel-body">
                                <div class="formmargin">
                                    <label message class="col-sm-2 control-label">Product Name</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control formcontrolheight" id="txtProductName" placeholder="Enter Product Name"
                                            parsley-trigger="change" required>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="submit" id="btnPrdoctUpdate" class="btn btn-success">Update</button>
                                    </div>
                                      <div class="col-sm-2">
                                          <div ng-click="deleteProduct()" class="btn btn-sm btn-danger">
                                                                                            <span class="fa fa-trash-o" aria-hidden="true"></span>&nbsp;Remove
                                                                                                </div>
                                    </div>
                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">

            <div class="row">


                <!-- /.col-md-6 -->

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                            </div>
                        </div>
                        <div class="panel-body p-20">

                            <table id="tblProduct" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Product Name</th>
                                         <th>Edit</th>        
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->


                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
    </section>
