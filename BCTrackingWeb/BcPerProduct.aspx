﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="BcPerProduct.aspx.cs" Inherits="BCTrackingWeb.BcPerProduct" %>


    <%--<div class="main-page">--%>
    <div class="container-fluid" ng-init="getProductList();getProduct()">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">List BC Per Product</h2>

            </div>

            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="BcPerProduct.html"><span>BC Per Product</span></a></li>

                </ul>
            </div>

            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid">

            <div class="row">

                <div class="col-md-12">
                    <div class="panel">

                        <div class="panel-body p-20">


                            <div class="panel-body">

                                <div class="formmargin">


                                    <div class="col-md-6">

                                        <select class="form-control" id="productIds">
                                            <option value="0">Select Product</option>
                                        </select>

                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-success" ng-click="getProductList()">
                                            Show
                                                List
                                        </button>
                                    </div>
                                    <%--       <div class="col-md-2">
                                            <select class="form-control" id="allBanks">
                                                <option value="0">Select Bank</option>
                                            </select>

                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" id="bankCircle">
                                                <option value="0">Select Circle</option>
                                            </select>

                                        </div>

                                        <div class="col-md-2">
                                            <select class="form-control" id="bankZone">
                                                <option value="0">Select Zone</option>
                                            </select>

                                        </div>--%>
                                </div>
                                <br />
                                <br />


                                <div class="formmargin">
                                    <%--  <div class="col-md-2">
                                            <select class="form-control" id="bankRegion">
                                                <option value="0">Select Region</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" id="bankBranch">
                                                <option value="0">Select Branch</option>
                                                <!--
                                                <option class="dissolv" value="5">Branch1</option>
                                                <option class="dissolv" value="32">branch2</option>
                                                    -->
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" id="bankCategory">
                                                <option value="0">Select Category</option>
                                                <option class="dissolv" value="5">Urban</option>
                                                <option class="dissolv" value="32">Semi Urban</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" id="bankSsa">
                                                <option value="0">Select SSA</option>
                                                <option class="dissolv" value="5">SSA1</option>
                                                <option class="dissolv" value="32">SSA3</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" id="bankState">
                                                <option value="0">Select State</option>
                                            </select>
                                        </div>--%>
                                </div>
                            </div>


                            <hr />
                            <table id="tblProducts" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Product ID</th>
                                        <th>Product Name</th>
                                        <th>BC Code</th>
                                         <th>Name</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>

            </div>
        </div>

    </section>


    <%-- </div>

    
    <script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/report.js" type="text/javascript"></script>
    <script src="js/getbc.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            var t = $('#example').DataTable({});


        });
    </script>--%>

