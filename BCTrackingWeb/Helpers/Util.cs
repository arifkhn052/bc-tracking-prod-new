﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SBI.Web.Helpers
{
    public class Util
    {
        public const string HomePage = "Home.aspx";
        public const string ReportPage = "ReportList.aspx";

        public static bool IsUserLoggedIn()
        {
            if (System.Web.HttpContext.Current.Session["UserInfo"] == null)
                return false;
            else
                return true;
        }

        //public static void SavePageView(string pageName)
        //{
        //    if (System.Web.HttpContext.Current.Session["DBSessionId"] != null)
        //        Common1.SavePageView(pageName, Convert.ToInt32(System.Web.HttpContext.Current.Session["DBSessionId"]));
        //}

        //public static string GetCurrentUserName()
        //{
        //    if (System.Web.HttpContext.Current.Session["UserInfo"] != null)
        //        return ((User)System.Web.HttpContext.Current.Session["UserInfo"]).name;
        //    else
        //        return string.Empty;
        //}

        public static bool IsUserAuthorized(string pageName)
        {
            if (ConfigurationManager.AppSettings["PageWithNoPermisssion"].Contains(pageName))
                return true;

            if (System.Web.HttpContext.Current.Session["PermittedPages"] != null)
            {
                if (System.Web.HttpContext.Current.Session["PermittedPages"].ToString().ToLower().Contains(pageName.ToLower()))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public static string GeneratePassword()
        {
            string strPwdchar = "abcdefghijklmnopqrstuvwxyz0123456789#+@&$ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder strPwd = new StringBuilder(string.Empty);
            Random rnd = new Random();
            for (int i = 0; i <= 8; i++)
            {
                int iRandom = rnd.Next(0, strPwdchar.Length - 1);
                strPwd.Append(strPwdchar.Substring(iRandom, 1));
            }
            return strPwd.ToString();
        }

        public static string GetMD5(string text)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                str.Append(result[i].ToString("x2"));
            }
            return str.ToString();
        }

        public static bool SendMail(string to, string subject, string message)
        {
            var smtpClientName = ConfigurationManager.AppSettings["SMTP"].ToString();
            var smtpUserName = ConfigurationManager.AppSettings["SMTPUserName"].ToString();
            var smtpPassword = ConfigurationManager.AppSettings["SMTPPassword"].ToString();
            var mailFrom = ConfigurationManager.AppSettings["MailFrom"].ToString();
            var mailPort = ConfigurationManager.AppSettings["SMTPPort"].ToString();
            bool enableSSl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
            String username = smtpUserName;  // Replace with your SMTP username.
            String password = smtpPassword;  // Replace with your SMTP password.
            String host = smtpClientName;
            int port = Convert.ToInt16(mailPort);
            try
            {
                using (var client = new System.Net.Mail.SmtpClient(host, port))
                {
                    client.Credentials = new System.Net.NetworkCredential(username, password);
                    client.EnableSsl = enableSSl;

                    MailMessage msg = new MailMessage(mailFrom, to, subject, message);
                    msg.IsBodyHtml = true;
                    msg.SubjectEncoding = System.Text.Encoding.UTF8;
                    msg.BodyEncoding = System.Text.Encoding.UTF8;
                    msg.Priority = MailPriority.High;

                    client.Send(msg);
                    return true;
                }
            }
            catch (Exception ex)
            {


                return false;
            }
            
        }

       
    }
}