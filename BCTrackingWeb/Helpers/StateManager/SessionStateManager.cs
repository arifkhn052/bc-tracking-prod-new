﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SessionStateManager.cs" company="BCG" author="Nagarro">
//   All rights reserved. Copyright (c) 2014.
// </copyright>
// <summary>
//   Represents Session state manager, Author : BCG
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SBI.Web
{
    using System.Web;

    /// <summary>
    /// Represents Session state manager, Author : BCG
    /// </summary>
    /// <typeparam name="T">
    /// Type of class whose reference is o be stored in session
    /// </typeparam>
    public class SessionStateManager<T> : StateManagerBase<T>
        where T : StateEntityBase, new()
    {
        // The variable is declared to be volatile to ensure that assignment to the 
        // mInstance variable completes before the instance variable can be accessed.
        // [ThreadStatic]
        #region Constants and Fields

        /// <summary>
        ///   The _instance.
        /// </summary>
        private static volatile SessionStateManager<T> instance;

        /// <summary>
        ///   The _sync object.
        /// </summary>
        private static object syncObject = new object();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Prevents a default instance of the <see cref="SessionStateManager{T}" /> class from being created.
        /// </summary>
        private SessionStateManager()
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets the data of session entity.
        /// </summary>
        /// <value> The data. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Required in implementation")]
        public static T Data
        {
            get
            {
                return Instance.StateEntity;
            }
            set
            {
                Instance.StateEntity = value;
            }
        }

        /// <summary>
        ///   Gets or sets the state entity.
        /// </summary>
        /// <value> The state entity. </value>
        public override T StateEntity
        {
            get
            {
                if (HttpContext.Current.Session != null)
                {
                    if (HttpContext.Current.Session[this.Key] == null)
                    {
                        // this._stateEntity = new T();
                        this.Initialize(false);
                    }

                    return (T)HttpContext.Current.Session[this.Key];
                }
                else
                {
                    return null;
                }
            }

            set
            {
                if (HttpContext.Current.Session != null)
                {
                    HttpContext.Current.Session[this.Key] = value;
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets Instance is private static property to return the same Instance of the class every time. Note: Double - checked serialized initialization of Class pattern is used.
        /// </summary>
        private static SessionStateManager<T> Instance
        {
            get
            {
                // Check for null before acquiring the lock.
                if (instance == null)
                {
                    // Use a mSyncObject to lock on, to avoid deadlocks among multiple threads.
                    lock (syncObject)
                    {
                        // Again check if mInstance has been initialized, 
                        // since some other thread may have acquired the lock first and constructed the object.
                        if (instance == null)
                        {
                            instance = new SessionStateManager<T>();
                        }
                    }
                }

                return instance;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Clears the data of session entity.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Required as par design")]
        public static void ClearData()
        {
            Instance.Clear();
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public override void Clear()
        {
            base.Clear();

            // _instance = null;
            HttpContext.Current.Session.Remove(this.Key);
        }

        #endregion
    }
}