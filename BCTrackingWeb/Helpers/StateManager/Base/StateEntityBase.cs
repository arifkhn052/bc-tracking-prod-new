﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateEntityBase.cs" company="BCG" author="Nagarro">
//   All rights reserved. Copyright (c) 2014.
// </copyright>
// <summary>
//   Represents abstract base class for State entities
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SBI.Web
{
    using System;

    /// <summary>
    /// Represents abstract base class for State entities
    /// </summary>
    public abstract class StateEntityBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StateEntityBase" /> class
        /// </summary>
        protected StateEntityBase()
        {
            this.LastUpdatedDateTime = DateTime.Now;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the key for state entity
        /// </summary>
        /// <value> The key. </value>
        public string Key
        {
            get
            {
                return this.GetType().Name + "_" + this.GetType().GUID.ToString();
            }
        }

        /// <summary>
        /// Gets the last updated date time for entity
        /// </summary>
        /// <value> The last updated date time</value>
        public DateTime LastUpdatedDateTime { get; private set; }

        #endregion
    }
}