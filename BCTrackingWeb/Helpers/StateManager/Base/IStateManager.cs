﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStateManager.cs" company="BCG" author="Nagarro">
//   All rights reserved. Copyright (c) 2014.
// </copyright>
// <summary>
//   Defines a contract for state manager
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SBI.Web
{
    /// <summary>
    /// Defines a contract for state manager
    /// </summary>
    /// <typeparam name="T">
    /// Type of class whose reference is o be stored in session
    /// </typeparam>
    public interface IStateManager<T>
        where T : StateEntityBase, new()
    {
        #region Public Properties

        /// <summary>
        ///   Gets the key for state entity.
        /// </summary>
        /// <value> The key. </value>
        string Key { get; }

        /// <summary>
        ///   Gets or sets the state entity.
        /// </summary>
        /// <value> The state entity. </value>
        T StateEntity { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Clears this instance.
        /// </summary>
        void Clear();

        /// <summary>
        /// Generates the key.
        /// </summary>
        /// <returns>
        /// The generate key. 
        /// </returns>
        string GenerateKey();

        /// <summary>
        /// Generates the key.
        /// </summary>
        /// <param name="stateEntity">
        /// The state entity. 
        /// </param>
        /// <returns>
        /// The generate key. 
        /// </returns>
        string GenerateKey(T stateEntity);

        /// <summary>
        /// Initializes the state entity.
        /// </summary>
        /// <param name="isFirstTime">
        /// if set to <c>true</c> then generates the key else not. 
        /// </param>
        void Initialize(bool isFirstTime);

        #endregion
    }
}