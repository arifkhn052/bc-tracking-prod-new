﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="user_registration.aspx.cs" Inherits="ERikshaManagement.user_registration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">User Registration</h2>
                                   
                                </div>                             
                           
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
            							<li><a href="#">CMS</a></li>
            							<li class="active">User Registration</li>
            						</ul>
                                </div>
                                <!-- /.col-md-6 -->
                           
                                <!-- /.col-md-6 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                       
                               

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                         
                                            <div class="panel-body">

                                                <form class="p-20">
                                                    <h5 class="underline mt-n">Account Info</h5>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                        		<label for="name13">Full Name</label>
                                                        		<input type="text" class="form-control"  placeholder="Enter Your Full Name">
                                                        	</div>
                                                        </div>
                                                        <!-- /.col-md-6 -->

                                                            <div class="col-md-6">
                                                            <div class="form-group">
                                                        		<label for="contact13">Vehicle Number</label>
                                                        		<input type="text" class="form-control"  placeholder="Enter Vehicle Number">
                                                        	</div>
                                                    	</div>
                                                        <!-- /.col-md-6 -->
                                                    </div> 
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                             <div class="form-group">
                                                        		<label for="contact13">Contact No</label>
                                                        		<input type="text" class="form-control" id="contact13" placeholder="Enter Your Mobile Phone Number">
                                                        	</div>
                                                        
                                                        </div>
                                                        <!-- /.col-md-6 -->

                                                            <div class="col-md-6">
                                                               <div class="form-group">
                                                        		<label for="name13">Address</label>
                                                        		<input type="text" class="form-control" id="Enter Name" placeholder="Enter Your Full Name">
                                                        	</div>
                                                    	</div>
                                                        <!-- /.col-md-6 -->
                                                    </div>
                                                    

                                                 

                                                    <div class="row">
                                                 
                                                        <!-- /.col-md-12 -->

                                                        <div class="col-md-12">
                                                            <div class="btn-group pull-right mt-10" role="group">
                                                              
                                                                <button type="button" class="btn bg-black btn-wide"><i class="fa fa-arrow-right"></i>Submit</button>
                                                            </div>
                                                            <!-- /.btn-group -->
                                                        </div>
                                                        <!-- /.col-md-12 -->
                                                    </div>

                                                </form>

                                                
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                                <!-- /.row -->


                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

</asp:Content>
