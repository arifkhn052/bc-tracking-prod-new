﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="searchLog.aspx.cs" Inherits="BCTrackingWeb.searchLog" %>

<!DOCTYPE html>

<div class="container-fluid">
    <div class="row page-title-div">
        <div class="col-md-6">
            <h2 class="title">List of BC log </h2>

        </div>

        <!-- /.col-md-6 text-right -->
    </div>
    <!-- /.row -->
    <div class="row breadcrumb-div">
        <div class="col-md-6">
            <ul class="breadcrumb">
                <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                <li><a href=""><span>log</span></a></li>

            </ul>
        </div>

        <!-- /.col-md-6 --
        </div>
        <!-- /.row -->
    </div>
<!-- /.container-fluid -->

<section class="section" ng-init="GetBanks();datetimeBind();SearchLoges()">
    <div class="container-fluid">

        <div class="row">


            <!-- /.col-md-6 -->

            <div class="col-md-12">
                <div class="panel">

                    <div class="panel-body p-20">
                        <div class="panel-body">

                            <div class="formmargin">


                                <div class="col-md-3">
                                    <strong>Bank</strong>
                                    <select runat="server" class="form-control"
                                        id="selBank">
                                    </select>

                                </div>
                                <div class="col-md-3">

                                    <strong>Sheet</strong>
                                    <select runat="server" class="form-control" id="selSheet">
                                        <option value="-1">--Select-- </option>
                                        <option value="all">All</option>
                                        <option value="BCs">BCs</option>
                                        <option value="Certificates">Certificate</option>
                                        <option value="Experience">Experience</option>
                                        <option value="Device">Device</option>
                                        <option value="ConnectivityDetail">Connectivity Detail</option>
                                        <option value="PrimaryLocation">Primary Location</option>
                                        <option value="OpreationalAreas">Opreational Areas</option>
                                        <option value="Remuneration">Remuneration</option>

                                    </select>


                                </div>

                                <div class="col-md-3">
                                    <strong>Date</strong>
                                    <input type="text" class="form-control" id="txtDate">
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">

                            <div class="formmargin">

                                <div class="col-md-3 pull-right">
                                    <strong></strong>
                                    <button type="button" id="btnsearch" class="btn btn-success" ng-click="SearchLoges()">Show Log</button>
                                </div>

                            </div>

                        </div>
                        <hr />
                        <table id="tblsearchLog" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Excel Name</th>
                                    <th>Sheet Name</th>
                                   <th>Total Records</th>
                                    <th>Uploaded Records</th>
                                    <th>Failed Records </th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>


                        <!-- /.col-md-12 -->
                    </div>
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-md-6 -->


            <!-- /.col-md-8 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.section -->
