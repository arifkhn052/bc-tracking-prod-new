﻿$(document).ready(function () {

    //$('#frmMain').parsley();
      
    //$('.loader').bind('ajaxStart', function () {
    //    $(this).show();
    //}).bind('ajaxStop', function () {
    //    $(this).hide();
    //});
   
    var loggedInName = '';
    var loggedInRole;
    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: "GetUserHandler.ashx?mode=Current",
        async: false,
        success: function (user) {

            loggedInName = user.UserName;
            loggedInRole = user.UserRoles;
            var isAdmin = false;
            var isBankAdmin = false;

            for(var i=0; i<loggedInRole.length; i++)
            {
                if(loggedInRole[i].Role=='Admin')
                {
                    isAdmin = true;
                    break;
                }

                if (loggedInRole[i].Role == 'BankAdmin') {
                    isBankAdmin = true;
                    break;
                }

               
            }
           
            if (!isAdmin && !isBankAdmin) {
               
                $("#bclistBankWise").hide();

            }
        }
    });
});


