﻿$(document).ready(function () {
    

    var fullurl = $(location).attr('href');

    var b = getParameterByName('id')
    var userId = 0;
    if (b != null)
        userId = b;

    $('form').parsley();

    var defaultOption = '--Select--';
    var defaultOptionValue = -1;
    GetBanks();

    if (userId != 0) {
        
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: "GetBCHandler.ashx?mode=User&userId=" + userId,
            async: false,
            success: function (user) {
                
                $('#txtUserName').val(user.UserName);
                $('#txtFirstName').val(user.FirstName);
                $('#txtLastName').val(user.LastName);
                $('#selRole').val(user.UserRoles[0].RoleId);
                $('#selBank').val(user.BankId);

            }
        });
    }

    function getParameterByName(name) //courtesy Artem
    {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else {
            if ((results[1].indexOf('?')) > 0)
                return decodeURIComponent(results[1].substring(0, results[1].indexOf('?')).replace(/\+/g, " "));
            else
                return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    function GetBanks() {

        var requrl = "GetBCHandler.ashx?mode=Bank";
      
        $('.loader').show();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: requrl,
            async: false,
            success: function (Banks) {
                //console('Got it!');
                
                for (var cnt = 0; cnt < Banks.length; cnt++) {

                    $('#selBank').append($('<option>', {
                        value: Banks[cnt].BankId,
                        text: Banks[cnt].BankName
                    }));

                }

                $('.loader').hide();

            },
            error: function (err) {
             //   alert(err);
                $('.loader').hide();
            }

        });

    }



    $('#btnSubmitUser').on('click', function () {
        
        var user = new Object();
        user.userId = 0;
        user.UserName = $('#txtUserName').val();
        user.FirstName = $('#txtFirstName').val();
        user.LastName = $('#txtLastName').val();
        user.BankId = $('#selBank').val();
        user.UserPassword = $('#txtPassword').val();

        user.UserRoles = [];
        var userRole = new Object();
        userRole.RoleId = $('#selRole').val();
        user.UserRoles.push(userRole);
       

        $.ajax({
            url: "InsertUpdateUserHandler.ashx",
            method: "POST",
            datatype: "json",
            data: { "user": JSON.stringify(user) },
            success: function () {
                alert('User Saved!');
            },
            error: function (xmlHttpRequest, errorString, errorMessage) {
                alert(errorMessage);
            }
        });
    });





});