﻿   (function($){
$(document).ready(function () {
    $.ajaxSetup({
        cache: false
    });
 

    var leadTable = $('#tblLeadList').dataTable({
        "oLanguage": {
            "sZeroRecords": "No records to display",
            "sSearch": "Search "
        },
        "sDom": 'T<"clear">lfrtip',
        "tableTools": {

            "sSwfPath": "Scripts/jquery/copy_csv_xls_pdf.swf"
        },
        "iDisplayLength": 15,
        "aLengthMenu": [[25, 50, 100, 200, 300], [25, 50, 100, 200, "All"]],
        "bSortClasses": false,
        "bStateSave": false,
        "bPaginate": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bDestroy": true,
        "sAjaxSource": "StateList.aspx/getState",
        "columnDefs": [

            {
                "orderable": false,
                "targets": -2,
                "render": function (data, type, full, meta) {
                    return '<a  class="btn btn-success btn-sm btn-labeled btn-rounded" href="editState.aspx?stateId=' + full[0] + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                }
            }, { orderable: true, "targets": -1 }, {
                "orderable": true,
                "targets": -1,
                "render": function (data, type, full, meta) {
                    return '<a href="#" class="btn btn-danger btn-sm btn-labeled btn-rounded"  onclick="return deleteUser(' + full[0] + ');"><i class="fa fa-trash-o" aria-hidden="true"></a>';


                }
            }
        ],
        "bDeferRender": true,
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success":
                            function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                $("#tblLeadList").show();
                            }
            });
        }
    });
    //  leadTable.fnSetFilteringDelay(300);
});

function deleteUser(stateid) {

    if (confirm("Do you want to delete?")) {

        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: "DeleteBcHandler.ashx?mode=state&stateid=" + stateid,
            async: false,
            success: function (user) {
                window.location.reload();

            }
        });
    }
    return false;
}

   }(jQuery));