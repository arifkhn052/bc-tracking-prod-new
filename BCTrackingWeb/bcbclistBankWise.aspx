﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bcbclistBankWise.aspx.cs" Inherits="BCTrackingWeb.bcbclistBankWise" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <style>
        .col-sm-4 {
         margin-bottom:15px;}
    </style>
    <div class="container-fluid" ng-init="getBcBranchWise();GetBanks()">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">List of Bank Wise BC's</h2>

            </div>
           
            <!-- /.col-md-6 text-right -->
        </div>
        <!-- /.row -->
        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a><span>Bank Wise BC's</span></a></li>

                </ul>
            </div>

            <!-- /.col-md-6 -->
        </div>
          <div class="col-md-6 pull-right" style="margin-right:-15%;margin-top:15px" ng-show="hidebtnDiv">
             
                                        <button type="button" style="background-color: #e15349;"  ng-click="redirectToDashBoard()" class="btn btn-primary btn-lg">
                                            DashBoard
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </button>
                  
                                        <button type="button"  ng-click="redirectToTopList()" class="btn btn-primary btn-lg">
                                             List of Count
                                              <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                        </button>

            </div>

        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid">

            <div class="row">
                <!-- /.col-md-6 -->
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body p-20">
                            <div class="panel-body">

                                <div class="formmargin">
                                    <div class="form-group">

                                    <div class="col-sm-4">
                                        <select id="selBank" class="form-control">
                                          <%--  <option>Select Bank</option>--%>

                                        </select>
                                    </div>
                                      
                                  
                                     <div class="col-sm-4">
                                        <select id="selState" class="form-control">
                                            <option value="0">Select State</option>

                                        </select>
                                    </div>
                                       
                                     <div class="col-sm-4">
                                        <select id="selCity" class="form-control">
                                            <option value="0">Select City </option>

                                        </select>
                                    </div>
                                     <div class="col-sm-4">
                                        <select id="selDistrict" class="form-control">
                                            <option value="0">Select District</option>

                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select id="bankBranch" class="form-control">
                                            <option value="0">Select Branch</option>

                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="button" ng-click="getBcBranchWiseFilter()" class="btn btn-success">
                                            Show List
                                        </button>
                                    </div>
                                      
                                    </div>

                                    <%--  <div class="col-sm-3">
                                            <select id="bankCircle" class="form-control" id="bankCircle">
                                                <option value="0">Select Circle</option>

                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <select id="bankState" class="form-control" id="bankState">
                                                <option value="0">Select State</option>
                                                <!--<option class="dissolv" value="2">Maharashtra</option>-->
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <select id="bankZone" class="form-control" id="bankZone">
                                                <option value="0">Select Zone</option>
                                                <!--<option class="dissolv" value="3">NMZ</option>-->
                                            </select>
                                        </div>--%>
                                </div>


                            </div>

                            <div class="panel-body">

                                <div class="formmargin">
                                    <%--   <div class="col-sm-3">
                                            <select id="bankRegion" class="form-control" id="bankRegion">
                                                <option value="0">Select Region</option>
                                                <!--<option class="dissolv" value="4">Navi Mumbai</option>-->
                                            </select>
                                        </div>--%>
                                    <%--  <div class="col-sm-3">
                                            <select id="branchCategory" class="form-control" id="bankCategory">
                                                <option value="0">Select Category</option>
                                                <!--
                                                <option class="dissolv" value="5">Urban</option>
                                                <option class="dissolv" value="32">Semi Urban</option>
                                                -->
                                            </select>
                                        </div>--%>
                                    <%--<div class="col-sm-3">
                                            <select id="bankBranch" class="form-control" >
                                                <option value="0">Select Branch</option>

                                            </select>
                                        </div>--%>
                                </div>


                            </div>
                            <hr />
                            <table id="tblBranchList" class="display" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>BC Code</th>
                                        <th>Name</th>
                                        <th>Phone</th>

                                        <th>Certificates</th>
                                        <th>Villages</th>
                                        <th>Action</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->


                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.section -->



    <%--<script src="js/jquery-1.12.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/metro.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>
    <script src="js/report.js" type="text/javascript"></script>
    <script src="js/getbc.js" type="text/javascript"></script>--%>
    </form>
</body>
</html>
