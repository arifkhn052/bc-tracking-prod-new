﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bcGroupProduct.aspx.cs" Inherits="BCTrackingWeb.bcGroupProduct" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    
    <div class="container-fluid" ng-init="getProduct()">
        <div class="row page-title-div">
            <div class="col-md-6">
                <h2 class="title">Add Group Activity</h2>
            </div>
        </div>

        <div class="row breadcrumb-div">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                    <li><a ui-sref="groupProduct">Add Group Activity</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <section class="section">
        <div class="container-fluid"  ng-init="getProductforDropdownBind()">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                            </div>
                        </div>
                        <div class="panel-body p-20">
                            <div class="panel-body">
                               <div class="row">
                                    <label message class="col-sm-3 control-label"> Activity Group Name</label>
                                    <div class="col-sm-9">
                                    <input type="text" class="form-control formcontrolheight" id="txtgroupProductName" placeholder="Enter group Activity Name"
                                            parsley-trigger="change" >
                                   </div>

                               </div>
                                 <div class="row">
                                    <label message class="col-sm-3 control-label"> Select Activity</label>
                                    <div class="col-sm-6">
                                     <select class="form-control" id="productIds">
                                            <option value="0">Select Activity </option>
                                        </select>
                                   </div>
                                       <div class="col-sm-3">
                                            <button type="button" ng-click="addMoreProduct()" id="btnaddMore" class="btn btn-danger">Add More</button>
                                            </div>

                               </div>
                                <div class="row">
                                     <div class="col-sm-3">
                                         </div>
                                    
                                            
                                           <a ng-repeat="item in products" > <span ng-click="removeProduct($index)" class="label label-danger">{{item.ProductName}} | X</a>
                                         
                                    </div>
                                  <div class="row">
                                      </div>
                                 <div class="row">
                                     <div class="col-sm-11">
                                         </div>
                                    
                                              <button type="button" id="btnAddgroupProduct" class="btn btn-success">Submit</button>
                                         
                                         
                                    </div>

                               

                                <%--<div class="formmargin">
                                        <label message class="col-sm-2 control-label">Select Product</label>
                                    <div class="col-sm-7">
                                           <select class="form-control" id="productIds">
                                            <option value="0">Select Product</option>
                                        </select>
                                <br />
                                        {{products}}
                                    <label message class="col-sm-2 control-label">Group Product</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control formcontrolheight" id="txtgroupProductName" placeholder="Enter group Product Name"
                                            parsley-trigger="change" >
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" id="btnAddgroupProduct" class="btn btn-success">Submit</button>
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">

            <div class="row">


                <!-- /.col-md-6 -->

                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                            </div>
                        </div>
                        <div class="panel-body p-20">

                            <table id="tblProduct" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>                                        
                                        <th>Group Activity Name</th>
                                         <th>Created by</th>
                                         <th>Edit</th>        
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>


                            <!-- /.col-md-12 -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->


                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
    </section>
    </form>
</body>
</html>
