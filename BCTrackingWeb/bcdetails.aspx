﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bcdetails.aspx.cs" Inherits="BCTrackingWeb.bcdetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple markers</title>
    <style>
          /* Always set the map height explicitly to define the size of the div
          element that contains the map. /
        #map {
          height: 100%;
        }
        / Optional: Makes the sample page fill the window. /
        html, body {
          height: 100%;
          margin: 0;
          padding: 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
     <div id="map"></div>
    <script>

        function initMap() {
            var myLatLng = { lat: -25.363, lng: 131.044 };

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBcAq7SySoYLnmBn9RRkL34a6XsCHUXERQ&callback=initMap">
    </script>
    </form>
</body>
</html>
