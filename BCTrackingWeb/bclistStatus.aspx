﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="bclistStatus.aspx.cs" Inherits="BCTrackingWeb.bclistStatus" %>

        <div class="container-fluid" ng-init="datetimeBind();getListByStatus()">
            <div class="row page-title-div">
                <div class="col-md-6">
                    <h2 class="title">List of  By Status</h2>

                </div>

                <!-- /.col-md-6 text-right -->
            </div>
            <!-- /.row -->
            <div class="row breadcrumb-div">
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="#" ui-sref="home"><i class="fa fa-home"></i>Home</a></li>
                          <li><a ><span>BC list By Status</span></a></li>

                    </ul>
                </div>

                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

        <section class="section">
            <div class="container-fluid">

                <div class="row">


                    <!-- /.col-md-6 -->

                    <div class="col-md-12">
                        <div class="panel">
                           
                            <div class="panel-body p-20">
                                <div class="panel-body">

                                    <div class="formmargin">

                                        
                    <div class="col-md-4">
                         <label for="">From: </label>
                   
                          <input type='text' class="form-control" id="dtStartDate"  placeholder="dd/mm/yyyy" autocomplete="off" />

                    </div>
                                          <div class="col-md-4">
                         <label for="">To: </label>
                   
                          <input type='text' class="form-control" id="dtEndDate"  placeholder="dd/mm/yyyy" autocomplete="off" />

                    </div>
                                 
                                        <div class="col-md-4" >
                                               <label for="">Status: </label>
                       <select class="form-control" id="drpStatus">
                            <option>All</option>
                            <option>Active</option>
                            <option>Inactive</option>
                           <option>Dormant</option>
                           <option>Blacklisted</option>
                        </select>

                    </div>
                                        <div class="col-md-12">
                                               <label for="" style="color:white">. </label>
                         <button type="button"  ng-click="getListByStatusFilter()"  class="btn btn-success">Show List</button>
                    </div>
                    
                                        

                                    </div>


                                </div>
                           
                                
                                     <hr />
                                <table id="tblbclistStatus" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
               
                <th>ID</th>
                <th>BC Code</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Certificates</th>
                <th>Villages</th>

                <th>Action</th>
                <th></th>
                <th></th>
                <th></th>

            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>


                                <!-- /.col-md-12 -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-md-6 -->


                    <!-- /.col-md-8 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.section -->

